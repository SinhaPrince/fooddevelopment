//
//  AppUrl.swift
//  E-RX
//
//  Created by SinhaAirBook on 29/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

struct Domain {
    
    static let development = "http://3.140.159.167:3032/api/v1/user/"
    static let socketUrl = "http://3.22.227.41:2020"
    static let chatHistory = "http://3.22.227.41:2020/api/v1/user/"
    static let production = "https://e-rx.cc:2021/api/v1/user/"
    static let socketUrlProduction = "https://e-rx.cc:2021"

}
extension Domain {
    static func baseUrl() -> String {
        return Domain.development
    }
    static func socketURL() -> String{
        return Domain.socketUrlProduction
    }
    static func chatHistoryUrl() ->String{
        return Domain.production
    }
}

struct APIEndpoint {
    static let signInApi                       = "userLogin"
    static let directLogin                     = "directLogin"
    static let userSignup                      = "userSignup"
    static let userUpdateDetails               = "userUpdateDetails"
    static let checkEmailAndMobileAvailability = "checkEmailAndMobileAvailability"
    static let checkUserMobileNumber           = "checkUserMobileNumber"
    static let userLogoutService               = "userLogout"
    static let terminateAccount                = "terminateAccount"
    static let userUpdateProfile               = "userUpdateProfile"
    static let checkMobilForForgotPassword     = "checkMobilForForgotPassword"
    static let forgotPassword                  = "forgotPassword"
    static let changeNotificationStatus        = "changeNotificationStatus"
    static let updatePhoneConatct              = "updatePhoneConatct"
    static let getContactList                  = "getContactList"
    static let sendFrndRequest                 = "sendFrndRequest"
    static let getNotificationList             = "getNotificationList"
    static let getInviteByData                 = "getInviteByData"
    static let acceptOrDeclinedFrndRequest     = "acceptOrDeclinedFrndRequest"
    static let getFriendList                   = "getFriendList"
    static let homeDataApi                     = "homeDataApi"
    static let removeFriend                    = "removeFriend"
    static let changeMobileNumber              = "changeMobileNumber"
    static let changeEmail                     = "changeEmail"
    static let checkUserMobileAndEmail         = "checkUserMobileAndEmail"
    static let clearNotification               = "clearNotification"

}


enum HTTPHeaderField: String {
    
    case authentication  = "Authorization"
    case contentType     = "Content-Type"
    case acceptType      = "Accept"
    case acceptEncoding  = "Accept-Encoding"
    case acceptLangauge  = "Accept-Language"
    
    var header:String{
        
        switch self {
        case .authentication:
            return "Authorization"
        case .contentType:
            return "Content-Type"
        case .acceptType:
            return "Accept"
        case .acceptEncoding:
            return "Accept-Encoding"
        case .acceptLangauge:
            return "Accept-Language"
        
        }
        
    }
    
}

enum ContentType: String {
    case json            = "application/json"
    case multipart       = "multipart/form-data"
    case ENUS            = "en-us"
}

enum MultipartType: String {
    case image = "Image"
    case csv = "CSV"
}

enum MimeType: String {
    case image = "image/png"
    case csvText = "text/csv"
}

enum UploadType: String {
    case avatar
    case file
}



