//
//  AppFont.swift
//  HMSADMIN
//
//  Created by Atendra on 17/07/19.
//  Copyright © 2019 Atendra. All rights reserved.
//


import Foundation
import UIKit
enum AppFontName:String{
    case Poppins  = "Poppins"
    
}

enum AppFont:String{
    case BlackItalic        = "BlackItalic"
    case ExtraLight         = "ExtraLight"
    case ExtraLightItalic   = "ExtraLightItalic"
    case ExtraBold         = "ExtraBold"
    case Bold              = "Bold"
    case Light             = "Light"
    case ExtraBoldItalic   = "ExtraBoldItalic"
    case Italic            = "Italic"
    case ThinItalic        = "ThinItalic"
    case LightItalic       = "LightItalic"
    case Black             = "Black"
    case Medium            = "Medium"
    case BoldItalic        = "BoldItalic"
    case SemiBold          = "SemiBold"
    case Regular           = "Regular"
    case Thin              = "Thin"
    case SemiBoldItalic    = "SemiBoldItalic"
    case MediumItalic      = "MediumItalic"
    
    
    
    
    func size(_ name: AppFontName,size:CGFloat) -> UIFont{
        if let font = UIFont(name: self.fullFontName(name.rawValue), size: size + 1.0){
            print(font)
            return font
        }
        fatalError("Font '\(fullFontName)' does not exist.")
    }
    
    fileprivate func fullFontName(_ fontName:String)->String{
        return rawValue.isEmpty ? fontName : fontName + "-" + rawValue
    }
}

//func availableFonts() {
//
//        // Get all fonts families
//for family in UIFont.familyNames {
//            // Show all fonts for any given family
//    for name in UIFont.fontNames(forFamilyName: family) {
//        print("\(name)")
//            }
//        }
//    }
