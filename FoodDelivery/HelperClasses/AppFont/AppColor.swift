//
//  AppColor.swift
//
//  Created by Sandeep Vishwakarma on 4/16/18.
//  Copyright © 2018 Sandeep. All rights reserved.
//


import UIKit

struct AppColor {
    
    //MARK: - Constant colors
    public static let textColor = #colorLiteral(red: 0.1622368991, green: 0.1733389795, blue: 0.1902409196, alpha: 1)
    public static let placeHolderColor = #colorLiteral(red: 0.6461701989, green: 0.6461701989, blue: 0.6461701989, alpha: 1)
    public static let themeColor = #colorLiteral(red: 0.6350092888, green: 0.1693820059, blue: 0.2047639489, alpha: 1)
    public static let whiteColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    public static let subtitleColor = #colorLiteral(red: 0.3510887921, green: 0.3510887921, blue: 0.3510887921, alpha: 1)
    public static let stepperColor = #colorLiteral(red: 0.3788953424, green: 0.8141426444, blue: 0.3015330732, alpha: 1)
    public static let appColor = #colorLiteral(red: 0.290571034, green: 0.696982801, blue: 0.7504272461, alpha: 1)
    
    //TODO: Blue gradient
    public static let primaryColor              = #colorLiteral(red: 0.8431372549, green: 0.8509803922, blue: 0.8588235294, alpha: 1)
    public static let primaryColor2             = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
    public static let primaryColorBorder        = #colorLiteral(red: 0.862745098, green: 0.8549019608, blue: 0.8745098039, alpha: 1)
    public static let offerBorder               = #colorLiteral(red: 0.8431372549, green: 0.8509803922, blue: 0.8588235294, alpha: 1)
    public static let notifyShadow              = #colorLiteral(red: 0.9019607843, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
    public static let pink                      = #colorLiteral(red: 0.9098039216, green: 0.1137254902, blue: 0.5803921569, alpha: 1)
    public static let lightpink                 = #colorLiteral(red: 0.9647058824, green: 0.6745098039, blue: 0.8549019608, alpha: 1)
    public static let red               = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
    public static let secondaryColor    = #colorLiteral(red: 0.9647058824, green: 0.9725490196, blue: 0.9882352941, alpha: 1)
    public static let textcolor         = #colorLiteral(red: 0.2666666667, green: 0.2588235294, blue: 0.3176470588, alpha: 1)
    public static let lightGrayApp = #colorLiteral(red: 0.4756349325, green: 0.4756467342, blue: 0.4756404161, alpha: 1)
    public static let labelColor                = #colorLiteral(red: 0.2666666667, green: 0.2588235294, blue: 0.3176470588, alpha: 1)
    public static let optbackground     = #colorLiteral(red: 0.9254901961, green: 0.9215686275, blue: 0.9725490196, alpha: 1)
    public static let btnbackground     = #colorLiteral(red: 0.8745098039, green: 0.8549019608, blue: 0.9450980392, alpha: 1)
    public static let upcomingbackgound = #colorLiteral(red: 0.9019607843, green: 0.9058823529, blue: 0.9568627451, alpha: 1)
    public static let green             = #colorLiteral(red: 0, green: 0.6980392157, blue: 0, alpha: 1)
    public static let white             = #colorLiteral(red: 0.9999127984, green: 1, blue: 0.9998814464, alpha: 1)
    public static let black             = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    public static let lightBlack        = #colorLiteral(red: 0.1215686275, green: 0.1137254902, blue: 0.1568627451, alpha: 1)
    public static let lightGray         = #colorLiteral(red: 0.3510887921, green: 0.3510887921, blue: 0.3510887921, alpha: 1)
    public static let cellLightGray     = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
    public static let clearColor       = UIColor.clear
    public static let defaultColor      = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    public static let enteredColor      = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    public static let darkOrange        = #colorLiteral(red: 1, green: 0.2745098039, blue: 0, alpha: 1)
    public static let textColorrrr         = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    
    public static let appBlack = UIColor.black
    public static let appLightBlack = #colorLiteral(red: 0.2666666667, green: 0.2588235294, blue: 0.3176470588, alpha: 1)
    public static let appLightGray = #colorLiteral(red: 0.5843137255, green: 0.6078431373, blue: 0.6431372549, alpha: 1)
}

