//
//  Indicator.swift
//  E-RX
//
//  Created by SinhaAirBook on 11/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation
import UIKit
import Lottie

open class Indicator {
    
    var containerView = UIView()
    var progressView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var animationView: AnimationView?
    
    lazy var loaderView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }()
    
    open class var shared: Indicator {
        
        struct Static {
            
            static let instance: Indicator = Indicator()
            
        }
        
        return Static.instance
        
    }
    
    open func showProgressView(_ view: UIView) {
        
        containerView.frame = view.frame
        containerView.center = view.center
        containerView.backgroundColor = UIColor(hex: 0xffffff, alpha: 0.3)
        //
        progressView.frame = CGRect(x: 0, y: (UIScreen.main.bounds.height * 0.22), width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - ((UIScreen.main.bounds.height * 0.22)))
        //progressView.center = view.center
        progressView.backgroundColor = .clear
        progressView.alpha = 0.7
        progressView.clipsToBounds = true
        progressView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        if #available(iOS 13.0, *) {
            activityIndicator.style = .large
        } else {
            activityIndicator.style = .gray
            // Fallback on earlier versions
        }
        activityIndicator.color = AppColor.pink
        activityIndicator.center = CGPoint(x: progressView.bounds.width / 2, y: progressView.bounds.height / 2)
        
        progressView.addSubview(activityIndicator)
        containerView.addSubview(progressView)
        view.addSubview(containerView)
        view.bringSubviewToFront(containerView)
        activityIndicator.startAnimating()
    }
    
    
    ////
    func showLoader(view: UIView) {
        loaderView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        animationView = .init(name: "Change_Location")
        animationView?.isHidden = false
        animationView?.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        //animationView?.frame.origin = loaderView.center
        animationView?.contentMode = UIView.ContentMode(rawValue: UIView.ContentMode.scaleAspectFit.rawValue/2)!
        animationView?.animationSpeed = 1.75
        animationView?.loopMode = .loop
        view.addSubview(animationView!)
        view.addSubview(loaderView)
        loaderView.bringSubviewToFront(view)
        animationView?.bringSubviewToFront(view)
        animationView?.play()
    }
    
    func hideLoaderView(){
        animationView?.isHidden = true
        self.animationView?.stop()
        loaderView.removeFromSuperview()
        animationView?.removeFromSuperview()
    }
    
    func showLottieAnimation(view: UIView) {
        
        containerView.frame = view.frame
        containerView.center = view.center
        containerView.backgroundColor = UIColor(hex: 0xffffff, alpha: 0.3)
        //
        progressView.frame = CGRect(x: 0, y: (UIScreen.main.bounds.height * 0.22), width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - ((UIScreen.main.bounds.height * 0.22)))
        //progressView.center = view.center
        progressView.backgroundColor = .clear
        progressView.alpha = 0.7
        progressView.clipsToBounds = true
        progressView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        if #available(iOS 13.0, *) {
            activityIndicator.style = .large
        } else {
            activityIndicator.style = .gray
            // Fallback on earlier versions
        }
        activityIndicator.color = AppColor.pink
        activityIndicator.center = CGPoint(x: progressView.bounds.width / 2, y: progressView.bounds.height / 3)
        
        progressView.addSubview(activityIndicator)
        containerView.addSubview(progressView)
        view.addSubview(containerView)
        view.bringSubviewToFront(containerView)
        activityIndicator.startAnimating()
    }
    func hideLoaderPresent() {
        
        activityIndicator.stopAnimating()
        containerView.removeFromSuperview()
    }
    open func hideProgressView() {
        
        activityIndicator.stopAnimating()
        containerView.removeFromSuperview()
        
    }
}

extension UIColor {
    
    convenience init(hex: UInt32, alpha: CGFloat) {
        let red = CGFloat((hex & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hex & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hex & 0xFF)/256.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
        
    }
}

