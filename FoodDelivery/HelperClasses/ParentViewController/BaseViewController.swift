//  BaseViewController.swift
//
//  Created by Sandeep Vishwakarma on 4/16/18.
//  Copyright © 2018 Sandeep. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import MapKit
import CoreLocation


struct Constant {
    
    static let checkInternet = NSLocalizedString("Please check your Internet connection", comment: "")
    static let enterUsername = NSLocalizedString("Please enter your login and Display Name.", comment: "")
    static let shouldContainAlphanumeric = NSLocalizedString("Field should contain alphanumeric characters only in a range 3 to 20. The first character must be a letter.", comment: "")
    static let password = NSLocalizedString("Password did", comment: "")
    static let countryCode = NSLocalizedString("Please select your Country Code.", comment: "")
    static let phoneNo = NSLocalizedString("Please enter your valid Phone Number.", comment: "")
    static let logoutMessage = NSLocalizedString("Do you really want to logout!", comment: "")
    static let tokenExpire = NSLocalizedString("This credential is logged in through other device", comment: "")
}

struct RegularExtentionName {
    
    static let user = "^[^_][\\w\\u00C0-\\u1FFF\\u2C00-\\uD7FF\\s]{2,19}$"
    static let passord = "^[a-zA-Z][a-zA-Z0-9]{7,14}$"
    static let mobile = "^[0-9]{10}$"
}



class ParentViewController: UIViewController,CLLocationManagerDelegate {
    
    var baseDelegate: BaseViewControllerDelegate?
    let navButtonWidth = 40.0
    let edgeInset = CGFloat(20.0)
    var subControllerName = String()
    internal var GradinetView = UIView()
    internal var  statusbarView = UIView()
    let AlertObj = UIAlertController()
    var delagateSearch : serachTapDelegate?
    var viewModel = ViewModel()
    let realm = try! Realm()
    //Maps Variables
    //    private var infoWindow = WarningMessageView()
    //    var delegateWarningMessageView :LoadWarningMessageView?
    let datePicker = UIDatePicker()
    
    var viewTranslation = CGPoint(x: 0, y: 0)
    
    let backgroundView : UIView = {
        let view = UIView()
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        return view
    }()
    
    var refreshDelegate: RefreshDelegate?
    var refreshControl: UIRefreshControl {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = .clear
        return refreshControl
    }
    
    var locationManager = CLLocationManager()
    var lat = String()
    var long = String()
    
    lazy var topView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    var topbarHeight: CGFloat {
        if #available(iOS 13.0, *) {
            return (view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0.0) +
                (self.navigationController?.navigationBar.frame.height ?? 0.0)
        } else {
            // Fallback on earlier versions
            return 74
        }
    }
    private var infoText = "" {
        didSet {
            
            alertWithHandler(message: infoText == kTokenExpireMessage ? Constant.tokenExpire: infoText) {
                
                if self.infoText == kTokenExpireMessage {
                    
                    let realm = try! Realm()
                    
                    try! realm.write {
                        
                        let deletedNotifications = realm.objects(DataUser.self)
                        realm.delete(deletedNotifications)
                        kAppDelegate.appNavigator = ApplicationNavigator(window: kAppDelegate.window,sendViewController:"LoginVC")
                    }
                    
                }else if self.infoText == Constant.checkInternet || self.infoText == "URLSessionTask failed with error: The network connection was lost."{
                    // self.delegateWarningMessageView?.loadWarinigView()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeTheLocationManager()
        
        //self.infoWindow = loadWarningNiB()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.baseDelegate = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initializeTheLocationManager()
    {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locationManager.location?.coordinate
        
        self.lat = location?.latitude.description ?? ""
        self.long = location?.longitude.description ?? ""
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    //MARK - Setup
    func defaultConfiguration() -> Bool {
        
        //MARK: - Reachability
        let updateLoginInfo: ((_ status: NetworkConnectionStatus) -> Void)? = { [weak self] status in
            let notConnection = status == .notConnection
            if notConnection{
                self?.infoText = Constant.checkInternet
                // notConnection = false
            }
        }
        
        Reachability.instance.networkStatusBlock = { status in
            updateLoginInfo?(status)
        }
        updateLoginInfo?(Reachability.instance.networkConnectionStatus())
        
        return self.infoText == Constant.checkInternet
    }
    
    
    
    //MARK:- Refresh handler
    
    @objc func handleRefresh(){
        self.refreshDelegate?.refresh()
    }
    
    // MARK: - Handle errors
    func handleError(_ error: String?) {
        guard let error = error else {
            return
        }
        var infoText = error
        if error.contains("The Internet connection appears to be offline.")  {
            infoText = Constant.checkInternet
        }
        
        self.infoText = infoText
    }
    
    //MARK: - Validation helpers
    func isValid(userName: String?,regulerExp:String) -> Bool {
        
        let characterSet = CharacterSet.whitespaces
        let trimmedText = userName?.trimmingCharacters(in: characterSet)
        let regularExtension = regulerExp
        let predicate = NSPredicate(format: "SELF MATCHES %@", regularExtension)
        let isValid = predicate.evaluate(with: trimmedText)
        return isValid
        
    }
    
    // MARK: UI/UX Functions
    func setupBackgroundView() {
        view.backgroundColor = .clear
        view.insertSubview(backgroundView, at: 0)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.topAnchor.constraint(equalTo: view.topAnchor, constant: 60).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 30).isActive = true
        backgroundView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        backgroundView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        backgroundView.layer.cornerRadius = 20
        backgroundView.clipsToBounds = true
    }
    
    // Show Activity Indicator
    func showActivityIndicator(view:UIView) {
        
        DispatchQueue.main.async {
            
            Indicator.shared.showProgressView(view)
        }
    }
    
    // Hide Activity Indicator
    func hideActivityIndicator() {
        
        DispatchQueue.main.async {
            
            Indicator.shared.hideProgressView()
        }
    }
    
    // MARK: Drag dismiss function
    @objc func handleDismiss(sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .changed:
            
            viewTranslation = sender.translation(in: view)
            if viewTranslation.y > 0 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.view.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
                })
            }
        case .ended:
            if viewTranslation.y < 100 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.view.transform = .identity
                })
            } else {
                dismiss(animated: true, completion: nil)
            }
        default:
            break
        }
    }
    
    @objc func actionContinue() {
        print("No one can stop me.")
    }
    
    func backViewController() -> UIViewController {
        let numberOfViewControllers: Int = self.navigationController!.viewControllers.count
        if numberOfViewControllers >= 2 {
            return self.navigationController!.viewControllers[numberOfViewControllers - 2]
        }
        return self.navigationController!.viewControllers[numberOfViewControllers-1]
    }
    
    
    //MARK:-Show and Hide UiElements
    
    func showHideView(view:[UIView],hide:Bool){
        
        for view in view{
            
            view.isHidden = hide
        }
        
    }
    
    
    func addDoneButtonOnKeyboard(txtFld : [UITextField])
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = AppColor.whiteColor
        doneToolbar.items = [flexSpace,done]
        doneToolbar.sizeToFit()
        doneToolbar.backgroundColor = AppColor.appColor
        //self.textView.inputAccessoryView = doneToolbar
        
        for txtfld in txtFld{
            
            txtfld.inputAccessoryView = doneToolbar
        }
        
    }
    
    func alertWithHandler2(message : String,block_Yes:  @escaping ()->Void){
        
        let alertController = UIAlertController(title:CommonClass.sharedInstance.createString(Str: "YouPick!"), message: message, preferredStyle: .alert)
        
        // Create OK button
        let OKAction = UIAlertAction(title: comClass.createString(Str: "Yes"), style: .default) { (action:UIAlertAction!) in
            
            
            block_Yes()
            
        }
        alertController.addAction(OKAction)
        
        // Create Cancel button
        let cancelAction = UIAlertAction(title: comClass.createString(Str: "No"), style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped");
        }
        alertController.addAction(cancelAction)
        // alertController.view.backgroundColor = AppColor.appColor
        // Present Dialog message
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    @objc func doneButtonAction()
    {
        view.endEditing(true)
        // self.textViewDescription.resignFirstResponder()
    }
    
    
    
    func fontAndColor(lbl:[UILabel],text:[String])  {
        
        for i in 0..<lbl.count{
            lbl[i].font = AppFont.Regular.size(AppFontName.Poppins, size: 17)
            lbl[i].textColor = AppColor.textColor
            lbl[i].text = text[i]
            lbl[i].textAlignment =  Localize.currentLanguage() == "ar" ? .right : .left
        }
        
    }
    
    
    // =====  TextField Padding ======
    func configureView(txtFld:[UITextField]){
        
        for txtFld in txtFld{
            let paddingPhoneNumber = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: txtFld.frame.height))
            if Localize.currentLanguage() == "en" {
                txtFld.leftView = paddingPhoneNumber
                txtFld.leftViewMode = UITextField.ViewMode.always
            }else{
                txtFld.rightView = paddingPhoneNumber
                txtFld.rightViewMode = UITextField.ViewMode.always
            }
            
        }
        
    }
    
    // =====TextField Font Size Color =====
    
    func txtFldFontSizeConfig(txtFld:[UITextField],placeHolderText:[String]){
        
        for i in 0..<txtFld.count{
            
            txtFld[i].placeholder = placeHolderText[i]
            txtFld[i].font = AppFont.Regular.size(AppFontName.Poppins, size: 17)
            txtFld[i].textColor = AppColor.textColor
            Localize.currentLanguage() == "en" ? (txtFld[i].textAlignment = .left) : (txtFld[i].textAlignment = .right)
        }
        
    }
    
    // ===== Lable Font Size Color =====
    
    open func setupLable(_ title: [String],_ lbl: [UILabel],_ aliment: [NSTextAlignment],_ font: [String],_ size: [CGFloat]){
        
        for i in 0..<lbl.count{
            lbl[i].text = comClass.createString(Str: title[i])
            lbl[i].textAlignment = aliment[i]
            
            if font[i] == AppFont.Regular.rawValue{
                lbl[i].font = AppFont.Regular.size(AppFontName.Poppins, size: size[i])
            }else if font[i] == AppFont.Medium.rawValue{
                lbl[i].font = AppFont.Medium.size(AppFontName.Poppins, size: size[i])
            }else{
                lbl[i].font = AppFont.Bold.size(AppFontName.Poppins, size: size[i])
            }
        }
    }
    
    open func setupTxtFld(_ title: [String],_ txt: [UITextField],_ aliment: [NSTextAlignment],_ font: [String],_ size: [CGFloat]){
        
        for i in 0..<txt.count{
            txt[i].placeholder = comClass.createString(Str: title[i])
            txt[i].textAlignment = aliment[i]
            
            if font[i] == AppFont.Regular.rawValue{
                txt[i].font = AppFont.Regular.size(AppFontName.Poppins, size: size[i])
            }else if font[i] == AppFont.Medium.rawValue{
                txt[i].font = AppFont.Medium.size(AppFontName.Poppins, size: size[i])
            }else{
                txt[i].font = AppFont.Bold.size(AppFontName.Poppins, size: size[i])
            }
        }
    }
    
    
    
    open func provideAttributedTextToControlLeftAllignCenter(_ title:String, _ subtitle:String,_ titleFont:UIFont,_ subtitleFont:UIFont,_ titleColor:UIColor,_ subtitleColor:UIColor,_ alignment: NSTextAlignment? = .center) -> NSMutableAttributedString{
        let myMutableString = NSMutableAttributedString()
        
        
        let myMutableString1 = NSAttributedString(string: "\(title)", attributes:[ .foregroundColor :titleColor,.font:titleFont])
        
        let myMutableString2 = NSAttributedString(string: "\(subtitle)", attributes:[.foregroundColor :subtitleColor,.font:subtitleFont])
        
        myMutableString.append(myMutableString1)
        myMutableString.append(myMutableString2)
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 0
        //paragraphStyle.paragraphSpacing = 0// Whatever line spacing you want in points
        paragraphStyle.alignment = alignment ?? .center
        
        // *** Apply attribute to string ***
        myMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, myMutableString.length))
        
        return myMutableString
    }
    
    open func provideAttributedTextToControlLeftAllignCenter1(_ title:String, _ subtitle:String, _ title2:String,_ titleFont:UIFont,_ subtitleFont:UIFont,_ titleFont2:UIFont,_ titleColor:UIColor,_ subtitleColor:UIColor,_ titleColor2:UIColor) -> NSMutableAttributedString{
        
        let myMutableString = NSMutableAttributedString()
        
        let myMutableString1 = NSAttributedString(string: "\(title)", attributes:[ .foregroundColor :titleColor,.font:titleFont])
        
        let myMutableString2 = NSAttributedString(string: "\(subtitle)", attributes:[.foregroundColor :subtitleColor,.font:subtitleFont])
        let myMutableString3 = NSAttributedString(string: "\(title2)", attributes:[.foregroundColor :titleColor2,.font:titleFont2])
        
        myMutableString.append(myMutableString1)
        myMutableString.append(myMutableString2)
        myMutableString.append(myMutableString3)
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 0 // Whatever line spacing you want in points
        paragraphStyle.alignment = .center
        
        // *** Apply attribute to string ***
        myMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, myMutableString.length))
        
        return myMutableString
    }
    
    // ====== Button Font Size Color =====
    
    
    
    @available(iOS, deprecated: 9.0)
    func hideStatusBar(_ hide: Bool) {
        UIApplication.shared.setStatusBarHidden(hide, with: .none)
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.isHidden = hide
    }
    
   
    
    func goBackToIndex(_ backIndex: Int) {
        //self.goBackToIndex(backIndex, animated: true)
        
        //kAppDelegate.openDrawer()
    }
    
    func goBackToIndex(_ backIndex: Int, animated animate: Bool) {
        if (self.navigationController!.viewControllers.count - backIndex) > 0 {
            let controller: ParentViewController = (self.navigationController!.viewControllers[(self.navigationController!.viewControllers.count - 1 - backIndex)] as! ParentViewController)
            self.navigationController!.popToViewController(controller, animated: animate)
        }
    }
    
    //Get indexpath for tableview
    func getIndexPathFor(sender : AnyObject, tblView : UITableView) -> NSIndexPath? {
        let rect : CGRect = sender.convert(sender.bounds, to: tblView)
        if let indexPath  = tblView.indexPathForRow(at: rect.origin) {
            return indexPath as NSIndexPath?;
        }
        return nil
    }
    
    func addDoneButtonOnKeyboard( textfield : UITextField) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: MAIN_SCREEN_WIDTH, height: 50))
        let doneToolbar: UIToolbar = UIToolbar(frame: rect)
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title:"Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(tapDone(sender:)))
        doneToolbar.items = [done , flexSpace]
        doneToolbar.sizeToFit()
        textfield.inputAccessoryView = doneToolbar
    }
    
    @objc func tapDone(sender : UIButton) {
        view.endEditing(true)
    }
    
    func addSwipGesture(view:UIView){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case .right:
                self.dismiss(animated: true, completion: nil)
            case .down:
                print("Swiped down")
            case .left:
                print("Swiped left")
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func alert(message : String)
    {
        let  alert = UIAlertController(title: CommonClass.sharedInstance.createString(Str: "YouPick!"), message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: CommonClass.sharedInstance.createString(Str: "OK"), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func alertWithHandlerr(message : String , block:  @escaping ()->Void ){
        
        let  alert = UIAlertController(title: CommonClass.sharedInstance.createString(Str: "YouPick!"), message: CommonClass.sharedInstance.createString(Str: message), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: CommonClass.sharedInstance.createString(Str: "OK"), style: .default, handler: {(action : UIAlertAction) in
            
            block()
            
        }))
        alert.view.backgroundColor = .clear
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    
    //MARK:- Add Blur View
    @available(iOS 13.0, *)
    func blurView(){
        let blurEffect = UIBlurEffect(style: .systemUltraThinMaterialDark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(blurView, at: 0)
        blurView.frame = CGRect(x: 0, y: 40, width: view.bounds.width, height: view.bounds.height)
    }
    func addBlurOnView(view:UIView){
        
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemUltraThinMaterialLight)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.insertSubview(blurEffectView, at: 0)
        } else {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.insertSubview(blurEffectView, at: 0)
        }
        
    }
    func transparentView(){
        let view = UIView(frame: CGRect(x: 0, y: 40, width: self.view.bounds.width, height: self.view.bounds.height))
        view.backgroundColor = .black
        view.alpha = 0.5
        view.layer.cornerRadius = 20
        self.view.insertSubview(view, at: 0)
    }
}

//MARK: - Screen sizes


protocol serachTapDelegate {
    
    func serachTap()
    
}


extension UIWindow {
    static var key: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}

//MARK:- Firebase Setup
//MARK:-
import Firebase
import FirebaseAuth
extension ParentViewController{
    
    func setupPhoneAuthentication(_ phoneNumber:String, verification: @escaping (String) -> ()){
        Auth.auth().languageCode = Localize.currentLanguage()
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (code, error) in
            Indicator.shared.hideLoaderPresent()
            
            guard error == nil else{
                if error?.localizedDescription == "TOO_SHORT"{
                    self.handleError("Phone number is too short")
                }else{
                    self.handleError(error?.localizedDescription)
                }
                return
            }
            verification(code ?? "")
        }
    }
    
    func otpVerification(_ otp:String,_ vCode:String, completion: @escaping () ->()){
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: vCode, verificationCode: otp)
        
        Auth.auth().signIn(with: credential) { (result, error) in
            Indicator.shared.hideLoaderPresent()
            
            guard error == nil else{
               // self.handleError(error?.localizedDescription)
                self.handleError("Invalid OTP")
                return
            }
            print(result?.credential ?? "")
            print(Auth.auth().currentUser!)
            completion()
        }
    }
}
//MARK:- getCountryCode by IP
extension ParentViewController{
    func getCurrentCountryCode(_ complition:@escaping (String)->()){
        showActivityIndicator(view: view)
        ApiManager.instance.fetchApiService(method: .get, url: "http://ip-api.com/json") { (result) in
            switch result{
            case .success(let data):
                print(data)
                if data["status"].string == "success"{
                    let countryCode = data["countryCode"].stringValue
                    complition(countryCode)
                }
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
        }
    }
    
    func getDialCode(_ code:String,_ complition:@escaping (String)->()){
        showActivityIndicator(view: view)
        ApiManager.instance.fetchApiService(method: .get, url: "https://restcountries.eu/rest/v1/alpha/\(code)") { (result) in
            switch result{
            case .success(let responseJASON):
                print(responseJASON)
                if let callingCodesArray = responseJASON["callingCodes"].arrayObject as NSArray?{
                    complition("+" + (callingCodesArray.firstObject as? String ?? "91"))
                }
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
        }
    }
    func format(with mask: String, phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex // numbers iterator

        // iterate over the mask characters until the iterator of numbers ends
        for ch in mask where index < numbers.endIndex {
            if ch == "X" {
                // mask requires a number in this place, so take the next one
                result.append(numbers[index])

                // move numbers iterator to the next index
                index = numbers.index(after: index)

            } else {
                result.append(ch) // just append a mask character
            }
        }
        return result
    }
}
