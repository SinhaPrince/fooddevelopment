//
//  NavigationController.swift
//  FoodDelivery
//
//  Created by Cst on 4/10/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

enum UINavigationBarButtonType: Int {
    case back
    case backHomeNAll
    case black
    case menu
    case search
    case map
    case filter
    case address
    case resetAll
    case edit
    case language
    case cancel
    case cross
    case backFromPresent
    var iconImage: UIImage? {
        switch self {
        case .back:  return UIImage(named: "back_side")
        case .backHomeNAll: return UIImage(named: "back_side")
        case .black: return UIImage()
        case .menu: return UIImage(named: "menu")
        case .search: return UIImage(named: "search")
        case .map: return UIImage(named: "map")
        case .filter: return UIImage(named: "list_black")
        case .address: return UIImage()
        case .resetAll: return UIImage()
        case .edit: return #imageLiteral(resourceName: "editpink")
        case .language: return #imageLiteral(resourceName: "globe")
        case .cancel : return nil
        case .cross : return #imageLiteral(resourceName: "close_a")
        case .backFromPresent : return UIImage(named: "close_cross")
        }
    }
}
protocol BaseViewControllerDelegate {
    func navigationBarButtonDidTapped(_ buttonType: UINavigationBarButtonType)
}
extension ParentViewController{
    
    
    /// UINavigationBar Setup
    /// - Parameters:
    ///   - title: Title Of Navigation bar
    ///   - leftBarButtonsType: Left Item of NavigationBar
    ///   - rightBarButtonsType: Right Item of NavigationBar
    func setupNavigationBarTitle(_ title: String, leftBarButtonsType: [UINavigationBarButtonType], rightBarButtonsType: [UINavigationBarButtonType], titleViewFrame: CGRect = CGRect(x: 0, y: 0, width: 200, height: 60)) {
        hideNavigationBar(false)
        self.title = comClass.createString(Str: title)
       // self.navigationItem.prompt = " "

//        if sizeConfiguration() == "S"{
//            self.navigationItem.prompt = " "
//        }
        
        var rightBarButtonItems = [UIBarButtonItem]()
        for rightButtonType in rightBarButtonsType {
            let rightButtonItem = getBarButtonItem(for: rightButtonType, isLeftBarButtonItem: false)
            rightBarButtonItems.append(rightButtonItem)
        }
        if rightBarButtonItems.count > 0 {
            self.navigationItem.rightBarButtonItems = rightBarButtonItems
        }
        var leftBarButtonItems = [UIBarButtonItem]()
        for leftButtonType in leftBarButtonsType {
            let leftButtonItem = getBarButtonItem(for: leftButtonType, isLeftBarButtonItem: true)
            leftBarButtonItems.append(leftButtonItem)
        }
        if leftBarButtonItems.count > 0 {
            self.navigationItem.leftBarButtonItems = leftBarButtonItems
            
        }
        
    }
    
    
    func getBarButtonItem(for type: UINavigationBarButtonType, isLeftBarButtonItem: Bool) -> UIBarButtonItem {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.setTitleColor(.black, for: UIControl.State())
        button.titleLabel?.font = AppFont.Medium.size(.Poppins, size: 16)
        button.titleLabel?.textAlignment = .right
        
        button.tag = type.rawValue
        button.contentHorizontalAlignment = .leading
       //
        if let iconImage = type.iconImage {
            if type == .back || type == .backHomeNAll{
                button.setImage(Localize.currentLanguage() == "en" ? UIImage(named: "back_side") : UIImage(named: "rightBack"), for: .normal)
                button.backgroundColor = .white
              //  button.imageEdgeInsets = UIEdgeInsets(top: 0, left: isLeftBarButtonItem ? edgeInset : edgeInset, bottom: 0, right: isLeftBarButtonItem ? edgeInset : edgeInset)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
                button.cornerRadius = 8
                if type == .back{
                    button.shadowRadius = 5
                    button.shadowOffset = CGSize(width: 0, height: 0)
                    button.shadowColor = .black
                    button.shadowOpacity = 0.3
                }
                button.borderWidth = 0.5
                button.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                
            }else{
                button.setImage(iconImage, for: .normal)
            }
        }
        button.addTarget(self, action: #selector(ParentViewController.navigationButtonTapped(_:)), for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }
    
    func addColorToNavigationBarAndSafeArea(color:CAGradientLayer,className:String){
        subControllerName = className
        ScreeNNameClass.shareScreenInstance.screenName = subControllerName
    }
    
    func transparentNavigation(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(color: UIColor.clear), for: UIBarMetrics.default)
                self.navigationController?.navigationBar.shadowImage = UIImage(color: UIColor.clear)
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func nontransparentNavigation(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(color: AppColor.pink), for: UIBarMetrics.default)
                self.navigationController?.navigationBar.shadowImage = UIImage(color: AppColor.pink)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    func hideNavigationBar(_ hide: Bool) {
        self.navigationController?.setNavigationBarHidden(hide, animated: false)
    }
    
    @objc func navigationButtonTapped(_ sender: AnyObject) {
        guard let buttonType = UINavigationBarButtonType(rawValue: sender.tag) else { return }
        switch buttonType {
        case .back ,.backHomeNAll, .backFromPresent: backButtonTapped()
        case .black:
            print("Do Nothing")
        case .cross:
            closeSideMenu()
        default :
            self.baseDelegate?.navigationBarButtonDidTapped(buttonType)
        }
    }
    
    //MARK:- UINavigation Item Action
    //MARK:-
    func backButtonTapped() {
        if self.navigationController!.viewControllers.count > 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func closeSideMenu(){
        kAppDelegate.appNavigator.drawerController.setDrawerState(.closed, animated: true)
    }
    
    func SizeWithString(text: String,font: UIFont, maxWidth : CGFloat,numberOfLines: Int) -> CGRect{

        let font = font//(name: "HelveticaNeue", size: fontSize)!
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: maxWidth, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = numberOfLines
        label.font = font
        label.text = text

        label.sizeToFit()

        return label.frame
    }
    
    func sizeConfiguration() -> String {
        switch UIDevice.current.screenType {
        case .iPhone_11Pro_X_XS:
             
            return "11Pro_X_XS"
        case .iPhone_XSMax_11ProMax:
            return "11ProMax"
        case .iPhone_XR_11:
            return "11_XR"
        case .iPhones_4_4S, .iPhones_6_6s_7_8, .iPhones_5_5s_5c_SE, .iPhones_6Plus_6sPlus_7Plus_8Plus:
            return "S"
        case .iPhone_12_Pro:
            return "12_Pro"
        case .iPhone_12ProMax:
            return "12ProMax"
        default:
            return "S"
        }
    }
}
