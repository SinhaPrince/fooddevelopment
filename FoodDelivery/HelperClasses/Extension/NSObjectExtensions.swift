//
//  NSObjectExtensions.swift
//  Shoshli
//
//  Created by Atendra on 23/07/19.
//  Copyright © 2019 Mobulous. All rights reserved.
//

import Foundation
extension NSObject {
    class var className: String {
        return String(describing: self)
    }
}
