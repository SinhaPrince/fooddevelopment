//
//  AppConstants.swift
//  LinkMyParking
//
//  Created by Mobulous on 06/04/20.
//  Copyright © 2020 Mobulous. All rights reserved.
//

import UIKit

class AppConstants: NSObject {
    
    // MARK: API Keys
    
    static let kSimulatorDeviceToken = "b1e2d3bb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326"
    
    static let autoCompletebaseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    static let AlreadyLogined = "Invalid User."
    static let colorGreen = UIColor(red: 61/255, green: 189/255, blue: 158/255, alpha: 1.0)
    
    
    
    //MARK : --- lat Long ----
    static let lat                   = UserDefaults.standard.value(forKey: "lat") as? String
    static let long                  = UserDefaults.standard.value(forKey: "long") as? String
    static let token                 = UserDefaults.standard.value(forKey: "token") as? String
    static let deviceToken           = UserDefaults.standard.value(forKey: "DeviceToken") as? String
    static let email                 = UserDefaults.standard.value(forKey: "email") as? String
    static let id                    = UserDefaults.standard.value(forKey: "id") as? String
    static let mobile                = UserDefaults.standard.value(forKey: "mobile") as? String
    static let status                = UserDefaults.standard.value(forKey: "status") as? String
    static let notification_status   = UserDefaults.standard.value(forKey: "notification_status") as? String
    static let DeviceType            = "Iphone"
    static let appName               = "Bite Me"
    
    
    
    
    struct staticTexts {
        
    }
    
    
      
      static let kChoosePicture           = "Choose Picture"
      static let kPhotoLibrary            = "Photo Library"
      static let kCamera                  = "Camera"
      static let kGallery                 = "Gallery"
      static let kCancel                  = "Cancel"
      static let kOk                      = "OK"
    
    
    
    
    // MARK: DateFormater types
    static let kDateFormatter            =  "MM-dd-yyyy"
    static let kDateFormatter1           =  "MM-dd-yyyy"
    static let kDateFormatter24Hour      =  "ss.SSSS"
    static let kTimeFormatter24Hour      =  "HH:mm"
    static let kTimeFormatter12Hour      =  "HH"
    static let kDateTimeDateFormatter    =  "yyyy-MM-dd HH:mm"
    static let kDayTimeDateFormatter     =  "EEE, d MMM yyyy"
    static let kDayTimeFormatter         =  "EEE, d MMM yyyy, hh:mm a"
    
    
    // MARK:- WEEK DAYS Keys
    static let kSunday                         = "Sunday"
    static let kMonday                         = "Monday"
    static let kTuesday                        = "Tuesday"
    static let kWednusday                      = "Wednusday"
    static let kThursday                       = "Thursday"
    static let kFriday                         = "Friday"
    static let kSaturday                       = "Saturday"
    
    
    // MARK:- Internet & Server Connection Keys
    
    static let kInternetNotAvailable        = "Check your Internet Connection"
    static let kInternetServerNotResponding    = "Server Not Responding"
    static let kNotAvailable                   = "Not Available"
    
    
    static func suffixNumber(number:NSNumber) -> NSString {

        var num:Double = number.doubleValue;
        let sign = ((num < 0) ? "-" : "" );

        num = fabs(num);

        if (num < 1000.0){
            return "\(sign)\(num)" as NSString;
        }

        let exp:Int = Int(log10(num) / 3.0 ); //log10(1000));

        let units:[String] = ["K","M","G","T","P","E"];

//        let roundedNum:Double = round(10 * num / pow(1000.0,Double(exp))) / 10;
        
        //return "\(sign)\(roundedNum)\(units[exp-1])" as NSString;
        
        let roundedNum:Double = (10 * num / pow(1000.0,Double(exp))) / 10;

        let y = (roundedNum*100).rounded()/100
        
        return "\(sign)\(y)\(units[exp-1])" as NSString;
    }
    
    //MARK:- Date Conversion
    static func dateTimeConversion(createdAt:String) -> String{
        
        
        var newTimeZone = String()
        newTimeZone = newTimeZone.timeDateConversion(formateDate:String(createdAt.prefix(19)))
        let start = String.Index(utf16Offset: 11, in: newTimeZone)
        let end = String.Index(utf16Offset: 18, in: newTimeZone)
        let substring = String(newTimeZone[start...end])
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        let dateObj = dateFormatter.date(from: "\(newTimeZone.prefix(10))")
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var timeT = String()
        timeT = timeT.timeConversion12(time24: "\(substring.prefix(5))")
        print(timeT)
        return "\(dateFormatter.string(from: dateObj!))"
    }
    
    //MARK:- Time Conversion
    static func timeconversion(createdAt:String) -> String{
        
        var newTimeZone = String()
        newTimeZone = newTimeZone.timeDateConversion(formateDate:String(createdAt.prefix(19)))
        let start = String.Index(utf16Offset: 11, in: newTimeZone)
        let end = String.Index(utf16Offset: 18, in: newTimeZone)
        let substring = String(newTimeZone[start...end])
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var timeT = String()
        timeT = timeT.timeConversion12(time24: "\(substring.prefix(5))")
        print(timeT)
        return timeT
    }
    
    
}
extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    func toInt() -> Int? {
        return NumberFormatter().number(from: self)?.intValue
    }
    
    
    func removeHTMLTag() -> String {
        
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
        
    }
}
