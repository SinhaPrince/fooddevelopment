//
//  SaveDefaultAddress.swift
//  FoodDelivery
//
//  Created by Cst on 4/16/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import Foundation
import RealmSwift

class SaveDefaultAddress:Object{
    @objc dynamic var latitude:String?
    @objc dynamic var longitude:String?
    @objc dynamic var name:String?
    convenience init(_ latitude:String,_ longitude:String,_ name:String){
        self.init()
        self.latitude = latitude
        self.longitude = longitude
        self.name = name
    }
}
