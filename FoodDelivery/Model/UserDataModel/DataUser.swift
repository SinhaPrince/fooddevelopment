//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 07, 2021
//
import Foundation
import RealmSwift
import SwiftyJSON
class DataUser: Object {
    
    @objc dynamic var Id: String?
    @objc dynamic var status: String?
    @objc dynamic var userType: String?
    @objc dynamic var profilePic: String?
    dynamic var deleteStatus: Bool? = false
    dynamic var notificationStatus: Bool? = false
    dynamic var totalOrders: Int? = 0
    @objc dynamic var email: String?
    @objc dynamic var countryCode: String?
    @objc dynamic var mobileNumber: String?
    @objc dynamic var firstName: String?
    @objc dynamic var lastName: String?
    @objc dynamic var latitude: String?
    @objc dynamic var longitude: String?
    @objc dynamic var deviceType: String?
    @objc dynamic var deviceToken: String?
    @objc dynamic var createdAt: String?
    @objc dynamic var updatedAt: String?
    @objc dynamic var address: String?
    dynamic var _v: Int? = 0
    @objc dynamic var jwtToken: String?
    @objc dynamic var gender: String?
    @objc dynamic var nationality: String?
    @objc dynamic var dob: String?
     dynamic var profileimage: UIImage?

    convenience init(_ json: JSON)  {
        
        // try self.init(from: decoder)
        self.init()
        Id = json["_id"].stringValue
        status = json["status"].stringValue
        userType = json["userType"].stringValue
        profilePic = json["profilePic"].stringValue
        deleteStatus = json["deleteStatus"].boolValue
        notificationStatus = json["notificationStatus"].boolValue
        totalOrders = json["totalOrders"].intValue
        email = json["email"].stringValue
        countryCode = json["countryCode"].stringValue
        mobileNumber = json["mobileNumber"].stringValue
        firstName = json["firstName"].stringValue
        lastName = json["lastName"].stringValue
        latitude = json["latitude"].stringValue
        longitude = json["longitude"].stringValue
        deviceType = json["deviceType"].stringValue
        deviceToken = json["deviceToken"].stringValue
        createdAt = json["createdAt"].stringValue
        updatedAt = json["updatedAt"].stringValue
        _v = json["__v"].intValue
        jwtToken = json["jwtToken"].stringValue
        address = json["address"].stringValue
        dob = json["dob"].stringValue
        gender = json["gender"].stringValue
        nationality = json["nationality"].stringValue
        profileimage = #imageLiteral(resourceName: "profile-user")
    }
   
    
}
