//
//  UserData.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 07, 2021
//
import Foundation
import RealmSwift
import SwiftyJSON
struct UserData {
    
    let status: Int?
    let message: String?
    let data: DataUser?
    
    init(_ json: JSON) {
        status = json["status"].intValue
        message = json["message"].stringValue
        data = DataUser(json["data"])
        
        let realm = try! Realm()
        try! realm.write({
            let deletedNotifications = realm.objects(DataUser.self)
            realm.delete(deletedNotifications)
            realm.add(data!)
        })
    }
}
