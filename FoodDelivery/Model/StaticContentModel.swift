//
//  StaticContentModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 09, 2021
//
import Foundation
import SwiftyJSON

struct StaticContentModel {

	let status: Int?
	let message: String?
	let data: [StaticData]?

	init(_ json: JSON) {
		status = json["status"].intValue
		message = json["message"].stringValue
		data = json["data"].arrayValue.map { StaticData($0) }
	}

}
