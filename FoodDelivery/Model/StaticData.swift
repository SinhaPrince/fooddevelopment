//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 09, 2021
//
import Foundation
import SwiftyJSON

struct StaticData {

	let status: String?
	let Id: String?
	let title: String?
	let description: String?
	let type: String?
	let userType: String?
	let createdAt: String?
	let updatedAt: String?
	let _v: Int?

	init(_ json: JSON) {
		status = json["status"].stringValue
		Id = json["_id"].stringValue
		title = json["title"].stringValue
		description = json["description"].stringValue
		type = json["type"].stringValue
		userType = json["userType"].stringValue
		createdAt = json["createdAt"].stringValue
		updatedAt = json["updatedAt"].stringValue
		_v = json["__v"].intValue
	}

}
