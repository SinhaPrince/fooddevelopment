//
//  ApplicationNavigator.swift
//  E-RX
//
//  Created by SinhaAirBook on 10/07/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation
import RealmSwift
import KYDrawerController

class ApplicationNavigator {
    
    //MARK:- Variables
    var drawerController = KYDrawerController.init(drawerDirection: Localize.currentLanguage() == "en" ? .left : .right, drawerWidth: UIScreen.main.bounds.width - 75)
    //MARK:- initializer
    init(window: UIWindow? = nil,sendViewController:String? = nil) {
        
        let sendViewController = sendViewController
        
        
        if sendViewController == "TutorialScreenVC"{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let menuVC = storyboard.instantiateViewController(withIdentifier: "TutorialScreenVC") as! TutorialScreenVC
            let navigationController = UINavigationController(rootViewController: menuVC)
            //navigationController.isNavigationBarHidden = true
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
        }else if sendViewController == "HomeVC" {
            
            let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
            let menuVC = storyboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
            let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC")
            let navigationController = UINavigationController(rootViewController: drawerController)
            self.drawerController.mainViewController = homeVC
            self.drawerController.drawerViewController = menuVC
            drawerController.setDrawerState(.closed, animated: true)
            navigationController.isNavigationBarHidden = false
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
            
        }else{
            DispatchQueue.main.async {
//                let userData = realm.objects(DataUser.self)
//                if userData.count > 0 {
//                    try! realm.write({
//                        let deletedNotifications = realm.objects(DataUser.self)
//                        realm.delete(deletedNotifications)
//                    })
//                }
                
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let menuVC = storyboard.instantiateViewController(withIdentifier: "WelcomeScreenVC") as! WelcomeScreenVC
                let navigationController = UINavigationController(rootViewController: menuVC)
               // navigationController.isNavigationBarHidden = true
                window?.rootViewController = navigationController
                window?.makeKeyAndVisible()
            }
           
            
        }
        
        
    }
    
    func screenOrientation(currentLanguage: String){
        if currentLanguage == "en"{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
    }
    
}
