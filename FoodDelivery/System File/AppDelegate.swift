//
//  AppDelegate.swift
//  FoodDelivery
//
//  Created by call soft on 22/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import KYDrawerController
import IQKeyboardManager
import RealmSwift
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "erx.message"
    var push : PushNotification?

    var drawerController = KYDrawerController.init(drawerDirection: .left, drawerWidth: UIScreen.main.bounds.width - 75)
    
    var appNavigator = ApplicationNavigator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //com.farheen.FoodDelivery
        sleep(3)
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
        IQKeyboardManager.shared().toolbarTintColor = UIColor.black
        push = PushNotification(application: application)
        GMSPlacesClient.provideAPIKey("AIzaSyBqkaQe-laP88wY3T0HZ5VVo2naXl59jx8")
        GMSServices.provideAPIKey("AIzaSyBqkaQe-laP88wY3T0HZ5VVo2naXl59jx8")

        let realm = try! Realm()
            let userData = realm.objects(DataUser.self)
        if userData.count > 0 {
            appNavigator = ApplicationNavigator(window: window, sendViewController: "HomeVC")
        }else{
            appNavigator = ApplicationNavigator(window: window, sendViewController: "TutorialScreenVC")
        }
        
        return true
    }

   
    
    //MARK:- Check AutoLogin
    
    
    func checkAutoLogin() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let menuVC = storyboard.instantiateViewController(withIdentifier: "TutorialScreenVC") as! TutorialScreenVC
        let navigationController = UINavigationController(rootViewController: menuVC)
        navigationController.isNavigationBarHidden = true
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()

    }
    func logOutController() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let menuVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navigationController = UINavigationController(rootViewController: menuVC)
        navigationController.isNavigationBarHidden = true
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()

       }
    func initController(identifer:String) {
           
           let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
           let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TabBar") as! TabBar
           let navigationController = UINavigationController(rootViewController: nextViewController)
        navigationController.isNavigationBarHidden = true
        self.window?.rootViewController = navigationController

           self.window?.makeKeyAndVisible()
           
       }

    func initControllerrr() {
            
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let menuVC = storyboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC")
        let navigationController = UINavigationController(rootViewController: drawerController)
        self.drawerController.mainViewController = homeVC
        self.drawerController.drawerViewController = menuVC
        drawerController.setDrawerState(.closed, animated: true)
        navigationController.isNavigationBarHidden = true
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
            
        }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    
    }


}

