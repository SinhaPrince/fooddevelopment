//
//  TabBar.swift
//  FoodDelivery
//
//  Created by call soft on 22/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import Foundation
import UIKit

class TabBar: UITabBarController {
    
    var controllerInstance4 =  UIViewController()
    
    let arrImageNormal : NSArray = ["home_s","notification_un","cart","qr-code_un","more_un"]
    let arrImageSelected : NSArray = ["home_s","notification_un","cart","qr-code_un","more_un"]
    let titleArray  = ["Home","My Expenses","","My Receipts","My Profile"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTabBarSelectedImage(normalArr: arrImageNormal, selectedArr: arrImageSelected)
        self.setTabBar()
        //Set Initial Selected Tab
        self.selectedIndex = 0
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //self.setTabBar()
        //self.tabBar.items![3].badgeValue = UserDefaults.standard.value(forKey: "badgeCount") as? String ?? "0"
    }
    
    
    func setTabBar() {
        
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenHeight = screenSize.height
        var kBarHeight:CGFloat
        if screenHeight == 812{
            kBarHeight = 87
        }
        else if screenHeight == 896{
            kBarHeight = 87
        }
        else{
            kBarHeight = 54
        }
        
        var tabFrame: CGRect = tabBar.frame
        tabFrame.size.height = kBarHeight
        tabFrame.origin.y = view.frame.size.height - kBarHeight
        tabBar.frame = tabFrame
        let numberOfItems = CGFloat(tabBar.items!.count)
        
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height:kBarHeight)
                print(tabBarItemSize)
            case 1334:
                print("iPhone 6/6S/7/8")
                let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: kBarHeight)
                print(tabBarItemSize)
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: kBarHeight)
                print(tabBarItemSize)
            case 2436:
                print("iPhone X")
                
                let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: kBarHeight)
                print(tabBarItemSize)
            default:
                let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: kBarHeight)
                print(tabBarItemSize)
                
            }
        }
        print(kBarHeight)
        
        // remove default border
//        tabBar.frame.size.width = self.view.frame.width + 5
//        tabBar.frame.origin.x = -2
        
        
    }
    
    
    func setTabBarSelectedImage(normalArr: NSArray, selectedArr : NSArray)
    {
        
        for (index, _) in (self.tabBar.items?.enumerated())!
        {
            
            
            print(index)
            let tabItem2  = self.tabBar.items![index]
            tabItem2.selectedImage = UIImage(named:selectedArr.object(at: index) as! String )?.withRenderingMode(.alwaysOriginal)
            tabItem2.image=UIImage(named:normalArr.object(at: index) as! String )?.withRenderingMode(.alwaysOriginal)
            
            //tabItem2.title = titleArray[index]
            //tabItem2.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "SegoeUI-SemiBold", size: 10)!, NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .normal)
            //tabItem2.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "SegoeUI-SemiBold", size: 10)!, NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
            
            
            
        }
    }
    
}

extension UIImage
{
    func makeImageWithColorAndSize(color: UIColor, size: CGSize) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 20, y: 0, width: size.width, height: 2))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        
        return image!
    }
}

extension UIImage {
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: 5)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

@IBDesignable
class CustomizedTabBar : UITabBar {

    private var shapeLayer : CALayer?

    override func draw(_ rect: CGRect) {
        self.addShape()
    }

    private func addShape() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = createPath()
        //shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.fillColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 1.0
        shapeLayer.shadowColor = UIColor.lightGray.cgColor
        shapeLayer.shadowOffset = CGSize(width: 2, height: 2)
        
        if let oldShapeLayer = self.shapeLayer {
            self.layer.replaceSublayer(oldShapeLayer, with: shapeLayer)
        }
        else
        {
            self.layer.insertSublayer(shapeLayer, at: 0)
        }
        self.shapeLayer = shapeLayer
    }

    func createPath() -> CGPath {
        let height: CGFloat = 47.0
        let path = UIBezierPath()
        let centerWidth = self.frame.width/2

        path.move(to: CGPoint(x: 0, y: 0))

        path.addLine(to: CGPoint(x: (centerWidth - height * 2), y: 0))

        //first curve down
        path.addCurve(to: CGPoint(x: centerWidth, y: height), controlPoint1: CGPoint(x: (centerWidth - 30), y: 0), controlPoint2: CGPoint(x: (centerWidth - 35), y: height))

        //second curve down
        path.addCurve(to: CGPoint(x: (centerWidth + height * 2), y: 0), controlPoint1: CGPoint(x: (centerWidth + 35), y: height), controlPoint2: CGPoint(x: (centerWidth + 30), y: 0))

        //complete the rect
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
        path.addLine(to: CGPoint(x: 0, y: self.frame.height))
        path.close()

        return path.cgPath


    }


}
