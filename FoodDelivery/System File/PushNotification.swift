//
//  PushNotification.swift
//  E-RX
//
//  Created by SinhaAirBook on 11/07/20.
//  Copyright © 2020 macbook. All rights reserved.
//
import UIKit
import UserNotifications
import Firebase
import FirebaseAuth
import FirebaseMessaging

class PushNotification:NSObject, UNUserNotificationCenterDelegate,MessagingDelegate {
    
    
    public init(application:UIApplication) {
        
        super.init()
 
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(
        options: [.alert, .sound, .badge]) {
            [weak self] granted, error in
            print("Notification granted: \(granted)")
            guard granted else { return }
            self?.getNotificationSettings()
        }
        
        FirebaseApp.configure()

        Messaging.messaging().delegate = self
        
    }
    
    required init(coder aDecoder: NSCoder) {
           fatalError("init(coder:) has not been implemented")
       }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("User Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func userNotification(message: String, title: String, count: Int){
        //creating the notification content
        let content = UNMutableNotificationContent()
        content.title = title
        content.subtitle = ""
        content.body = message
        content.badge = count as NSNumber
        //getting the notification trigger
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        //getting the notification request
        let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: content, trigger: trigger)
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func clearNotification(){
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        let notifications =  UNUserNotificationCenter.current()
        notifications.removeAllPendingNotificationRequests()
        notifications.removeAllDeliveredNotifications()
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if let userInfo = notification.request.content.userInfo as? [String:Any] {
            if let apsDict = userInfo["aps"] as?NSDictionary {
                let badge = apsDict.value(forKey: "badge") as? Int ?? 0
                UIApplication.shared.applicationIconBadgeNumber = badge
                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
              
                completionHandler([.alert,.badge,.sound])
            }
            
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let userInfo = response.notification.request.content.userInfo as? [String:Any] {
            if let apsDict = userInfo["aps"] as?NSDictionary {
                let badge = apsDict.value(forKey: "badge") as? Int ?? 0
                UIApplication.shared.applicationIconBadgeNumber = badge
                redirectToScreen(userInfo["type"] as? String ?? "", userInfo["value"] as? String ?? "",userInfo["notificationId"] as? String ?? "")
              //  NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                completionHandler()
            }
            
        }
    }
    
    func redirectToScreen(_ screenType: String,_ id:String,_ nId:String? = nil){
    }
    

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
      print("Firebase registration token: \(fcmToken)")

      let dataDict:[String: String] = ["token": fcmToken ?? ""]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    
    }
    
}

