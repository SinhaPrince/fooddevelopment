//
//  ViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 4/7/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
class ViewModel: NSObject{
    var success = {(message :String) -> () in }
    var errorMessage = {(message : String) -> () in }
    let apiHandel = ApiManager.instance
    
    func getSignService(_ url:String,_ param:[String:Any],_ header: [String:String]? = nil){
        
        apiHandel.GetPostSessionService(param, url, .post, header) { (result) in
            
            DispatchQueue.main.async {
                Indicator.shared.hideLoaderPresent()
            }
            switch result{
           
            case .success(let data):
                let json = JSON(data)
                let resData = UserData(json)
                if resData.status == 200{
                    self.success(resData.message ?? "")
                }else{
                    self.errorMessage(resData.message ?? "")
                }
                break
            case .failure(let error):
                self.errorMessage(error.localizedDescription)
                break
            }
        }
        
    }
}

