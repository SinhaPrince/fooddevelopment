//
//  RestaurantProfileOfferRequest.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class RestaurantProfileOfferRequest: UIViewController {
    
    //MARK:- Outlets
    var navObj = UIViewController()
    
    //MARK:- Properties
    
    
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallizers()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    //MARK:- Button Action
    @IBAction func btnActionCancelRequest(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnActionLoader(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantProfileOfferConfirmation") as! RestaurantProfileOfferConfirmation
            self.navObj.navigationController?.present(vc, animated: true, completion: nil)
        })
    }
    
    
}


//MARK:- Custom Methods
extension RestaurantProfileOfferRequest {
    func initiallizers() {
        
    }
    
    
    
}




