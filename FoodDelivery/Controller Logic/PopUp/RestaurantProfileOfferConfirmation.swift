//
//  RestaurantProfileOfferConfirmation.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class RestaurantProfileOfferConfirmation: UIViewController {
    
    //MARK:- Outlets
    
    
    //MARK:- Properties
    
    
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallizers()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    //MARK:- Button Action
    @IBAction func btnActionConfirm(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}


//MARK:- Custom Methods
extension RestaurantProfileOfferConfirmation {
    func initiallizers() {
        
    }
    
    
    
}




