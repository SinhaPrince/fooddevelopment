//
//  RestaurantProfileOffer.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class RestaurantProfileOffer: UIViewController {
    
    //MARK:- Outlets
    
    
    //MARK:- Properties
    var navObj = UIViewController()
    
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallizers()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    //MARK:- Button Action
    @IBAction func btnActionYes(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantProfileOfferRequest") as! RestaurantProfileOfferRequest
            vc.navObj = self.navObj
            self.navObj.navigationController?.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func btnActionNo(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}


//MARK:- Custom Methods
extension RestaurantProfileOffer {
    func initiallizers() {
        
    }
    
    
    
}



