//
//  RunningOrderTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 08/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class RunningOrderTableCell: UITableViewCell {

    //MARK:- Outlets
    
    @IBOutlet weak var imgRestaurantLogo: UIImageView!
    @IBOutlet weak var imgItemLogo: UIImageView!
    @IBOutlet weak var lblItemID: UILabel!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var lblItemQty: UILabel!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    @IBOutlet weak var btnReOrder: UIButton!
    @IBOutlet weak var btnRatings: UIButton!
    
    var item:MyOrderData?{
        didSet{
            guard let data = item else {
                return
            }
            imgRestaurantLogo.isHidden = data.isPast
            lblItemID.isHidden = data.isPast
            lblItemName.setup(data.itemName, AppColor.appLightBlack, AppFont.Regular.size(.Poppins, size: 25), .center)
            lblItemPrice.setup("AED \(data.productAmount)", AppColor.appLightBlack, AppFont.SemiBold.size(.Poppins, size: 22), .center)
            lblItemID.setup(data.orderNumber, AppColor.appLightBlack, AppFont.Medium.size(.Poppins, size: 12), .right)
            lblItemQty.setup("Items: \(data.itemQuantity)", AppColor.appLightGray, AppFont.Regular.size(.Poppins, size: 15), .left)
            lblDeliveryTime.setup(data.itemStatus, AppColor.appLightGray, AppFont.Regular.size(.Poppins, size: 15), .right)
            imgRestaurantLogo.image = data.brandLogo
            imgItemLogo.image = data.itemLogo
            btnReOrder.setTitle(data.isPast ? "Re-Order" : "Track-Order", for: .normal)
            btnRatings.setTitle(data.isPast ? "Rating" : "Cancel", for: .normal)

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

struct MyOrderData {
    var brandLogo:UIImage
    var itemLogo:UIImage
    var itemName:String
    var orderNumber:String
    var productAmount:String
    var itemQuantity:String
    var itemStatus:String
    var isPast:Bool
}

extension UILabel{
    func setup(_ title:String,_ color:UIColor,_ font:UIFont,_ aligment:NSTextAlignment){
        self.text = title
        self.textColor = color
        self.textAlignment = aligment
        self.font = font
    }
}
