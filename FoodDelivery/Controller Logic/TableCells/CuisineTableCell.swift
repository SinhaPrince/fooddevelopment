//
//  CuisineTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 22/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class CuisineTableCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var item: CuisineArrayEn?{
        didSet{
            lblTitle.text = item?.itemText
            imgCheck.isHidden = !(item?.isSelected ?? false)
            if item?.isSelected ?? false{
                vwOuter.layer.borderColor = AppColor.pink.cgColor
            }else{
                vwOuter.layer.borderColor = AppColor.appLightGray.cgColor
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
