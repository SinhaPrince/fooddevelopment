//
//  CardTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 09/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class CardTableCell: UITableViewCell {

    //MARK:- Outlers
    
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var lblCardName: UILabel!
    @IBOutlet weak var lblCardNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
