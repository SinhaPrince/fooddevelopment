//
//  SideMenuTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class SideMenuTableCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var imgRightArrow: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnSwitch: UISwitch!
    @IBOutlet weak var imageMenu: UIImageView!
    var item: SideMenuData?{
        didSet{
            guard let data = item else {return}
            imageMenu.image = data.icon
            lblTitle.setup(data.title, AppColor.appLightBlack, AppFont.Regular.size(.Poppins, size: 14), .left)
            lblSubtitle.setup(data.subTitle, AppColor.appLightGray, AppFont.Regular.size(.Poppins, size: 12), .left)
            imgRightArrow.isHidden = true
            
            if data.title.contains("Push"){
                btnSwitch.isHidden = false
            }else{
                btnSwitch.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
