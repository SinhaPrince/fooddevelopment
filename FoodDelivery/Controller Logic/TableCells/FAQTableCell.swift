//
//  FAQTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class FAQTableCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var item:FaQData?{
        didSet{
            lblTitle.text = item?.answer
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
