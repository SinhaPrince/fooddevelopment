//
//  LoyaltySecondTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 09/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class LoyaltySecondTableCell: UITableViewCell {
    //MARK:- Outlets
    
    @IBOutlet weak var imgDiscount: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblSubtitle: UILabel!
    
    @IBOutlet weak var lblHeading: UILabel!
    
    @IBOutlet weak var btnReward: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
