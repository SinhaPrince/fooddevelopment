//
//  PaymentTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 04/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class PaymentTableCell: UITableViewCell {

    //MARK:- Outlets
    
    @IBOutlet weak var outerVw: UIView!
    @IBOutlet weak var imgPayments: UIImageView!
    @IBOutlet weak var lblPayments: UILabel!
    
    @IBOutlet weak var imgRadio: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
