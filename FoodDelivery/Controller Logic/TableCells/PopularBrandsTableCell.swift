//
//  PopularBrandsTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 22/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class PopularBrandsTableCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var imgBrands: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
