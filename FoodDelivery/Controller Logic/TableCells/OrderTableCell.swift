//
//  OrderTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 26/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class OrderTableCell: UITableViewCell {

    //MARK:- Outlets
    
    @IBOutlet weak var lblItem: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
