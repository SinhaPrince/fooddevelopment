//
//  FavouritesTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import SkeletonView
class FavouritesTableCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var btnFavt: UIButton!
    @IBOutlet weak var imageRest: UIImageView!
    @IBOutlet weak var lblRestName: UILabel!
    @IBOutlet weak var btnRating: UIButton!
    @IBOutlet weak var lblCusineName: UILabel!
    @IBOutlet weak var lblMinOrderAmount: UILabel!
    @IBOutlet weak var btnDeliveryFee: UIButton!
    @IBOutlet weak var btnTime: UIButton!
    @IBOutlet weak var btnTracking: UIButton!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var viewClose: UIView!
    @IBOutlet weak var lblClose: UILabel!
    
    var item:RestaurantData?{
        didSet{
            guard let data = item else {
                return
            }
            btnFavt.isHidden = false

            btnStatus.isHidden = true
            if data.oepnStatus ?? false{
                btnStatus.setTitle("Open", for: .normal)
                btnStatus.setImage(UIImage(named: "open"), for: .normal)
                btnStatus.tintColor = .green
                viewClose.isHidden = true
                lblClose.isHidden = true
            }else if (data.closeStatus ?? false){
                viewClose.isHidden = false
                lblClose.isHidden = false
                lblClose.setup("Restaurant Closed", .white, AppFont.Bold.size(.Poppins, size: 18), .center)
                btnStatus.setTitle("Closed", for: .normal)
                btnStatus.setImage(UIImage(named: "open"), for: .normal)
                btnStatus.tintColor = .red
            }else{
                viewClose.isHidden = false
                lblClose.isHidden = false
                lblClose.setup("Restaurant Busy", .white, AppFont.Bold.size(.Poppins, size: 18), .center)
                btnStatus.setTitle("Busy", for: .normal)
                btnStatus.setImage(UIImage(named: "open"), for: .normal)
                btnStatus.tintColor = .orange
            }
            btnRating.setImage(#imageLiteral(resourceName: "star"), for: .normal)
            imageRest.sd_setImage(with: URL(string: data.image ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"), options: .lowPriority, context: [:])
            btnFavt.setImage(((data.isFav ?? false) ? #imageLiteral(resourceName: "heart") : #imageLiteral(resourceName: "heart_g")), for: .normal)
            lblRestName.setup(data.branchNameEn ?? "", AppColor.appLightBlack, AppFont.Medium.size(.Poppins, size: 20), .left)
            //btnRating.isHidden = item?.avgRating ?? 0 > 0 ? false : true
            btnRating.setTitle(item?.avgRating?.description ?? "", for: .normal)
            btnRating.titleLabel?.font = AppFont.Medium.size(.Poppins, size: 11)
            btnTracking.setImage(#imageLiteral(resourceName: "tracking"), for: .normal)
            btnDeliveryFee.setImage(#imageLiteral(resourceName: "scooter"), for: .normal)

            if item?.serviceType == "Market Place"{
                btnTracking.isHidden = true
            }else{
                btnTracking.isHidden = false
            }
            
            var cusineName = ""
            data.brandData?.cuisineArrayEn?.forEach({item in
                cusineName.append(item.itemText ?? "")
                cusineName.append(", ")
            })
            cusineName.removeLast()
            cusineName.removeLast()
            lblCusineName.setup(cusineName, AppColor.appLightGray, AppFont.Medium.size(.Poppins, size: 12), .left)
            btnTime.setTitle("\(item?.areaData?.deliveryTime ?? 0) Mins", for: .normal)
            btnTracking.setTitle("Live Tracking", for: .normal)
            lblMinOrderAmount.provideAttributedTextToControlLeftAllignCenter("AED \(data.areaData?.minimumOrderValue ?? 0)", " Min Order", AppFont.Medium.size(.Poppins, size: 10), AppFont.Medium.size(.Poppins, size: 10), AppColor.pink, AppColor.appLightGray, .left)
            btnDeliveryFee.provideAttributedTextToControlLeftAllignCenter("AED \(data.areaData?.deliveryFee ?? 0)", " Delivery fee", AppFont.Medium.size(.Poppins, size: 10), AppFont.Medium.size(.Poppins, size: 10), AppColor.appLightBlack, AppColor.appLightGray,.right)
            
            self.hideSkeletonfromView([vwOuter,btnTime,btnRating,btnFavt,btnStatus,btnTracking,btnDeliveryFee,lblClose,lblRestName,lblCusineName,lblMinOrderAmount,vwOuter,contentView])
        }
        
        
    }
    
    var favtItem:FavoriteData?{
        didSet{
            guard let data = favtItem else {
                return
            }
            btnFavt.isHidden = false
            btnStatus.isHidden = true
            if data.restaurantData?.oepnStatus ?? false{
                btnStatus.setTitle("Open", for: .normal)
                btnStatus.setImage(UIImage(named: "open"), for: .normal)
                btnStatus.tintColor = .green
                viewClose.isHidden = true
                lblClose.isHidden = true
            }else if !(data.restaurantData?.oepnStatus ?? false){
                viewClose.isHidden = false
                lblClose.isHidden = false
                lblClose.setup("Restaurant Close", .white, AppFont.Bold.size(.Poppins, size: 18), .center)
                btnStatus.setTitle("Close", for: .normal)
                btnStatus.setImage(UIImage(named: "open"), for: .normal)
                btnStatus.tintColor = .red
            }else{
                viewClose.isHidden = false
                lblClose.isHidden = false
                lblClose.setup("Restaurant Busy", .white, AppFont.Bold.size(.Poppins, size: 18), .center)
                btnStatus.setTitle("Busy", for: .normal)
                btnStatus.setImage(UIImage(named: "open"), for: .normal)
                btnStatus.tintColor = .orange
            }
            if item?.serviceType == "Market Place"{
                btnTracking.isHidden = true
            }else{
                btnTracking.isHidden = false
            }
            imageRest.sd_setImage(with: URL(string: data.restaurantData?.image ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"), options: .lowPriority, context: [:])

            lblRestName.setup(data.restaurantData?.branchNameEn ?? "", AppColor.appLightBlack, AppFont.Medium.size(.Poppins, size: 20), .left)
            btnRating.setImage(#imageLiteral(resourceName: "star"), for: .normal)
           // btnRating.isHidden = item?.avgRating ?? 0 > 0 ? false : true
            btnRating.setTitle(data.restaurantData?.avgRating?.description ?? "", for: .normal)
            btnRating.titleLabel?.font = AppFont.Medium.size(.Poppins, size: 11)
            var cusineName = ""
            data.brandData?.cuisineArrayEn?.forEach({item in
                cusineName.append(item.itemText ?? "")
                cusineName.append(", ")
            })
            cusineName.removeLast()
            cusineName.removeLast()
            lblCusineName.setup(cusineName, AppColor.appLightGray, AppFont.Medium.size(.Poppins, size: 12), .left)
            btnTime.setTitle("\(data.areaData?.deliveryTime ?? 0) Mins", for: .normal)
            btnTracking.setTitle("Live Tracking", for: .normal)
            btnTracking.setImage(#imageLiteral(resourceName: "tracking"), for: .normal)

            lblMinOrderAmount.provideAttributedTextToControlLeftAllignCenter("AED \(data.areaData?.minimumOrderValue ?? 0)", " Min Order", AppFont.Medium.size(.Poppins, size: 10), AppFont.Medium.size(.Poppins, size: 10), AppColor.pink, AppColor.appLightGray, .left)
            btnDeliveryFee.provideAttributedTextToControlLeftAllignCenter("AED \(data.areaData?.deliveryFee ?? 0)", " Delivery fee", AppFont.Medium.size(.Poppins, size: 10), AppFont.Medium.size(.Poppins, size: 10), AppColor.appLightBlack, AppColor.appLightGray,.right)
            btnDeliveryFee.setImage(#imageLiteral(resourceName: "scooter"), for: .normal)
            self.hideSkeletonfromView([vwOuter,btnTime,btnRating,btnFavt,btnStatus,btnTracking,btnDeliveryFee,lblClose,lblRestName,lblCusineName,lblMinOrderAmount,vwOuter,contentView])

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.vwOuter.layer.cornerRadius = 23.0
        self.vwOuter.layer.shadowColor = UIColor.darkGray.cgColor
        self.vwOuter.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        self.vwOuter.layer.shadowRadius = 6.0
        self.vwOuter.layer.shadowOpacity = 0.2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func hideSkeletonfromView(_ element:[UIView]){
        element.forEach({item in
            item.stopSkeletonAnimation()
            item.hideSkeleton()
        })
        
    }
    
}

extension UILabel{
    open func provideAttributedTextToControlLeftAllignCenter(_ title:String, _ subtitle:String,_ titleFont:UIFont,_ subtitleFont:UIFont,_ titleColor:UIColor,_ subtitleColor:UIColor,_ alignment: NSTextAlignment? = .center){
        let myMutableString = NSMutableAttributedString()
        
        
        let myMutableString1 = NSAttributedString(string: "\(title)", attributes:[ .foregroundColor :titleColor,.font:titleFont])
        
        let myMutableString2 = NSAttributedString(string: "\(subtitle)", attributes:[.foregroundColor :subtitleColor,.font:subtitleFont])
        
        myMutableString.append(myMutableString1)
        myMutableString.append(myMutableString2)
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 0
        //paragraphStyle.paragraphSpacing = 0// Whatever line spacing you want in points
        paragraphStyle.alignment = alignment ?? .center
        
        // *** Apply attribute to string ***
        myMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, myMutableString.length))
        self.attributedText = myMutableString
    }
}
extension UIButton{
    open func provideAttributedTextToControlLeftAllignCenter(_ title:String, _ subtitle:String,_ titleFont:UIFont,_ subtitleFont:UIFont,_ titleColor:UIColor,_ subtitleColor:UIColor,_ alignment: NSTextAlignment? = .center){
        let myMutableString = NSMutableAttributedString()
        
        
        let myMutableString1 = NSAttributedString(string: "\(title)", attributes:[ .foregroundColor :titleColor,.font:titleFont])
        
        let myMutableString2 = NSAttributedString(string: "\(subtitle)", attributes:[.foregroundColor :subtitleColor,.font:subtitleFont])
        
        myMutableString.append(myMutableString1)
        myMutableString.append(myMutableString2)
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 0
        //paragraphStyle.paragraphSpacing = 0// Whatever line spacing you want in points
        paragraphStyle.alignment = alignment ?? .center
        
        // *** Apply attribute to string ***
        myMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, myMutableString.length))
        self.setAttributedTitle(myMutableString, for: .normal)
    }
}
