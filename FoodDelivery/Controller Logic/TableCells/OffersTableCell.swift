//
//  OffersTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 02/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class OffersTableCell: UITableViewCell {

    //MARK:- Outlets
    
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
