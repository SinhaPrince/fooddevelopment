//
//  RestaurantDetailTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 02/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class RestaurantDetailTableCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var btnItems: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnItems.cornerRadius = 8
        btnItems.layer.maskedCorners = [.layerMinXMinYCorner,.layerMinXMaxYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
