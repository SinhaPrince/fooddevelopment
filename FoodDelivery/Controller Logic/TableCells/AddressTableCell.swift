//
//  AddressTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class AddressTableCell: UITableViewCell {

    //MARK:- Outlets
    
    @IBOutlet weak var imgRadio: UIImageView!
    @IBOutlet weak var lblAddressType: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    var item: AddressData?{
        didSet{
            imgRadio.image = item?.defaultStatus ?? false ? #imageLiteral(resourceName: "radio_s"):#imageLiteral(resourceName: "radio_un")
            lblAddress.text = item?.address
            lblAddressType.text = item?.addressType
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
