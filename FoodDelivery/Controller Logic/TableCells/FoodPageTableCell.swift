//
//  FoodPageTableCell.swift
//  FoodDelivery
//
//  Created by call soft on 03/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class FoodPageTableCell: UITableViewCell {

    //MARK:- Outlets
    
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var imgRadio: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwBottom: UIView!
    
    var item:UpdateBaseMenuData?{
        didSet{
            lblTitle.text = item?.category ?? ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
