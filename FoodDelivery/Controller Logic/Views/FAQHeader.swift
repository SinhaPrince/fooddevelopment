//
//  FAQHeader.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class FAQHeader: UITableViewHeaderFooterView {

    //MARK:- Outlets
    
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnArrow: UIButton!
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
