//
//  RangeSliderTrackLayer.swift
//  FoodDelivery
//
//  Created by call soft on 02/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

class RangeSliderTrackLayer: CALayer {
 weak var rangeSlider: RangeSlider?

 override func draw(in ctx: CGContext) {
   if let slider = rangeSlider {
     // Clip
     let cornerRadius = bounds.height * slider.curvaceousness / 2.0
     let path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
     ctx.addPath(path.cgPath)

     // Fill the track
     ctx.setFillColor(slider.trackTintColor.cgColor)
     ctx.addPath(path.cgPath)
     ctx.fillPath()

     // Fill the highlighted range
     ctx.setFillColor(slider.trackHighlightTintColor.cgColor)
     let lowerValuePosition =  CGFloat(slider.positionForValue(value: slider.lowerValue))
     let upperValuePosition = CGFloat(slider.positionForValue(value: slider.upperValue))
     let rect = CGRect(x: lowerValuePosition, y: 0.0, width: upperValuePosition - lowerValuePosition, height: bounds.height)
   ctx.fill(rect)
  }
 }
}
