//
//  RatingCollectionCell.swift
//  FoodDelivery
//
//  Created by call soft on 02/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class RatingCollectionCell: UICollectionViewCell {

    //MARK:- Outlets
    @IBOutlet weak var imgRatings: UIImageView!
    @IBOutlet weak var lblRatings: UILabel!
    @IBOutlet weak var vwOuter: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
