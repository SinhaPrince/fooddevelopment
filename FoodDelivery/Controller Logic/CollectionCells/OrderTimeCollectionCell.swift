//
//  OrderTimeCollectionCell.swift
//  FoodDelivery
//
//  Created by call soft on 22/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class OrderTimeCollectionCell: UICollectionViewCell {

    //MARK:- Outlets
    @IBOutlet weak var lblRatings: UILabel!
    @IBOutlet weak var vwOuter: UIView!
    
    var item: CategoryData?{
        didSet{
            
            if item?.isSelected ?? false{
                lblRatings.text = item?.name
                lblRatings.textColor = .white
                vwOuter.layer.borderColor = UIColor.white.cgColor
                vwOuter.layer.borderWidth = 1
                vwOuter.backgroundColor = AppColor.pink
            }else{
                lblRatings.text = item?.name
                lblRatings.textColor = AppColor.appLightBlack
                vwOuter.layer.borderColor = UIColor.darkGray.cgColor
                vwOuter.layer.borderWidth = 1
                vwOuter.backgroundColor = AppColor.white
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
