//
//  RestaurantInfoCollectionCell.swift
//  FoodDelivery
//
//  Created by call soft on 26/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class RestaurantInfoCollectionCell: UICollectionViewCell {

    //MARK:- Outlets
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var lblItem: UILabel!
    
    var item:CuisineArrayEn?{
        didSet{
            lblItem.text = item?.itemText ?? ""
        }
    }
    var itemData: SearchTypeCollectionData?{
        didSet{
            vwOuter.cornerRadius = 15
            vwOuter.layer.shadowColor = UIColor.darkGray.cgColor
           vwOuter.layer.shadowOffset = CGSize(width: 1, height: 2)
            vwOuter.layer.shadowRadius = 6
            vwOuter.layer.shadowOpacity = 0.2
            if itemData?.isSelected ?? false{
                vwOuter.backgroundColor = AppColor.pink
                lblItem.attributedText = GlobalMethods.shared.provideAttributedTextToControlLeftAllignSingleCenter(itemData?.title ?? "", AppFont.Medium.size(.Poppins, size: 14), AppColor.white)
            }else{
                vwOuter.backgroundColor = AppColor.white
                lblItem.attributedText = GlobalMethods.shared.provideAttributedTextToControlLeftAllignSingleCenter(itemData?.title ?? "", AppFont.Medium.size(.Poppins, size: 14), AppColor.lightGray)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
