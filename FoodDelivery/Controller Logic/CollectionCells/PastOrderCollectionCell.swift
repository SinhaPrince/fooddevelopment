//
//  PastOrderCollectionCell.swift
//  FoodDelivery
//
//  Created by call soft on 24/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class PastOrderCollectionCell: UICollectionViewCell {

    //MARK:- Outlets
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var btnReOrder: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        vwOuter.cornerRadius = 23
        
        
    }

}
