//
//  BrandsCollectionCell.swift
//  FoodDelivery
//
//  Created by call soft on 24/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class BrandsCollectionCell: UICollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var imgBrands: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //vwOuter.cornerRadius = vwOuter.frame.width / 5
    }

}
