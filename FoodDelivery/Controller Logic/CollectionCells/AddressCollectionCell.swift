//
//  AddressCollectionCell.swift
//  FoodDelivery
//
//  Created by call soft on 04/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class AddressCollectionCell: UICollectionViewCell {

    //MARK:- Outlets
    
    @IBOutlet weak var imgRadio: UIImageView!
    
    @IBOutlet weak var lblAddressType: UILabel!
    
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
