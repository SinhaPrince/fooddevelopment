//
//  MyOrderVC.swift
//  FoodDelivery
//
//  Created by call soft on 08/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class MyOrderVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblVw: UITableView!
    
    @IBOutlet weak var btnRunning: UIButton!
    @IBOutlet weak var btnPast: UIButton!
    
    
    //MARK:- Properties
    var orderRunningNames = ["Whipping cream","Stir-Fried Spicy"]
    var orderRunningPrice = ["AED 03.99","AED 09.99"]
    var orderRunningImg = [UIImage.init(named: "whip.jpg"),UIImage.init(named: "stirfriedspicy.jpg")]
    var orderPastImg = [UIImage.init(named: "tortilla.jpg"),UIImage.init(named: "friedshrimps.jpg")]
    var orderPastNames = ["Tortilla Wrap","Fried Shrimps"]
    var orderPastPrice = ["AED 18.99","AED 29.00"]
    var orderType = "running"
    
    let myOrderVM = MyOrderViewModel()
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        self.myOrderVM.controller = self
        setupNavigation()
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "My Order")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .left
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
    }

    
    //MARK:- Button Action
  
    @IBAction func btnActionOrder(_ sender: UIButton) {
        
        orderType = sender.tag == 1 ? "running" : "past"
        initiallizers()
  
    }
    func giveShadowToButton(btn:UIButton,shadowColor:UIColor,textColor:UIColor,backgroundColor:UIColor) {
        btn.setTitleColor(textColor, for: .normal)
        btn.backgroundColor = backgroundColor
        btn.shadowColor = shadowColor
        btn.shadowOffset = CGSize(width: 0, height: 8)
        btn.shadowOpacity = 1
        btn.shadowRadius = 5
    }
    
    
}

//MARK:- Custom Methods
extension MyOrderVC {
    func initiallizers() {
        if orderType == "running" {
            giveShadowToButton(btn: btnRunning, shadowColor: AppColor.lightpink, textColor: AppColor.white, backgroundColor: AppColor.pink)
            giveShadowToButton(btn: btnPast, shadowColor: AppColor.primaryColor, textColor: AppColor.labelColor, backgroundColor: AppColor.white)
            myOrderVM.OrderData.removeAll()
            myOrderVM.OrderData.append(contentsOf: [MyOrderData(brandLogo: #imageLiteral(resourceName: "burger"), itemLogo: #imageLiteral(resourceName: "tortilla"), itemName: "Whipping cream", orderNumber: "#1154", productAmount: "03.99", itemQuantity: "3", itemStatus: "Delivery time: 20 Min", isPast: false),MyOrderData(brandLogo: #imageLiteral(resourceName: "pizzahut"), itemLogo: #imageLiteral(resourceName: "stirfriedspicy"), itemName: "Stir-Fried Spicy", orderNumber: "#1155", productAmount: "03.99", itemQuantity: "2", itemStatus: "Delivery time: 40 Min", isPast: false),MyOrderData(brandLogo: #imageLiteral(resourceName: "burger"), itemLogo: #imageLiteral(resourceName: "tortilla"), itemName: "Whipping cream", orderNumber: "#1154", productAmount: "03.99", itemQuantity: "3", itemStatus: "Delivery time: 20 Min", isPast: false),MyOrderData(brandLogo: #imageLiteral(resourceName: "pizzahut"), itemLogo: #imageLiteral(resourceName: "stirfriedspicy"), itemName: "Stir-Fried Spicy", orderNumber: "#1155", productAmount: "03.99", itemQuantity: "2", itemStatus: "Delivery time: 40 Min", isPast: false),MyOrderData(brandLogo: #imageLiteral(resourceName: "burger"), itemLogo: #imageLiteral(resourceName: "tortilla"), itemName: "Whipping cream", orderNumber: "#1154", productAmount: "03.99", itemQuantity: "3", itemStatus: "Delivery time: 20 Min", isPast: false),MyOrderData(brandLogo: #imageLiteral(resourceName: "pizzahut"), itemLogo: #imageLiteral(resourceName: "stirfriedspicy"), itemName: "Stir-Fried Spicy", orderNumber: "#1155", productAmount: "03.99", itemQuantity: "2", itemStatus: "Delivery time: 40 Min", isPast: false)])
        }
        else
        {
           giveShadowToButton(btn: btnRunning, shadowColor: AppColor.primaryColor, textColor: AppColor.labelColor, backgroundColor: AppColor.white)
            giveShadowToButton(btn: btnPast, shadowColor: AppColor.lightpink, textColor: AppColor.white, backgroundColor: AppColor.pink)
            myOrderVM.OrderData.removeAll()
            myOrderVM.OrderData.append(contentsOf: [MyOrderData(brandLogo: #imageLiteral(resourceName: "burger"), itemLogo: #imageLiteral(resourceName: "burger"), itemName: "BURGUR KING", orderNumber: "#1154", productAmount: "03.99", itemQuantity: "3", itemStatus: "Delivered", isPast: true),MyOrderData(brandLogo: #imageLiteral(resourceName: "pizzahut"), itemLogo: #imageLiteral(resourceName: "pizzahut"), itemName: "PIZZA HUT", orderNumber: "#1155", productAmount: "03.99", itemQuantity: "2", itemStatus: "Delivered", isPast: true),MyOrderData(brandLogo: #imageLiteral(resourceName: "burger"), itemLogo: #imageLiteral(resourceName: "burger"), itemName: "BURGUR KING", orderNumber: "#1154", productAmount: "03.99", itemQuantity: "3", itemStatus: "Delivered", isPast: true),MyOrderData(brandLogo: #imageLiteral(resourceName: "pizzahut"), itemLogo: #imageLiteral(resourceName: "pizzahut"), itemName: "PIZZA HUT", orderNumber: "#1155", productAmount: "03.99", itemQuantity: "2", itemStatus: "Delivered", isPast: true)])
        }
        configureTable()
    }
    
    
    
}
//MARK:- UITableViewDelegate and UITableViewDatasource
extension MyOrderVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.myOrderVM.OrderData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.myOrderVM.cellInstance(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.myOrderVM.rowHeight
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.myOrderVM.rowHeight
    }
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
         vc.isFrom = orderType
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
   
    
}
