//
//  MyOrderViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 4/25/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class MyOrderViewModel:BaseViewModel,CellRepresentable{
    
    
    var OrderData = [MyOrderData]()
    var isPast = Bool()
    
    var rowHeight: CGFloat = UITableView.automaticDimension
    var controller = UIViewController()
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "RunningOrderTableCell", bundle: nil), forCellReuseIdentifier: "RunningOrderTableCell")
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RunningOrderTableCell") as? RunningOrderTableCell else {
            return UITableViewCell()
        }
        cell.item = OrderData[indexPath.row]
        cell.btnReOrder.tag = indexPath.row
        cell.btnReOrder.addTarget(self, action: #selector(TrackOrder), for: .touchUpInside)
        cell.btnRatings.tag = indexPath.row
        cell.btnRatings.addTarget(self, action: #selector(CancelOrder), for: .touchUpInside)
        return cell
    }
    
    @objc func TrackOrder(_sender:UIButton) {
        if !OrderData[_sender.tag].isPast {
           print("Track Order")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OrderTrackingVC") as! OrderTrackingVC
             vc.isFrom = "running"
            controller.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            print("Re-Order")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            controller.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    @objc func CancelOrder(_sender:UIButton) {
        if !OrderData[_sender.tag].isPast {
            print("Cancel")
        }
        else
        {
            print("Ratings")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FoodReviewVC") as! FoodReviewVC
            controller.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
