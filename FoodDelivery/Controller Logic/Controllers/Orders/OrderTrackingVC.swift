//
//  OrderTrackingVC.swift
//  FoodDelivery
//
//  Created by call soft on 08/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Foundation

class OrderTrackingVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnDelAddress: UIButton!
    @IBOutlet weak var btnDelTime: UIButton!
    @IBOutlet weak var vwBottom: UIView!
    
    //MARK:- Properties
    var isFrom = String()
    var fromLat = Double()
    var fromLong = Double()
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        vwBottom.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        let transparentButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        transparentButton.setImage(#imageLiteral(resourceName: "back_side"), for: .normal)
        transparentButton.borderWidth = 0.7
        transparentButton.borderColor = .white
        transparentButton.backgroundColor = .clear
        transparentButton.contentHorizontalAlignment = .center
        transparentButton.tag = UINavigationBarButtonType.backHomeNAll.rawValue
        transparentButton.addTarget(self, action: #selector(self.navigationButtonTapped(_:)), for: .touchUpInside)
        transparentButton.cornerRadius = 8
        let title = UILabel()
        title.text = comClass.createString(Str: "Order Tracking")
        title.textColor = AppColor.textcolor
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .left
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: transparentButton)
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
    }
    
    //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionMoreDetails(_ sender: UIButton) {
        if isFrom == "running" || isFrom == "past" {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
            vc.isFrom = isFrom
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    @IBAction func btnActionCall(_ sender: UIButton) {
        print("Call")
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}





