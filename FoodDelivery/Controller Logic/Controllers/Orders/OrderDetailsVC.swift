//
//  OrderDetailsVC.swift
//  FoodDelivery
//
//  Created by call soft on 26/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class OrderDetailsVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var btnTrack: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var tblVw: UITableView!
    
    @IBOutlet weak var tblVwHeight: NSLayoutConstraint!
    //MARK:- Properties
    var isFrom = String()
    var tblData = ["Whipping Cream","Tortilla Wrap","Fried Shrimps","Whipping Cream","Whipping Cream"]
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
            initiallizers()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        setupNavigation()
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])

        let title = UILabel()
        title.text = comClass.createString(Str: "Order Details")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .left
        
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
       
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tblVwHeight.constant = tblVw.contentSize.height
    }
    
   //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        }
    @IBAction func btnActionTrackOrder(_ sender: UIButton) {
       
        if isFrom == "running" {
            
             print("Track Order")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OrderTrackingVC") as! OrderTrackingVC
            vc.isFrom = isFrom
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
           
             print("Re-Order")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }

        
    }
    @IBAction func btnActionCancel(_ sender: UIButton) {
        
        if isFrom == "running" {
            
            print("Cancel Order")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
           
            print("Ratings")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FoodReviewVC") as! FoodReviewVC
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    
    
}

//MARK:- Custom Methods
extension OrderDetailsVC {
    func initiallizers() {
        configureTable()
        if isFrom == "running" {
            
            btnTrack.setTitle("Track Order", for: .normal)
            btnCancel.setTitle("Cancel", for: .normal)
        }
        else
        {
           
            btnTrack.setTitle("Re-Order", for: .normal)
            btnCancel.setTitle("Ratings", for: .normal)
        }
    }
}


//MARK:- UITableViewDelegate and UITableViewDatasource
extension OrderDetailsVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : OrderTableCell = tblVw.dequeueReusableCell(withIdentifier: "OrderTableCell") as! OrderTableCell
        cell.lblItem.text = tblData[indexPath.row]
            return cell
        
           
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return vwHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 65
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.register(UINib.init(nibName: "OrderTableCell", bundle: nil), forCellReuseIdentifier: "OrderTableCell")
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
