//
//  OrderConfirmationVC.swift
//  FoodDelivery
//
//  Created by call soft on 04/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class OrderConfirmationVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblRider: UILabel!
    @IBOutlet weak var lblSendOrder: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var btnCancelOrder: UIButton!
    //MARK:- Properties
    var controller = UIViewController()
    var count = 5
    var timer = Timer()
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallizers()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        btnCancelOrder.addTarget(self, action: #selector(btnCancelOrderTap), for: .touchUpInside)
    }
    @objc func update() {
        if(count > 0) {
            count = count - 1
            lblTimer.text = "\(count.description)sec"
        }else{
            timer.invalidate()
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OnItsWayVC") as! OnItsWayVC
               self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "Order Confirmation")
        title.textColor = AppColor.black

        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .right
        title.numberOfLines = 0
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
    }
   
    //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionOrderTracking(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OnItsWayVC") as! OnItsWayVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionTime(_ sender: UIButton) {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantDetailVC") as! RestaurantDetailVC
//        vc.itemAdded = "2"
//        self.navigationController?.pushViewController(vc, animated: true)
        kAppDelegate.appNavigator = ApplicationNavigator(window: kAppDelegate.window, sendViewController: "HomeVC")
    }
    
    @objc func btnCancelOrderTap(){
        kAppDelegate.appNavigator = ApplicationNavigator(window: kAppDelegate.window, sendViewController: "HomeVC")
    }
    
}


//MARK:- Custom Methods
extension OrderConfirmationVC {
    func initiallizers() {
        lblSendOrder.attributedText = GlobalMethods.shared.provideAttributedTextToControlLeftAllignCenter("Send your order to", "Burger King", AppFont.Regular.size(.Poppins, size: 18), AppFont.SemiBold.size(.Poppins, size: 22), AppColor.textcolor, AppColor.textcolor, "\n")
        lblRider.attributedText = GlobalMethods.shared.provideAttributedTextToControlLeftAllign("FoodApp rider", "Finding an available rider", AppFont.Regular.size(.Poppins, size: 18), AppFont.Regular.size(.Poppins, size: 14), AppColor.textcolor, AppColor.defaultColor, "\n")
    }
    
    
    
}
