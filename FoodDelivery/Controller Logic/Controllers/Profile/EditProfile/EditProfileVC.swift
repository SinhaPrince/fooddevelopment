//
//  EditProfileVC.swift
//  FoodDelivery
//
//  Created by call soft on 08/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import Foundation
import DropDown
import ADCountryPicker

protocol GetDefaultAddressBack {
    func selectedAddress(_ name:String)
}

class EditProfileVC: ParentViewController, BaseViewControllerDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var txtFirstName: UITextField!
    
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var txtAddress: UITextField!
    
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var txtDOB: UITextField!
    
    @IBOutlet weak var lblNationality: UILabel!
    @IBOutlet weak var txtNationality: UITextField!
    
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var txtGender: UITextField!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var btnCountryCode: UIButton!
    
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    //MARK:- Properties
    var imagePicker = UIImagePickerController()
    var imageData = Data()
    var dropDown = DropDown()
    var index = Int()
    var arrLanguage = ["English","Arabic"]
    var picker = ADCountryPicker()
    let titleRight = UILabel()
    let Title = UILabel()
    let validation = Validation()
    var addressData:AddressListModel?
    var imageURL: String?
   
    
    let aws = AWSS3Manager.shared
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
        setupNavigation()
        aws.initializeS3()
        txtFirstName.maxLength = 20
        txtLastName.maxLength = 20
        setDataOnScreen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.baseDelegate = self
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.isEditing = true
        imgProfile.isHidden = false
        
    }
    
    //MARK:-Setup Navigation
    func setupNavigation(){
        transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [.language])
        Title.text = comClass.createString(Str: "Profile")
        Title.textColor = AppColor.textcolor
        Title.font = AppFont.Medium.size(.Poppins, size: 16)
        Title.textAlignment = .right
        Title.numberOfLines = 0
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: Title))
        titleRight.text = comClass.createString(Str: "English")
        titleRight.textColor = AppColor.textcolor
        titleRight.font = AppFont.Medium.size(.Poppins, size: 14)
        titleRight.textAlignment = .right
        titleRight.numberOfLines = 0
        self.navigationItem.rightBarButtonItems?.append(UIBarButtonItem(customView: titleRight))
    }
    func navigationBarButtonDidTapped(_ buttonType: UINavigationBarButtonType) {
        if buttonType == .language{
            dropDown.dataSource = arrLanguage
            dropDown.anchorView = titleRight
            dropDown.bottomOffset = CGPoint(x: 0, y:titleRight.bounds.height)
            dropDown.backgroundColor = UIColor.white
            dropDown.show()
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.titleRight.text = self.dropDown.selectedItem
                self.index = index
            }
            
        }
    }
    
    //MARK:- Button Action
    
    @IBAction func btnActionSelectDate(_ sender: UIButton) {

        openDatePicker("Date", "")
        
    }
    
    @IBAction func btnActionCamera(_ sender: UIButton) {
        openActionSheet()
    }
    @IBAction func btnActionAddressList(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressVC
        vc.comingFrom = "editprofile"
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionSave(_ sender: UIButton) {
        self.checkValidation()
    }
    @IBAction func btnActionCountryCode(_ sender: UIButton) {
        picker.delegate = self
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)
        
    }
    func openDatePicker(_ type:String,_ interval:String){
        
        let size:CGFloat = type == "Date" ? 390 : 280
     // ...........BaseView Setup..............//
        var baseview: UIView!
        baseview = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height-size, width: self.view.frame.size.width, height: size))
        // baseview.backgroundColor = UIColor(red:0.75, green:0.44, blue:0.99, alpha:1.0)
        baseview.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        baseview.tag = 668
        self.view.addSubview(baseview)
        self.view.bringSubviewToFront(baseview)
        self.view.endEditing(true)
        
        let viewGreen = UIView(frame: CGRect(x: 0, y: 0, width: baseview.frame.size.width, height: 50))
        viewGreen.backgroundColor = AppColor.pink
        baseview.addSubview(viewGreen)
        
        //............Button Setup............//
        //(x: baseview.frame.size.width-100, y: 0, width: 100, height: 50)
        let doneButton: UIButton = UIButton(frame: CGRect(x: baseview.frame.size.width-100, y: 0, width: 100, height: 50))
        doneButton.setTitle("Done", for: .normal)
        doneButton.titleLabel?.font = AppFont.Regular.size(.Poppins, size: 17)
        doneButton.setTitleColor(UIColor.white, for: .normal)
        doneButton.backgroundColor = UIColor.clear
        doneButton.tag = type == "Date" ? 1 : 2
        doneButton.addTarget(self, action: #selector(doneButtonActionFordatePicker), for: .touchUpInside)
        baseview.addSubview(doneButton)
        
        let cancelButton: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.titleLabel?.font = AppFont.Regular.size(.Poppins, size: 17)
        cancelButton.setTitleColor(UIColor.white, for: .normal)
        cancelButton.backgroundColor = UIColor.clear
        cancelButton.addTarget(self, action: #selector(cancelButtonActionFordatePicker), for: .touchUpInside)
        baseview.addSubview(cancelButton)
        
        // ...............Date Picker Setup..................//
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(identifier: "UTC")!
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.year = 0

        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        components.year = -100
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        
        var datePickerView: UIDatePicker!
        datePickerView = UIDatePicker(frame: CGRect(x:25, y: 50, width: baseview.frame.width, height:baseview.frame.height - 50))
        datePickerView.datePickerMode = type == "Date" ? .date : .time
        datePickerView.backgroundColor = UIColor.white
        if #available(iOS 13.4, *) {
            if #available(iOS 14.0, *) {
                datePickerView.preferredDatePickerStyle = type == "Date" ? .inline : .wheels
            } else {
                // Fallback on earlier versions
                datePickerView.preferredDatePickerStyle = .wheels
            }
        }
        datePickerView.tag = 5454
//        if type == "Time" && interval == "day"{
//           // datePickerView.minimumDate = Date() // Change for current date
//        }else{
            datePickerView.minimumDate = minDate // Change for current date
        //}
        datePickerView.maximumDate = maxDate
        baseview.addSubview(datePickerView)
        
    }

    
    @objc func doneButtonActionFordatePicker(_ sender:UIButton)
    {
        if let baseViewTag = self.view.viewWithTag(668)
        {
            if let datePicker = self.view.viewWithTag(5454) as? UIDatePicker
            {
                let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-YYYY"
                txtDOB.text = dateFormatter.string(from: datePicker.date)
                baseViewTag.removeFromSuperview()
            }
        }
    }
    
    @objc func cancelButtonActionFordatePicker()
    {
        if let baseViewTag = self.view.viewWithTag(668)
        {
            baseViewTag.removeFromSuperview()
        }
    }
    

}

//MARK:- Custom Methods
extension EditProfileVC : UITextFieldDelegate,ADCountryPickerDelegate, dataPass,GetDefaultAddressBack {
    func selectedAddress(_ name: String) {
        txtAddress.text = name
    }
    func data(date: String, isFrom: String) {
        print(date)
        txtDOB.text = date
    }
    
    func initiallizers() {
        
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        txtPhone.delegate = self
        txtAddress.delegate = self
        txtDOB.delegate = self
        txtNationality.delegate = self
        txtGender.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        btnDropDown.addTarget(self, action: #selector(btnGenderTap), for: .touchUpInside)
        self.setDataOnScreen()
    }
    
    
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        _ = picker.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        lblCountryCode.text = dialCode
        
    }
    
    @objc func btnGenderTap(){
        dropDown.anchorView = txtGender // UIView or UIBarButtonItem
        dropDown.dataSource = ["Male", "Female", "Other"]
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            txtGender.text = item
            dropDown.hide()
        }
        
    }
  
}


//MARK:- UIImagePickerDelegate
extension EditProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //TODO :-> ============= Open camera and gallery in ActionSheet ===========
    
    func openActionSheet() {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Take Photo".localized(), style: .default) { (action) in
            self.imagePicker.sourceType = .camera
            DispatchQueue.main.async {
                // self.present(self.imagePicker, animated: true, completion: nil)
                comClass.openCamera(imagePicker: self.imagePicker, vc: self)
            }
            
        }
        let gallery = UIAlertAction(title: "Choose Photo".localized(), style: .default) { (action) in
            self.imagePicker.sourceType = .photoLibrary
            DispatchQueue.main.async {
                // self.present(self.imagePicker, animated: true, completion: nil)
                comClass.openGallery(imagePicker: self.imagePicker, vc: self)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action) in
        }
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        if let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            if let imageData = chosenImage.jpegData(compressionQuality: 1.0) as NSData? {
                //  self.imgProfile.image = chosenImage
                self.imageData = imageData as Data
                self.tapUploadImage(chosenImage)
            }else{
                print("imageData nahi aaya gya")
            }
        } else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func tapUploadImage(_ profileImage:UIImage?) {
        
        guard let image = profileImage else { return } //1
        aws.uploadImage(image: image, progress: {[weak self] ( uploadProgress) in
            
            guard let strongSelf = self else { return }
            Indicator.shared.showProgressView(strongSelf.view)
        }) {[weak self] (uploadedFileUrl, error) in
            
            guard let strongSelf = self else { return }
            Indicator.shared.hideProgressView()
            if let finalPath = uploadedFileUrl as? String { // 3
                strongSelf.imageURL = finalPath
                strongSelf.imgProfile.setImage(withImageId: finalPath, placeholderImage: #imageLiteral(resourceName: "profile-user"))
                
            } else {
                print("\(String(describing: error?.localizedDescription))") // 4
            }
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePicker = UIImagePickerController()
        dismiss(animated: true, completion: nil)
    }
    
}
