//
//  EditProfile+Api.swift
//  FoodDelivery
//
//  Created by Cst on 4/8/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import SwiftyJSON
import AWSS3
import AWSCore
extension EditProfileVC: CustomDelegate{
    
    /// Show User Data On UI From Realm DataBase
    func setDataOnScreen(){
        txtFirstName.text = comClass.getInfoById().firstName
        txtLastName.text = comClass.getInfoById().lastName
        txtEmail.text = comClass.getInfoById().email
        txtPhone.text = comClass.getInfoById().mobileNumber
        txtAddress.text = comClass.getInfoById().address
        txtDOB.text = comClass.getInfoById().dob
        txtGender.text = comClass.getInfoById().gender
        txtNationality.text = comClass.getInfoById().nationality
        lblCountryCode.text = comClass.getInfoById().countryCode
        self.imgProfile.setImage(withImageId: comClass.getInfoById().profilePic ?? "", placeholderImage: #imageLiteral(resourceName: "profile-user"))
        imageURL = comClass.getInfoById().profilePic ?? ""
    }
    
    func checkValidation(){
        
        if txtFirstName.text?.isEmpty ?? false{
            self.show_Alert(message: "Please enter First Name!")
            return
        }
        if txtLastName.text?.isEmpty ?? false{
            self.show_Alert(message: "Please enter Last Name!")
            return
        }
        if !validation.validateEmail(txtEmail.text){
            self.show_Alert(message: "Please enter valid Email Id!")
            return
        }
        if txtPhone.text?.isEmpty ?? false{
            self.show_Alert(message: "Please enter valid Phone Number!")
            return
        }
        if txtAddress.text?.isEmpty ?? false{
            self.show_Alert(message: "Please enter you Address!")
            return
        }
        if txtDOB.text?.isEmpty ?? false{
            self.show_Alert(message: "Please enter your DOB!")
            return
        }
       
        if self.txtPhone.text ?? "" == (comClass.getInfoById().mobileNumber) && self.txtEmail.text ?? "" == (comClass.getInfoById().email){
            self.editProfileService()
        }
        else if self.txtPhone.text ?? "" == (comClass.getInfoById().mobileNumber){
            DispatchQueue.main.async {
                self.changeMobileEmail(APIEndpoint.changeEmail, ["email" : self.txtEmail.text ?? ""], true) { (data) in
                    print(data)
                    self.editProfileService()
                }
            }
        }else if self.txtPhone.text ?? "" == (comClass.getInfoById().mobileNumber){
            DispatchQueue.main.async {
                self.changeMobileEmail(APIEndpoint.changeMobileNumber, ["countryCode" : self.lblCountryCode.text ?? "","mobileNumber":self.txtPhone.text ?? ""], true) { (data) in
                    print(data)
                    self.editProfileService()
                }
            }
        } else{
            self.checkMobileAndEmail()
        }
        
    }
    
    /// Update User Profile Data
    func editProfileService(){
        let params = ["firstName":self.txtFirstName.text ?? "","lastName":self.txtLastName.text ?? "","dob":self.txtDOB.text ?? "",
                      "nationality":self.txtNationality.text ?? "","gender":self.txtGender.text ?? "","address":self.txtAddress.text ?? "",
                      "latitude":self.lat,"longitude":self.long,"profilePic":self.imageURL] as [String:Any]
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        Indicator.shared.showLottieAnimation(view: view)
        ApiManager.instance.fetchApiService(method: .post, url: APIEndpoint.userUpdateDetails, passDict: params, header: header) { (result) in
            Indicator.shared.hideLoaderPresent()
            switch result{
            case .success(let data):
                print(data)
                let userData = UserData(data)
                if userData.status == 200 {
                    DispatchQueue.main.async {
                        self.alertWithHandler(message: userData.message ?? "") {
                            self.navigationController?.popViewController(animated: false)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.handleError( userData.message ?? "")
                    }
                }
                break
            case .failure(let error):
                self.handleError(error.localizedDescription)
                break
            }
        }
        
        
        
    }
    
    /// Change Mobile Number and Email Id
    /// - Parameters:
    ///   - apiName: we are using two api to change Mobile Number and Email id
    ///   - param: api Parameters
    ///   - complition: it will call when both api data fetch successfully
    func changeMobileEmail(_ apiName:String,_ param:[String:Any],_ isPhoneExits:Bool? = false,_ isEmailExits:Bool? = false,_ complition: @escaping (_ some: String) -> Void = { _ in }){
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        Indicator.shared.showLottieAnimation(view: view)
        
        ApiManager.instance.fetchApiService(method: .post, url: apiName, passDict: param, header: header) { (result) in
            DispatchQueue.main.async {
                Indicator.shared.hideLoaderPresent()
            }
            switch result{
            case .success(let json):
                let message = json["message"].stringValue
                if json["status"].intValue == 200{
                    if message == "Mobile number updated successfully"{
                        self.editProfileService()
                    }else if message == "Email updated successfully"{
                        //complition(message)
                        if isPhoneExits ?? false{
                            self.editProfileService()
                        }else{
                            self.changeMobileEmail(APIEndpoint.changeMobileNumber, ["countryCode" : self.lblCountryCode.text ?? "","mobileNumber":self.txtPhone.text ?? ""])
                        }
                        
                    }
                }else{
                    DispatchQueue.main.async {
                        self.handleError(message)
                    }
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    self.handleError(error.localizedDescription)
                }
                break
            }
        }
    }
    
    /// To Check Both Email and Mobile already present in database or not
    func checkMobileAndEmail() {
        let param = ["countryCode":self.lblCountryCode.text ?? "",
                     "mobileNumber":self.txtPhone.text ?? ""] as [String:Any]
        Indicator.shared.showLottieAnimation(view: view)
        ApiManager.instance.fetchApiService(method: .post, url: "checkUserMobile", passDict: param) { (result) in
            DispatchQueue.main.async {
                Indicator.shared.hideLoaderPresent()
            }
            switch result{
            case .success(let json):
                if json["status"].intValue == 200{
                    
                    DispatchQueue.main.async {
                        self.setupPhoneAuthentication((self.lblCountryCode.text ?? "") + (self.txtPhone.text ?? "")) { (vCode) in
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterCodeVC") as! EnterCodeVC
                            vc.vCode = vCode
                            vc.delegate = self
                            vc.phoneNumber = (self.lblCountryCode.text ?? "") + (self.txtPhone.text ?? "")
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    
                }else{
                    self.handleError(json["message"].stringValue)
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    self.handleError(error.localizedDescription)
                }
                break
            }
        }
        
    }
    
    /// Custom Protocol Method , it will call when otp authention preform successfullly
    
    func getvCodeAndotp(vCode: String, otp: String) {
        Indicator.shared.showLottieAnimation(view: view)

        otpVerification(otp, vCode) {
            self.changeMobileEmail(APIEndpoint.changeMobileNumber, ["countryCode" : self.lblCountryCode.text ?? "","mobileNumber":self.txtPhone.text ?? ""]) { (data) in
                print(data)
                self.editProfileService()
            }
        }
    }
   
}



