//
//  SideMenuViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 4/26/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class SideMenuViewModel:BaseViewModel,CellRepresentable{
    
    var rowHeight: CGFloat = UITableView.automaticDimension
    let sideMenuData = [SideMenuData(title: "Favourites", subTitle: "See Favorite item", icon: UIImage(named: "fav")!),SideMenuData(title: "Order History", subTitle: "Check your Orders", icon: UIImage(named: "order")!),SideMenuData(title: "Payment Card", subTitle: "See my payment card option", icon: UIImage(named: "card1")!),SideMenuData(title: "Push-Notifications", subTitle: "Set up push notification", icon: UIImage(named: "notification")!),SideMenuData(title: "Term & Conditions", subTitle: "Check all Term & Conditions", icon: UIImage(named: "terms")!),SideMenuData(title: "FAQ's", subTitle: "Frequently Ask Questions", icon: UIImage(named: "faq")!),SideMenuData(title: "Privacy Policy", subTitle: "Read All Privacy Policy", icon: UIImage(named: "privacy")!),SideMenuData(title: "Support", subTitle: "Take support from us", icon: UIImage(named: "support")!),SideMenuData(title: "About Us", subTitle: "Read about us", icon: UIImage(named: "about")!),SideMenuData(title: comClass.isDataExist() ? "Logout" : "Login", subTitle: "Out from application", icon: UIImage(named: "logout")!)]
    
    private var dataService: ApiManager?
    var controller = UIViewController()
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "SideMenuTableCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableCell")
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableCell") as? SideMenuTableCell else {
            return UITableViewCell()
        }
        cell.item = self.sideMenuData[indexPath.row]
        return cell
    }
    
    func didSelect(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            kAppDelegate.appNavigator.drawerController.setDrawerState(.closed, animated: true)
            
            if !comClass.isDataExist(){
                controller.show_Alert(message: "Login Required!")
                return
            }
            
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoritesVC") as! MyFavoritesVC
            controller.navigationController?.pushViewController(vc, animated: true)
        case 1:
            kAppDelegate.appNavigator.drawerController.setDrawerState(.closed, animated: true)
            if !comClass.isDataExist(){
                controller.show_Alert(message: "Login Required!")
                return
            }
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
            controller.navigationController?.pushViewController(vc, animated: true)
        case 2:
            kAppDelegate.appNavigator.drawerController.setDrawerState(.closed, animated: true)
            if !comClass.isDataExist(){
                controller.show_Alert(message: "Login Required!")
                return
            }
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyCardVC") as! MyCardVC
            controller.navigationController?.pushViewController(vc, animated: true)
        case 3:
            print("Push Notifications")
        case 4:
            kAppDelegate.appNavigator.drawerController.setDrawerState(.closed, animated: true)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TermAndConditionVC") as! TermAndConditionVC
            vc.data = staticContentData.first?.data?[2]

            controller.navigationController?.pushViewController(vc, animated: true)
        case 5:
            kAppDelegate.appNavigator.drawerController.setDrawerState(.closed, animated: true)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FAQVC") as! FAQVC
            controller.navigationController?.pushViewController(vc, animated: true)
        case 6:
            kAppDelegate.appNavigator.drawerController.setDrawerState(.closed, animated: true)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TermAndConditionVC") as! TermAndConditionVC
            vc.data = staticContentData.first?.data?[3]
            controller.navigationController?.pushViewController(vc, animated: true)
        case 7:
            
            kAppDelegate.appNavigator.drawerController.setDrawerState(.closed, animated: true)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SupportVC") as! SupportVC
            controller.navigationController?.pushViewController(vc, animated: true)
        case 8:
            print("About Us")
            kAppDelegate.appNavigator.drawerController.setDrawerState(.closed, animated: true)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TermAndConditionVC") as! TermAndConditionVC
           // vc.isFrom = "aboutus"
            vc.data = staticContentData.first?.data?[1]

            controller.navigationController?.pushViewController(vc, animated: true)
        case 9:
            print("Logout")
            //let appDel = UIApplication.shared.delegate as! AppDelegate
            logoutService()
        
        default:
            break
        }

    }
}

struct SideMenuData{
    var title:String
    var subTitle:String
    var icon: UIImage
}
