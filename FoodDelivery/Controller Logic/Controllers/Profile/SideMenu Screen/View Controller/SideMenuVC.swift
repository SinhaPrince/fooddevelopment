//
//  SideMenuVC.swift
//  FoodDelivery
//
//  Created by call soft on 25/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class SideMenuVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var btnCross: UIButton!
    //MARK:- Properties
    let appDel = UIApplication.shared.delegate as! AppDelegate
    var tblData = ["Favourites","Order History","Payment Card","Push-Notifications","Term & Conditions","FAQ's","Privacy Policy","Support","About Us","Logout"]
    var tblSubTitleData = ["See Favorite item","Check your Orders","See my payment card option","Set up push notification","Check all Term & Conditions","Frequently Ask Questions","Read All Privacy Policy","Take support from us","Read about us","Set up push notification"]
    
    let sideMenuVM = SideMenuViewModel(networking: ApiManager())
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillLayoutSubviews() {
        tblVw.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.sideMenuVM.controller = self
        self.sideMenuVM.staticContentService()
        self.hideNavigationBar(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        initiallizers()
        tblVw.reloadData()
        btnCross.addTarget(self, action: #selector(croosTap), for: .touchUpInside)

        self.view.layer.cornerRadius = 60
        self.view.clipsToBounds = true
        self.view.layer.maskedCorners = [.layerMaxXMinYCorner]

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.hideNavigationBar(false)
    }
   
    @IBAction func btnActionProfile(_ sender: UIButton) {
        appDel.appNavigator.drawerController.setDrawerState(.closed, animated: true)

        if !comClass.isDataExist(){
            self.handleError("Login Required!")
            return
        }
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func croosTap(){
        self.closeSideMenu()
    }
    
}

//MARK:- Custom Methods
extension SideMenuVC {
    func initiallizers() {
        tblVw.cornerRadius = 25
        tblVw.layer.maskedCorners = [.layerMinXMaxYCorner]
        self.lblEmail.text = comClass.getInfoById().email
        self.lblUserName.text = comClass.isDataExist() ? comClass.getInfoById().firstName?.appending(" " + (comClass.getInfoById().lastName ?? "")) : "Guest User"
        if comClass.isDataExist(){
            self.imgProfile.sd_setImage(with: URL(string: comClass.getInfoById().profilePic ?? "") , completed: nil)
        }else{
            self.imgProfile.image = UIImage(named: "profile-user")
            self.imgProfile.backgroundColor = .white
        }
        
        configuretableView()
    }
    
    func configuretableView(){

        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.backgroundColor = AppColor.clearColor
        tblVw.reloadData()

    }
    
    
}

//MARK:- UITableViewDelegate and UITableViewDatasource
extension SideMenuVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sideMenuVM.sideMenuData.count
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return sideMenuVM.cellInstance(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.sideMenuVM.didSelect(tableView, didSelectRowAt: indexPath)
    }
    
}

//MARK:-Login Service Call
import RealmSwift
extension SideMenuViewModel{
    func logoutService(){
        if !comClass.isDataExist(){
            kAppDelegate.appNavigator = ApplicationNavigator(window: kAppDelegate.window, sendViewController: "LoginVC")
            return
        }
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        Indicator.shared.showLottieAnimation(view: controller.view)
        ApiManager.instance.fetchApiService(method: .get, url: APIEndpoint.userLogoutService, header: header) { (result) in
            Indicator.shared.hideLoaderPresent()
            switch result{
            //Rating = self.rating
           // TimeRange = [self.minTime ?? 15,self.maxTime ?? 30]
          //  DistanceRange = [self.minDis ?? 0,self.maxDis ?? 20]
            case .success(let data):
                if data["status"].intValue == 200{
                    let realm = try! Realm()
                    try! realm.write({
                        let deletedNotifications = realm.objects(DataUser.self)
                        realm.delete(deletedNotifications)
                    })
                    kAppDelegate.appNavigator = ApplicationNavigator(window: kAppDelegate.window, sendViewController: "LoginVC")
                }else{
                    self.controller.show_Alert(message: data["message"].stringValue)
                }
                break
            case .failure(let error):
                self.controller.show_Alert(message: error.localizedDescription)
                break
            }
        }
    }
}

//MARK:-Static ContentApi
//MARK:-
extension SideMenuViewModel{
    func staticContentService(){
       // Indicator.shared.showLottieAnimation(view: view)
        ApiManager.instance.fetchApiService(method: .get, url: "getStaticContent") { (result) in
//            DispatchQueue.main.async {
//                Indicator.shared.hideLoaderPresent()
//            }
            switch result{
            case .success(let json):
                if json["status"].intValue == 200{
                    if staticContentData.count > 0 {
                        staticContentData.removeAll()
                    }
                    staticContentData.append(StaticContentModel(json))
                }else{
                    self.controller.show_Alert(message: json["message"].stringValue)
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    self.controller.show_Alert(message: error.localizedDescription)
                }
                break
            }
        }
    }
}
var staticContentData = [StaticContentModel]()
