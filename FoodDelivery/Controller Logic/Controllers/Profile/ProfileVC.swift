//
//  ProfileVC.swift
//  FoodDelivery
//
//  Created by call soft on 08/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class ProfileVC: ParentViewController, BaseViewControllerDelegate {
    
    
    //MARK:- Outlets
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var txtFirstName: UITextField!
    
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var txtAddress: UITextField!
    
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var txtDOB: UITextField!
    
    @IBOutlet weak var lblNationality: UILabel!
    @IBOutlet weak var txtNationality: UITextField!
    
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var txtGender: UITextField!
    
    @IBOutlet weak var btnEditProfile: UIButton!
    
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    //MARK:- Properties
    var addressData:AddressListModel?

    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
        setupNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.baseDelegate = self

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setDataOnScreen()
       // self.fetchAddress("getAddressList")

    }
    
    
    /// Returrn All Saved User Addresses
    /// - Parameter url: ApiKey
    func fetchAddress(_ url:String){
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        ApiManager().fetchApiService(method: .get, url: url, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                self.addressData = AddressListModel(data)
                if self.addressData?.status == 200{
                    let data = self.addressData?.data?.filter({item in
                        item.defaultStatus == true
                    })
                    //if there is no default address
                    if data?.count == 0{
                        return
                    }
                    self.txtAddress.text = data?.first?.landmark
                }else{
                    self.show_Alert(message: self.addressData?.message ?? "")
                }
                break
            case .failure(let error):
                self.show_Alert(message: error.localizedDescription)
                break
            }
        })

    }
    
    //MARK:-Setup Navigation
    func setupNavigation(){
        transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "Profile")
        title.textColor = AppColor.textcolor
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .right
        title.numberOfLines = 0
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
        let editButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        editButton.setImage(#imageLiteral(resourceName: "editpink"), for: .normal)
        editButton.tag = UINavigationBarButtonType.edit.rawValue
        let titleRight = UILabel()
        titleRight.text = comClass.createString(Str: "Edit Profile")
        titleRight.textColor = AppColor.textcolor
        titleRight.font = AppFont.Medium.size(.Poppins, size: 14)
        titleRight.textAlignment = .right
        titleRight.numberOfLines = 0
        editButton.addTarget(self, action: #selector(ParentViewController.navigationButtonTapped(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = (UIBarButtonItem(customView: editButton))
        self.navigationItem.rightBarButtonItems?.append(UIBarButtonItem(customView: titleRight))
    }
    
    
    func navigationBarButtonDidTapped(_ buttonType: UINavigationBarButtonType) {
        if buttonType == .edit{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- Button Action
   
    @IBAction func btnActionAddressList(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

//MARK:- Custom Methods
extension ProfileVC : UITextFieldDelegate{
    
    func initiallizers() {
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        txtPhone.delegate = self
        txtAddress.delegate = self
        txtDOB.delegate = self
        txtNationality.delegate = self
        txtGender.delegate = self
    }
    
    func setDataOnScreen(){
       
        txtFirstName.text = comClass.getInfoById().firstName
        txtLastName.text = comClass.getInfoById().lastName
        txtEmail.text = comClass.getInfoById().email
        txtPhone.text = comClass.getInfoById().mobileNumber
        txtAddress.text = comClass.getInfoById().address
        txtDOB.text = comClass.getInfoById().dob
        txtGender.text = comClass.getInfoById().gender
        txtNationality.text = comClass.getInfoById().nationality
        lblCountryCode.text = comClass.getInfoById().countryCode

        profileImage.setImage(withImageId: comClass.getInfoById().profilePic ?? "", placeholderImage: #imageLiteral(resourceName: "profile-user"))
    }
   
}

