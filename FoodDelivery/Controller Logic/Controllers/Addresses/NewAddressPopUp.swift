//
//  NewAddressPopUp.swift
//  FoodDelivery
//
//  Created by call soft on 04/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class NewAddressPopUp: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnOffice: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    //MARK:- Properties
    var navObj = UIViewController()
    var addressData: AddressData?
    var address:String?
    var delegate: MapInfoDelegate?
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
        txtAddress.maxLength = 20
        self.moveIn()
    }
    //MARK:- Button Action
    @IBAction func btnActionSave(_ sender: UIButton) {
        
        if txtAddress.text?.isEmpty ?? false{
            self.show_Alert(message: "Please enter other address field!")
            return
        }
        self.dismiss(animated: true, completion: {
            self.delegate?.addressType!(type: self.txtAddress.text ?? "")
        })
    }
    @IBAction func btnActionRadio(_ sender: UIButton) {
        if sender.tag == 1 {
            btnHome.isSelected = true
            btnOther.isSelected = false
            btnOffice.isSelected = false
            txtAddress.text = "Home"
            txtAddress.isHidden = true

        }else if sender.tag == 2{
            btnHome.isSelected = false
            btnOther.isSelected = false
            btnOffice.isSelected = true
            txtAddress.text = "Office"
            txtAddress.isHidden = true

        }else{
            btnOffice.isSelected = false
            btnHome.isSelected = false
            btnOther.isSelected = true
            txtAddress.isHidden = false
            txtAddress.text = ""
        }
    }
    
    func moveIn() {
        self.topView.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
        self.topView.alpha = 0.0
        
        UIView.animate(withDuration: 2) {
            self.topView.transform = CGAffineTransform.identity
            self.topView.alpha = 1.0
        }
    }
}


//MARK:- Custom Methods
extension NewAddressPopUp {
    func initiallizers() {
        txtAddress.placeholder = "Set Name Manually"
        self.btnOther.setTitle("Other", for: .normal)
        self.btnOther.setImage(#imageLiteral(resourceName: "radio_un"), for: .normal)
        self.btnOther.setImage(#imageLiteral(resourceName: "radio_s"), for: .selected)
        self.btnOther.setTitleColor(AppColor.pink, for: .selected)
        if addressData != nil {
            lblTitle.text = "Update Location as"
            lblSubtitle.text = addressData?.address ?? ""
            txtAddress.text = addressData?.address ?? ""
            btnSave.setTitle("Update", for: .normal)
            if addressData?.addressType == "Home"{
                btnHome.isSelected = true
                btnOffice.isSelected = false
                btnOther.isSelected = false
                txtAddress.text = "Home"
                txtAddress.isHidden = true

            }else if addressData?.addressType == "Office"{
                btnHome.isSelected = false
                btnOffice.isSelected = true
                txtAddress.text = "Office"
                btnOther.isSelected = false
                txtAddress.isHidden = true

            }else{
                btnHome.isSelected = false
                btnOffice.isSelected = false
                txtAddress.text = addressData?.addressType
                btnOther.isSelected = true
                txtAddress.isHidden = false

            }
        }
        else
        {
            lblTitle.text = "Set Location as"
            lblSubtitle.text = address
            txtAddress.text = "Home"
            btnSave.setTitle("Save", for: .normal)
            btnHome.isSelected = true
            btnOffice.isSelected = false
        }
    }
}


