//
//  MyAddressVC.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import SkeletonView
class MyAddressVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblVw: UITableView!
    
    //MARK:- Properties
    let addressVM = AddressListVM(networking: ApiManager())
    var comingFrom: String?
    var delegate: GetDefaultAddressBack?
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Indicator.shared.hideLoaderPresent()
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .always
        setupNavigation()
        self.tblVw.register(UINib(nibName: "AddressTableCell", bundle: nil), forCellReuseIdentifier: "AddressTableCell")
        self.tblVw.dataSource = self
        self.tblVw.isSkeletonable = true
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight, duration: 0.4, autoreverses: true)
        self.tblVw.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7215660045)), animation: animation, transition: .crossDissolve(0.5))
        initiallizers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationItem.largeTitleDisplayMode = .always
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "My Address")
        title.textColor = AppColor.textcolor
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .right
        title.numberOfLines = 0
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
        self.navigationItem.title = "Saved Addresses"
        navigationController?.navigationBar.largeTitleTextAttributes =
            [NSAttributedString.Key.foregroundColor: AppColor.black,
             NSAttributedString.Key.font: AppFont.Medium.size(.Poppins, size: 22)]
    }
   
    //MARK:- Button Action
    @IBAction func btnActionAddNewAddress(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewAddressVC") as! NewAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

//MARK:- Custom Methods
extension MyAddressVC {
    func initiallizers() {
       // Indicator.shared.showLottieAnimation(view: view)
        addressVM.fetchAddress("getAddressList")
        addressVM.showAlertClosure = {
            if let error = self.addressVM.error {
                print(error.localizedDescription)
                self.handleError(error.localizedDescription)
            }else if self.addressVM.statusCode == 200{
                Indicator.shared.hideLoaderPresent()

                if self.addressVM.statusMessage == comClass.createString(Str: "Address deleted successfully") || self.addressVM.statusMessage == comClass.createString(Str: "Status updated successfully") {
                    Indicator.shared.showLottieAnimation(view: self.view)

                    self.addressVM.fetchAddress("getAddressList")
                }
            }else{
                self.handleError(self.addressVM.statusMessage)
            }
        }
        addressVM.didFinishFetch = {
            Indicator.shared.hideLoaderPresent()
            self.configureTable()
        }
        addressVM.btnEditTapAction = {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NewAddressVC") as! NewAddressVC
            vc.addressData = self.addressVM.addressData?.data?[self.addressVM.index ?? 0]
            //vc.address = "52 Riverside St. Norcross, GA 30092"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
   
}
//MARK:- UITableViewDelegate and UITableViewDatasource
extension MyAddressVC : UITableViewDelegate, UITableViewDataSource,SkeletonTableViewDataSource{
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int{
        5
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "AddressTableCell"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressVM.addressData?.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        return addressVM.cellInstance(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return addressVM.rowHeight
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return addressVM.rowHeight
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if self.comingFrom == "editprofile"{
            self.delegate?.selectedAddress(self.addressVM.addressData?.data?[indexPath.row].address ?? "")
            self.navigationController?.popViewController(animated: false)
        }else{
            self.addressVM.deleteAndSelectAddress("updateAddressStatus", ["addressId" : self.addressVM.addressData?.data?[indexPath.row].Id ?? ""])
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            Indicator.shared.showLottieAnimation(view: view)
            self.addressVM.deleteAndSelectAddress("deleteAddress", ["addressId" : self.addressVM.addressData?.data?[indexPath.row].Id ?? ""])
      }
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
                tableView.dataSource?.tableView?(tableView, commit: .delete, forRowAt: indexPath)
                return
            }
            deleteButton.backgroundColor = UIColor.red
        
        
            return [deleteButton]
        }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        var actions = [UIContextualAction]()

        let delete = UIContextualAction(style: .normal, title: nil) { [weak self] (contextualAction, view, completion) in

            // Delete something
            Indicator.shared.showLottieAnimation(view: view)
            self?.addressVM.deleteAndSelectAddress("deleteAddress", ["addressId" : self?.addressVM.addressData?.data?[indexPath.row].Id ?? ""])
            completion(true)
        }

        if #available(iOS 13.0, *) {
            let largeConfig = UIImage.SymbolConfiguration(pointSize: 17.0, weight: .bold, scale: .large)
            delete.image = UIImage(systemName: "trash", withConfiguration: largeConfig)?.withTintColor(.white, renderingMode: .alwaysTemplate).addBackgroundCircle(.systemRed)
            delete.backgroundColor = .systemBackground
            delete.title = "Delete"
            actions.append(delete)
        } else {
            // Fallback on earlier versions
        }
        let config = UISwipeActionsConfiguration(actions: actions)
        config.performsFirstActionWithFullSwipe = false

        return config
    }
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.reloadData()
        self.tblVw.stopSkeletonAnimation()
        self.tblVw.hideSkeleton()
    }
 
}

protocol SearchViewAnimateble : class{ }

extension SearchViewAnimateble where Self: UIViewController{

func showSearchBar(searchBar : UISearchBar) {
    searchBar.alpha = 0
    navigationItem.titleView = searchBar
    navigationItem.setRightBarButtonItems(nil, animated: true)
    UIView.animate(withDuration: 0.5, animations: {
        searchBar.alpha = 1
    }, completion: { finished in
        searchBar.becomeFirstResponder()
    })
}

func hideSearchBar( searchBarButtonItem : [UIBarButtonItem], titleView : UIView) {
    navigationItem.setRightBarButtonItems(searchBarButtonItem, animated: true)
    //navigationItem.titleView?.removeFromSuperview()
    titleView.alpha = 0
    
    UIView.animate(withDuration: 0.3, animations: {
        self.navigationItem.titleView = titleView
        titleView.alpha = 1

    }, completion: { finished in

    })
 }
}
extension UIImage {

    func addBackgroundCircle(_ color: UIColor?) -> UIImage? {

        let circleDiameter = max(size.width * 2, size.height * 2)
        let circleRadius = circleDiameter * 0.5
        let circleSize = CGSize(width: circleDiameter, height: circleDiameter)
        let circleFrame = CGRect(x: 0, y: 0, width: circleSize.width, height: circleSize.height)
        let imageFrame = CGRect(x: circleRadius - (size.width * 0.5), y: circleRadius - (size.height * 0.5), width: size.width, height: size.height)

        let view = UIView(frame: circleFrame)
        view.backgroundColor = color ?? .systemRed
        view.layer.cornerRadius = circleDiameter * 0.5

        UIGraphicsBeginImageContextWithOptions(circleSize, false, UIScreen.main.scale)

        let renderer = UIGraphicsImageRenderer(size: circleSize)
        let circleImage = renderer.image { ctx in
            view.drawHierarchy(in: circleFrame, afterScreenUpdates: true)
        }

        circleImage.draw(in: circleFrame, blendMode: .normal, alpha: 1.0)
        draw(in: imageFrame, blendMode: .normal, alpha: 1.0)

        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return image
    }
}
