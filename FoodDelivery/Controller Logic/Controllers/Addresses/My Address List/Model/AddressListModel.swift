//
//  AddressListModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 15, 2021
//
import Foundation
import SwiftyJSON

struct AddressListModel {

	var message: String?
	var data: [AddressData]?
	var status: Int?

	init(_ json: JSON) {
		message = json["message"].stringValue
		data = json["data"].arrayValue.map { AddressData($0) }
		status = json["status"].intValue
	}

}
