//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 15, 2021
//
import Foundation
import SwiftyJSON

struct AddressData {

	var deleteStatus: Bool?
	var buildingAndApart: String?
	var longitude: Double?
	var status: String?
	var latitude: Double?
	var createdAt: String?
	var _v: Int?
	var Id: String?
	var addressType: String?
	var userId: String?
	var address: String?
	var landmark: String?
	var updatedAt: String?
    var defaultStatus: Bool?
	init(_ json: JSON) {
		deleteStatus = json["deleteStatus"].boolValue
        defaultStatus = json["defaultStatus"].boolValue
		buildingAndApart = json["buildingAndApart"].stringValue
		longitude = json["longitude"].doubleValue
		status = json["status"].stringValue
		latitude = json["latitude"].doubleValue
		createdAt = json["createdAt"].stringValue
		_v = json["__v"].intValue
		Id = json["_id"].stringValue
		addressType = json["addressType"].stringValue
		userId = json["userId"].stringValue
		address = json["address"].stringValue
		landmark = json["landmark"].stringValue
		updatedAt = json["updatedAt"].stringValue
	}

}
