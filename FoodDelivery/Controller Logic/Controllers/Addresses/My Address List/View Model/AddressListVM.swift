//
//  AddressListVM.swift
//  FoodDelivery
//
//  Created by Cst on 4/15/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class AddressListVM: BaseViewModel,CellRepresentable {
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "AddressTableCell", bundle: nil), forCellReuseIdentifier: "AddressTableCell")
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableCell") as? AddressTableCell else {
            return UITableViewCell()
        }
        cell.item = addressData?.data?[indexPath.row]
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(btnEditTap), for: .touchUpInside)
        return cell
    }
    
    @objc func btnEditTap(_ sender:UIButton){
        index = sender.tag
    }
    var btnEditTapAction: (() -> ())?

    var index:Int?{
        didSet{
            self.btnEditTapAction?()
        }
    }
    var rowHeight: CGFloat = UITableView.automaticDimension
    var addressData:AddressListModel?{
        didSet{
            self.didFinishFetch?()
        }
    }
    
    private var dataService: ApiManager?
    
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    func fetchAddress(_ url:String){
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        self.dataService?.fetchApiService(method: .get, url: url, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                self.error = nil
                self.isLoading = false
                self.addressData = AddressListModel(data)
                self.statusCode = data["status"].intValue
                self.statusMessage = data["message"].stringValue
                break
            case .failure(let error):
                self.error = error
                self.isLoading = false
                break
            }
        })

    }
    
    func deleteAndSelectAddress(_ url:String,_ param:[String:Any]){
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        self.dataService?.fetchApiService(method: .post, url: url, passDict: param, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                self.error = nil
                self.isLoading = false
                self.statusCode = data["status"].intValue
                self.statusMessage = data["message"].stringValue
                break
            case .failure(let error):
                self.error = error
                self.isLoading = false
                break
            }
        })

    }
    
    
}
