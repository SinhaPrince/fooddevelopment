//
//  AddressParserModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 26, 2021
//
import Foundation
import SwiftyJSON

struct AddressParserModel {

	var plusCode: PlusCode?
	var results: [Results]?
	var status: String?

	init(_ json: JSON) {
		plusCode = PlusCode(json["plus_code"])
		results = json["results"].arrayValue.map { Results($0) }
		status = json["status"].stringValue
	}

}