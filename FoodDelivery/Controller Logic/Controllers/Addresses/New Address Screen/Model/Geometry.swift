//
//  Geometry.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 26, 2021
//
import Foundation
import SwiftyJSON

struct Geometry {

	var location: LocationAddress?
	var locationType: String?
	var bounds: Bounds?
	var viewport: Viewport?

	init(_ json: JSON) {
		location = LocationAddress(json["location"])
		locationType = json["location_type"].stringValue
		bounds = Bounds(json["bounds"])
		viewport = Viewport(json["viewport"])
	}

}
