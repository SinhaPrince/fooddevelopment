//
//  AddressComponents.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 26, 2021
//
import Foundation
import SwiftyJSON

struct AddressComponents {

	var longName: String?
	var shortName: String?
	var types: [String]?

	init(_ json: JSON) {
		longName = json["long_name"].stringValue
		shortName = json["short_name"].stringValue
		types = json["types"].arrayValue.map { $0.stringValue }
	}

}