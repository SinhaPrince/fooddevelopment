//
//  Viewport.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 26, 2021
//
import Foundation
import SwiftyJSON

struct Viewport {

	var southwest: Southwest?
	var northeast: Northeast?

	init(_ json: JSON) {
		southwest = Southwest(json["southwest"])
		northeast = Northeast(json["northeast"])
	}

}