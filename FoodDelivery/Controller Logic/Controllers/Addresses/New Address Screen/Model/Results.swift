//
//  Results.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 26, 2021
//
import Foundation
import SwiftyJSON

struct Results {

	var addressComponents: [AddressComponents]?
	var geometry: Geometry?
	var formattedAddress: String?
	var placeId: String?
	var types: [String]?

	init(_ json: JSON) {
		addressComponents = json["address_components"].arrayValue.map { AddressComponents($0) }
		geometry = Geometry(json["geometry"])
		formattedAddress = json["formatted_address"].stringValue
		placeId = json["place_id"].stringValue
		types = json["types"].arrayValue.map { $0.stringValue }
	}

}