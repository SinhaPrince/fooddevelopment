//
//  PlusCode.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 26, 2021
//
import Foundation
import SwiftyJSON

struct PlusCode {

	var compoundCode: String?
	var globalCode: String?

	init(_ json: JSON) {
		compoundCode = json["compound_code"].stringValue
		globalCode = json["global_code"].stringValue
	}

}