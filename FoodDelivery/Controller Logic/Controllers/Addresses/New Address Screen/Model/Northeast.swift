//
//  Northeast.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 26, 2021
//
import Foundation
import SwiftyJSON

struct Northeast {

	var lng: Double?
	var lat: Double?

	init(_ json: JSON) {
		lng = json["lng"].doubleValue
		lat = json["lat"].doubleValue
	}

}