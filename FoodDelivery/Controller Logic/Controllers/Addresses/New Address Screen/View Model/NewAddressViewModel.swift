//
//  View Model.swift
//  FoodDelivery
//
//  Created by Cst on 4/14/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import MapKit
class NewAddressViewModel: BaseViewModel, MKMapViewDelegate {
    
    private var dataService: ApiManager?
    
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    
    func setAnnotationMap(_ mapView:MKMapView,_ lattitude: String,_ longitude: String){
        let restLat = lattitude as NSString
        let restLong = longitude as NSString
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: restLat.doubleValue, longitude: restLong.doubleValue)
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotation(annotation)
        let region = MKCoordinateRegion( center: CLLocationCoordinate2D(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude), latitudinalMeters: CLLocationDistance(exactly: 5000)!, longitudinalMeters: CLLocationDistance(exactly: 5000)!)
        mapView.setRegion(mapView.regionThatFits(region), animated: true)
        
    }
    
   
    
    //MARK:- Network Call
    
    func addAddressService(_ url:String,_ params:[String:Any]){
        
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        self.dataService?.fetchApiService(method: .post, url: url, passDict: params, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                self.error = nil
                self.isLoading = false
                if data["status"].intValue == 200{
                    self.didFinishFetch?()
                }else{
                    self.statusMessage = data["message"].stringValue
                }
                break
            case .failure(let error):
                self.error = error
                self.isLoading = false
                break
            }
        })
    }
}
