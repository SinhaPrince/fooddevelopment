//
//  NewAddressVC.swift
//  FoodDelivery
//
//  Created by call soft on 04/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass
import CoreLocation
import MaterialComponents
import GooglePlaces
import GoogleMaps
import Alamofire
import SwiftyJSON
// MARK: - State
enum State {
    case closed
    case open
}

extension State {
    var opposite: State {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}

class NewAddressVC: ParentViewController,MapInfoDelegate, AddressbackDelegate, UITextFieldDelegate {
    
    func address(address: String, subAddress: String) {
        self.btnActionSetLocation(btnSetLocation)
    }
    
    //MARK:- Outlets
    @IBOutlet weak var mpMapView: GMSMapView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    // Injection
    let newAddressVM = NewAddressViewModel(networking: ApiManager())
    var latitude: String?
    var longitude: String?
    var addressData: AddressData?
    var landmark: String?
    var mapRegionTimer: Timer?
    
    var parseAddressModel:AddressParserModel?
    
    // MARK: - Constants
    
    private let popupOffset: CGFloat = 180
    
    // MARK: - Views
    
    private lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0
        return view
    }()
    
    private lazy var popupView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 10
        return view
    }()
    
    private lazy var closedTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Set Location"
        label.font = AppFont.Medium.size(.Poppins, size: 16)
        label.textColor = AppColor.pink
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        return label
    }()
    
    private lazy var openTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Set Location"
        label.font = AppFont.SemiBold.size(.Poppins, size: 20)
        label.textColor = .black
        label.textAlignment = .center
        label.alpha = 0
        label.isUserInteractionEnabled = true
        //  label.transform = CGAffineTransform(scaleX: 0.65, y: 0.65).concatenating(CGAffineTransform(translationX: 0, y: -15))
        return label
    }()
    
    private lazy var txtAddress: MDCFilledTextField = {
        let textField = MDCFilledTextField()
        textField.isUserInteractionEnabled = false
        textField.font = AppFont.Medium.size(.Poppins, size: 14)
        textField.textColor = AppColor.appLightBlack
        textField.label.setup("Address", AppColor.appLightGray, AppFont.Regular.size(.Poppins, size: 12), .left)
        textField.setFloatingLabelColor(AppColor.appLightGray, for: .normal)
        textField.setFloatingLabelColor(AppColor.appLightGray, for: .editing)
        textField.setNormalLabelColor(AppColor.appLightGray, for: .normal)
        textField.placeholder = "Address"
        textField.setUnderlineColor(.clear, for: .normal)
        textField.setUnderlineColor(.clear, for: .editing)
        textField.setFilledBackgroundColor(.clear, for: .normal)
        textField.setFilledBackgroundColor(.clear, for: .editing)
        return textField
    }()
    private lazy var btnSetLocation: UIButton = {
        let btn = UIButton()
        btn.setTitle("Name your Location", for: .normal)
        btn.setTitleColor(AppColor.white, for: .normal)
        btn.contentHorizontalAlignment = .center
        btn.titleLabel?.font = AppFont.Medium.size(.Poppins, size: 18)
        btn.backgroundColor = AppColor.pink
        btn.layer.cornerRadius = 30
        btn.clipsToBounds = true
        btn.addTarget(self, action: #selector(btnActionSetLocation), for: .touchUpInside)
        return btn
    }()
    private lazy var txtAddress2: MDCFilledTextField = {
        let textField = MDCFilledTextField()
        textField.font = AppFont.Medium.size(.Poppins, size: 14)
        textField.textColor = AppColor.appLightBlack
        textField.label.setup("Building/Apartment No", AppColor.appLightGray, AppFont.Regular.size(.Poppins, size: 12), .left)
        textField.setFloatingLabelColor(AppColor.appLightGray, for: .normal)
        textField.setFloatingLabelColor(AppColor.appLightGray, for: .editing)
        textField.setNormalLabelColor(AppColor.appLightGray, for: .normal)
        textField.placeholder = "Building/Apartment No"
        textField.setUnderlineColor(.clear, for: .normal)
        textField.setUnderlineColor(.clear, for: .editing)
        textField.setFilledBackgroundColor(.clear, for: .normal)
        textField.setFilledBackgroundColor(.clear, for: .editing)
        textField.delegate = self
        return textField
    }()
    
    private lazy var view1: UIView = {
        let view = UIView()
        view.layer.borderColor = AppColor.appLightGray.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 8
        
        return view
    }()
    private lazy var view2: UIView = {
        let view = UIView()
        view.layer.borderColor = AppColor.appLightGray.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 8
        
        return view
    }()
    private lazy var view3: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.isUserInteractionEnabled = true
        return view
    }()
    private lazy var verticalStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fillEqually
        stack.spacing = 25
        stack.addArrangedSubview(view1)
        stack.addArrangedSubview(view2)
        stack.addArrangedSubview(btnSetLocation)
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    // MARK: - Layout
    
    private var bottomConstraint = NSLayoutConstraint()
    
    private func layout() {
        
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(overlayView)
        overlayView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        overlayView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        overlayView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        overlayView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        
        popupView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(popupView)
        view.bringSubviewToFront(popupView)
        popupView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        popupView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bottomConstraint = popupView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: popupOffset)
        bottomConstraint.isActive = true
        popupView.heightAnchor.constraint(equalToConstant: 320).isActive = true
        
        
        popupView.addSubview(verticalStack)
        NSLayoutConstraint.activate([verticalStack.topAnchor.constraint(equalTo: popupView.topAnchor, constant: 60),
                                     verticalStack.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 15),
                                     verticalStack.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: -15),
                                     verticalStack.heightAnchor.constraint(equalToConstant: 230)])
        
        closedTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        popupView.addSubview(closedTitleLabel)
        closedTitleLabel.leadingAnchor.constraint(equalTo: popupView.leadingAnchor).isActive = true
        closedTitleLabel.trailingAnchor.constraint(equalTo: popupView.trailingAnchor).isActive = true
        closedTitleLabel.topAnchor.constraint(equalTo: popupView.topAnchor, constant: 20).isActive = true
        
        openTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        popupView.addSubview(openTitleLabel)
        openTitleLabel.leadingAnchor.constraint(equalTo: popupView.leadingAnchor).isActive = true
        openTitleLabel.trailingAnchor.constraint(equalTo: popupView.trailingAnchor).isActive = true
        openTitleLabel.topAnchor.constraint(equalTo: popupView.topAnchor, constant: 20).isActive = true
        view3.translatesAutoresizingMaskIntoConstraints = false
        popupView.addSubview(view3)
        
        NSLayoutConstraint.activate([view3.topAnchor.constraint(equalTo: popupView.topAnchor, constant: 0),
                                     view3.leadingAnchor.constraint(equalTo: popupView.leadingAnchor, constant: 0),
                                     view3.trailingAnchor.constraint(equalTo: popupView.trailingAnchor, constant: 0),
                                     view3.heightAnchor.constraint(equalToConstant: 45)])
        
        txtAddress.translatesAutoresizingMaskIntoConstraints = false
        view1.addSubview(txtAddress)
        txtAddress2.translatesAutoresizingMaskIntoConstraints = false
        view2.addSubview(txtAddress2)
        NSLayoutConstraint.activate([txtAddress.topAnchor.constraint(equalTo: view1.topAnchor, constant: 0),
                                     txtAddress.bottomAnchor.constraint(equalTo: view1.bottomAnchor, constant: 0),
                                     txtAddress.leadingAnchor.constraint(equalTo: view1.leadingAnchor, constant: 10),
                                     txtAddress.trailingAnchor.constraint(equalTo: view1.trailingAnchor, constant: -10),
                                     txtAddress2.topAnchor.constraint(equalTo: view2.topAnchor, constant: 0),
                                     txtAddress2.bottomAnchor.constraint(equalTo: view2.bottomAnchor, constant: 0),
                                     txtAddress2.leadingAnchor.constraint(equalTo: view2.leadingAnchor, constant: 10),
                                     txtAddress2.trailingAnchor.constraint(equalTo: view2.trailingAnchor, constant: -10)])
    }
    
    // MARK: - Animation
    /// The current state of the animation. This variable is changed only when an animation completes.
    private var currentState: State = .closed
    
    /// All of the currently running animators.
    private var runningAnimators = [UIViewPropertyAnimator]()
    
    /// The progress of each animator. This array is parallel to the `runningAnimators` array.
    private var animationProgress = [CGFloat]()
    
    private lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(popupViewPanned(recognizer:)))
        return recognizer
    }()
    /// Animates the transition, if the animation is not already running.
    private func animateTransitionIfNeeded(to state: State, duration: TimeInterval) {
        
        // ensure that the animators array is empty (which implies new animations need to be created)
        guard runningAnimators.isEmpty else { return }
        
        // an animator for the transition
        let transitionAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1, animations: {
            switch state {
            case .open:
                self.bottomConstraint.constant = 0
                self.popupView.layer.cornerRadius = 20
                self.overlayView.alpha = 0.5
                self.closedTitleLabel.transform = CGAffineTransform(scaleX: 1.6, y: 1.6).concatenating(CGAffineTransform(translationX: 0, y: 15))
                self.openTitleLabel.transform = .identity
            case .closed:
                self.bottomConstraint.constant = self.popupOffset
                self.popupView.layer.cornerRadius = 0
                self.overlayView.alpha = 0
                self.closedTitleLabel.transform = .identity
                self.openTitleLabel.transform = CGAffineTransform(scaleX: 0.65, y: 0.65).concatenating(CGAffineTransform(translationX: 0, y: -15))
            }
            self.view.layoutIfNeeded()
        })
        
        // the transition completion block
        transitionAnimator.addCompletion { position in
            
            // update the state
            switch position {
            case .start:
                self.currentState = state.opposite
            case .end:
                self.currentState = state
            case .current:
                ()
            }
            
            // manually reset the constraint positions
            switch self.currentState {
            case .open:
                self.bottomConstraint.constant = 0
            case .closed:
                self.bottomConstraint.constant = self.popupOffset
            }
            
            // remove all running animators
            self.runningAnimators.removeAll()
            
        }
        
        // an animator for the title that is transitioning into view
        let inTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeIn, animations: {
            switch state {
            case .open:
                self.openTitleLabel.alpha = 1
            case .closed:
                self.closedTitleLabel.alpha = 1
            }
        })
        inTitleAnimator.scrubsLinearly = false
        
        // an animator for the title that is transitioning out of view
        let outTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeOut, animations: {
            switch state {
            case .open:
                self.closedTitleLabel.alpha = 0
            case .closed:
                self.openTitleLabel.alpha = 0
            }
        })
        outTitleAnimator.scrubsLinearly = false
        
        // start all animators
        transitionAnimator.startAnimation()
        inTitleAnimator.startAnimation()
        outTitleAnimator.startAnimation()
        
        // keep track of all running animators
        runningAnimators.append(transitionAnimator)
        runningAnimators.append(inTitleAnimator)
        runningAnimators.append(outTitleAnimator)
        
    }
    
    
    @objc private func popupViewPanned(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            
            // start the animations
            animateTransitionIfNeeded(to: currentState.opposite, duration: 1)
            
            // pause all animations, since the next event may be a pan changed
            runningAnimators.forEach { $0.pauseAnimation() }
            
            // keep track of each animator's progress
            animationProgress = runningAnimators.map { $0.fractionComplete }
            
        case .changed:
            
            // variable setup
            let translation = recognizer.translation(in: popupView)
            var fraction = -translation.y / popupOffset
            
            // adjust the fraction for the current state and reversed state
            if currentState == .open { fraction *= -1 }
            if runningAnimators[0].isReversed { fraction *= -1 }
            
            // apply the new fraction
            for (index, animator) in runningAnimators.enumerated() {
                animator.fractionComplete = fraction + animationProgress[index]
            }
            
        case .ended:
            
            // variable setup
            let yVelocity = recognizer.velocity(in: popupView).y
            let shouldClose = yVelocity > 0
            
            // if there is no motion, continue all animations and exit early
            if yVelocity == 0 {
                runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
                break
            }
            
            // reverse the animations based on their current state and pan motion
            switch currentState {
            case .open:
                if !shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            case .closed:
                if shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if !shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            }
            
            // continue all animations
            runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
            
        default:
            ()
        }
    }
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
        self.txtAddress.isUserInteractionEnabled = true
        self.initializeTheLocationManager()
        mpMapView.settings.allowScrollGesturesDuringRotateOrZoom = false
        mpMapView.settings.myLocationButton = false
        mpMapView.settings.compassButton = false
        mpMapView.isMyLocationEnabled = true
        initialSetup()
        layout()
        view3.addGestureRecognizer(panRecognizer)
    }
    
    private func initialSetup(){
        if  addressData != nil {
            //Map Config
            self.latitude = addressData?.latitude?.description
            self.longitude = addressData?.longitude?.description
            self.configureMap(self.latitude ?? "", self.longitude ?? "")
            self.txtAddress.text = addressData?.address ?? ""
            self.landmark = addressData?.landmark ?? ""
            self.txtAddress2.text = addressData?.buildingAndApart ?? ""
            self.viewWillLayoutSubviews()
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                //Map Config
                self.latitude = self.lat
                self.longitude = self.long
                self.configureMap(self.latitude ?? "", self.longitude ?? "")
                let latitude = self.lat as NSString
                let logitude = self.long as NSString
                self.parseAddress(latitude: latitude.doubleValue, logitude: logitude.doubleValue) { (address,subLocality) in
                    self.txtAddress.text = address
                    self.landmark = subLocality
                    self.viewWillLayoutSubviews()
                }
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        view2.borderColor = AppColor.appLightGray
        self.txtAddress2.errorHandler("", .red)
    }
    
    @objc func btnActionSetLocation(_ sender: UIButton) {
        
        if txtAddress2.text?.isEmpty ?? false{
            view2.borderColor = .red
            self.txtAddress2.errorHandler("Please enter Building/Apartment No!", .red)
            return
        }
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewAddressPopUp") as! NewAddressPopUp
        if addressData != nil{
            vc.addressData = addressData
        }else{
            vc.address = self.txtAddress.text ?? ""
        }
        vc.delegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func addressPlaceId(id: String,address:String) {
        
        let placeClient = GMSPlacesClient()
        
        placeClient.lookUpPlaceID(id) { (place, error) -> Void in
            if let place = place {
                //Map Config
                self.txtAddress.text = address
                self.landmark = place.formattedAddress
                self.addressData?.address = place.formattedAddress
                self.latitude = place.coordinate.latitude.description
                self.longitude = place.coordinate.longitude.description
                self.configureMap(self.latitude ?? "", self.longitude ?? "")
            }
        }
    }
    
    func addressType(type: String) {
        print(type)
        Indicator.shared.showLottieAnimation(view: view)
        var param = ["address":self.txtAddress.text ?? "",
                     "landmark":self.landmark ?? "",
                     "buildingAndApart":self.txtAddress2.text ?? "",
                     "addressType":type,
                     "latitude":self.latitude ?? "",
                     "longitude":self.longitude ?? ""] as [String:Any]
        if addressData != nil{
            // addressId
            param["addressId"] = addressData?.Id
        }
        self.newAddressVM.addAddressService( addressData != nil ? "updateAddress" : "addAddress", param)
        
        newAddressVM.updateLoadingStatus = {
            if !(self.newAddressVM.isLoading){
                Indicator.shared.hideLoaderPresent()
            }
        }
        
        newAddressVM.showAlertClosure = {
            if let error = self.newAddressVM.error {
                print(error.localizedDescription)
                self.handleError(error.localizedDescription)
            }else if let statusError = self.newAddressVM.statusMessage{
                self.handleError(statusError)
            }
        }
        newAddressVM.didFinishFetch = {
            Indicator.shared.hideLoaderPresent()
            self.alertWithHandler(message: ("\((self.addressData != nil) ? "Updated" : "Address") added successfully")) {
                self.navigationController?.popViewController(animated: false)
            }
            
        }
        
    }
  
    
}
//MARK:- Custom Methods
extension NewAddressVC:GMSMapViewDelegate{
    
    func initiallizers() {
        //  transparentNavigation()
        let transparentButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        transparentButton.setImage(#imageLiteral(resourceName: "back_side"), for: .normal)
        transparentButton.borderWidth = 0.7
        transparentButton.borderColor = .white
        transparentButton.backgroundColor = .clear
        transparentButton.contentHorizontalAlignment = .center
        transparentButton.tag = UINavigationBarButtonType.backHomeNAll.rawValue
        transparentButton.addTarget(self, action: #selector(self.navigationButtonTapped(_:)), for: .touchUpInside)
        transparentButton.cornerRadius = 8
        let title = UILabel()
        title.text = comClass.createString(Str: "Update your Location")
        title.textColor = AppColor.textcolor
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .left
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: transparentButton)
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
        var search = UISearchController(searchResultsController: nil)
        //setup search result table
        let table = SearchResult()
        table.delegate = self
        search = UISearchController(searchResultsController: table)
        search.searchResultsUpdater = table as? UISearchResultsUpdating
        search.searchBar.sizeToFit()
        search.searchBar.setImage(#imageLiteral(resourceName: "search"), for: .search, state: .normal)
        if #available(iOS 13.0, *) {
            search.automaticallyShowsCancelButton = false
        } else {
            // Fallback on earlier versions
        }
        //  UISearchBar.appearance().tintColor = UIColor(named: "Button") // using this to set the text color of the 'Cancel' button since the search bar ignores the global tint color property for some reason
        
        if #available(iOS 11.0, *) {
            
            // Search bar placeholder text color
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedString.Key.foregroundColor: AppColor.lightGrayApp])
            
            // Search bar text color
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
            
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor.white
            // Insertion cursor color
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = UIColor.black
            
        }
        
        // Search bar clear icon
        UISearchBar.appearance().setImage(UIImage(named: "clear"), for: .clear, state: .normal)
        UISearchBar.appearance().shadowColor = UIColor.black
        UISearchBar.appearance().shadowOpacity = 0.2
        UISearchBar.appearance().shadowRadius = 5
        self.navigationItem.searchController = search
    }
    
    //MARK:- Google Map Configuration
    
    func configureMap(_ latitude:String,_ longitude:String){
        self.mpMapView.clear()
        let location = CLLocationCoordinate2D(latitude: (latitude as NSString).doubleValue, longitude: (longitude as NSString).doubleValue)
        let camera = GMSCameraPosition.camera(withLatitude: (latitude as NSString).doubleValue, longitude: (longitude as NSString).doubleValue, zoom: 14) //Set default lat and long
        self.mpMapView.camera = camera
        let marker = GMSMarker()
        marker.icon = #imageLiteral(resourceName: "pin")
        marker.iconView?.isUserInteractionEnabled = true
        marker.position = location
        marker.map = self.mpMapView
        self.mpMapView.delegate = self
        

    }
    
    
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.mpMapView.clear()
        self.latitude = position.target.latitude.description
        self.longitude = position.target.longitude.description
        let marker = GMSMarker()
        marker.icon = #imageLiteral(resourceName: "pin")
        marker.iconView?.isUserInteractionEnabled = true
        CATransaction.begin()
        CATransaction.setAnimationDuration(2.0)
        marker.position = position.target
        CATransaction.commit()
        marker.map = self.mpMapView
        self.parseAddress(latitude: position.target.latitude, logitude: position.target.longitude) { (address,subLocality) in
            self.txtAddress.text = address
            self.landmark = subLocality
            self.viewWillLayoutSubviews()
        }
    }
    
    func addressParser(latitude: String, longitude : String){
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=AIzaSyBqkaQe-laP88wY3T0HZ5VVo2naXl59jx8"
        AF.request(url).validate().responseJSON { response in
               switch response.result {
               case .success:
                if let data = response.data{
                    self.parseAddressModel = AddressParserModel(JSON(data))
                    var formatedAddress = ""
                    self.parseAddressModel?.results?.first?.addressComponents?.forEach({item in
                        if !(item.longName?.contains("Unnamed") ?? false){
                            formatedAddress.append(item.longName ?? "")
                            formatedAddress.append(",")
                        }
                    })
                    self.txtAddress.text = formatedAddress
                    self.landmark = formatedAddress
                    self.viewWillLayoutSubviews()

                }
               case .failure(let error):
                   print(error)
               }
           }
        
    }
}
