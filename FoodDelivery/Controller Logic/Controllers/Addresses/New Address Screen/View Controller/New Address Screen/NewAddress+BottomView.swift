//
//  NewAddress+BottomView.swift
//  FoodDelivery
//
//  Created by Cst on 5/6/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass
import CoreLocation
import MapKit
import Alamofire
import SwiftyJSON
import GoogleMaps
import GooglePlaces
// MARK: - View Controller
class ViewController: UIViewController {
    
    // MARK: - Constants
    
    private let popupOffset: CGFloat = 440

    // MARK: - Views
    
    private lazy var contentImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "content")
        return imageView
    }()
    
    private lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0
        return view
    }()
    
    private lazy var popupView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 10
        return view
    }()
    
    private lazy var closedTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Reviews"
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        label.textColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var openTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Reviews"
        label.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.heavy)
        label.textColor = .black
        label.textAlignment = .center
        label.alpha = 0
        label.transform = CGAffineTransform(scaleX: 0.65, y: 0.65).concatenating(CGAffineTransform(translationX: 0, y: -15))
        return label
    }()
    
    private lazy var reviewsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "compressed_rtdk")
        return imageView
    }()
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layout()
        popupView.addGestureRecognizer(panRecognizer)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Layout
    
    private var bottomConstraint = NSLayoutConstraint()
    
    private func layout() {
        
        contentImageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(contentImageView)
        contentImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        contentImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        contentImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        contentImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(overlayView)
        overlayView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        overlayView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        overlayView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        overlayView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        popupView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(popupView)
        popupView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        popupView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bottomConstraint = popupView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: popupOffset)
        bottomConstraint.isActive = true
        popupView.heightAnchor.constraint(equalToConstant: 500).isActive = true
        
        closedTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        popupView.addSubview(closedTitleLabel)
        closedTitleLabel.leadingAnchor.constraint(equalTo: popupView.leadingAnchor).isActive = true
        closedTitleLabel.trailingAnchor.constraint(equalTo: popupView.trailingAnchor).isActive = true
        closedTitleLabel.topAnchor.constraint(equalTo: popupView.topAnchor, constant: 20).isActive = true
        
        openTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        popupView.addSubview(openTitleLabel)
        openTitleLabel.leadingAnchor.constraint(equalTo: popupView.leadingAnchor).isActive = true
        openTitleLabel.trailingAnchor.constraint(equalTo: popupView.trailingAnchor).isActive = true
        openTitleLabel.topAnchor.constraint(equalTo: popupView.topAnchor, constant: 30).isActive = true
        
        reviewsImageView.translatesAutoresizingMaskIntoConstraints = false
        popupView.addSubview(reviewsImageView)
        reviewsImageView.leadingAnchor.constraint(equalTo: popupView.leadingAnchor).isActive = true
        reviewsImageView.trailingAnchor.constraint(equalTo: popupView.trailingAnchor).isActive = true
        reviewsImageView.bottomAnchor.constraint(equalTo: popupView.bottomAnchor).isActive = true
        reviewsImageView.heightAnchor.constraint(equalToConstant: 428).isActive = true
        
    }
    
    // MARK: - Animation
    
    /// The current state of the animation. This variable is changed only when an animation completes.
    private var currentState: State = .closed
    
    /// All of the currently running animators.
    private var runningAnimators = [UIViewPropertyAnimator]()
    
    /// The progress of each animator. This array is parallel to the `runningAnimators` array.
    private var animationProgress = [CGFloat]()
    
    private lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(popupViewPanned(recognizer:)))
        return recognizer
    }()
    
    /// Animates the transition, if the animation is not already running.
    private func animateTransitionIfNeeded(to state: State, duration: TimeInterval) {
        
        // ensure that the animators array is empty (which implies new animations need to be created)
        guard runningAnimators.isEmpty else { return }
        
        // an animator for the transition
        let transitionAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1, animations: {
            switch state {
            case .open:
                self.bottomConstraint.constant = 0
                self.popupView.layer.cornerRadius = 20
                self.overlayView.alpha = 0.5
                self.closedTitleLabel.transform = CGAffineTransform(scaleX: 1.6, y: 1.6).concatenating(CGAffineTransform(translationX: 0, y: 15))
                self.openTitleLabel.transform = .identity
            case .closed:
                self.bottomConstraint.constant = self.popupOffset
                self.popupView.layer.cornerRadius = 0
                self.overlayView.alpha = 0
                self.closedTitleLabel.transform = .identity
                self.openTitleLabel.transform = CGAffineTransform(scaleX: 0.65, y: 0.65).concatenating(CGAffineTransform(translationX: 0, y: -15))
            }
            self.view.layoutIfNeeded()
        })
        
        // the transition completion block
        transitionAnimator.addCompletion { position in
            
            // update the state
            switch position {
            case .start:
                self.currentState = state.opposite
            case .end:
                self.currentState = state
            case .current:
                ()
            }
            
            // manually reset the constraint positions
            switch self.currentState {
            case .open:
                self.bottomConstraint.constant = 0
            case .closed:
                self.bottomConstraint.constant = self.popupOffset
            }
            
            // remove all running animators
            self.runningAnimators.removeAll()
            
        }
        
        // an animator for the title that is transitioning into view
        let inTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeIn, animations: {
            switch state {
            case .open:
                self.openTitleLabel.alpha = 1
            case .closed:
                self.closedTitleLabel.alpha = 1
            }
        })
        inTitleAnimator.scrubsLinearly = false
        
        // an animator for the title that is transitioning out of view
        let outTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeOut, animations: {
            switch state {
            case .open:
                self.closedTitleLabel.alpha = 0
            case .closed:
                self.openTitleLabel.alpha = 0
            }
        })
        outTitleAnimator.scrubsLinearly = false
        
        // start all animators
        transitionAnimator.startAnimation()
        inTitleAnimator.startAnimation()
        outTitleAnimator.startAnimation()
        
        // keep track of all running animators
        runningAnimators.append(transitionAnimator)
        runningAnimators.append(inTitleAnimator)
        runningAnimators.append(outTitleAnimator)
        
    }
    
    @objc private func popupViewPanned(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            
            // start the animations
            animateTransitionIfNeeded(to: currentState.opposite, duration: 1)
            
            // pause all animations, since the next event may be a pan changed
            runningAnimators.forEach { $0.pauseAnimation() }
            
            // keep track of each animator's progress
            animationProgress = runningAnimators.map { $0.fractionComplete }
            
        case .changed:
            
            // variable setup
            let translation = recognizer.translation(in: popupView)
            var fraction = -translation.y / popupOffset
            
            // adjust the fraction for the current state and reversed state
            if currentState == .open { fraction *= -1 }
            if runningAnimators[0].isReversed { fraction *= -1 }
            
            // apply the new fraction
            for (index, animator) in runningAnimators.enumerated() {
                animator.fractionComplete = fraction + animationProgress[index]
            }
            
        case .ended:
            
            // variable setup
            let yVelocity = recognizer.velocity(in: popupView).y
            let shouldClose = yVelocity > 0
            
            // if there is no motion, continue all animations and exit early
            if yVelocity == 0 {
                runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
                break
            }
            
            // reverse the animations based on their current state and pan motion
            switch currentState {
            case .open:
                if !shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            case .closed:
                if shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if !shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            }
            
            // continue all animations
            runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
            
        default:
            ()
        }
    }
    
}

// MARK: - InstantPanGestureRecognizer
/// A pan gesture that enters into the `began` state on touch down instead of waiting for a touches moved event.
class InstantPanGestureRecognizer: UIPanGestureRecognizer {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        if (self.state == UIGestureRecognizer.State.began) { return }
        super.touchesBegan(touches, with: event)
        self.state = UIGestureRecognizer.State.began
    }
    
}





@objc protocol MapInfoDelegate {
    @objc optional func mapInfoData(data:MKMapItem)
    @objc optional func addressType(type:String)
    @objc optional func addressPlaceId(id:String,address:String)
}

extension MapInfoDelegate{
    
    func locationPermission(complition:@escaping (String) -> ()){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                //  UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
                complition(("No access"))
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                complition(("Access"))
            @unknown default:
                break
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    /// Geo Code Address Parsing
    
//    /// - Returns: Address on String
//    func parseAddress(selectItem: MKPlacemark? = nil,latitude: Double? = nil,logitude: Double? = nil,complition:@escaping (String,String) -> ()){
//        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude ?? 0),\(logitude ?? 0)&key=AIzaSyBqkaQe-laP88wY3T0HZ5VVo2naXl59jx8"
//        AF.request(url).validate().responseJSON { response in
//            switch response.result {
//            case .success:
//                if let data = response.data{
//                    var formatedAddress = ""
//                    AddressParserModel(JSON(data)).results?.first?.addressComponents?.forEach({item in
//                        if !(item.longName?.contains("Unnamed") ?? false){
//                            formatedAddress.append(item.longName ?? "")
//                            formatedAddress.append(",")
//                        }
//                    })
//
//                    if AddressParserModel(JSON(data)).results?.first?.addressComponents?.first?.longName?.contains("Unnamed") ?? false{
//                        complition(formatedAddress,AddressParserModel(JSON(data)).results?.first?.addressComponents?[1].longName ?? "")
//                    }else{
//                        complition(formatedAddress,AddressParserModel(JSON(data)).results?.first?.addressComponents?.first?.longName ?? "")
//                    }
//
//
//                }
//            case .failure(let error):
//                print(error)
//            }
//        }
//    }
    
    func parseAddress(selectItem: MKPlacemark? = nil,latitude: Double? = nil,logitude: Double? = nil,complition:@escaping (String,String) -> ()){
       
        
        let cordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude ?? 0.0, longitude: logitude ?? 0.0)
        
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(cordinate) { response, error in
            
            if let address = response?.results() {
                let lines = address.first
                if let addressNew = lines?.lines {
                    let address = self.makeAddressString(inArr: addressNew)
                    
                    if let subLocality = lines?.subLocality{
                        complition(address.replacingOccurrences(of: "Unnamed Road,", with: ""),subLocality)
                    }
                    else if let locality = lines?.locality{
                        complition(address.replacingOccurrences(of: "Unnamed Road,", with: ""),locality)
                    }
                }
            }
        }
    }
    
    func makeAddressString(inArr:[String]) -> String {
        
        var fVal:String = ""
        for val in inArr {
            fVal =  fVal + val + " "
        }
        return fVal
    }
}

//MARK:- Address Search TableView Class
//MARK:-
class SearchResult:ParentViewController{
    
    var mapView: MKMapView? = nil
    var delegate: MapInfoDelegate?
    lazy var tableView:UITableView = {
        let table = UITableView()
        table.backgroundColor = .clear
       // table.allowsSelection = false
        table.separatorStyle = .singleLine
        return table
    }()
    
    // Injection
    let searchResultVM = SearchResultViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(tableView)
        tableView.frame = view.bounds
        tableView.dataSource = self
        tableView.delegate = self
        self.searchResultVM.setup(tableView: self.tableView)

    }

}
//MARK:- UITableView Datasource
extension SearchResult:UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //self.searchResultVM.mapItem?.count ?? 0
        self.searchResultVM.placeData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.searchResultVM.cellInstance(tableView, indexPath: indexPath)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70

    }
}
//MARK:- UITableView Delegate
extension SearchResult:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
           // self.delegate?.mapInfoData!(data: (self.searchResultVM.mapItem?[indexPath.row])!)
            self.delegate?.addressPlaceId?(id: self.searchResultVM.placeData[indexPath.row].place_id ?? "", address: self.searchResultVM.placeData[indexPath.row].description ?? "")
        }
    }
}
//MARK:- UISearchResultsUpdating
extension SearchResult: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        self.searchResultVM.updateSearchResult(for: searchController)
        self.searchResultVM.didFinishFetch = {
            self.tableView.reloadData()
        }
    }
}
//MARK:- SearchResultCell
//MARK:-
class SearchResultCell:UITableViewCell{
    
    lazy var lblAddress: UILabel = {
        let lable = UILabel()
        lable.numberOfLines = 0
       // lable.frame = CGRect(x: 20, y: 0, width: UIScreen.main.bounds.size.width - 40, height: 40)
        lable.font = AppFont.Medium.size(.Poppins, size: 15)
        lable.textColor = AppColor.pink
        contentView.addSubview(lable)
        lable.translatesAutoresizingMaskIntoConstraints = false
        
        return lable
    }()
    
    lazy var pin:UIImageView = {
        let image = UIImageView(image: #imageLiteral(resourceName: "pin"))
        image.contentMode = .scaleAspectFit
        contentView.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    func setupCell(){

        NSLayoutConstraint.activate([pin.widthAnchor.constraint(equalToConstant: 30),
                                     pin.heightAnchor.constraint(equalToConstant: 30),
                                     pin.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
                                     pin.centerYAnchor.constraint(equalTo: lblAddress.centerYAnchor)])
        
        NSLayoutConstraint.activate([lblAddress.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
                                     lblAddress.leadingAnchor.constraint(equalTo: pin.trailingAnchor, constant: 10),
                                     lblAddress.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
                                     lblAddress.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)])
        
    }
}
//MARK:- SearchResult ViewModel
//MARK:-
class SearchResultViewModel:BaseViewModel,CellRepresentable{
    
    var rowHeight: CGFloat = 45
    let googleAutoCompeteApi = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=establishment|geocode&location=%@,%@&radius=500&language=en&key=%@"
    var arrPlaces = NSMutableArray(capacity: 100)
    let operationQueue = OperationQueue()
    let googleServerkey = "AIzaSyBqkaQe-laP88wY3T0HZ5VVo2naXl59jx8"
    var currentLat = comClass.getInfoById().latitude ?? ""
    var currentLong = comClass.getInfoById().longitude ?? ""
    var comingFrom = String()
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        tableView.register(SearchResultCell.self, forCellReuseIdentifier: SearchResultCell.description())
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultCell.description()) as! SearchResultCell
        cell.setupCell()
        cell.lblAddress.attributedText = GlobalMethods.shared.provideAttributedTextToControlLeftAllignCenter((self.placeData[indexPath.row].name ?? ""),  self.placeData[indexPath.row].description ?? "", AppFont.Medium.size(.Poppins, size: 16), AppFont.Medium.size(.Poppins, size: 12), AppColor.black, AppColor.lightGrayApp,"\n",.left)
        return cell
    }
    
    var mapItem:[MKMapItem]?{
        didSet{
            self.didFinishFetch?()
        }
    }
    var placeData = [PlaceDataModel]()
      
    private var tableView:UITableView?
    private var mapView:MKMapView?
    
    
    func setup(tableView:UITableView){
        self.tableView = tableView
    }
    
    func updateSearchResult(for searchController: UISearchController) {
        let searchBarText = searchController.searchBar.text ?? ""
        self.forwardGeoCoding(searchTexts: searchBarText)
    }
    func updateSearchResult1(for text: String) {
        let searchBarText = text
        self.forwardGeoCoding(searchTexts: searchBarText)
    }

    func parseAddress(selectItem: MKPlacemark) -> String{
        let firstSpace = ((selectItem.subThoroughfare != nil) && (selectItem.thoroughfare != nil)) ? " ": ""
        let comma = (selectItem.subThoroughfare != nil || selectItem.thoroughfare != nil) && (selectItem.subAdministrativeArea != nil || selectItem.administrativeArea != nil) ? ", ":""
        let secondSpace = (selectItem.subAdministrativeArea != nil && selectItem.administrativeArea != nil) ? " ":""
        let addressLine = String(format: "%@%@%@%@%@%@%@",
                                 // street number
                                 selectItem.subThoroughfare ?? "",
                                 firstSpace,
                                 //street name
                                 selectItem.thoroughfare ?? "",
                                 comma,
                                 // city
                                 selectItem.locality ?? "",
                                 secondSpace,
                                 //state
                                 selectItem.administrativeArea ?? ""
        )
        return addressLine
    }
    
    
    //MARK: - Search place from Google -
    
    
    func forwardGeoCoding(searchTexts:String) {
        googlePlacesResult(input: searchTexts) { (result) -> Void in
            let searchResult:NSDictionary = ["keyword":searchTexts,"results":result]
            if result.count > 0
            {
                let features = searchResult.value(forKey: "results") as! [AnyObject]
                self.arrPlaces = NSMutableArray(capacity: 100)
                self.placeData.removeAll()
                for dictAddress in features   {
                    if let content = dictAddress.value(forKey:"description") as? String {
                        
                        let place_id = dictAddress.value(forKey:"place_id") as? String ?? ""
                        
                        self.arrPlaces.addObjects(from: [content])
                        self.placeData.append(PlaceDataModel(name: content, country: "", description: content,place_id: place_id))
                    }
                }
                DispatchQueue.main.async {
                    self.didFinishFetch?()
                }
            }
        }
    }
    
    func googlePlacesResult(input: String, completion: @escaping (_ result: NSArray) -> Void) {
        let searchWordProtection = input.replacingOccurrences(of: " ", with: "")
        if searchWordProtection.count != 0 {
            let urlString = NSString(format: googleAutoCompeteApi as NSString,input,currentLat,currentLong,googleServerkey)
            let url = NSURL(string: urlString.addingPercentEscapes(using: String.Encoding.utf8.rawValue)!)
            let defaultConfigObject = URLSessionConfiguration.default
            let delegateFreeSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: OperationQueue.main)
            let request = NSURLRequest(url: url! as URL)
            let task =  delegateFreeSession.dataTask(with: request as URLRequest,completionHandler: {
                (data, response, error) -> Void in
                if let data = data {
                    do {
                        let jSONresult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                        let results:NSArray = jSONresult["predictions"] as! NSArray
                        let status = jSONresult["status"] as! String
                        
                        if status == "NOT_FOUND" || status == "REQUEST_DENIED" {
                            let userInfo:NSDictionary = ["error": jSONresult["status"]!]
                            
                            let newError = NSError(domain: "API Error", code: 666, userInfo: userInfo as [NSObject : AnyObject] as [NSObject : AnyObject] as? [String : Any])
                            let arr:NSArray = [newError]
                            completion(arr)
                            return
                        } else {
                            completion(results)
                        }
                    }
                    catch {
                        print("json error: \(error)")
                    }
                } else if error != nil {
                    // print(error.description)
                }
            })
            task.resume()
        }
    }
    
}


protocol AddressbackDelegate {
    func address(address:String,subAddress:String)
}
struct PlaceDataModel {
    var name: String?
    var country: String?
    var description: String?
    var place_id: String?
    var lat: String?
    var long: String?
}
