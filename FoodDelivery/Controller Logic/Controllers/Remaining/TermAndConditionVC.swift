//
//  TermAndConditionVC.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class TermAndConditionVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDesc:UILabel!
    
    //MARK:- Properties
    
    var data:StaticData?
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .always
        self.transparentNavigation()
        self.setupNavigationBarTitle( data?.title ?? "", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationItem.largeTitleDisplayMode = .always
    }
}


//MARK:- Custom Methods
extension TermAndConditionVC {
    func initiallizers() {
        self.lblDesc.text = data?.description
    }
}



