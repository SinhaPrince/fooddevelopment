//
//  NotificationVC.swift
//  FoodDelivery
//
//  Created by call soft on 04/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import SkeletonView
class NotificationVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblVw: UITableView!
    
    //MARK:- Properties
    let clearButton = UIButton()

    //MARK:- Injection
    let NotiVM = NotificationVM(networking: ApiManager())
    
    lazy var noData: UILabel = {
        let lbl = UILabel()
        lbl.setup("No Notification Yet!", AppColor.appLightBlack, AppFont.Medium.size(.Poppins, size: 16), .center)
        lbl.frame = view.bounds
        lbl.isHidden = true
        view.bringSubviewToFront(lbl)
        view.addSubview(lbl)
        return lbl
    }()
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if comClass.isDataExist(){
            self.tblVw.register(UINib(nibName: "NotificationTableCell", bundle: nil), forCellReuseIdentifier: "NotificationTableCell")
            self.tblVw.dataSource = self
            self.tblVw.isSkeletonable = true
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight, duration: 0.4, autoreverses: true)
            self.tblVw.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7215660045)), animation: animation, transition: .crossDissolve(0.5))
        }
        initiallizers()
        setupNavigation()
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "Notification")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .left
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
        clearButton.isHidden = true
        clearButton.setTitle(comClass.createString(Str: "Clear all"), for: .normal)
        clearButton.setTitleColor(AppColor.black, for: .normal)
        clearButton.titleLabel?.font = AppFont.Medium.size(.Poppins, size: 14)
        clearButton.addTarget(self, action: #selector(clearButtonTap), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: clearButton)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //tblVw.rowHeight = tblVw.contentSize.height
        
    }
    
    /// Notification Clear Service
    @objc func clearButtonTap(){
        Indicator.shared.showLottieAnimation(view: view)
        self.NotiVM.ClearNotificationservice(APIEndpoint.clearNotification)
        self.NotiVM.updateLoadingStatus = {
            let _ = self.NotiVM.isLoading ? self.activityIndicatorStart() : self.activityIndicatorStop()
        }
        NotiVM.showAlertClosure = {
            if self.NotiVM.statusCode == 200{
               self.networkCalling()
           }else if let error = self.NotiVM.error {
                print(error.localizedDescription)
                self.handleError(error.localizedDescription)
            }else if let statusError = self.NotiVM.statusMessage{
                self.handleError(statusError)
            }
        }
    }
}


//MARK:- Network Calling from ViewModel
extension NotificationVC {
    
    func initiallizers() {
        if comClass.isDataExist(){
            networkCalling()
        }else{
            self.clearButton.isHidden = true
            self.noData.isHidden = false
        }
       
    }
    
    func networkCalling(){
        // Fetch Notification Data
        Indicator.shared.showLottieAnimation(view: view)

        self.NotiVM.fetchNotificationService(APIEndpoint.getNotificationList)
        
        NotiVM.updateLoadingStatus = {
            let _ = self.NotiVM.isLoading ? self.activityIndicatorStart() : self.activityIndicatorStop()
        }
        
        NotiVM.showAlertClosure = {
            if let error = self.NotiVM.error {
                print(error.localizedDescription)
                self.handleError(error.localizedDescription)
            }else if let statusError = self.NotiVM.statusMessage{
                self.handleError(statusError)
            }
        }
        
        NotiVM.didFinishFetch = {
            if self.NotiVM.notificationData?.data?.count ?? 0 > 0 {
                self.clearButton.isHidden = false
                self.noData.isHidden = true

            }else{
                self.clearButton.isHidden = true
                self.noData.isHidden = false
            }
            self.configureTable()
        }
        
    }
   
    // MARK: - UI Setup
    private func activityIndicatorStart() {
        // Code for show activity indicator view
        // ...
        Indicator.shared.showLottieAnimation(view: view)
        print("start")
    }
    
    private func activityIndicatorStop() {
        // Code for stop activity indicator view
        // ...
        Indicator.shared.hideLoaderPresent()
        print("stop")
    }
}
//MARK:- UITableViewDelegate and UITableViewDatasource
extension NotificationVC :  UITableViewDataSource,SkeletonTableViewDataSource{
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int{
        5
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "NotificationTableCell"
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.NotiVM.notificationData?.data?.count ?? 0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.NotiVM.cellInstance(self.tblVw, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.NotiVM.rowHeight
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.NotiVM.rowHeight
    }
    func configureTable() {
        tblVw.dataSource = self
        tblVw.reloadData()
        self.tblVw.stopSkeletonAnimation()
        self.tblVw.hideSkeleton()
    }
}

