//
//  NotificationModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 12, 2021
//
import Foundation
import SwiftyJSON

class NotificationModel {

	var status: Int?
	var message: String?
	var data: [NoticationInfo]?

	init(_ json: JSON) {
		status = json["status"].intValue
		message = json["message"].stringValue
		data = json["data"].arrayValue.map { NoticationInfo($0) }
	}

}
