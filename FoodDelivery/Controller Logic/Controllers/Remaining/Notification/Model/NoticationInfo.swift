//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 12, 2021
//
import Foundation
import SwiftyJSON

class NoticationInfo {

	var status: String?
	var isSeen: Bool?
	var deleteStatus: Bool?
	var Id: String?
	var notiTo: String?
	var notiTitle: String?
	var notiMessage: String?
	var notiType: String?
	var type: String?
	var createdAt: String?
	var updatedAt: String?
	var _v: Int?

	init(_ json: JSON) {
		status = json["status"].stringValue
		isSeen = json["isSeen"].boolValue
		deleteStatus = json["deleteStatus"].boolValue
		Id = json["_id"].stringValue
		notiTo = json["notiTo"].stringValue
		notiTitle = json["notiTitle"].stringValue
		notiMessage = json["notiMessage"].stringValue
		notiType = json["notiType"].stringValue
		type = json["type"].stringValue
		createdAt = json["createdAt"].stringValue
		updatedAt = json["updatedAt"].stringValue
		_v = json["__v"].intValue
	}

}
