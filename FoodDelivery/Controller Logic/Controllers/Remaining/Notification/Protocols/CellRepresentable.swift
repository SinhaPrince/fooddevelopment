//
//  CellRepresentable.swift
//  FoodDelivery
//
//  Created by Cst on 4/12/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

@objc protocol CellRepresentable {
    var rowHeight: CGFloat { get }
    @objc optional func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
    @objc optional func collectionCellInstance1(_ tableView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell
}

