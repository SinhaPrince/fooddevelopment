//
//  NotificationVM.swift
//  FoodDelivery
//
//  Created by Cst on 4/12/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
class BaseViewModel: NSObject {
    var error: Error? {
        didSet { self.showAlertClosure?() }
    }
    var statusCode:Int?{
        didSet { self.showAlertClosure?() }
    }
    var statusMessage:String?{
        didSet { self.showAlertClosure?() }
    }
    var isLoading: Bool = false {
        didSet { self.updateLoadingStatus?() }
    }
    // MARK: - Closures for callback, since we are not using the ViewModel to the View.
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var didFinishFetch: (() -> ())?
}

class NotificationVM: BaseViewModel, CellRepresentable{
    
    var rowHeight: CGFloat = UITableView.automaticDimension
    var notificationData:NotificationModel?{
        didSet{
            self.didFinishFetch?()
        }
    }
   
    private var dataService: ApiManager?
    
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "NotificationTableCell", bundle: nil), forCellReuseIdentifier: "NotificationTableCell")
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableCell") as? NotificationTableCell else {
            return UITableViewCell()
        }

        cell.item = self.notificationData?.data?[indexPath.row]
       return cell
    }
    
    //MARK:- Network Call
    
    /// Get Notification List Service
    /// - Parameter url: Api EndPoint
    func fetchNotificationService(_ url:String){
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        self.isLoading = true
        self.dataService?.fetchApiService(method: .get, url: url, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                self.notificationData = NotificationModel(data)
                if self.notificationData?.status != 200{
                    self.statusMessage = self.notificationData?.message
                }
                self.error = nil
                self.isLoading = false
                break
            case .failure(let error):
                self.error = error
                self.isLoading = false
                break
            }
        })

    }
    
    func ClearNotificationservice(_ url:String){
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        self.dataService?.fetchApiService(method: .get, url: url, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                self.error = nil
                self.isLoading = false
                if data["status"].intValue == 200{
                    self.statusCode = data["status"].intValue
                }
                self.statusMessage = data["message"].stringValue
                break
            case .failure(let error):
                self.error = error
                self.isLoading = false
                break
            }
        })

    }
}
