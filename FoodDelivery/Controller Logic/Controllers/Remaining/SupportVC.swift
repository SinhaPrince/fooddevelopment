//
//  SupportVC.swift
//  FoodDelivery
//
//  Created by call soft on 05/04/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class SupportVC: ParentViewController {
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallizers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .always
        self.transparentNavigation()
        self.setupNavigationBarTitle( "Support", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationItem.largeTitleDisplayMode = .always
    }
    
    //MARK:- Button Action
   
    @IBAction func btnActionCall(_ sender: UIButton) {
        makePhoneCall(phoneNumber:"1234567890")
    }
    
    @IBAction func btnActionMail(_ sender: UIButton) {
        print("Send Mail")
//        let mailId = "support@bite.me.com"
//        if let Url = URL(string: "mailto:\(mailId)"){
//            UIApplication.shared.open(Url, options: [:], completionHandler: nil)
//        }
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["support@bite.me.com"])
            mail.setMessageBody("<p>This is test Mail!</p>", isHTML: true)
            present(mail, animated: true, completion: nil)
        }
        else
        {
            let email = "support@bite.me.com"
            if let newurl = URL(string: "mailto:\(email)") {
                UIApplication.shared.open(newurl, options: [:], completionHandler: nil)
            }
        }
        
        
        //sendEmail()
    }
    
    
    
    
}


//MARK:- Custom Methods
extension SupportVC : MFMailComposeViewControllerDelegate {
    func initiallizers() {
        
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
            controller.dismiss(animated: true)
            switch result {
            case .cancelled:
                print("Mail cancelled")
            case .saved:
                print("Mail saved")
            case .sent:
                self.allertInfo(_title: "Mail Info", message: "Mail is sent successfuly", actionTitle: "OK")
                print("Mail sent")
            case .failed:
                self.allertInfo(_title: "Mail Info", message: "Mail isn't sent.",actionTitle: "OK")
                print("Mail sent failure: \(error?.localizedDescription)")
            default:
                break
            }

        }

        func allertInfo(_title:String, message:String, actionTitle:String) {

            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
       
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)

        }
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["support@bite.me.com"])
            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)

            present(mail, animated: true)
        } else {
            self.show_Alert(message: "Can't send mail")
        }
    }

  
    
    //MARK : Funcation to make a Call
    func makePhoneCall(phoneNumber: String) {
        if let phoneURL = NSURL(string: ("tel://" + phoneNumber)) {
            print(phoneURL)
            let alert = UIAlertController(title: ("Call " + phoneNumber  + "?"), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Call", style: .default, handler: { (action) in
                UIApplication.shared.open(phoneURL as URL)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}




