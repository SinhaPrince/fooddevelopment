//
//  FAQVC.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class FAQVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblVw: UITableView!
    
    //MARK:- Properties
    var arrData = ["Lorem ipsum dolor sit amet, consetetur sadipscing elitr ?","How to Upload Photos?","Lorem Ipsum is dummy text?","Lorem ipsum dolor sit amet, consetetur sadipscing elitr ?"]
    var selectedSection = -1
    
    var dataFAQ = [FaqModel]()
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallizers()
        tblVw.sectionHeaderHeight = UITableView.automaticDimension
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .always
        self.transparentNavigation()
        self.setupNavigationBarTitle( "FAQ's", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationItem.largeTitleDisplayMode = .always
    }
   
   
}


//MARK:- Custom Methods
extension FAQVC {
    func initiallizers() {
        fetchFAQService()
    }
    
    func fetchFAQService(){
        Indicator.shared.showLottieAnimation(view: view)
        ApiManager.instance.fetchApiService(method: .get, url: "getFaq") { (result) in
            Indicator.shared.hideLoaderPresent()
            switch result{
            case .success(let data):
                self.dataFAQ.append(FaqModel(data))
                if self.dataFAQ.first?.status == 200{
                    self.configureTable()
                }else{
                    self.handleError(self.dataFAQ.first?.message ?? "")
                }
                break
            case .failure(let error):
                self.handleError(error.localizedDescription)
                break
            }
        }
    }
}

//MARK:- UITableViewDelegate and UITableViewDatasource
extension FAQVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataFAQ.first?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == self.selectedSection {
            return 1
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("FAQHeader", owner: self, options: nil)?.first as! FAQHeader
        
        headerView.btnArrow.tag = section
        headerView.btnArrow.addTarget(self, action: #selector(MoveNext), for: .touchUpInside)
        if section == self.selectedSection {
            headerView.btnArrow.setImage(UIImage.init(named: "up_aa.jpg"), for: .normal)
        }
        else{
            headerView.btnArrow.setImage(UIImage.init(named: "down_arrow.jpg"), for: .normal)
        }
        headerView.lblTitle.text = self.dataFAQ.first?.data?[section].question
        return headerView
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : FAQTableCell = tblVw.dequeueReusableCell(withIdentifier: "FAQTableCell") as! FAQTableCell
        cell.item = self.dataFAQ.first?.data?[indexPath.section]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        //tblVw.backgroundColor = AppColor.clearColor
        tblVw.register(UINib.init(nibName: "FAQTableCell", bundle: nil), forCellReuseIdentifier: "FAQTableCell")
        tblVw.register(UINib.init(nibName: "FAQHeader", bundle: nil), forCellReuseIdentifier: "FAQHeader")
        tblVw.reloadData()
    }
    @objc func MoveNext(_ sender: UIButton) {
            print(sender.tag)
            if sender.tag == self.selectedSection {
                self.selectedSection = -1
            }else {
                self.selectedSection = sender.tag
            }
            self.tblVw.reloadData()
    }
}

import SwiftyJSON
struct FaqModel {

    let status: Int?
    let message: String?
    let data: [FaQData]?

    init(_ json: JSON) {
        status = json["status"].intValue
        message = json["message"].stringValue
        data = json["data"].arrayValue.map { FaQData($0) }
    }

}
struct FaQData {

    let status: String?
    let Id: String?
    let question: String?
    let answer: String?
    let createdAt: String?
    let updatedAt: String?
    let _v: Int?

    init(_ json: JSON) {
        status = json["status"].stringValue
        Id = json["_id"].stringValue
        question = json["question"].stringValue
        answer = json["answer"].stringValue
        createdAt = json["createdAt"].stringValue
        updatedAt = json["updatedAt"].stringValue
        _v = json["__v"].intValue
    }

}
