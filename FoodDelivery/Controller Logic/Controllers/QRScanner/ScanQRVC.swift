//
//  ScanQRVC.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

class ScanQRVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var bottomVw: UIView!
    @IBOutlet weak var scannerView: QRScannerView!{
        didSet {
            scannerView.delegate = self
        }
    }
    
    //MARK:- Properties
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //bottomVw.layer.maskedCorners = [.layerMinXMinYCorner,.layerMinXMaxYCorner]
        bottomVw.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        initiallizers()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            if !scannerView.isRunning {
            scannerView.startScanning()
        }
        self.hideNavigationBar(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideNavigationBar(false)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }
    //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionScan(_ sender: UIButton) {
        
    }
    @IBAction func btnActionSearch(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoyaltyPageVC") as! LoyaltyPageVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}


//MARK:- Custom Methods
extension ScanQRVC {
    func initiallizers() {
        
    }
    
    
    
}


extension ScanQRVC: QRScannerViewDelegate {
    func qrScanningDidStop() {
        
        print("stop scanning")
    }
    
    func qrScanningDidFail() {
        //presentAlert(withTitle: "Error", message: "Scanning Failed. Please try again")
        failed()
        self.show_Alert(message: "Scanning Failed. Please try again")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        
        if let dictData = str {
            print(dictData)
        }
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        dismiss(animated: true)
    }
    
    func found(code: String) {
        print(code)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}


