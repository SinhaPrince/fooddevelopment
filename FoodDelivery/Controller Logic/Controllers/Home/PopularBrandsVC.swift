//
//  PopularBrandsVC.swift
//  FoodDelivery
//
//  Created by call soft on 22/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class PopularBrandsVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var collVw: UICollectionView!
    
    
    //MARK:- Properties
    
    var popularBrands = [UIImage(named: "pizza.jpg")!,
    UIImage(named: "burger.jpg")!,
    UIImage(named: "subway.jpg")!,
    UIImage(named: "burger.jpg")!,
    UIImage(named: "pizza.jpg")!,UIImage(named: "pizza.jpg")!,
    UIImage(named: "burger.jpg")!,
    UIImage(named: "subway.jpg")!,
    UIImage(named: "burger.jpg")!,
    UIImage(named: "pizza.jpg")!,UIImage(named: "pizza.jpg")!,
    UIImage(named: "burger.jpg")!,
    UIImage(named: "subway.jpg")!,
    UIImage(named: "burger.jpg")!,
    UIImage(named: "pizza.jpg")!,UIImage(named: "pizza.jpg")!,
    UIImage(named: "burger.jpg")!,
    UIImage(named: "subway.jpg")!,
    UIImage(named: "burger.jpg")!,
    UIImage(named: "pizza.jpg")!]
    var orderPastNames = ["Tortilla Wrap","Fried Shrimps"]
    var orderPastPrice = ["AED 18.99","AED 29.00"]
    
    var orderType = "running"
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
        setupNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])

        let title = UILabel()
        title.text = comClass.createString(Str: "Popular Brands")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .left
        
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
       
    }
    //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func giveShadowToButton(btn:UIButton,shadowColor:UIColor,textColor:UIColor,backgroundColor:UIColor) {
        btn.setTitleColor(textColor, for: .normal)
        btn.backgroundColor = backgroundColor
        btn.shadowColor = shadowColor
        btn.shadowOffset = CGSize(width: 0, height: 8)
        btn.shadowOpacity = 1
        btn.shadowRadius = 5
    }
    
    
}

//MARK:- Custom Methods
extension PopularBrandsVC {
    func initiallizers() {
        
        //tblVw.reloadData()
        //configureTable()
        configureBrandsCollection()
    }
    
    
    func configureBrandsCollection(){
        
        collVw.delegate = self
        collVw.dataSource = self
        collVw.backgroundColor = AppColor.clearColor
        collVw.register(UINib.init(nibName: "BrandsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BrandsCollectionCell")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //layout.itemSize = CGSize(width: self.collVw.frame.width/4 + 1.25, height: 90)
        layout.itemSize = CGSize(width: 90, height: 90)
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        layout.scrollDirection = .vertical
        collVw.collectionViewLayout = layout
        collVw.reloadData()
        print("CollBrands",self.collVw.frame.width)
        print("Item Size",layout.itemSize)
    }
    
}


//MARK:- UICollectionViewDelegate,UICollectionViewDataSource
extension PopularBrandsVC : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return popularBrands.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : BrandsCollectionCell = collVw.dequeueReusableCell(withReuseIdentifier: "BrandsCollectionCell", for: indexPath) as! BrandsCollectionCell
        cell.imgBrands.image = popularBrands[indexPath.row]
        
        return cell
        
        
    }
}
//MARK:- UITableViewDelegate and UITableViewDatasource
//extension PopularBrandsVC : UITableViewDelegate, UITableViewDataSource{
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return popularBrands.count
//
//
//
//
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell : PopularBrandsTableCell = tblVw.dequeueReusableCell(withIdentifier: "PopularBrandsTableCell") as! PopularBrandsTableCell
//
//
//        //cell.vwOuter.cornerRadius = 30
//
//        cell.imgBrands.image = popularBrands[indexPath.row]
//
//        return cell
//
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 135
//    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 135
//    }
//    func configureTable() {
//        tblVw.delegate = self
//        tblVw.dataSource = self
//        tblVw.backgroundColor = AppColor.clearColor
//        tblVw.register(UINib.init(nibName: "PopularBrandsTableCell", bundle: nil), forCellReuseIdentifier: "PopularBrandsTableCell")
//
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
//         vc.isFrom = orderType
//        self.navigationController?.pushViewController(vc, animated: true)
//
//
//    }
//    @objc func TrackOrder(_sender:UIButton) {
//        print("Re-Order")
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
//        self.navigationController?.pushViewController(vc, animated: true)
//
//    }
//    @objc func CancelOrder(_sender:UIButton) {
//        print("Ratings")
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "FoodReviewVC") as! FoodReviewVC
//        self.navigationController?.pushViewController(vc, animated: true)
//
//    }
//
//}
