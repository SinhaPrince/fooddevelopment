//
//  CartCells.swift
//  FoodDelivery
//
//  Created by Cst on 5/3/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class CartCells: UITableViewCell {

    //MARK:- Outlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
