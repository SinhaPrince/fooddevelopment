//
//  CartVC.swift
//  FoodDelivery
//
//  Created by call soft on 03/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class CartVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var btnAddMore: UIButton!
    @IBOutlet weak var txtVwAddNote: UITextView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var itemList: UITableView!
    @IBOutlet weak var bottomVw: UIView!
    //MARK:- Properties
    var count = 0
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "Cart")
        title.textColor = AppColor.black
        btnAddMore.addTarget(self, action: #selector(btnActionEdit(_:)), for: .touchUpInside)
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .right
        title.numberOfLines = 0
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
       
    }
    
    //MARK:- Button Action

    @objc func btnActionEdit(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FoodPageVC") as! FoodPageVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActionCheckOut(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
    
}

//MARK:- Custom Methods
extension CartVC :UITextViewDelegate{
    func initiallizers() {
        txtVwAddNote.delegate = self
        configureViewTextView()
        bottomVw.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        configureTableView()
    }
}

// MARK: - TextFieldDelegate
extension CartVC {
    
    func configureViewTextView(){
        
        txtVwAddNote.text = "Message"
        txtVwAddNote.textColor = AppColor.placeHolderColor
        txtVwAddNote.delegate = self
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if txtVwAddNote.text == "Message"{
            txtVwAddNote.text = ""
            txtVwAddNote.textColor = UIColor.black
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if txtVwAddNote.text == ""
        {
            txtVwAddNote.text = "Message"
            txtVwAddNote.textColor =  AppColor.placeHolderColor
        }
        
    }
}

//MARK:- UITableDatasource
extension CartVC: UITableViewDataSource{
    
    func configureTableView(){
        self.itemList.dataSource = self
        self.itemList.reloadData()
        self.itemList.tableFooterView = footerView
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCells") as! CartCells
        cell.btnEdit.addTarget(self, action: #selector(btnActionEdit), for: .touchUpInside)
        return cell
    }
}
