//
//  OnItsWayVC.swift
//  FoodDelivery
//
//  Created by call soft on 04/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Foundation

class OnItsWayVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var vwBottom: UIView!
    //MARK:- Properties
    var fromLat = Double()
    var fromLong = Double()
    var controller = UIViewController()
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vwBottom.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        setupNavigation()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideNavigationBar(false)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionETA(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OrderTrackingVC") as! OrderTrackingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


//MARK:- Navigation setup
extension OnItsWayVC{
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "On its Way")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .left
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
        
    }
}
