//
//  AddNewCardVC.swift
//  FoodDelivery
//
//  Created by call soft on 09/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class AddNewCardVC: ParentViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var txtCardName: UITextField!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtCVV: UITextField!
    @IBOutlet weak var txtExpDate: UITextField!
    
    //MARK:- Properties
    var cardImg = [UIImage.init(named: "card.jpg"),UIImage.init(named: "card_a.jpg")]
    var tblData = ["8745********1452","6536********2536"]
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "Add New Card")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .right
        title.numberOfLines = 0
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
        
        let scanCard = UIButton()
        scanCard.setTitle("Scan Card", for: .normal)
        scanCard.setTitleColor(AppColor.black, for: .normal)
        scanCard.titleLabel?.font = AppFont.Medium.size(.Poppins, size: 16)
        scanCard.addTarget(self, action: #selector(btnActionScan), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: scanCard)
       
    }
    
    //MARK:- Button Action
    @objc func btnActionScan(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ScanQRVC") as! ScanQRVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionSave(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
  
}


