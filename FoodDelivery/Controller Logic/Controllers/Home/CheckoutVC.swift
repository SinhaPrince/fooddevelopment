//
//  CheckoutVC.swift
//  FoodDelivery
//
//  Created by call soft on 04/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class CheckoutVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var addressCollVw: UICollectionView!
    @IBOutlet weak var collVw: UICollectionView!
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    
    //MARK:- Properties
    
    var listData = ["Credit Card","ApplePay","Samsung Pay","Ali Pay","Cash on Delivery"]
    var imgPayArr = [UIImage.init(named: "cc.jpg"),UIImage.init(named: "apple.jpg"),UIImage.init(named: "samsung.jpg"),UIImage.init(named: "ali.jpg"),UIImage.init(named: "cod.jpg")]
    var collData = ["Now","After 1Hr.","After 2Hr.","After 3Hr.","After 4Hr.","After 5Hr."]
    var addressArr = ["Home","Office"]
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallizers()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "Checkout")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .right
        title.numberOfLines = 0
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tblHeight.constant = tblVw.contentSize.height
    }
    //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionPay(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OrderConfirmationVC") as! OrderConfirmationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


//MARK:- Custom Methods
extension CheckoutVC {
    func initiallizers() {
        configureTable()
        configureOrderTimeCollection()
        configureAddressCollection()
        
    }
    
    func configureOrderTimeCollection(){
        
        collVw.delegate = self
        collVw.dataSource = self
        collVw.backgroundColor = AppColor.clearColor
        collVw.register(UINib.init(nibName: "OrderTimeCollectionCell", bundle: nil), forCellWithReuseIdentifier: "OrderTimeCollectionCell")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.collVw.frame.width / 3, height: 50)
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        collVw.collectionViewLayout = layout
        collVw.reloadData()
        
    }
    
    func configureAddressCollection () {
        
        addressCollVw.delegate = self
        addressCollVw.dataSource = self
        addressCollVw.backgroundColor = AppColor.clearColor
        addressCollVw.register(UINib.init(nibName: "AddressCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AddressCollectionCell")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 248 , height: 140)
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        addressCollVw.collectionViewLayout = layout
        addressCollVw.reloadData()
        
    }
    
    
}



//MARK:- UICollectionViewDelegate,UICollectionViewDataSource
extension CheckoutVC : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == addressCollVw {
            return addressArr.count
        }
        else
        {
            return collData.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == addressCollVw {
            let cell : AddressCollectionCell = addressCollVw.dequeueReusableCell(withReuseIdentifier: "AddressCollectionCell", for: indexPath) as! AddressCollectionCell
            cell.btnEdit.tag = indexPath.row
            cell.lblAddressType.text = addressArr[indexPath.row]
            cell.btnEdit.addTarget(self, action: #selector(EditAddress), for: .touchUpInside)
            
//            if indexPath.row == 0 {
//                cell.imgRadio.image = UIImage.init(named: "radio_s")
//            }
//            else
//            {
                cell.imgRadio.image = UIImage.init(named: "radio_un")
            //}
            return cell
        }
        else {
            let cell : OrderTimeCollectionCell = collVw.dequeueReusableCell(withReuseIdentifier: "OrderTimeCollectionCell", for: indexPath) as! OrderTimeCollectionCell
            
            cell.lblRatings.text = collData[indexPath.item]
            
            
            //            if indexPath.row == 1 {
            //                cell.lblRatings.textColor = AppColor.white
            //                cell.vwOuter.backgroundColor = AppColor.pink
            //                cell.vwOuter.borderColor = AppColor.pink
            //                cell.vwOuter.borderWidth = 1.5
            //            }
            //            else
            //            {
            cell.lblRatings.textColor = AppColor.textcolor
            cell.vwOuter.backgroundColor = AppColor.white
            cell.vwOuter.borderColor = AppColor.primaryColor
            cell.vwOuter.borderWidth = 1.5
            //            }
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == addressCollVw {
            
            if let cell : AddressCollectionCell = addressCollVw.cellForItem(at: indexPath) as? AddressCollectionCell {
                    cell.imgRadio.image = UIImage.init(named: "radio_s")
            }
        }
        else
        {
            if let cell : OrderTimeCollectionCell = collVw.cellForItem(at: indexPath) as? OrderTimeCollectionCell {
                cell.lblRatings.textColor = AppColor.white
                cell.vwOuter.backgroundColor = AppColor.pink
                cell.vwOuter.borderColor = AppColor.pink
                cell.vwOuter.borderWidth = 1.5
            }
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == addressCollVw {
            print("Address Collection De")
            if let cell : AddressCollectionCell = addressCollVw.cellForItem(at: indexPath) as? AddressCollectionCell {
                    cell.imgRadio.image = UIImage.init(named: "radio_un")
            }
        }
        else
        {
            if let cell : OrderTimeCollectionCell = collVw.cellForItem(at: indexPath) as? OrderTimeCollectionCell {
                cell.lblRatings.textColor = AppColor.textcolor
                cell.vwOuter.backgroundColor = AppColor.white
                cell.vwOuter.borderColor = AppColor.primaryColor
                cell.vwOuter.borderWidth = 1.5
            }
        }
    }
    
    @objc func EditAddress(_sender:UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewAddressVC") as! NewAddressVC
     //   vc.address = "52 Riverside St. Norcross, GA 30092"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
//MARK:- UITableViewDelegate and UITableViewDatasource
extension CheckoutVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PaymentTableCell = tblVw.dequeueReusableCell(withIdentifier: "PaymentTableCell") as! PaymentTableCell
        cell.lblPayments.text = listData[indexPath.row]
        cell.imgPayments.image = imgPayArr[indexPath.row]
        cell.imgRadio.image = UIImage.init(named: "radio_un")
//        if indexPath.row == 1 {
//            cell.imgRadio.image = UIImage.init(named: "radio_s")
//        }
//        else
//        {
//            cell.imgRadio.image = UIImage.init(named: "radio_un")
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.backgroundColor = AppColor.clearColor
        tblVw.register(UINib.init(nibName: "PaymentTableCell", bundle: nil), forCellReuseIdentifier: "PaymentTableCell")
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell : PaymentTableCell = tblVw.cellForRow(at: indexPath) as? PaymentTableCell {
            cell.imgRadio.image = UIImage.init(named: "radio_s")
        }
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell : PaymentTableCell = tblVw.cellForRow(at: indexPath) as? PaymentTableCell {
            
            cell.imgRadio.image = UIImage.init(named: "radio_un")
        }
    }
}

