//
//  MyCardVC.swift
//  FoodDelivery
//
//  Created by call soft on 09/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class MyCardVC: ParentViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var tblVw: UITableView!
    
    //MARK:- Properties
    var cardImg = [UIImage.init(named: "card.jpg"),UIImage.init(named: "card_a.jpg")]
    var tblData = ["8745********1452","6536********2536"]
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
    }
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "My Card")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .right
        title.numberOfLines = 0
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
       
    }

    
    @IBAction func btnActionAddNewCard(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddNewCardVC") as! AddNewCardVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}

//MARK:- Custom Methods
extension MyCardVC {
    func initiallizers() {
        
        configureTable()
        
        
    }
    
    
    
}


//MARK:- UITableViewDelegate and UITableViewDatasource
extension MyCardVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : CardTableCell = tblVw.dequeueReusableCell(withIdentifier: "CardTableCell") as! CardTableCell
        cell.lblCardNumber.text = tblData[indexPath.row]
        cell.imgCard.image = cardImg[indexPath.row]
        
         return cell
         
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.backgroundColor = AppColor.clearColor
        tblVw.register(UINib.init(nibName: "CardTableCell", bundle: nil), forCellReuseIdentifier: "CardTableCell")
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}


