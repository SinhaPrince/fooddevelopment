//
//  HomeReuesableProtocols.swift
//  FoodDelivery
//
//  Created by Cst on 4/13/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import FSPagerView

protocol HomeReuseableProtocols {
    var rowHeight: CGFloat { get }
    func cellInstance(_ pagerView: FSPagerView, index: Int) -> FSPagerViewCell

}
