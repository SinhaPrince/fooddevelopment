//
//  HomeVC+AddressManagement.swift
//  FoodDelivery
//
//  Created by Cst on 4/16/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import RealmSwift
extension HomeVC:MapInfoDelegate,FiterDelegate{
    
    func getFilterData() {
        self.homeViewModel.filterHomeResult()
    }
    func checkingLocationPermission(){
        self.locationPermission { (permission) in
            if permission == "Access"{
                let defaultAddress = self.realm.objects(SaveDefaultAddress.self)
                if defaultAddress.count > 0 {
                    let strLat = (defaultAddress.first?.latitude ?? "") as NSString
                    let strLong = (defaultAddress.first?.longitude ?? "") as NSString
                    self.parseAddress(latitude: strLat.doubleValue, logitude: strLong.doubleValue) { (address,subLocality) in
                        self.myAddressService(subLocality, subLocality)
                    }
                    self.initiallizers(defaultAddress.first?.latitude ?? "", defaultAddress.first?.longitude ?? "")
                    
                }else{
                    let strLat = self.lat as NSString
                    let strLong = self.long as NSString
                    self.parseAddress(latitude: strLat.doubleValue, logitude: strLong.doubleValue) { (address,subLocality) in
                        self.myAddressService(subLocality, subLocality)
                    }
                    self.initiallizers(self.lat, self.long)
                }
                
            }else{
                let defaultAddress = self.realm.objects(SaveDefaultAddress.self)
                if defaultAddress.count > 0 {
                    let strLat = (defaultAddress.first?.latitude ?? "") as NSString
                    let strLong = (defaultAddress.first?.longitude ?? "") as NSString
                    self.parseAddress(latitude: strLat.doubleValue, logitude: strLong.doubleValue) { (address,subLocality) in
                        self.myAddressService(subLocality, subLocality)
                    }
                    self.initiallizers(defaultAddress.first?.latitude ?? "", defaultAddress.first?.longitude ?? "")
                    // presentation pop
                    DispatchQueue.main.async {
                        let vc = LocationPermissionPopUp()
                        vc.modalPresentationStyle = .overFullScreen
                        vc.controller = self
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                }else{
                    //Full Screen popup
                    let vc = LocationPermissionVC()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
    }
    
    func fetchAddress(){
        
        self.homeViewModel.fetchAddress("getAddressList") { (status, error) in
            guard error == nil else{
                self.handleError(error?.localizedDescription)
                return
            }
            guard status == 200 else{
                self.handleError(self.homeViewModel.addressData?.message ?? "")
                return
            }
            if self.homeViewModel.addressData?.data?.count ?? 0 > 0{
                let data = self.homeViewModel.addressData?.data?.filter({item in
                    item.defaultStatus == true
                })
                //if there is no default address
                if data?.count == 0{
                    self.checkingLocationPermission()
                    return
                }
                self.myAddressService(data?.first?.landmark ?? "", data?.first?.addressType ?? "")
                
                self.initiallizers(data?.first?.latitude?.description ?? "", data?.first?.longitude?.description ?? "")
                self.locationPermission { (permission) in
                    if permission == "No access"{
                        DispatchQueue.main.async {
                            let vc = LocationPermissionPopUp()
                            vc.modalPresentationStyle = .overFullScreen
                            vc.controller = self
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                
            }else{
                
                self.checkingLocationPermission()
                
            }
        }
    }
    
    func getNotificationCount(){
        
        if !comClass.isDataExist(){
            return
        }
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        ApiManager().fetchApiService(method: .get, url: "getNotificationCount", header: header) { (result) -> (Void) in
            switch result{
            case .success(let data):
                if data["status"].intValue == 200{
                    let count = data["data"].intValue
                    if count == 0{
                        self.lblNotiCount.isHidden = true
                    }else{
                        self.lblNotiCount.clipsToBounds = true
                        self.lblNotiCount.isHidden = false
                        self.lblNotiCount.text = count.description
                    }
                }else{
                    self.show_Alert(message: data["message"].stringValue)
                }
                break
            case .failure(let error):
                self.show_Alert(message: error.localizedDescription)
                break
            }
        }

    }
    
}

//MARK:- NetWork calling
extension HomeVC{
    
    
    func initiallizers(_ lat: String,_ long: String) {
       
        let realm = try! Realm()
        try! realm.write({
            let updateUserData = realm.objects(DataUser.self)
            updateUserData.first?.latitude = lat
            updateUserData.first?.longitude = long
        })
        lattt = lat
        longg = long
        self.homeViewModel.lat = lat
        self.homeViewModel.long = long
        //API Calling
        restaurantListService(lat, long)
        self.homeViewModel.didFinishFetch = {
            self.configureTable()
        }
        
    }
   
    func restaurantListService(_ lat:String,_ long:String){
        
        var param = [String:Any]()
        if !comClass.isDataExist(){
            param = ["longitude" : long,"latitude":lat] as [String:Any]
        }else{
            param = ["longitude" : long,"latitude":lat,"userId":comClass.getInfoById().Id ?? ""] as [String:Any]
        }
         
        self.homeViewModel.fetchRestaurantList("getAllRestaurantsData", param) { (status, error) in
            guard error == nil else{
                self.handleError(error?.localizedDescription)
                return
            }
            guard status == 200 else{
                self.handleError(self.homeViewModel.restaurantListData?.message ?? "")
                return
            }
            if self.homeViewModel.restaurantListData?.data?.count ?? 0 <= 0{
                self.showNoDataView(false)
            }else{
                self.showNoDataView(true)
            }
            self.popularBrandsService(lat, long)
        }

    }
    
    //MARK:- Network Call
    
    func popularBrandsService(_ lat:String,_ long:String){
        let data = SaveDefaultAddress(lat, long, "")
        let realm = try! Realm()
        try! realm.write({
            let deletedNotifications = realm.objects(SaveDefaultAddress.self)
            realm.delete(deletedNotifications)
            realm.add(data)
        })
        let param = ["longitude" : long,"latitude":lat] as [String:Any]
        self.homeViewModel.fetchPopularBrandService("getPopulourBrand", param) { (status, error) in
            guard error == nil else{
                self.homeViewModel.controller.show_Alert(message: error?.localizedDescription ?? "")
                return
            }
            guard status == 200 else{
                self.homeViewModel.controller.show_Alert(message: self.homeViewModel.popularBrandData?.message ?? "")
                return
            }
            self.tblVw.isHidden = false
            self.btnfilter.isHidden = false
            self.homeViewModel.filterHomeResult()
        }
        
       
    }
    
   //MARK:- No Data View Setup
    
    func showNoDataView(_ isNoDataViewShown:Bool){
        noDataAtLocation.isHidden = isNoDataViewShown
        if !isNoDataViewShown{
            locationLottie.backgroundColor = .clear
            Indicator.shared.showLoader(view: locationLottie)
            self.addBlurOnView(view: noDataAtLocation)
            self.btnChangeLocation.addTarget(self, action: #selector(btnChangeLocTap), for: .touchUpInside)
        }
       
    }
    @objc func btnChangeLocTap(){
        Indicator.shared.hideLoaderView()
        if comClass.isDataExist(){
            self.navigationBarButtonDidTapped(.address)
        }else{
            let vc = LocationPermissionVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}

var lattt = String()
var longg = String()
