//
//  HomeVC.swift
//  FoodDelivery
//
//  Created by call soft on 24/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import FSPagerView
import DropDown
import RealmSwift
import SkeletonView
class HomeVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var footerView:UIView!
    @IBOutlet weak var btnViewAll:UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnsearch:UIButton!
    @IBOutlet weak var btnfilter: UIButton!
    @IBOutlet weak var bottomTabView: UIView!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var addressButtonView: UIView!
    @IBOutlet weak var lblNotiCount: UILabel!
    @IBOutlet weak var noDataAtLocation: UIView!
    @IBOutlet weak var locationLottie: UIView!
    @IBOutlet weak var noDataLbl: UILabel!
    @IBOutlet weak var btnChangeLocation: UIButton!
    //MARK:- Properties
    let addressButton = UIButton()
    let myAddressLable = UILabel(frame: CGRect(x: 15, y: 5, width: UIScreen.main.bounds.width - 100, height: 10))

    var dropDown = DropDown()
    var index = Int()
    var arrLanguage = ["Busy","Closed","Open"]
    // we set a variable to hold the contentOffSet before scroll view scrolls
    var lastContentOffset: CGFloat = 0
    //MARK:- Injection
    let homeViewModel = HomeViewModel(networking: ApiManager())
    let dispatchGroup = DispatchGroup()
    //MARK:- Lifecycle Methods45x
    override func viewDidLoad() {
        super.viewDidLoad()
       // tblVw.isHidden = true
        btnfilter.isHidden = true
        noDataAtLocation.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        self.tblVw.register(UINib(nibName: "FavouritesTableCell", bundle: nil), forCellReuseIdentifier: "FavouritesTableCell")
        self.tblVw.dataSource = self
        self.tblVw.isSkeletonable = true
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight, duration: 0.4, autoreverses: true)
        self.tblVw.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7215660045)), animation: animation, transition: .crossDissolve(0.5))
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        self.getNotificationCount()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigation()
        self.initializeTheLocationManager()
        self.baseDelegate = self
        self.homeViewModel.controller = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            if !isDataThere{
               // Indicator.shared.showLottieAnimation(view: self.view)
                isDataThere = !isDataThere
            }
            comClass.isDataExist() ? self.fetchAddress() : self.checkingLocationPermission()
        }
        self.getNotificationCount()
        btnViewAll.addTarget(self, action: #selector(allRestaurants), for: .touchUpInside)
    }
    
}


//MARK:- UITableViewDelegate and UITableViewDatasource
extension HomeVC : UITableViewDelegate, UITableViewDataSource,SkeletonTableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int{
        5
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "FavouritesTableCell"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        else if section == 1{
            return 1
        }
        else if section == 2{
            return 1
        }
        else{
            //return self.homeViewModel.restaurantListData?.data?.count ?? 0
            return self.homeViewModel.mainHomeData?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.homeViewModel.cellInstance(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 3{
            //,Change your filter to find exactly what you want.
            
            let button = UIButton(frame: CGRect(x: 30, y: 15, width: UIScreen.main.bounds.width, height: 40))
            button.setTitle( self.homeViewModel.mainHomeData?.count ?? 0 > 0 ? "All Restaurants" : "Oops! No Brands and Restaurant Found!", for: .normal)
            button.backgroundColor = .white
            button.setTitleColor(AppColor.appLightBlack, for: .normal)
            button.titleLabel?.font = AppFont.Medium.size(.Poppins, size:   self.homeViewModel.mainHomeData?.count ?? 0 > 0 ? 16 : 15)
            button.contentHorizontalAlignment = self.homeViewModel.mainHomeData?.count ?? 0 > 0 ? .left : .center
            button.titleEdgeInsets = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 0)
            return button
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 3{
            return 40
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 178
        }
        else if indexPath.section == 1 || indexPath.section == 2{
            if self.homeViewModel.mainHomeData?.count ?? 0 == 0{
                if indexPath.section == 2{
                    return 0
                }else{
                    return 154.5
                }
            }
            return 154.5
        }
        else{
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 178
        }
        else if indexPath.section == 1 || indexPath.section == 2{
            if self.homeViewModel.mainHomeData?.count ?? 0 == 0{
                if indexPath.section == 2{
                    return 0
                }else{
                    return 154.5
                }
            }
            return 154.5
        }
        else{
            return self.homeViewModel.rowHeight
        }
    }
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        self.tblVw.stopSkeletonAnimation()
        self.tblVw.hideSkeleton()
        tblVw.backgroundColor = AppColor.clearColor
        tblVw.tableFooterView = self.footerView
        self.btnViewAll.isHidden = self.homeViewModel.mainHomeData?.count ?? 0 > 0 ? false : true
        btnViewAll.setTitle("View All(\(self.homeViewModel.mainHomeData?.count ?? 0))", for: .normal)
        tblVw.reloadData()
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(indexPath.section)
        
        if indexPath.section == 3{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantDetailVC") as! RestaurantDetailVC
            vc.id = self.homeViewModel.mainHomeData?[indexPath.row].Id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.lastContentOffset < scrollView.contentOffset.y {
            // did move up
           // headerView.fadeOutWithHidden()
            bottomTabView.fadeOut()

            
        } else if self.lastContentOffset > scrollView.contentOffset.y {
            // did move down
           // self.headerView.isHidden = false
           // headerView.fadeInWithShown()
            bottomTabView.fadeIn()
        } else {
            // didn't move
        }
    }
}

var isDataThere = false

//MARK:- UIButton Action
extension HomeVC{
    @objc func allRestaurants(){
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AllRestaurantsVC") as! AllRestaurantsVC
        vc.restaurantData = self.homeViewModel.mainHomeData ?? []
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionPstOrder(_ sender: UIButton) {
        print("My Past Orders View All")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PastOrderListVC") as! PastOrderListVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionBrands(_ sender: UIButton) {
        print("Popular Brands View All")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PopularBrandsVC") as! PopularBrandsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionRestaurants(_ sender: UIButton) {
    
    }
    @IBAction func btnActionNotification(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionCart(_ sender: UIButton) {
        
//        if !comClass.isDataExist(){
//            self.show_Alert(message: "Login Required!")
//            return
//        }
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionScan(_ sender: UIButton) {
        
//        if !comClass.isDataExist(){
//            self.handleError("Login Required!")
//            return
//        }
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ScanQRVC") as! ScanQRVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionMore(_ sender: UIButton) {
        kAppDelegate.appNavigator.drawerController.setDrawerState(.opened, animated: false)
    }
}
