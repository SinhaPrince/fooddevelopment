//
//  Home+Naviagtion.swift
//  FoodDelivery
//
//  Created by Cst on 4/18/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

extension HomeVC:BaseViewControllerDelegate{
    
    func setupNavigation(){
        self.hideNavigationBar(true)
        mapButton.titleLabel?.textAlignment = .right
        mapButton.contentHorizontalAlignment = .left
        mapButton.setTitle("MapView", for: .normal)
        mapButton.titleLabel?.font = AppFont.Regular.size(.Poppins, size: 13)
        mapButton.setTitleColor(#colorLiteral(red: 0.2666666667, green: 0.2588235294, blue: 0.3176470588, alpha: 1), for: .normal)
        mapButton.setImage(UIImage(named: "map"), for: .normal)
        mapButton.tag = UINavigationBarButtonType.map.rawValue
        mapButton.addTarget(self, action: #selector(ParentViewController.navigationButtonTapped(_:)), for: .touchUpInside)

       // kAppDelegate.appNavigator.drawerController.navigationItem.rightBarButtonItems?.append(UIBarButtonItem(customView: mapButton))
        btnsearch.addTarget(self, action: #selector(btnSearchTap), for: .touchUpInside)
        btnfilter.addTarget(self, action: #selector(btnFilterTap), for: .touchUpInside)

    }
    
    func myAddressService(_ address:String,_ type:String){
        addressButton.removeFromSuperview()
        myAddressLable.removeFromSuperview()
        addressButton.titleLabel?.textAlignment = .left
        addressButton.contentHorizontalAlignment = .left
        
        addressButton.setTitle(comClass.createString(Str: type), for: .normal)
        addressButton.titleLabel?.font = AppFont.Medium.size(.Poppins, size: 14)
        addressButton.setTitleColor(AppColor.pink, for: .normal)
        addressButton.setImage(UIImage(named: "down_arrow_pink"), for: .normal)
        addressButton.tintColor = AppColor.pink
        addressButton.tag = UINavigationBarButtonType.address.rawValue
        addressButton.alignImageRight()
        addressButton.addTarget(self, action: #selector(ParentViewController.navigationButtonTapped(_:)), for: .touchUpInside)
        //Title View
        myAddressLable.font = AppFont.Regular.size(.Poppins, size: 10)
        myAddressLable.text = "My Address"
        myAddressLable.textColor = AppColor.lightGrayApp
       // let titleView = UIView(frame: CGRect(x: 15, y: 3, width: UIScreen.main.bounds.width - 100, height: 40))
        addressButtonView.addSubview(addressButton)
        addressButtonView.addSubview(myAddressLable)
        addressButton.frame = CGRect(x: 15, y: 10, width: UIScreen.main.bounds.width - 100, height: 30)
        addressButtonView.backgroundColor = .clear
       // kAppDelegate.appNavigator.drawerController.navigationItem.titleView = titleView
    }
    
    @objc func btnSearchTap(){
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchForRestaurantVC") as! SearchForRestaurantVC
        vc.restaurantData = self.homeViewModel.restaurantListData?.data ?? []
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func btnFilterTap(){
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
//        vc.restaurantData = self.homeViewModel.restaurantListData?.data ?? []
//        vc.filterDelegate = self
//        self.navigationController?.pushViewController(vc, animated: true)
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        let navigationControlr = UINavigationController(rootViewController: vc)
        vc.restaurantData = self.homeViewModel.restaurantListData?.data ?? []
        vc.filterDelegate = self
        self.present(navigationControlr, animated: true, completion: nil)
    }
    
    func navigationBarButtonDidTapped(_ buttonType: UINavigationBarButtonType) {
        if buttonType == .address{
            if !comClass.isDataExist(){
                self.show_Alert(message: "Login Required!")
                return
            }
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if buttonType == .map{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MapViewVC") as! MapViewVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
extension UIButton {
    
    /// Makes the ``imageView`` appear just to the right of the ``titleLabel``.
    func alignImageRight() {
        if let titleLabel = self.titleLabel, let imageView = self.imageView {
            // Force the label and image to resize.
            titleLabel.sizeToFit()
            imageView.sizeToFit()
            imageView.contentMode = .scaleAspectFit
            
            // Set the insets so that the title appears to the left and the image appears to the right.
            // Make the image appear slightly off the top/bottom edges of the button.
            self.titleEdgeInsets = UIEdgeInsets(top: 0, left: -1 * imageView.frame.size.width,
                                                bottom: 0, right: imageView.frame.size.width)
            self.imageEdgeInsets = UIEdgeInsets(top: 0, left: titleLabel.frame.size.width,
                                                bottom: 0, right: -1 * titleLabel.frame.size.width)
        }
    }
}
