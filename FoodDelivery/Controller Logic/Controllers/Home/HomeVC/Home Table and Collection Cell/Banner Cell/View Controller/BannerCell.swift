//
//  BannerCell.swift
//  FoodDelivery
//
//  Created by Cst on 5/2/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import FSPagerView
class BannerCell: UITableViewCell {
    
    //MARK:- Outlet 
    @IBOutlet weak var bannerView:FSPagerView!
    @IBOutlet weak var pageCount:UIPageControl!
    
    //Injection
    let viewModel = BannerViewModel(networking: ApiManager())
    var latitude = String()
    var longitude = String()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func bannerService(){
        let param = ["longitude":longitude,"latitude":latitude] as [String:Any]
        self.viewModel.fetchBannerService("getNewBannerList", param) { (status, error) in
            guard error == nil else{
                self.viewModel.controller.show_Alert(message: error?.localizedDescription ?? "")
                return
            }
            guard status == 200 else{
                self.viewModel.controller.show_Alert(message: self.viewModel.bannerData?.message ?? "")
                return
            }
            self.configurePagerView()

        }
        
    }
    
}
//MARK:- FSPagerView Delegate
extension BannerCell:FSPagerViewDelegate,FSPagerViewDataSource{
    
    func configurePagerView() {
        bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        bannerView.dataSource = self
        bannerView.delegate = self
        
        bannerView.clipsToBounds = true
        bannerView.automaticSlidingInterval = 5.0
        bannerView.transformer = FSPagerViewTransformer(type: .linear)
        //pageControl.numberOfPages = viewmodel.bannerData.first?.banners?.count ?? 0
        pageCount.numberOfPages = viewModel.bannerData?.data?.count ?? 0
        pageCount.contentHorizontalAlignment = .right
        bannerView.reloadData()
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        viewModel.bannerData?.data?.count ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        self.viewModel.cellInstance(pagerView, index: index)
    }
    func pagerView(_ pagerView: FSPagerView, willDisplay cell: FSPagerViewCell, forItemAt index: Int) {
        pageCount.currentPage = index
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        
        if self.viewModel.bannerData?.data?[index].restaurantData?.count ?? 0 > 0 {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantDetailVC") as! RestaurantDetailVC
            vc.id = self.viewModel.bannerData?.data?[index].restaurantData?.first?.restaurantId ?? ""
            viewModel.controller.navigationController?.pushViewController(vc, animated: true)
        }else{
            viewModel.controller.show_Alert(message: "Restaurant data not found!")
        }
       
    }
}
