//
//  BannerViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 5/2/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import FSPagerView
class BannerViewModel: BaseViewModel,HomeReuseableProtocols {
    
    var rowHeight: CGFloat = 100
    var bannerData:BannerlistModel?{
        didSet{
            self.didFinishFetch?()
        }
    }
    private var dataService: ApiManager?
    var controller = UIViewController()
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    func cellInstance(_ pagerView: FSPagerView, index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let urlString = (bannerData?.data?[index].enImage ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        cell.imageView?.sd_setImage(with: URL(string: urlString ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"), options: .lowPriority, context: [:])

       // cell.imageView?.image = #imageLiteral(resourceName: "banner")
        cell.imageView?.contentMode = .scaleAspectFill
        return cell
    }
   
    //MARK:- Network Call
    
    /// Get Banner List Service
    /// - Parameter url: Api EndPoint
    func fetchBannerService(_ url:String,_ param:[String:Any],complition:@escaping (Int?,Error?) ->()){
        self.dataService?.fetchApiService(method: .post, url: url, passDict: param, callback: { (result) in
            switch result{
            case .success(let data):
                self.bannerData = BannerlistModel(data)
                complition(self.bannerData?.status,nil)
                break
            case .failure(let error):
                complition(nil,error)
                break
            }
        })

    }
}
