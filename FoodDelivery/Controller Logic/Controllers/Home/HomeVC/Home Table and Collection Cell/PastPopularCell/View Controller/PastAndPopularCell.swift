//
//  PastAndPopularCell.swift
//  FoodDelivery
//
//  Created by Cst on 5/2/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import RealmSwift
class PastAndPopularCell: UITableViewCell {

    //MARK:- Outlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK:- Injection
    let viewModel = PastPopularViewModel(networking: ApiManager())
    var delegate: PopularBrandCountDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnViewAll.addTarget(self, action: #selector(gotoAllPastOrder), for: .touchUpInside)
        self.viewModel.didFinishFetch = {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func gotoAllPastOrder(){
        //PastOrderListVC
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PastOrderListVC") as! PastOrderListVC
        viewModel.controller.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- UICollectionView DataSource
extension PastAndPopularCell:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    public func collectionSetup(_ tag:Int,_ headerTitle:String,_ viewAll:Bool){
        self.collectionView.tag = tag
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.backgroundColor = AppColor.clearColor
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        if tag == 1{
            layout.itemSize = CGSize(width: 256 , height: 120)
        }else{
            layout.itemSize = CGSize(width: 128, height: 120)
        }
        
        
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = layout
        self.lblTitle.setup(headerTitle, AppColor.appLightBlack, AppFont.Medium.size(.Poppins, size: 16), .left)
        self.btnViewAll.isHidden = viewAll
        self.btnViewAll.setTitleColor(AppColor.appLightBlack, for: .normal)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1{
            return 5
        }else{
            return self.viewModel.filterBrandData?.count ?? 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        viewModel.collectionCellInstance1(collectionView, indexPath: indexPath)
    }
    
}

//MARK:- UICollectionView Delegate
extension PastAndPopularCell:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
            viewModel.controller.navigationController?.pushViewController(vc, animated: true)
        }else{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantDetailVC") as! RestaurantDetailVC
            vc.id = viewModel.filterBrandData?[indexPath.row].brandId
            vc.comingFrom = "Brand"
            viewModel.controller.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

protocol PopularBrandCountDelegate {
    func numberOfBrands(_ count:Int)
}
