//
//  PastPopularViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 5/2/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class PastPopularViewModel:BaseViewModel,CellRepresentable{
    
    var rowHeight: CGFloat = 100
    
    var popularBrandData:PopularBrandModel?{
        didSet{
            filterBrandData = popularBrandData?.data
        }
    }
    var filterBrandData:[BrandData]?{
        didSet{
            self.didFinishFetch?()
        }
    }
    
    private var dataService: ApiManager?
    var controller = UIViewController()
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    
    func collectionCellInstance1(_ tableView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        if tableView.tag == 1{
            tableView.register(UINib(nibName: "PastOrderCollectionCell", bundle: nil), forCellWithReuseIdentifier: "PastOrderCollectionCell")
            let cell : PastOrderCollectionCell = tableView.dequeueReusableCell(withReuseIdentifier: "PastOrderCollectionCell", for: indexPath) as! PastOrderCollectionCell
            cell.btnReOrder.addTarget(self, action: #selector(ReOrderTap), for: .touchUpInside)
            return cell
        }else{
            tableView.register(UINib(nibName: "BrandsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BrandsCollectionCell")
            guard let cell = tableView.dequeueReusableCell(withReuseIdentifier: "BrandsCollectionCell", for: indexPath) as? BrandsCollectionCell else {
                return UICollectionViewCell()
            }
            if filterBrandData?.count ?? 0 > indexPath.row {
                cell.imgBrands?.sd_setImage(with: URL(string: filterBrandData?[indexPath.row].brandData?.logo ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"), options: .lowPriority, context: [:])
            }

            return cell
        }
    }
    
//    /// Get Popular Brands Service
//    /// - Parameters:
//    ///   - url: Api EndPoint
//    ///   - params: Lat and Long as Parameter
//    func fetchPopularBrandService(_ url:String,_ params:[String:Any],complition:@escaping (Int?,Error?) ->()){
//        self.dataService?.fetchApiService(method: .post, url: url,passDict: params, callback: { (result) in
//            switch result{
//            case .success(let data):
//                self.popularBrandData = PopularBrandModel(data)
//                complition(self.popularBrandData?.status,nil)
//                break
//            case .failure(let error):
//                complition(nil,error)
//                break
//            }
//        })
//    }
    
    @objc func ReOrderTap(){
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    func filterBrandResult(){
    
        var filterArray: [BrandData]?
            filterArray = self.popularBrandData?.data?.filter({ item in
                (item.avgRating ?? 0 >= Rating ?? 0)
            })
        
        if TimeRange?.last ?? 0 >= 28{
            TimeRange?[1] = 100
        }
        if TimeRange?.first ?? 0 <= 15{
            TimeRange?[0] = 0
        }
        if DistanceRange?.last ?? 0 >= 19{
            DistanceRange?[1] = 100
        }
//        if DistanceRange?.first ?? 0 <= 15{
//            DistanceRange?[0] = 0
//        }
            filterArray = filterArray?.filter({item in
                (item.areaData?.deliveryTime ?? 0 >= TimeRange?.first ?? 15) &&
                (item.areaData?.deliveryTime ?? 0 <= TimeRange?.last ?? 30)
            })
            filterArray = filterArray?.filter({item in
                item.dist?.calculated ?? 0 >= ((DistanceRange?.first ?? 0) * 1000) &&
                item.dist?.calculated ?? 0 <= ((DistanceRange?.last ?? 20) * 1000)
            })
        self.filterBrandData = filterArray

    }
}
