//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 13, 2021
//
import Foundation
import SwiftyJSON
import MapKit

class BrandData:NSObject, MKAnnotation {
    
    var areaData: AreaData?
    var deliveryTime: Int?
    var email: String?
    var busy3Status: Bool?
    var Id: String?
    var categoryData: [CategoryData]?
    var brandData: BrandDataa?
    var busy1Status: Bool?
    var oepnStatus: Bool?
    var menuId: String?
    var avgRating: Int?
    var brandId: String?
    var ratingByUsers: Int?
    var loyalty: String?
    var mobileNumber: String?
    var openTime: String?
    var closeStatus: Bool?
    var status: String?
    var countryCode: String?
    var isFav: Bool?
    var busy5Status: Bool?
    var closeTime: String?
    var totalOrders: Int?
    var branchNameEn: String?
    var index: Int?
    var descriptionEn: String?
    var location: Location?
    var image: String?
    var busy2Status: Bool?
    var serviceType: String?
    var branchNameAr: String?
    var busy4Status: Bool?
    var latitude: String?
    var longitude: String?
    var busyTime: Int?
    var dist: Dist?
    var deliveryFee: Int?
    var createdAt: String?
    var address: String?
    var totalRating: Int?
    var descriptionAr: String?
    var minimumOrderValue: Int?
    let coordinate: CLLocationCoordinate2D
    var title: String?
    var logo: String?
    init(_ json: JSON) {
        areaData = AreaData(json["areaData"])
        deliveryTime = json["deliveryTime"].intValue
        email = json["email"].stringValue
        busy3Status = json["busy3Status"].boolValue
        Id = json["_id"].stringValue
        categoryData = json["categoryData"].arrayValue.map { CategoryData($0) }
        brandData = BrandDataa(json["brandData"])
        busy1Status = json["busy1Status"].boolValue
        oepnStatus = json["oepnStatus"].boolValue
        menuId = json["menuId"].stringValue
        avgRating = json["avgRating"].intValue
        brandId = json["brandId"].stringValue
        ratingByUsers = json["ratingByUsers"].intValue
        loyalty = json["loyalty"].stringValue
        mobileNumber = json["mobileNumber"].stringValue
        openTime = json["openTime"].stringValue
        closeStatus = json["closeStatus"].boolValue
        status = json["status"].stringValue
        countryCode = json["countryCode"].stringValue
        isFav = json["isFav"].boolValue
        busy5Status = json["busy5Status"].boolValue
        closeTime = json["closeTime"].stringValue
        totalOrders = json["totalOrders"].intValue
        branchNameEn = json["branchNameEn"].stringValue
        index = json["index"].intValue
        descriptionEn = json["descriptionEn"].stringValue
        location = Location(json["location"])
        image = json["image"].stringValue
        busy2Status = json["busy2Status"].boolValue
        serviceType = json["serviceType"].stringValue
        branchNameAr = json["branchNameAr"].stringValue
        busy4Status = json["busy4Status"].boolValue
        latitude = json["latitude"].stringValue
        longitude = json["longitude"].stringValue
        busyTime = json["busyTime"].intValue
        dist = Dist(json["dist"])
        deliveryFee = json["deliveryFee"].intValue
        createdAt = json["createdAt"].stringValue
        address = json["address"].stringValue
        totalRating = json["totalRating"].intValue
        descriptionAr = json["descriptionAr"].stringValue
        minimumOrderValue = json["minimumOrderValue"].intValue
        dist = Dist(json["dist"])
        title = json["storeName"].stringValue
        logo = json["logo"].stringValue
        let lat = latitude as NSString?
        let long = longitude as NSString?
        coordinate  = CLLocationCoordinate2D(latitude: lat?.doubleValue ?? 0.0 , longitude: long?.doubleValue ?? 0.0)
        
    }
    
}

struct BrandDataa {

    var logo: String?
    var firstName: String?
    var cuisineArrayEn: [CuisineArrayEn]?
    var storeName: String?
    var lastName: String?
    var image: String?
    var Id: String?

    init(_ json: JSON) {
        logo = json["logo"].stringValue
        firstName = json["firstName"].stringValue
        cuisineArrayEn = json["cuisineArrayEn"].arrayValue.map { CuisineArrayEn($0) }
        storeName = json["storeName"].stringValue
        lastName = json["lastName"].stringValue
        image = json["image"].stringValue
        Id = json["_id"].stringValue
    }

}
