//
//  Location.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 13, 2021
//
import Foundation
import SwiftyJSON

class Location {

	var type: String?
	var coordinates: [Double]?

	init(_ json: JSON) {
		type = json["type"].stringValue
		coordinates = json["coordinates"].arrayValue.map { $0.doubleValue }
	}

}