//
//  Dist.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 13, 2021
//
import Foundation
import SwiftyJSON

class Dist {

	var calculated: Int?

	init(_ json: JSON) {
		calculated = json["calculated"].intValue
	}

}