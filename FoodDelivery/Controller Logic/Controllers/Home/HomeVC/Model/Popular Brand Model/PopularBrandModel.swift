//
//  PopularBrandModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 13, 2021
//
import Foundation
import SwiftyJSON
class PopularBrandModel {

	var status: Int?
	var message: String?
	var data: [BrandData]?

	init(_ json: JSON) {
		status = json["status"].intValue
		message = json["message"].stringValue
		data = json["data"].arrayValue.map { BrandData($0) }
	}

}
