//
//  CuisineArrayEn.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 13, 2021
//
import Foundation
import SwiftyJSON

class CuisineArrayEn {

	var itemId: Int?
	var itemText: String?
    var isSelected: Bool?
	init(_ json: JSON) {
		itemId = json["item_id"].intValue
		itemText = json["item_text"].stringValue
        isSelected = false
	}

}
