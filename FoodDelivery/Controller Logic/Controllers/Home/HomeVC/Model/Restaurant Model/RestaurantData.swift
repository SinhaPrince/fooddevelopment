//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 25, 2021
//
import Foundation
import SwiftyJSON
import MapKit

class RestaurantData:NSObject, MKAnnotation {
    
    var areaData: AreaData?
    var image: String?
    var openTime: String?
    var menuId: String?
    var avgRating: Int?
    var closeTime: String?
    var totalRating: Int?
    var ratingByUsers: Int?
    var Id: String?
    var restaurantId: String?
    var descriptionEn: String?
    var status: String?
    var busy3Status: Bool?
    var countryCode: String?
    var busyTime: Int?
    var categoryData: [CategoryData]?
    var loyalty: String?
    var address: String?
    var totalOrders: Int?
    var branchNameAr: String?
    var busy2Status: Bool?
    var dist: Dist?
    var minimumOrderValue: Int?
    var mobileNumber: String?
    var index: Int?
    var latitude: String?
    var busy4Status: Bool?
    var busy5Status: Bool?
    var branchNameEn: String?
    var serviceType: String?
    var deliveryFee: Int?
    var closeStatus: Bool?
    var oepnStatus: Bool?
    var email: String?
    var location: Location?
    var busy1Status: Bool?
    var brandData: BrandDataa?
    var descriptionAr: String?
    var longitude: String?
    var createdAt: String?
    var brandId: String?
    var deliveryTime: Int?
    var isFav: Bool?
    let coordinate: CLLocationCoordinate2D
    var title: String?
    init(_ json: JSON) {
        areaData = AreaData(json["areaData"])
        image = json["image"].stringValue
        openTime = json["openTime"].stringValue
        menuId = json["menuId"].stringValue
        avgRating = json["avgRating"].intValue
        closeTime = json["closeTime"].stringValue
        totalRating = json["totalRating"].intValue
        ratingByUsers = json["ratingByUsers"].intValue
        Id = json["_id"].stringValue
        restaurantId = json["restaurantId"].stringValue
        descriptionEn = json["descriptionEn"].stringValue
        status = json["status"].stringValue
        busy3Status = json["busy3Status"].boolValue
        countryCode = json["countryCode"].stringValue
        busyTime = json["busyTime"].intValue
        categoryData = json["categoryData"].arrayValue.map { CategoryData($0) }
        loyalty = json["loyalty"].stringValue
        address = json["address"].stringValue
        totalOrders = json["totalOrders"].intValue
        branchNameAr = json["branchNameAr"].stringValue
        busy2Status = json["busy2Status"].boolValue
        dist = Dist(json["dist"])
        minimumOrderValue = json["minimumOrderValue"].intValue
        mobileNumber = json["mobileNumber"].stringValue
        index = json["index"].intValue
        latitude = json["latitude"].stringValue
        busy4Status = json["busy4Status"].boolValue
        busy5Status = json["busy5Status"].boolValue
        branchNameEn = json["branchNameEn"].stringValue
        serviceType = json["serviceType"].stringValue
        deliveryFee = json["deliveryFee"].intValue
        closeStatus = json["closeStatus"].boolValue
        oepnStatus = json["oepnStatus"].boolValue
        email = json["email"].stringValue
        location = Location(json["location"])
        busy1Status = json["busy1Status"].boolValue
        brandData = BrandDataa(json["brandData"])
        descriptionAr = json["descriptionAr"].stringValue
        longitude = json["longitude"].stringValue
        createdAt = json["createdAt"].stringValue
        brandId = json["brandId"].stringValue
        deliveryTime = json["deliveryTime"].intValue
        isFav = json["isFav"].boolValue
        
        title = brandData?.storeName
        let lat = latitude as NSString?
        let long = longitude as NSString?
        coordinate  = CLLocationCoordinate2D(latitude: lat?.doubleValue ?? 0.0 , longitude: long?.doubleValue ?? 0.0)
    }
    
}

struct CategoryData {

    var createdAt: String?
    var menuId: String?
    var _v: Int?
    var index: Int?
    var brandId: String?
    var status: String?
    var Id: String?
    var deleteStatus: Bool?
    var updatedAt: String?
    var adminVerificationStatus: String?
    var name: String?
    var nameAr: String?
    var isSelected: Bool?
    init(_ json: JSON) {
        createdAt = json["createdAt"].stringValue
        menuId = json["menuId"].stringValue
        _v = json["__v"].intValue
        index = json["index"].intValue
        brandId = json["brandId"].stringValue
        status = json["status"].stringValue
        Id = json["_id"].stringValue
        deleteStatus = json["deleteStatus"].boolValue
        updatedAt = json["updatedAt"].stringValue
        adminVerificationStatus = json["adminVerificationStatus"].stringValue
        name = json["name"].stringValue
        nameAr = json["nameAr"].stringValue
        isSelected = false
    }

}
struct AreaData {

    var deliveryFee: Int?
    var deliveryTime: Int?
    var minimumOrderValue: Int?

    init(_ json: JSON) {
        deliveryFee = json["deliveryFee"].intValue
        deliveryTime = json["deliveryTime"].intValue
        minimumOrderValue = json["minimumOrderValue"].intValue
    }

}
