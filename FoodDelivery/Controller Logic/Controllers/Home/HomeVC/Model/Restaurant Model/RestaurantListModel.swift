//
//  RestaurantListModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 25, 2021
//
import Foundation
import SwiftyJSON

class RestaurantListModel {

	var status: Int?
	var message: String?
	var data: [RestaurantData]?

	init(_ json: JSON) {
		status = json["status"].intValue
		message = json["message"].stringValue
		data = json["data"].arrayValue.map { RestaurantData($0) }
	}

}
