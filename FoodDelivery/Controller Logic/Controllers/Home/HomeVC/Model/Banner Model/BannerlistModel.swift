//
//  BannerlistModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 13, 2021
//
import Foundation
import SwiftyJSON

class BannerlistModel {

	var status: Int?
	var message: String?
	var data: [BannerData]?

	init(_ json: JSON) {
		status = json["status"].intValue
		message = json["message"].stringValue
		data = json["data"].arrayValue.map { BannerData($0) }
	}

}
