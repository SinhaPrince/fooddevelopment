//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on April 13, 2021
//
import Foundation
import SwiftyJSON

class BannerData {
    
    var arDescription: String?
    var arTitle: String?
    var fromDate: String?
    var toActualTimeAndDate: String?
    var toTime: String?
    var _v: Int?
    var fromTime: String?
    var createdAt: String?
    var restaurantData: [RestaurantData]?
    var enTitle: String?
    var enDescription: String?
    var fromTimeAndDate: String?
    var updatedAt: String?
    var Id: String?
    var arOfferCode: String?
    var brandId: String?
    var arImage: String?
    var status: String?
    var restroData: [RestroData]?
    var enOfferCode: String?
    var toDate: String?
    var enImage: String?
    var deleteStatus: Bool?
    var brandData: BrandData?

    init(_ json: JSON) {
        arDescription = json["arDescription"].stringValue
        arTitle = json["arTitle"].stringValue
        fromDate = json["fromDate"].stringValue
        toActualTimeAndDate = json["toActualTimeAndDate"].stringValue
        toTime = json["toTime"].stringValue
        _v = json["__v"].intValue
        fromTime = json["fromTime"].stringValue
        createdAt = json["createdAt"].stringValue
        restaurantData = json["restaurantData"].arrayValue.map { RestaurantData($0) }
        enTitle = json["enTitle"].stringValue
        enDescription = json["enDescription"].stringValue
        fromTimeAndDate = json["fromTimeAndDate"].stringValue
        updatedAt = json["updatedAt"].stringValue
        Id = json["_id"].stringValue
        arOfferCode = json["arOfferCode"].stringValue
        brandId = json["brandId"].stringValue
        arImage = json["arImage"].stringValue
        status = json["status"].stringValue
        restroData = json["restroData"].arrayValue.map { RestroData($0) }
        enOfferCode = json["enOfferCode"].stringValue
        toDate = json["toDate"].stringValue
        enImage = json["enImage"].stringValue
        deleteStatus = json["deleteStatus"].boolValue
        brandData = BrandData(json["brandData"])

    }
}
struct RestroData {

    var itemId: Int?
    var itemText: String?

    init(_ json: JSON) {
        itemId = json["item_id"].intValue
        itemText = json["item_text"].stringValue
    }

}
