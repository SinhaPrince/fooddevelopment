//
//  HomeViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 4/13/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import FSPagerView
class HomeViewModel:BaseViewModel,HomeReuseableProtocols,CellRepresentable{
    
    
    var rowHeight: CGFloat = 261
    var restaurantListData: RestaurantListModel?{
        didSet{
            //self.didFinishFetch?()
           // mainHomeData = restaurantListData?.data
            filterHomeResult()
        }
    }
    
    var filterData: [RestaurantData]?{
        didSet{
            mainHomeData = filterData
        }
    }
    
    var mainHomeData: [RestaurantData]?{
        didSet{
            self.didFinishFetch?()
        }
    }
    
    var popularBrandData:PopularBrandModel?

    
    var addressData:AddressListModel?
    var delegate: PopularBrandCountDelegate?
    var lat: String?
    var long: String?
    private var dataService: ApiManager?
    var controller = UIViewController()
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    
    func cellInstance(_ pagerView: FSPagerView, index: Int) -> FSPagerViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
//        let urlString = (bannerData?.data?[index].arImage ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//        cell.imageView?.sd_setImage(with: URL(string: urlString ?? ""), completed: nil)
//        cell.imageView?.contentMode = .scaleAspectFit
//        cell.imageView?.clipsToBounds = true
        return cell
    }
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            tableView.register(UINib(nibName: "BannerCell", bundle: nil), forCellReuseIdentifier: "BannerCell")
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BannerCell") as? BannerCell else {
                return UITableViewCell()
            }
            cell.longitude = self.long ?? ""
            cell.latitude = self.lat ?? ""
            cell.viewModel.controller = controller
            cell.bannerService()
            return cell
        }
        else if indexPath.section == 1 || indexPath.section == 2{
            tableView.register(UINib(nibName: "PastAndPopularCell", bundle: nil), forCellReuseIdentifier: "PastAndPopularCell")
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PastAndPopularCell") as? PastAndPopularCell else {
                return UITableViewCell()
            }
            cell.viewModel.popularBrandData = self.popularBrandData
            cell.viewModel.controller = controller
            cell.collectionSetup(indexPath.section, indexPath.section == 1 ? "My Past Order" : "Popular Brands", indexPath.section == 1 ? false : true)
            cell.viewModel.filterBrandResult()
            return cell
        }else{
            tableView.register(UINib(nibName: "FavouritesTableCell", bundle: nil), forCellReuseIdentifier: "FavouritesTableCell")
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavouritesTableCell") as? FavouritesTableCell else {
                return UITableViewCell()
            }
           // cell.item = restaurantListData?.data?[indexPath.row]
            cell.item = mainHomeData?[indexPath.row]
            cell.btnFavt.tag = indexPath.row
            cell.btnFavt.addTarget(self, action: #selector(favtTap(_:)), for: .touchUpInside)
            cell.btnTracking.addTarget(self, action: #selector(liveTracking(_:)), for: .touchUpInside)
            return cell
        }
        
    }
    
    
    
    @objc func liveTracking(_ sender:UIButton){
        
        let vc = controller.storyboard?.instantiateViewController(withIdentifier: "OrderConfirmationVC") as! OrderConfirmationVC
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func favtTap(_ sender:UIButton){
        if !comClass.isDataExist(){
            controller.show_Alert(message: "Login Required!")
            return
        }
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        let params = ["restaurantId":self.mainHomeData?[sender.tag].Id ?? ""] as [String:Any]
        self.dataService?.fetchApiService(method: .post, url: "addToFavourite", passDict: params, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                if data["status"].intValue == 200{
                    if data["message"].stringValue == "Remove from favourite successfully"{
                        sender.setImage(#imageLiteral(resourceName: "heart_g"), for: .normal)
                    }else{
                        sender.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
                    }
                }else{
                    self.controller.show_Alert(message: data["message"].stringValue)
                }
                break
            case .failure(let error):
                self.controller.show_Alert(message: error.localizedDescription)
                break
            }
        })

    }

  
    
    //MARK:- Network Call

    /// Get RestaurantList Service
    /// - Parameters:
    ///   - url: Api EndPoint
    ///   - params: Lat and Long as Parameter and UserId If login
    func fetchRestaurantList(_ url:String,_ params:[String:Any],complition:@escaping (Int?,Error?) ->()) {
        self.dataService?.fetchApiService(method: .post, url: url,passDict: params, callback: { (result) in
            switch result{
            case .success(let data):
                self.restaurantListData = RestaurantListModel(data)
                complition(self.restaurantListData?.status,nil)
                break
            case .failure(let error):
                complition(nil,error)
                break
            }
        })
    }
    
    /// Returrn All Saved User Addresses
    /// - Parameter url: ApiKey
    func fetchAddress(_ url:String,complition:@escaping (Int?,Error?) ->()){
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        self.dataService?.fetchApiService(method: .get, url: url, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                self.addressData = AddressListModel(data)
                complition(self.addressData?.status,nil)
                break
            case .failure(let error):
               complition(nil,error)
                break
            }
        })

    }
    
    /// Get Popular Brands Service
    /// - Parameters:
    ///   - url: Api EndPoint
    ///   - params: Lat and Long as Parameter
    func fetchPopularBrandService(_ url:String,_ params:[String:Any],complition:@escaping (Int?,Error?) ->()){
        self.dataService?.fetchApiService(method: .post, url: url,passDict: params, callback: { (result) in
            switch result{
            case .success(let data):
                self.popularBrandData = PopularBrandModel(data)
                complition(self.popularBrandData?.status,nil)
                break
            case .failure(let error):
                complition(nil,error)
                break
            }
        })
    }
}

//MARK:- Filter Data
extension HomeViewModel{
   
    //,_ dietaryNeed:[CategoryData],_ cuisineArray:[CuisineArrayEn]
    func filterHomeResult(){
    
        var filterArray: [RestaurantData]?
            filterArray = self.restaurantListData?.data?.filter({ item in
                (item.avgRating ?? 0 >= Rating ?? 0)
            })
            filterArray = filterArray?.filter({item in
                (item.areaData?.deliveryTime ?? 0 >= TimeRange?.first ?? 15) && (item.areaData?.deliveryTime ?? 0 <= TimeRange?.last ?? 30)
            })
            filterArray = filterArray?.filter({item in
                item.dist?.calculated ?? 0 >= ((DistanceRange?.first ?? 0) * 1000) && item.dist?.calculated ?? 0 <= ((DistanceRange?.last ?? 20) * 1000)
            })
        var count = 0
        CuisineArray?.forEach({item in
            let dd = filterArray?.filter({dd in
                return filterCuisines(dd, item.itemText ?? "")
            })
            if let datt = dd{
                if count <= 0{
                    filterArray = datt
                }else{
                    filterArray?.append(contentsOf: datt)
                }
            }
            count += 1
        })
      
        self.filterData = filterArray
    }
    
    func filterCuisines(_ data: RestaurantData,_ searchText:String) -> Bool{
        guard let data = data.brandData?.cuisineArrayEn else {
            return false
        }
        return data.contains(where: {item in
            (item.itemText?.lowercased().contains(searchText.lowercased()) ?? false)
        })
    }
}

protocol FiterDelegate {
    func getFilterData()
}
