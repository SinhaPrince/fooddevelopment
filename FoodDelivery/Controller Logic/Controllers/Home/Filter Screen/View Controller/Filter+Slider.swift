//
//  Filter+Slider.swift
//  FoodDelivery
//
//  Created by Cst on 5/17/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import MultiSlider

extension FilterVC{
     func sliderSetup(){
        distance.addSubview(distanceSlider)
        distanceSlider.frame = distance.bounds
        time.addSubview(timeSlider)
        timeSlider.frame = time.bounds
    }
    @objc func sliderChanged(_ slider: MultiSlider) {
        print("thumb \(slider.draggedThumbIndex) moved")
        print("now thumbs are at \(slider.value)") // e.g., [1.0, 4.5, 5.0]
        isResetTap = false
        if self.timeSlider == slider{
            lblTime.text = "\(Int(slider.value.first ?? 0)) Min - \(Int(slider.value.last ?? 0)) Min"
            minTime = (Int(slider.value.first ?? 0))
            maxTime = (Int(slider.value.last ?? 0))
        }else{
            lblDistance.text = "\(Int(slider.value.first ?? 0)) KM - \(Int(slider.value.last ?? 0)) KM"
            minDis = (Int(slider.value.first ?? 0))
            maxDis = (Int(slider.value.last ?? 0))
        }
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backFromPresent], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "Filter Your Search")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 14)
        title.textAlignment = .center
        self.navigationItem.titleView = title
        let button = UIButton(frame:CGRect(x: 0, y: 0, width: 100, height: 30))
        button.sizeToFit()
        button.setTitle(comClass.createString(Str: "Reset All"), for: .normal)
        button.backgroundColor = .clear
        button.setTitleColor(AppColor.pink, for: .normal)
        button.titleLabel?.font = AppFont.Medium.size(.Poppins, size: 14)
        button.tag = UINavigationBarButtonType.resetAll.rawValue
        button.addTarget(self, action: #selector(ParentViewController.navigationButtonTapped(_:)), for: .touchUpInside)
        button.contentHorizontalAlignment = .center
        button.clipsToBounds = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        
    }

    func navigationBarButtonDidTapped(_ buttonType: UINavigationBarButtonType) {
        if buttonType == .resetAll{
            lblTime.text = "\((15)) Min - \((30)) Min"
            lblDistance.text = "\((0)) KM - \((20)) KM"
            timeSlider.value = [15,30]
            distanceSlider.value = [0,20]
            self.rating = 0
            resetRating([star1,star2,star3,star4,star5])
        }
    }
}
