//
//  FilterVC.swift
//  FoodDelivery
//
//  Created by call soft on 02/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import TagListView
import UIKit
import MultiSlider
class FilterVC: ParentViewController, BaseViewControllerDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var listVw: UICollectionView!
    @IBOutlet weak var collVw: UICollectionView!
    @IBOutlet weak var listViewHeightConstrant: NSLayoutConstraint!
    @IBOutlet weak var star1: UIButton!
    @IBOutlet weak var star2: UIButton!
    @IBOutlet weak var star3: UIButton!
    @IBOutlet weak var star4: UIButton!
    @IBOutlet weak var star5: UIButton!
    @IBOutlet weak var time: UIView!
    @IBOutlet weak var distance: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    
    lazy var timeSlider:MultiSlider = {
        let slider = MultiSlider()
        slider.orientation = .horizontal
        slider.minimumValue = 15
        slider.maximumValue = 30
        slider.outerTrackColor = .black
        slider.value = [15, 30]
        slider.tintColor = AppColor.pink
        slider.trackWidth = 2
        slider.showsThumbImageShadow = false
        slider.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
        slider.keepsDistanceBetweenThumbs = false
        time.addSubview(slider)
        slider.frame = time.bounds
        slider.thumbImage = #imageLiteral(resourceName: "slider")
        return slider
    }()
    lazy var distanceSlider:MultiSlider = {
        let slider = MultiSlider()
        slider.orientation = .horizontal
        slider.minimumValue = 0
        slider.maximumValue = 20
        slider.outerTrackColor = .black
        slider.value = [0, 20]
        slider.tintColor = AppColor.pink
        slider.trackWidth = 2
        slider.showsThumbImageShadow = false
        slider.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
        slider.keepsDistanceBetweenThumbs = false
        distance.addSubview(slider)
        slider.frame = distance.bounds
        slider.thumbImage = #imageLiteral(resourceName: "slider")
        return slider
    }()
    
    var isResetTap: Bool?
    
    //MARK:- Properties
    
    var collData = ["1","2","3","4","5"]
    //var listData = ["Vegetarian","Vegan","Gluten Free","Keto","Paleo","Kosher","Halal"]
    var listData = [ListData(name: "Vegetarian", isSelected: false),ListData(name: "Vegan", isSelected: false),ListData(name: "Gluten Free", isSelected: false),ListData(name: "Keto", isSelected: false),ListData(name: "Paleo", isSelected: false),ListData(name: "Kosher", isSelected: false),ListData(name: "Halal", isSelected: false)]
    var restaurantData = [RestaurantData]()
    let filterVM = FilterViewModel(networking: ApiManager())
    var DietaryData = [CategoryData]()
    var filterDelegate: FiterDelegate?
    var rating:Int?
    var maxTime: Int?
    var minTime: Int?
    var maxDis: Int?
    var minDis: Int?
    var comingFrom: String?
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
        setupNavigation()
        setDataOnScreen()
        sliderSetup()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        self.baseDelegate = self
    }

    override func viewWillDisappear(_ animated: Bool) {
       // self.filterDelegate?.getFilterData()
    }
   
    //MARK:- Button Action
   
    @IBAction func btnActionFilter(_ sender: UIButton) {
        print("Apply Filter")
        self.setFilterData()
    }
    @IBAction func btnActionOrders(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OffersVC") as! OffersVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionCuisines(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CuisinesVC") as! CuisinesVC
        vc.restaurantData = self.restaurantData
        vc.comingFrom = self.comingFrom
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func setFilterData(){
        
        if isResetTap ?? false{
            self.rating = 0
            self.minTime = 15
            self.maxTime = 30
            self.minDis = 0
            self.maxDis = 20
            if comingFrom == "Search"{
                CuisineArraySearch?.removeAll()
            }else{
                CuisineArray?.removeAll()
            }
        }
        if comingFrom == "Search"{
            RatingSearch = self.rating
            TimeRangeSearch = [self.minTime ?? 15,self.maxTime ?? 30]
            DistanceRangeSearch = [self.minDis ?? 0,self.maxDis ?? 20]
        }else{
            Rating = self.rating
            TimeRange = [self.minTime ?? 15,self.maxTime ?? 30]
            DistanceRange = [self.minDis ?? 0,self.maxDis ?? 20]
        }
        self.filterDelegate?.getFilterData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func setDataOnScreen(){
        if comingFrom == "Search"{
            if RatingSearch == 1{
                btnRatingTap(star1)
            }else if RatingSearch == 2{
                btnRatingTap(star2)
            }else if RatingSearch == 3{
                btnRatingTap(star3)
            }else if RatingSearch == 4{
                btnRatingTap(star4)
            }else if RatingSearch == 5{
                btnRatingTap(star5)
            }else{
                resetRating([star1,star2,star3,star4,star5])
            }
            
          if TimeRange?.last ?? 0 >= 100{
               TimeRange?[1] = 30
           }
           if TimeRange?.first ?? 0 <= 15{
               TimeRange?[0] = 15
           }
           if DistanceRange?.last ?? 0 >= 100{
               DistanceRange?[1] = 20
           }
            
            timeSlider.value = [CGFloat(TimeRangeSearch?.first ?? 0),CGFloat(TimeRangeSearch?.last ?? 0)]
            distanceSlider.value = [CGFloat(DistanceRangeSearch?.first ?? 0),CGFloat(DistanceRangeSearch?.last ?? 0)]
            lblTime.text = "\((TimeRangeSearch?.first ?? 0)) Min - \((TimeRangeSearch?.last ?? 0)) Min"
            lblDistance.text = "\((DistanceRangeSearch?.first ?? 0)) KM - \((DistanceRangeSearch?.last ?? 0)) KM"
        }else{
            if Rating == 1{
                btnRatingTap(star1)
            }else if Rating == 2{
                btnRatingTap(star2)
            }else if Rating == 3{
                btnRatingTap(star3)
            }else if Rating == 4{
                btnRatingTap(star4)
            }else if Rating == 5{
                btnRatingTap(star5)
            }else{
                resetRating([star1,star2,star3,star4,star5])
            }
            
            if TimeRange?.last ?? 0 >= 100{
                 TimeRange?[1] = 30
             }
             if TimeRange?.first ?? 0 <= 15{
                 TimeRange?[0] = 15
             }
             if DistanceRange?.last ?? 0 >= 100{
                 DistanceRange?[1] = 20
             }
            
            timeSlider.value = [CGFloat(TimeRange?.first ?? 0),CGFloat(TimeRange?.last ?? 0)]
            distanceSlider.value = [CGFloat(DistanceRange?.first ?? 0),CGFloat(DistanceRange?.last ?? 0)]
            lblTime.text = "\((TimeRange?.first ?? 0)) Min - \((TimeRange?.last ?? 0)) Min"
            lblDistance.text = "\((DistanceRange?.first ?? 0)) KM - \((DistanceRange?.last ?? 0)) KM"
        }

    }
}
//MARK:- TagListView datasouce and delegate
//MARK:-
extension FilterVC {
    
    func initiallizers() {
        
        self.restaurantData.forEach({[weak self] item in
            self?.DietaryData.append(contentsOf: item.categoryData!)
        })
        self.configreDietaryCollection()
        star2.addTarget(self, action: #selector(btnRatingTap), for: .touchUpInside)
        star1.addTarget(self, action: #selector(btnRatingTap), for: .touchUpInside)
        star3.addTarget(self, action: #selector(btnRatingTap), for: .touchUpInside)
        star4.addTarget(self, action: #selector(btnRatingTap), for: .touchUpInside)
        star5.addTarget(self, action: #selector(btnRatingTap), for: .touchUpInside)

    }

    @objc func btnRatingTap(_ sender:UIButton){
        isResetTap = false
        if sender.tag == 1{
            ratingLogic(sender, [star2,star3,star4,star5])
        }else if sender.tag == 2{
            ratingLogic(sender, [star1,star3,star4,star5])
        }else if sender.tag == 3{
            ratingLogic(sender, [star2,star1,star4,star5])
        }else if sender.tag == 4{
            ratingLogic(sender, [star2,star3,star1,star5])
        }else{
            ratingLogic(sender, [star2,star3,star4,star1])
        }
    }
    
    func resetRating(_ unSelected:[UIButton]){
        unSelected.forEach({item in
            item.setTitle(item.tag.description, for: .normal)
            item.backgroundColor = AppColor.white
            item.setTitleColor(AppColor.appLightBlack, for: .normal)
            item.setImage(UIImage(named: "star_grey"), for: .normal)
        })
    }
    
    func ratingLogic(_ selected:UIButton,_ unSelected:[UIButton]){
        selected.setTitle(selected.tag.description, for: .normal)
        selected.backgroundColor = AppColor.pink
        selected.setTitleColor(AppColor.white, for: .normal)
        selected.setImage(UIImage(named: "star_white"), for: .normal)
        rating = selected.tag
        unSelected.forEach({item in
            item.setTitle(item.tag.description, for: .normal)
            item.backgroundColor = AppColor.white
            item.setTitleColor(AppColor.appLightBlack, for: .normal)
            item.setImage(UIImage(named: "star_grey"), for: .normal)
        })
    }
  
}
//MARK:- UICollectionViewDelegate,UICollectionViewDataSource
extension FilterVC : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    
    
    func configreDietaryCollection(){
       // OrderTimeCollectionCell
        listVw.delegate = self
        listVw.dataSource = self
        listVw.tag = 2
        listVw.backgroundColor = AppColor.clearColor
        listVw.register(UINib.init(nibName: "OrderTimeCollectionCell", bundle: nil), forCellWithReuseIdentifier: "OrderTimeCollectionCell")
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 160, height: 50)
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        listVw.collectionViewLayout = layout
        listViewHeightConstrant.constant = 230
        listVw.reloadData()

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.DietaryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : OrderTimeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderTimeCollectionCell", for: indexPath) as! OrderTimeCollectionCell
        cell.item = DietaryData[indexPath.row]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        self.handleError("Under Development")
        
//        if self.DietaryData[indexPath.row].isSelected ?? false{
//            self.DietaryData[indexPath.row].isSelected = false
//        }else{
//            self.DietaryData[indexPath.row].isSelected = true
//        }
//        collectionView.reloadItems(at: [indexPath])
    }
   
}
struct ListData {
    var name:String
    var isSelected:Bool
}
extension CGFloat {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> CGFloat {
        let divisor = pow(10.0, CGFloat(places))
        return (self * divisor).rounded() / divisor
    }
}
//MARK:- Globle Variables for filtering
var Rating: Int? = 0
var TimeRange: [Int]? = [15,30]
var DistanceRange: [Int]? = [0,20]
var CuisineArray: [CuisineArrayEn]? = []

//MARK:- Globle Variables for search filtering
var RatingSearch: Int? = 0
var TimeRangeSearch: [Int]? = [15,30]
var DistanceRangeSearch: [Int]? = [0,20]
var CuisineArraySearch: [CuisineArrayEn]? = []
