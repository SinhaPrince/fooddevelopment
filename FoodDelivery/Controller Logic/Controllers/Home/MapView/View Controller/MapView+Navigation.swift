//
//  Home+Navigation.swift
//  FoodDelivery
//
//  Created by Cst on 4/10/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

extension MapViewVC:BaseViewControllerDelegate,SearchViewAnimateble,UISearchBarDelegate,FiterDelegate{
    
    func getFilterData() {
        self.brandVM.filterHomeResult()
    }
    
    func setupNavigation(){
        
        btnBack.tag = UINavigationBarButtonType.backHomeNAll.rawValue
        btnFilter.tag = UINavigationBarButtonType.filter.rawValue
        btnSearch.tag = UINavigationBarButtonType.search.rawValue
        btnBack.addTarget(self, action: #selector(ParentViewController.navigationButtonTapped(_:)), for: .touchUpInside)
        btnSearch.addTarget(self, action: #selector(ParentViewController.navigationButtonTapped(_:)), for: .touchUpInside)
        btnFilter.addTarget(self, action: #selector(ParentViewController.navigationButtonTapped(_:)), for: .touchUpInside)

        searchBar.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(searchBar)
        searchBar.returnKeyType = .done
        searchBar.delegate = self
        searchBar.enablesReturnKeyAutomatically = true
        searchBar.showsCancelButton = true
        searchBar.bringSubviewToFront(view)
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: AppColor.pink]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(cancelButtonAttributes, for: .normal)
        NSLayoutConstraint.activate([searchBar.topAnchor.constraint(equalTo: addressView.bottomAnchor, constant: 20),
                                     searchBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
                                     searchBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
                                     searchBar.heightAnchor.constraint(equalToConstant: 50)])
        
    }
    
    func myAddressService(_ address:String,_ type:String){
        addressButton.removeFromSuperview()
        myAddressLable.removeFromSuperview()
        addressButton.titleLabel?.textAlignment = .left
        addressButton.contentHorizontalAlignment = .left
        
        addressButton.setTitle(comClass.createString(Str: type), for: .normal)
        addressButton.titleLabel?.font = AppFont.Medium.size(.Poppins, size: 14)
        addressButton.setTitleColor(AppColor.pink, for: .normal)
        addressButton.setImage(UIImage(named: "down_arrow_pink"), for: .normal)
        addressButton.tintColor = AppColor.pink
        addressButton.tag = UINavigationBarButtonType.address.rawValue
        addressButton.alignImageRight()
        addressButton.addTarget(self, action: #selector(ParentViewController.navigationButtonTapped(_:)), for: .touchUpInside)
        //Title View
        myAddressLable.font = AppFont.Regular.size(.Poppins, size: 10)
        myAddressLable.text = "My Address"
        myAddressLable.textColor = AppColor.lightGrayApp
       // let titleView = UIView(frame: CGRect(x: 15, y: 3, width: UIScreen.main.bounds.width - 100, height: 40))
        addressView.addSubview(addressButton)
        addressView.addSubview(myAddressLable)
        addressButton.frame = CGRect(x: 15, y: 10, width: UIScreen.main.bounds.width - 100, height: 30)
        addressView.backgroundColor = .clear
       // kAppDelegate.appNavigator.drawerController.navigationItem.titleView = titleView
    }
    
    func navigationBarButtonDidTapped(_ buttonType: UINavigationBarButtonType) {
        if buttonType == .filter{
//            let allAnnotations = self.mpMapView.annotations
//            self.mpMapView.removeAnnotations(allAnnotations)
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
            vc.restaurantData = self.brandVM.restaurantListData?.data ?? []
            let navigationControlr = UINavigationController(rootViewController: vc)
            vc.filterDelegate = self
            self.present(navigationControlr, animated: true, completion: nil)
        }else if buttonType == .address{
            if !comClass.isDataExist(){
                self.handleError("Login Required!")
                return
            }
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAddressVC") as! MyAddressVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if buttonType == .search{
            addSearchBar()
        }
    }
    
    func addSearchBar(){
        searchBar.fadeIn()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.fadeOut()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.brandVM.searchBar(textDidChange: searchText)
    }
    
}
