//
//  MapViewVC.swift
//  FoodDelivery
//
//  Created by call soft on 01/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Foundation
import MapKit
import UIKit.UIGestureRecognizerSubclass

class MapViewVC: ParentViewController,MapInfoDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnTime: UIButton!
    @IBOutlet weak var btnRating: UIButton!
    @IBOutlet weak var btnFreeDel: UIButton!
    @IBOutlet weak var dishCollection: UICollectionView!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnFavt: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var googleMap: GMSMapView!
    @IBOutlet weak var gestureview: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var addressView: UIView!
    let addressButton = UIButton()
    let myAddressLable = UILabel(frame: CGRect(x: 15, y: 5, width: UIScreen.main.bounds.width - 100, height: 10))
    //MARK:- Properties
    var anoo = [MKAnnotation]()
    let annotation = MKPointAnnotation()
    private let popupOffset: CGFloat = 320

    lazy var searchBar:UISearchBar = {
        let search = UISearchBar()
        search.compatibleSearchTextField.backgroundColor = .white
        search.placeholder = "Brand Name,Cuisine.."
        search.setImage(#imageLiteral(resourceName: "search"), for: .search, state: .normal)
        search.alpha = 0
        search.layer.cornerRadius = 10
        search.clipsToBounds = true
        search.shadowColor = .black
        search.shadowOpacity = 0.3
        search.shadowRadius = 4
        search.layer.borderColor = UIColor.white.cgColor
        search.layer.borderWidth = 1
        search.delegate = self
        return search
    }()

    
    let brandVM = BrandLocationViewModel(networking: ApiManager())

    private lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(popupViewPanned(recognizer:)))
        return recognizer
    }()
 
    @objc private func popupViewPanned(recognizer: UIPanGestureRecognizer) {
        bottomView.fadeOut()
        logoImage.fadeOut()
    }
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // initiallizers()
        setupNavigation()
        self.initializeTheLocationManager()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(true)
        self.baseDelegate = self
        self.brandVM.controller = self
        self.bottomViewConfiguration()
        self.googleMap.settings.allowScrollGesturesDuringRotateOrZoom = false
//        self.googleMap.settings.myLocationButton = true
//        self.googleMap.isMyLocationEnabled = true
        googleMap.settings.myLocationButton = false
        googleMap.isMyLocationEnabled = true
        googleMap.delegate = self
        DispatchQueue.main.async {
            comClass.isDataExist() ? self.fetchAddress() : self.checkLocationPermission()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideNavigationBar(false)
    }
    //MARK:- Network Calling
    
    func popularBrandsService(_ lat:String,_ long:String){
        var param = [String:Any]()
        if !comClass.isDataExist(){
            param = ["longitude" : long,"latitude":lat] as [String:Any]
        }else{
            param = ["longitude" : long,"latitude":lat,"userId":comClass.getInfoById().Id ?? ""] as [String:Any]
        }
        self.brandVM.fetchPopularBrandService("getAllRestaurantsData", param) { (status, error) in
            guard error == nil else{
                self.handleError(error?.localizedDescription)
                return
            }
            guard status == 200 else{
                self.handleError(self.brandVM.restaurantListData?.message ?? "")
                return
            }
        }
      
        self.brandVM.filterHomeResult()
        brandVM.didFinishFetch = {
            self.googleMap.clear()
            
            let location = CLLocationCoordinate2D(latitude: (lat as NSString).doubleValue, longitude: (long as NSString).doubleValue)
            let marker = GMSMarker()
            marker.icon = #imageLiteral(resourceName: "pin")
            marker.position = location
            marker.map = self.googleMap
            var count = 0
            
            if self.brandVM.mainRestData?.count ?? 0 <= 0 {
                let camera = GMSCameraPosition.camera(withLatitude: (lat as NSString).doubleValue, longitude: (long as NSString).doubleValue, zoom: 10) //Set default lat and long
                self.googleMap.camera = camera
            }
            
            self.brandVM.mainRestData?.forEach({data in
                let location = CLLocationCoordinate2D(latitude: ((data.latitude ?? "") as NSString).doubleValue, longitude: ((data.longitude ?? "") as NSString).doubleValue)
                print("location: \(location)")
                let camera = GMSCameraPosition.camera(withLatitude: ((data.latitude ?? "") as NSString).doubleValue, longitude: ((data.longitude ?? "") as NSString).doubleValue, zoom: 10) //Set default lat and long
                self.googleMap.camera = camera
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
                imageView.cornerRadius = 30
                imageView.shadowColor = .black
                imageView.shadowOffset = CGSize(width: 4, height: 4)
                imageView.shadowRadius = 5
                imageView.shadowOpacity = 0.3
                imageView.circleCorner = true
                imageView.clipsToBounds = true
                imageView.contentMode = .scaleAspectFill
                imageView.isUserInteractionEnabled = true
                imageView.setImage(withImageId: data.brandData?.logo ?? "", placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"))
                let marker = GMSMarker()
                marker.iconView = imageView
                marker.iconView?.isUserInteractionEnabled = true
                marker.position = location
                marker.userData = data
                marker.snippet = data.brandData?.storeName ?? ""
                marker.map = self.googleMap
                count += 1
            })
           
        }
    }

//    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
//        self.googleMap.clear()
//        let marker = GMSMarker()
//        marker.icon = #imageLiteral(resourceName: "pin")
//        marker.iconView?.isUserInteractionEnabled = true
//        CATransaction.begin()
//        CATransaction.setAnimationDuration(2.0)
//        marker.position = position.target
//        CATransaction.commit()
//        marker.map = self.googleMap
//
//    }
    
    func fetchAddress(){
        
        self.brandVM.fetchAddress("getAddressList") { (status, error) in
            guard error == nil else{
                self.handleError(error?.localizedDescription)
                return
            }
            guard status == 200 else{
                self.handleError(self.brandVM.addressData?.message ?? "")
                return
            }
            if self.brandVM.addressData?.data?.count ?? 0 > 0{
                let data = self.brandVM.addressData?.data?.filter({item in
                    item.defaultStatus == true
                })
                if data?.count == 0 {
                    self.checkLocationPermission()
                    return
                }
                self.myAddressService(data?.first?.landmark ?? "", data?.first?.addressType ?? "")
                self.popularBrandsService(data?.first?.latitude?.description ?? "", data?.first?.longitude?.description ?? "")

            }else{
                self.checkLocationPermission()
            }
        }
    }

    func checkLocationPermission(){
        
        self.locationPermission { (permission) in
            
            if permission == "Access"{
               
                let defaultAddress = self.realm.objects(SaveDefaultAddress.self)
                if defaultAddress.count > 0 {
                    let strLat = (defaultAddress.first?.latitude ?? "") as NSString
                    let strLong = (defaultAddress.first?.longitude ?? "") as NSString
                    self.parseAddress(latitude: strLat.doubleValue, logitude: strLong.doubleValue) { (address,subLocality) in
                        self.myAddressService(subLocality, subLocality)
                    }
                    self.popularBrandsService(defaultAddress.first?.latitude ?? "", defaultAddress.first?.longitude ?? "")

                }else{
                    let strLat = self.lat as NSString
                    let strLong = self.long as NSString
                    self.parseAddress(latitude: strLat.doubleValue, logitude: strLong.doubleValue) { (address,subLocality) in
                        self.myAddressService(subLocality, subLocality)
                    }
                    self.popularBrandsService(self.lat, self.long)
                }
                

            }else{
                let defaultAddress = self.realm.objects(SaveDefaultAddress.self)
                if defaultAddress.count > 0 {
                    let strLat = (defaultAddress.first?.latitude ?? "") as NSString
                    let strLong = (defaultAddress.first?.longitude ?? "") as NSString
                    self.parseAddress(latitude: strLat.doubleValue, logitude: strLong.doubleValue) { (address,subLocality) in
                        self.myAddressService(subLocality,subLocality)
                    }
                    self.popularBrandsService(defaultAddress.first?.latitude ?? "", defaultAddress.first?.longitude ?? "")
                    
                }else{
                    //Full Screen popup
                    let vc = LocationPermissionVC()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
    }
    
    @objc func tapMap(){
        bottomView.fadeOut()
        logoImage.fadeOut()
    }
    
    func bottomViewConfiguration(){
        logoImage.contentMode = .scaleAspectFill
        bottomView.isUserInteractionEnabled = true
        gestureview.addGestureRecognizer(panRecognizer)
        bottomView.backgroundColor = .white
        googleMap.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapMap)))
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        bottomView.layer.shadowColor = UIColor.black.cgColor
        bottomView.layer.shadowOpacity = 0.1
        bottomView.alpha = 0
        logoImage.alpha = 0
      //  btnFavt.tintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        btnFavt.layer.cornerRadius = 15
        btnFavt.addTarget(self, action: #selector(getFavt), for: .touchUpInside)
        btnMore.addTarget(self, action: #selector(btnMoreTap), for: .touchUpInside)
    }
    
    @objc func getFavt(){
        let params = ["restaurantId":self.brandVM.restaurantInfo?.Id ?? ""] as [String:Any]
        self.brandVM.favtTap(params) { (status, error) in
            guard error == nil else{
                self.handleError(error?.localizedDescription ?? "")
                return
            }
            guard status == 200 else{
                self.handleError("Something went wrong!")
                return
            }
            if self.btnFavt.isSelected{
                self.btnFavt.setImage( #imageLiteral(resourceName: "heart_g") , for: .normal)
            }else{
                self.btnFavt.setImage( #imageLiteral(resourceName: "heart") , for: .normal)
            }
        }
    }
  
    @objc func btnMoreTap(){
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantDetailVC") as! RestaurantDetailVC
        vc.id = self.brandVM.restaurantInfo?.Id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

//MARK:- GMS MARKER
extension MapViewVC:GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let data = marker.userData as? RestaurantData
        self.brandVM.restaurantInfo = data
        print(data?.address ?? "")
        self.bottomView.fadeIn()
        self.logoImage.fadeIn()
       // self.logoImage.alpha = 1
        self.view.bringSubviewToFront(self.bottomView)
        self.view.bringSubviewToFront(self.logoImage)
        let restData = data
        self.logoImage.sd_setImage(with: URL(string: restData?.brandData?.logo ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"), options: .lowPriority, context: [:])
        self.lblTitle.provideAttributedTextToControlLeftAllignCenter(restData?.branchNameEn ?? "", "\n\(restData?.address ?? "")", AppFont.Medium.size(.Poppins, size: 20), AppFont.Light.size(.Poppins, size: 12), AppColor.appLightBlack, AppColor.appLightGray, .center)
        self.btnFavt.setImage((restData?.isFav ?? false) ? #imageLiteral(resourceName: "heart") : UIImage(named: "heart_w") , for: .normal)
        self.initiallizers()
        self.btnFavt.isSelected = restData?.isFav ?? false
       return true
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        bottomView.fadeOut()
        logoImage.fadeOut()
    }
}


extension MapViewVC:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.brandVM.restaurantInfo?.brandData?.cuisineArrayEn?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      return self.brandVM.collectionCellInstance1(collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidth: CGFloat = flowLayout.itemSize.width
        let cellSpacing: CGFloat = flowLayout.minimumInteritemSpacing
        var cellCount = CGFloat(collectionView.numberOfItems(inSection: section))
        var collectionWidth = collectionView.frame.size.width
        var totalWidth: CGFloat
        if #available(iOS 11.0, *) {
            collectionWidth -= collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right
        }
        repeat {
            totalWidth = cellWidth * cellCount + cellSpacing * (cellCount - 1)
            cellCount -= 1
        } while totalWidth >= collectionWidth
        
        if (totalWidth > 0) {
            let edgeInset = (collectionWidth - totalWidth) / 2
            return UIEdgeInsets.init(top: flowLayout.sectionInset.top, left: edgeInset, bottom: flowLayout.sectionInset.bottom, right: edgeInset)
        } else {
            return flowLayout.sectionInset
        }
    }
    func initiallizers() {
        dishCollection.backgroundColor = .white
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.itemSize = CGSize(width: 105, height: 40)
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        dishCollection.setCollectionViewLayout(layout, animated: true)
        dishCollection.delegate = self
        dishCollection.dataSource = self
        dishCollection.showsHorizontalScrollIndicator = false
        self.dishCollection.reloadData()
    }
  
}

extension UIView {
     func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
           self.alpha = 1.0
            self.isHidden = false

             }, completion: completion)
        
     }
 
    func fadeInWithShown(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
       UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
          self.alpha = 1.0
           self.isHidden = false

            }, completion: completion)
       
    }
    func fadeOutWithHidden(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
            self.isHidden = true
            }, completion: completion)
    }
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
            }, completion: completion)
    }
}
