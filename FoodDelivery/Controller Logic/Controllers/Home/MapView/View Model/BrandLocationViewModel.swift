//
//  BrandLocationViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 4/14/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import Foundation
import MapKit
protocol AnnotationViewReuseableProtocol {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView
}
class BrandLocationViewModel:BaseViewModel,AnnotationViewReuseableProtocol,CellRepresentable{
    var rowHeight: CGFloat = 40
   
    var mainRestData : [RestaurantData]?{
        didSet{
            self.didFinishFetch?()
        }
    }
    var filterData : [RestaurantData]?{
        didSet{
            mainRestData = filterData ?? []
        }
    }
    
    var restaurantListData: RestaurantListModel?{
        didSet{
           // self.mainRestData = restaurantListData?.data
            filterHomeResult()
        }
    }
    var restaurantInfo:RestaurantData?
    var addressData:AddressListModel?

    var controller = UIViewController()
    private var dataService: ApiManager?
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    
    //MARK:- MKAnnotation Service
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView {
       
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "newsAnnotationView")
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "newsAnnotationView")
        }
        annotationView?.frame = CGRect(x: 0, y: 0, width: 60, height: 60)

        if annotation is RestaurantData{
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
            imageView.cornerRadius = 30
            imageView.shadowColor = .black
            imageView.shadowOffset = CGSize(width: 4, height: 4)
            imageView.shadowRadius = 5
            imageView.shadowOpacity = 0.3
            imageView.circleCorner = true
            imageView.clipsToBounds = true
            imageView.contentMode = .scaleAspectFill
            let data:RestaurantData = annotation as! RestaurantData
            imageView.sd_setImage(with: URL(string: data.brandData?.logo ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"), options: .lowPriority, context: [:])

            imageView.isUserInteractionEnabled = true
            annotationView?.addSubview(imageView)
            annotationView?.isUserInteractionEnabled = true
           // annotationView?.detailCalloutAccessoryView = UIView()
            annotationView?.canShowCallout = true
        }
//        else{
//            annotationView?.image = #imageLiteral(resourceName: "dot_gps")
//            return annotationView!
//        }
        
        return annotationView!
    }
    //MARK:- UICollectionView Setup
    func collectionCellInstance1(_ tableView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        tableView.register(UINib(nibName: "RestaurantInfoCollectionCell", bundle: nil), forCellWithReuseIdentifier: "RestaurantInfoCollectionCell")
        guard let cell = tableView.dequeueReusableCell(withReuseIdentifier: "RestaurantInfoCollectionCell", for: indexPath) as? RestaurantInfoCollectionCell else {
            return UICollectionViewCell()
        }
        cell.item = restaurantInfo?.brandData?.cuisineArrayEn?[indexPath.row]
        return cell
    }
    
    
    //MARK:- Filter Logic Implementatiom
    
    func searchBar(_ searchBar: UISearchBar? = nil, textDidChange searchText: String) {
        
        filterContentForSearchText(searchText: searchText)
    }
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        if searchText != "" {
            
            filterData = (restaurantListData?.data?.filter {data in
                
                return (data.brandData?.storeName?.lowercased().contains(searchText.lowercased()) ?? false) || (filterCuisines(data, searchText))
                
            } ?? [RestaurantData]())
        }
        else { self.filterData = self.restaurantListData?.data}
    }
    
    func filterCuisines(_ data: RestaurantData,_ searchText:String) -> Bool{
        guard let data = data.brandData?.cuisineArrayEn else {
            return false
        }
        return data.contains(where: {item in
            (item.itemText?.lowercased().contains(searchText.lowercased()) ?? false)
        })
    }
}

//MARK:- Network Calling
extension BrandLocationViewModel{
    //MARK:- Favourite Api Service
    func favtTap(_ param:[String:Any],_ complition:@escaping (Int?,Error?)->()){
        
        if !comClass.isDataExist(){
            controller.show_Alert(message: "Login Required!")
            return
        }
        
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        self.dataService?.fetchApiService(method: .post, url: "addToFavourite", passDict: param, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                complition(data["status"].intValue,nil)
                break
            case .failure(let error):
                complition(nil,error)
                break
            }
        })
        
    }
    
    /// Get RestaurantList Service
    /// - Parameters:
    ///   - url: Api EndPoint
    ///   - params: Lat and Long as Parameter and UserId If login
    func fetchPopularBrandService(_ url:String,_ params:[String:Any],complition:@escaping (Int?,Error?) ->()) {
        self.dataService?.fetchApiService(method: .post, url: url,passDict: params, callback: { (result) in
            switch result{
            case .success(let data):
                self.restaurantListData = RestaurantListModel(data)
                complition(self.restaurantListData?.status,nil)
                break
            case .failure(let error):
                complition(nil,error)
                break
            }
        })
    }
    
    /// Returrn All Saved User Addresses
    /// - Parameter url: ApiKey
    func fetchAddress(_ url:String,complition:@escaping (Int?,Error?) ->()){
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        self.dataService?.fetchApiService(method: .get, url: url, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                self.addressData = AddressListModel(data)
                complition(self.addressData?.status,nil)
                break
            case .failure(let error):
                complition(nil,error)
                break
            }
        })
        
    }
    
    func filterHomeResult(){
    
        var filterArray: [RestaurantData]?
            filterArray = self.restaurantListData?.data?.filter({ item in
                (item.avgRating ?? 0 >= Rating ?? 0)
            })
            filterArray = filterArray?.filter({item in
                 (item.areaData?.deliveryTime ?? 0 >= TimeRange?.first ?? 15) &&
                 (item.areaData?.deliveryTime ?? 0 <= TimeRange?.last ?? 30)
            })
            filterArray = filterArray?.filter({item in
                item.dist?.calculated ?? 0 >= ((DistanceRange?.first ?? 0) * 1000) && item.dist?.calculated ?? 0 <= ((DistanceRange?.last ?? 20) * 1000)
            })
        self.filterData = filterArray

    }
}
