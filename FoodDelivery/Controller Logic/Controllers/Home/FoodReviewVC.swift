//
//  FoodReviewVC.swift
//  FoodDelivery
//
//  Created by call soft on 08/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import Cosmos

class FoodReviewVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var imgReview: UIImageView!
    @IBOutlet weak var txtVw: UITextView!
    @IBOutlet weak var vwRatings: CosmosView!
    
    //MARK:- Properties
    var finalRating = String()
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallizers()
        setupNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])

        let title = UILabel()
        title.text = comClass.createString(Str: "Food Review")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .left
        
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
       
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionDone(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
}


//MARK:- Custom Methods
extension FoodReviewVC {
    func initiallizers() {
        configureViewTextView()
        vwRatings.rating = 0.0
        vwRatings.settings.fillMode = .full
        vwRatings.didTouchCosmos = didTouchCosmos
        print(finalRating)
    }
    
    
   private func didTouchCosmos(_ rating: Double) {
        finalRating = "\(rating)"
    if rating <= 2.0 {
        imgReview.image = UIImage.init(named: "2star.jpg")
    }
    else if rating == 4.0 {
        imgReview.image = UIImage.init(named: "4star.jpg")
    }
    else if rating == 5.0 {
        imgReview.image = UIImage.init(named: "5star.png")
    }
    else
    {
        imgReview.image = UIImage.init(named: "2star.jpg")
    }
    
        print(finalRating)
        
    }
}

// MARK: - TextFieldDelegate
extension FoodReviewVC : UITextViewDelegate{
    
    func configureViewTextView(){
        
        txtVw.text = " Write here..."
        txtVw.textColor = AppColor.placeHolderColor
        txtVw.delegate = self
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if txtVw.text == " Write here..."{
            txtVw.text = ""
            txtVw.textColor = UIColor.black
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if txtVw.text == ""
        {
            txtVw.text = " Write here..."
            txtVw.textColor =  AppColor.placeHolderColor
        }
        
    }
}
