//
//  AllRestaurantsVC.swift
//  FoodDelivery
//
//  Created by call soft on 22/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class AllRestaurantsVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    
    //MARK:- Properties
    
    var orderPastImg = [UIImage.init(named: "tortilla.jpg"),UIImage.init(named: "friedshrimps.jpg")]
    var orderPastNames = ["Tortilla Wrap","Fried Shrimps"]
    var orderPastPrice = ["AED 18.99","AED 29.00"]
    var orderType = "running"
    var restaurantData = [RestaurantData]()
    let searchVM = AllRestaurantViewModel(networking: ApiManager())
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
        setupNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        
        let title = UILabel()
        title.text = comClass.createString(Str: "All Restaurants")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .left
        
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
        
    }
    
    
    
    func giveShadowToButton(btn:UIButton,shadowColor:UIColor,textColor:UIColor,backgroundColor:UIColor) {
        btn.setTitleColor(textColor, for: .normal)
        btn.backgroundColor = backgroundColor
        btn.shadowColor = shadowColor
        btn.shadowOffset = CGSize(width: 0, height: 8)
        btn.shadowOpacity = 1
        btn.shadowRadius = 5
    }
    
    
}

//MARK:- Custom Methods
extension AllRestaurantsVC:UITextFieldDelegate{
    
    @objc func textIsChanging(textField: UITextField) {
        self.searchVM.searchBar(textDidChange: textField.text ?? "")
    }
    

}

extension AllRestaurantsVC {
    func initiallizers() {
        searchVM.controller = self
        txtSearch.delegate = self
        txtSearch.addTarget(self, action: #selector(self.textIsChanging(textField:)), for: .editingChanged)
        initializeTheLocationManager()
        self.searchVM.restaurantListData = restaurantData
        self.configureTable()
        self.searchVM.didFinishFetch = {
            self.configureTable()
        }

    }
    
  
    
}
//MARK:- UITableViewDelegate and UITableViewDatasource
extension AllRestaurantsVC : UITableViewDelegate, UITableViewDataSource{
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchVM.mainRestData?.count ?? 0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.searchVM.cellInstance(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.searchVM.rowHeight
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.searchVM.rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantDetailVC") as! RestaurantDetailVC
        vc.id = self.searchVM.mainRestData?[indexPath.row].Id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:-ViewModel
class AllRestaurantViewModel: BaseViewModel,CellRepresentable {
    var rowHeight: CGFloat = UITableView.automaticDimension
    var controller = UIViewController()
    var mainRestData : [RestaurantData]?{
        didSet{
            self.didFinishFetch?()
        }
    }
    var filterData : [RestaurantData]?{
        didSet{
            mainRestData = filterData ?? []
        }
    }
    var restaurantListData: [RestaurantData]?{
        didSet{
            mainRestData = restaurantListData ?? []
        }
    }
    private var dataService: ApiManager?

    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    //Table CellForRow Setup
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        tableView.register(UINib(nibName: "FavouritesTableCell", bundle: nil), forCellReuseIdentifier: "FavouritesTableCell")
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavouritesTableCell") as? FavouritesTableCell else {
            return UITableViewCell()
        }
        cell.item = self.mainRestData?[indexPath.row]
        cell.btnFavt.tag = indexPath.row
        cell.btnFavt.addTarget(self, action: #selector(favtTap(_:)), for: .touchUpInside)
        cell.btnTracking.addTarget(self, action: #selector(liveTracking(_:)), for: .touchUpInside)
        return cell
        
    }
    @objc func liveTracking(_ sender:UIButton){
        let vc = controller.storyboard?.instantiateViewController(withIdentifier: "OnItsWayVC") as! OnItsWayVC
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func favtTap(_ sender:UIButton){
        
        if !comClass.isDataExist(){
            controller.show_Alert(message: "Login Required!")
            return
        }
        
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        let params = ["restaurantId":self.mainRestData?[sender.tag].Id ?? ""] as [String:Any]
        self.dataService?.fetchApiService(method: .post, url: "addToFavourite", passDict: params, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                if data["status"].intValue == 200{
                    if data["message"].stringValue == "Remove from favourite successfully"{
                        sender.setImage(#imageLiteral(resourceName: "heart_g"), for: .normal)
                    }else{
                        sender.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
                    }
                }else{
                    self.controller.show_Alert(message: data["message"].stringValue)
                }
                break
            case .failure(let error):
                self.controller.show_Alert(message: error.localizedDescription)
                break
            }
        })
        
    }
    func searchBar(_ searchBar: UISearchBar? = nil, textDidChange searchText: String) {
        if !(searchText.isEmpty){
            filterData = (restaurantListData?.filter {data in
                return (data.branchNameEn?.lowercased().contains(searchText.lowercased()) ?? false)
                
            } ?? [RestaurantData]())
        }else{
            filterData = restaurantListData
        }
       
    }
}
