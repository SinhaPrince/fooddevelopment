//
//  PastOrderListVC.swift
//  FoodDelivery
//
//  Created by call soft on 22/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class PastOrderListVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblVw: UITableView!
    
    //MARK:- Properties
    
    var orderPastImg = [UIImage.init(named: "tortilla.jpg"),UIImage.init(named: "friedshrimps.jpg"),UIImage.init(named: "tortilla.jpg")]
    var orderPastNames = ["Tortilla Wrap","Fried Shrimps","Tortilla Wrap"]
    var orderPastPrice = ["AED 18.99","AED 29.00","AED 18.99"]
    var orderType = "running"
    
    let myOrderVM = MyOrderViewModel()
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        myOrderVM.controller = self
        initiallizers()
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        
        let title = UILabel()
        title.text = comClass.createString(Str: "My Past Orders")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .left
        
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
        
    }
    
    //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func giveShadowToButton(btn:UIButton,shadowColor:UIColor,textColor:UIColor,backgroundColor:UIColor) {
        btn.setTitleColor(textColor, for: .normal)
        btn.backgroundColor = backgroundColor
        btn.shadowColor = shadowColor
        btn.shadowOffset = CGSize(width: 0, height: 8)
        btn.shadowOpacity = 1
        btn.shadowRadius = 5
    }
    
    
}
//MARK:- Custom Methods
extension PastOrderListVC {
    func initiallizers() {
       
        myOrderVM.OrderData.removeAll()
        myOrderVM.OrderData.append(contentsOf: [MyOrderData(brandLogo: #imageLiteral(resourceName: "burger"), itemLogo: #imageLiteral(resourceName: "burger"), itemName: "BURGUR KING", orderNumber: "#1154", productAmount: "03.99", itemQuantity: "3", itemStatus: "Delivered", isPast: true),MyOrderData(brandLogo: #imageLiteral(resourceName: "pizzahut"), itemLogo: #imageLiteral(resourceName: "pizzahut"), itemName: "PIZZA HUT", orderNumber: "#1155", productAmount: "03.99", itemQuantity: "2", itemStatus: "Delivered", isPast: true),MyOrderData(brandLogo: #imageLiteral(resourceName: "burger"), itemLogo: #imageLiteral(resourceName: "burger"), itemName: "BURGUR KING", orderNumber: "#1154", productAmount: "03.99", itemQuantity: "3", itemStatus: "Delivered", isPast: true),MyOrderData(brandLogo: #imageLiteral(resourceName: "pizzahut"), itemLogo: #imageLiteral(resourceName: "pizzahut"), itemName: "PIZZA HUT", orderNumber: "#1155", productAmount: "03.99", itemQuantity: "2", itemStatus: "Delivered", isPast: true)])
        configureTable()
    }
    
    
    
}
//MARK:- UITableViewDelegate and UITableViewDatasource
extension PastOrderListVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.myOrderVM.OrderData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.myOrderVM.cellInstance(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.myOrderVM.rowHeight
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.myOrderVM.rowHeight
    }
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
         vc.isFrom = orderType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func TrackOrder(_sender:UIButton) {
            print("Re-Order")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @objc func CancelOrder(_sender:UIButton) {
            print("Ratings")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FoodReviewVC") as! FoodReviewVC
            self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
