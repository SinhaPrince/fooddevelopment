//
//  ResetPasswordVC.swift
//  FoodDelivery
//
//  Created by call soft on 24/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class ResetPasswordVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var lblConfPassword: UILabel!
    @IBOutlet weak var txtConfPassword: UITextField!
    
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var btnPass: UIButton!
    @IBOutlet weak var btnConfPass: UIButton!
    
    //MARK:- Properties
    var passShow = Bool()
    var isFrom = String()
    var delegate: CustomDelegate?
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
            initiallizers()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
    }
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionChange(_ sender: UIButton) {
        print("Change")
        
        if txtPassword.text?.count ?? 0 < 6 || txtPassword.text?.count ?? 0 > 16 {
            handleError( "Password must contain minimum 6 characters and maximum 16 characters.")
            return
        }
        guard txtPassword.text == txtConfPassword.text else {
            handleError("Password didn't match!")
            return
        }
        
        self.delegate?.getPasswordBack?(password: txtPassword.text ?? "")
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func btnActionPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if !sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "view"), for: .normal)
        }else{
            sender.setImage(#imageLiteral(resourceName: "hide"), for: .normal)
        }
        txtPassword.isSecureTextEntry = sender.isSelected
    }
    @IBAction func btnActionNewPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        txtConfPassword.isSecureTextEntry = sender.isSelected
        if !sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "view"), for: .normal)
        }else{
            sender.setImage(#imageLiteral(resourceName: "hide"), for: .normal)
        }
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.back], rightBarButtonsType: [])
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 55))
           let titleImageView = UIImageView(image: UIImage(named: "header_logo"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: titleView.frame.width, height: titleView.frame.height)
           titleView.addSubview(titleImageView)
           navigationItem.titleView = titleView
    }

}

//MARK:- Custom Methods
extension ResetPasswordVC : UITextFieldDelegate{
    
    func initiallizers() {
      
        txtPassword.delegate = self
        txtConfPassword.delegate = self
        if isFrom == "signup" {
            lblTitle.text = "Set Password"
            btnChange.setTitle("Submit", for: .normal)
        }
        else
        {
            lblTitle.text = "Reset Password"
            btnChange.setTitle("Change", for: .normal)
        }
    }
}
