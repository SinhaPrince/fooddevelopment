//
//  LoginVC.swift
//  FoodDelivery
//
//  Created by call soft on 24/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import MaterialComponents

class LoginVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var viewOuterEmail: UIView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtEmail: MDCFilledTextField!
    
    @IBOutlet weak var viewOuterPassword: UIView!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtPassword: MDCFilledTextField!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    //MARK:- Properties
    
    let validation = Validation()

    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallizers()
    
        self.txtEmail.setUp("Email", "Enter your email", #imageLiteral(resourceName: "email"))
        self.txtPassword.setUp("Password", "Enter your password", #imageLiteral(resourceName: "lock"))
        let eye = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        eye.setImage(#imageLiteral(resourceName: "hide"), for: .normal)
        eye.setImage(#imageLiteral(resourceName: "view"), for: .selected)
        eye.contentMode = .scaleAspectFit
        eye.addTarget(self, action: #selector(btnEyeTap(_:)), for: .touchUpInside)
        self.txtPassword.trailingView = eye
        self.txtPassword.trailingViewMode = .always
        
    }
    
    
    
    @objc func btnEyeTap(_ sender:UIButton){
        sender.isSelected = !sender.isSelected
        self.txtPassword.isSecureTextEntry = !self.txtPassword.isSecureTextEntry
    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.black], rightBarButtonsType: [])
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 55))
           let titleImageView = UIImageView(image: UIImage(named: "header_logo"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: titleView.frame.width, height: titleView.frame.height)
           titleView.addSubview(titleImageView)
           navigationItem.titleView = titleView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        setupNavigation()
      
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideNavigationBar(false)
    }
    @IBAction func btnActionSignUp(_ sender: UIButton) {
    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
    self.navigationController?.pushViewController(vc, animated: true)
}
    @IBAction func btnActionLogin(_ sender: UIButton) {
        
        checkValidation()
    }
    @IBAction func btnActionForgotPassword(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}

//MARK:- Custom Methods
extension LoginVC : UITextFieldDelegate{
    func initiallizers() {
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtEmail.tag = 1
        txtPassword.tag = 2
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if !(textField.text?.isEmpty ?? false){
            if nextTag == 2{
                self.txtPassword.becomeFirstResponder()
            }else{
                self.btnActionLogin(btnLogin)
            }
            
        }else{
            textField.becomeFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtEmail.leadingAssistiveLabel.text = ""
        txtEmail.setLeadingAssistiveLabelColor(.red, for: .editing)
        txtEmail.setLeadingAssistiveLabelColor(.red, for: .normal)
        txtPassword.leadingAssistiveLabel.text = ""
        txtPassword.setLeadingAssistiveLabelColor(.red, for: .editing)
        txtPassword.setLeadingAssistiveLabelColor(.red, for: .normal)
        viewOuterEmail.borderColor = AppColor.appLightGray
        viewOuterPassword.borderColor = AppColor.appLightGray
        
    }
    
    func checkValidation(){
       
        if !validation.validateEmail(txtEmail.text){
            txtEmail.leadingAssistiveLabel.text = "Please enter valid email id!"
            txtEmail.setLeadingAssistiveLabelColor(.red, for: .editing)
            txtEmail.setLeadingAssistiveLabelColor(.red, for: .normal)
            viewOuterEmail.borderColor = .red
            return
        }
        if txtPassword.text?.count ?? 0 < 6 || txtPassword.text?.count ?? 0 > 16 {
            txtPassword.leadingAssistiveLabel.text = "Password must contain minimum 6 characters and maximum 16 characters."
            txtPassword.setLeadingAssistiveLabelColor(.red, for: .editing)
            txtPassword.setLeadingAssistiveLabelColor(.red, for: .normal)
            viewOuterPassword.borderColor = .red

            return
        }
        self.fetchSignService()
  
    }
}



extension MDCFilledTextField{
    func setUp(_ topText:String,_ placeHolder:String,_ leadingImage:UIImage){
        self.label.setup(topText, AppColor.appLightGray, AppFont.Regular.size(.Poppins, size: 12), .left)
        self.setFloatingLabelColor(AppColor.appLightGray, for: .normal)
        self.setFloatingLabelColor(AppColor.appLightGray, for: .editing)
        self.setNormalLabelColor(AppColor.appLightGray, for: .normal)
        self.placeholder = placeHolder
        self.setUnderlineColor(.clear, for: .normal)
        self.setUnderlineColor(.clear, for: .editing)
        self.setFilledBackgroundColor(.clear, for: .normal)
        self.setFilledBackgroundColor(.clear, for: .editing)
        let image = UIImageView()
        image.image = leadingImage
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        let view = UIView()
        view.backgroundColor = AppColor.appLightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        mainView.addSubview(view)
        mainView.addSubview(image)
        NSLayoutConstraint.activate([view.heightAnchor.constraint(equalTo: mainView.heightAnchor, multiplier: 0.8),
                                     view.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
                                     view.centerYAnchor.constraint(equalTo: mainView.centerYAnchor),
                                     view.widthAnchor.constraint(equalToConstant: 1),
                                     image.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
                                     image.heightAnchor.constraint(equalToConstant: 25),
                                     image.widthAnchor.constraint(equalToConstant: 25),
                                     image.centerYAnchor.constraint(equalTo: mainView.centerYAnchor)
        ])
        self.leadingView = mainView
        self.leadingViewMode = .always
        
    }
    
    func errorHandler(_ message:String,_ color:UIColor){
        self.leadingAssistiveLabel.text = message
        self.setLeadingAssistiveLabelColor(color, for: .editing)
        self.setLeadingAssistiveLabelColor(color, for: .normal)
    }
}
