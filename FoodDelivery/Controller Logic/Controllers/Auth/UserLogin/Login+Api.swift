//
//  Login+Api.swift
//  FoodDelivery
//
//  Created by Cst on 4/8/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
extension LoginVC{
    
    func fetchSignService(){
        
        Indicator.shared.showLottieAnimation(view: self.view)
        
        let params = ["email":self.txtEmail.text ?? "",
                      "password":self.txtPassword.text ?? "",
                      "deviceType":"ios",
                      "deviceToken":UserDefaults.standard.value(forKey: "DeviceToken") ?? ""] as [String:Any]
        viewModel.getSignService(APIEndpoint.signInApi, params)
        clouserSetup()
    }
    
    func clouserSetup(){
        viewModel.success = {[weak self] (message) in
            
            if message == "You have successfully logged in"{
                DispatchQueue.main.async {
                self?.alertWithHandler(message: message) {
                    
                        kAppDelegate.appNavigator = ApplicationNavigator(window: kAppDelegate.window, sendViewController: "HomeVC")
                    }
                }
            }else{
                self?.handleError(message)
            }
        }
        
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                self?.handleError(message)
                // self?.hideActivityIndicator()
            }
        }
        
    }
}
