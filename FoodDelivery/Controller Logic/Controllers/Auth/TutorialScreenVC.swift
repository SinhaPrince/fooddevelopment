//
//  TutorialScreenVC.swift
//  FoodDelivery
//
//  Created by call soft on 21/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import FSPagerView

class TutorialScreenVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var pagerVw: FSPagerView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var pageControl: FSPageControl!
    
    //MARK:- Properties
    var pagerImage = [UIImage(named: "rest.jpg")!,
                      UIImage(named: "pick.jpg")!,
                      UIImage(named: "fastest.jpg")!]
    
    var pagerText = ["Find a Restaurant","Pick The Food","Get Fastest Delivery"]
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallizers()
    }
    @IBAction func btnActionForward(_ sender: UIButton) {

        let nextIndex = pagerVw.currentIndex + 1 < pagerText.count + 1 ? pagerVw.currentIndex + 1 : 0
        
        if pagerVw.currentIndex == 0 || pagerVw.currentIndex == 1 {
            pagerVw.scrollToItem(at: nextIndex, animated: true)
        }
        else
        {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "WelcomeScreenVC") as! WelcomeScreenVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func btnActionSkip(_ sender: UIButton) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "WelcomeScreenVC") as! WelcomeScreenVC
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.hideNavigationBar(true)
        }
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            self.hideNavigationBar(false)
    }
}

//MARK:- Custom Methods
extension TutorialScreenVC {
    func initiallizers() {
        configurePagerView()
        
    }
    
    func configurePagerView() {
        pagerVw.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        
        pagerVw.dataSource = self
        pagerVw.delegate = self
        //pagerVw.automaticSlidingInterval = 2.0
        pagerVw.transformer = FSPagerViewTransformer(type: .linear)
        pageControl.numberOfPages = pagerImage.count
        //pageControl.contentHorizontalAlignment = .right

    }
    
    
    
}

//MARK: - FSPagerView DataSource and Delegate
extension TutorialScreenVC : FSPagerViewDataSource, FSPagerViewDelegate{
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        self.pageControl.numberOfPages = pagerImage.count
        return pagerImage.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.image = pagerImage[index]
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.clipsToBounds = true
        lblTitle.text = pagerText[index]
        pageControl.setImage(UIImage(named:"unselected_radio"), for: .normal)
        pageControl.setImage(UIImage(named:"selected_radio"), for: .selected)
        self.pageControl.currentPage = index
        return cell
        
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        
    }
    
}
