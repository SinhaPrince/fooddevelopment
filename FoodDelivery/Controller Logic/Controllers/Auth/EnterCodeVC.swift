//
//  EnterCodeVC.swift
//  FoodDelivery
//
//  Created by call soft on 24/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import Firebase
class EnterCodeVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var vwOtp: VPMOTPView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnResendCode: UIButton!
    
    //MARK:- Properties
    var enteredOtp = String()
    var isFrom = String()
    var vCode: String?
    var phoneNumber: String?
    var delegate: CustomDelegate?
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
            initiallizers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        setupNavigation()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideNavigationBar(false)
    }
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.back], rightBarButtonsType: [])
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 55))
           let titleImageView = UIImageView(image: UIImage(named: "header_logo"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: titleView.frame.width, height: titleView.frame.height)
           titleView.addSubview(titleImageView)
           navigationItem.titleView = titleView
    }

    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func btnActionNext(_ sender: UIButton) {
        otpVerificationq(enteredOtp, vCode ?? "")
    }
    
    func otpVerificationq(_ otp:String,_ vCode:String){
        
        self.delegate?.getvCodeAndotp?(vCode: vCode, otp: otp)
        self.navigationController?.popViewController(animated: false)

    }

    @IBAction func btnActionResendCode(_ sender: UIButton) {
        Indicator.shared.showLottieAnimation(view: view)
        setupPhoneAuthentication(phoneNumber ?? "") { (vCode) in
            self.vCode = vCode
        }
    }
    
    
}

//MARK:- Custom Methods
extension EnterCodeVC : VPMOTPViewDelegate{
    func initiallizers() {
        setOtpView()
        btnResendCode.setAttributedTitle(GlobalMethods.shared.provideAttributedTextToControlAllignCenterUnderLine1("Resend Code", AppFont.Medium.size(.Poppins, size: 12), AppColor.pink), for: .normal)
    }
  
    func setOtpView() {
        // Set OTPView
        vwOtp.otpFieldsCount = 6
        vwOtp.otpFieldBorderWidth = 1.5
        vwOtp.otpFieldSize = 50
        vwOtp.otpFieldSeparatorSpace = 5
        vwOtp.delegate = self
        vwOtp.otpFieldDisplayType = .square
        vwOtp.otpFieldDefaultBorderColor = AppColor.primaryColor
        vwOtp.otpFieldDefaultBackgroundColor = AppColor.white
        // Create the UI
        vwOtp.initalizeUI(.black)
        vwOtp.initializeUI()
            
    }
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        
        return true
    }
    
    func enteredOTP(otpString: String) {
        print("nothing")
        enteredOtp = otpString
        vwOtp.otpFieldDefaultBorderColor = AppColor.primaryColor
        vwOtp.otpFieldEnteredBackgroundColor = AppColor.white
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        return true
    }
}

