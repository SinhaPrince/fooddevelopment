//
//  WelcomeScreenVC.swift
//  FoodDelivery
//
//  Created by call soft on 23/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class WelcomeScreenVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnGuestVisit: UIButton!
    
    //MARK:- Properties
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        btnGuestVisit.isUserInteractionEnabled = true
        greetingDay { (title) in
            self.lblTitle.text = title
        }
        isDataThere = false
        initializeTheLocationManager()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(true)
        Rating = 0
        TimeRange = [15,30]
        DistanceRange = [0,20]
        CuisineArray?.removeAll()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideNavigationBar(false)
    }
    
    func greetingDay(complition:@escaping (String) -> ()){
        let hour = Calendar.current.component(.hour, from: Date())
        
        switch hour {
        case 6..<12 : print(NSLocalizedString("Morning", comment: "Morning"))
            complition("Good Morning")
            break
        case 12 : print(NSLocalizedString("Noon", comment: "Noon"))
            complition("Good Noon")
            break
        case 13..<17 : print(NSLocalizedString("Afternoon", comment: "Afternoon"))
            complition("Good Afternoon")
            break
        case 17..<22 : print(NSLocalizedString("Evening", comment: "Evening"))
            complition("Good Evening")
            break
        default: print(NSLocalizedString("Night", comment: "Night"))
            complition("Good Night")
            break
        }
    }
    
    //MARK:- Button Action
    @IBAction func btnActionLogin(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionSignup(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionGuestVisit(_ sender: UIButton) {
        
        //self.handleError("Under Development")
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.appNavigator = ApplicationNavigator(window: appDel.window, sendViewController: "HomeVC")
    }
}
