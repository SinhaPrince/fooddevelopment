//
//  ForgotPasswordVC.swift
//  FoodDelivery
//
//  Created by call soft on 24/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import ADCountryPicker
import SwiftyJSON
import NKVPhonePicker

class ForgotPasswordVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var btnCountryCode: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtPhoneField:NKVPhonePickerTextField!
    @IBOutlet weak var lblerrorPhone: UILabel!
    @IBOutlet weak var viewPhone: UIView!

    //MARK:- Properties
    var picker = ADCountryPicker()
    var jsonData: CheckMobileModel?

    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
        self.lblCountryCode.text = ""
        self.getCurrentCountryCode { (country) in
            self.setupPhone(country)
            self.getDialCode(country) { (code) in
                self.lblCountryCode.text = code
            }
        }
        
    }
    func setupPhone(_ code:String){
        txtPhoneField.phonePickerDelegate = self
        txtPhoneField.favoriteCountriesLocaleIdentifiers = [code, "IN"]
        txtPhoneField.rightToLeftOrientation = false
        txtPhoneField.shouldScrollToSelectedCountry = false
        txtPhoneField.flagSize = CGSize(width: 30, height: 50)
        txtPhoneField.enablePlusPrefix = false
        txtPhoneField.font = AppFont.Medium.size(.Poppins, size: 17)
        // Setting initial custom country
        let country = Country.countriesBy(countryCodes: [code])
        txtPhoneField.country = country.first

        // Setting custom format pattern for some countries
        txtPhoneField.customPhoneFormats = ["code" : "# ### ### ## ##",
                                           "IN": "## #### #########"]
        lblerrorPhone.setup("Please enter valid Phone Number!", .red, AppFont.Light.size(.Poppins, size: 9), .left)


    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        setupNavigation()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideNavigationBar(false)
    }
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.back], rightBarButtonsType: [])
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 55))
           let titleImageView = UIImageView(image: UIImage(named: "header_logo"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: titleView.frame.width, height: titleView.frame.height)
           titleView.addSubview(titleImageView)
           navigationItem.titleView = titleView
    }
    @IBAction func btnActionNext(_ sender: UIButton) {
         if self.txtPhoneField.text?.count ?? 0 < 10{
            self.lblerrorPhone.isHidden = false
            viewPhone.borderColor = .red
            return
        }
        DispatchQueue.main.async {
            self.checkMobileNumber()
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
        viewPhone.borderColor = AppColor.appLightGray
    
        self.lblerrorPhone.isHidden = true

    }
    @IBAction func btnActionCountryPicker(_ sender: UIButton) {
        picker.delegate = self
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)
    }
    
    
    
}

//MARK:- Custom Methods
extension ForgotPasswordVC : UITextFieldDelegate,ADCountryPickerDelegate{
    func initiallizers() {
        txtPhoneField.delegate = self
    }
  
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        guard let text = textField.text else { return false }
//        let newString = (text as NSString).replacingCharacters(in: range, with: string)
//        textField.text = format(with: "XXX - XXX XXXX", phone: newString)
//        return false
//    }
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        _ = picker.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        lblCountryCode.text = dialCode
        
    }
}

//MARK:- Api Implementation
//MARK:-
extension ForgotPasswordVC:CustomDelegate{
    
    func checkMobileNumber(){
        Indicator.shared.showLottieAnimation(view: view)
        
        let mobileNumber = txtPhoneField.phoneNumber?.suffix((txtPhoneField.phoneNumber?.count ?? 0) - (txtPhoneField.code?.count ?? 0))
        let countryCode = "+".appending(txtPhoneField.code ?? "")
        let param = ["mobileNumber":mobileNumber ?? "","countryCode":countryCode] as [String:Any]
        
        ApiManager.instance.GetPostSessionService(param, APIEndpoint.checkMobilForForgotPassword, .post) { (result) in
            DispatchQueue.main.async {
                Indicator.shared.hideLoaderPresent()
            }
            switch result{
            case .success(let data):
                
                let json = JSON(data)
                self.jsonData = CheckMobileModel(json)
                
                if self.jsonData?.status == 200{
                    DispatchQueue.main.async {
                        self.setupPhoneAuthentication(countryCode + (mobileNumber ?? "")) { (vCode) in
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterCodeVC") as! EnterCodeVC
                            vc.vCode = vCode
                            vc.delegate = self
                            vc.phoneNumber = (countryCode + (mobileNumber ?? ""))
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    
                }else{
                    DispatchQueue.main.async {
                        self.handleError(self.jsonData?.message)
                    }
                }
                
                break
            case .failure(let error):
                DispatchQueue.main.async {
                self.handleError(error.localizedDescription)
                }
                break
            }
        }
    }
    func getvCodeAndotp(vCode: String, otp: String) {
        Indicator.shared.showLottieAnimation(view: view)
        otpVerification(otp, vCode) {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getPasswordBack(password: String) {
        DispatchQueue.main.async {
            self.forgotPassword(password)
        }
    }
    
    func forgotPassword(_ password:String){
        //forgotPassword
        Indicator.shared.showLottieAnimation(view: view)
        let param = ["userId":jsonData?.data?.userId ?? "","password":password] as [String:Any]
        
        ApiManager.instance.fetchApiService(method: .post, url: APIEndpoint.forgotPassword, passDict: param) { (result) in
            Indicator.shared.hideLoaderPresent()

            switch result{
            case .success(let data):
                    if data["status"].intValue == 200{
                        self.alertWithHandler(message: data["message"].stringValue) {
                                
                                    kAppDelegate.appNavigator = ApplicationNavigator(window: kAppDelegate.window, sendViewController: "LoginVC")
                                
                                }
                        }else{
                            self.handleError(data["message"].stringValue)
                        }
               
                break
            case .failure(let error):
                self.handleError(error.localizedDescription)
                break
            }
        }
    }
}

//MARK:- Model

struct CheckMobileModel {

    let status: Int?
    let message: String?
    let data: checkData?

    init(_ json: JSON) {
        status = json["status"].intValue
        message = json["message"].stringValue
        data = checkData(json["data"])
    }

}

struct checkData {

    let userId: String?

    init(_ json: JSON) {
        userId = json["userId"].stringValue
    }

}
