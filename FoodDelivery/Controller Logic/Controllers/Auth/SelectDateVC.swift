//
//  SelectDateVC.swift
//  FoodDelivery
//
//  Created by call soft on 19/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import FSCalendar

protocol dataPass {
    func data(date:String,isFrom:String)
    
}
class SelectDateVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwCalendar: FSCalendar!
    @IBOutlet weak var btnDone: UIButton!
    
    // MARK: - Variables
    var fromDate = String()
    
    
    var isFrom = String()
    var date = String()
    var time = String()
    var dateTime = String()
    var delegate:dataPass?
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        print(isFrom)
        initiallizers()
    }
   
    
}

// MARK: - @IBAction
extension SelectDateVC {
    @IBAction func btnActionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnActionLeft(_ sender: UIButton) {
        vwCalendar.setCurrentPage(getPreviousMonth(date: vwCalendar.currentPage), animated: true)
    }
    @IBAction func btnActionRight(_ sender: UIButton) {
        vwCalendar.setCurrentPage(getNextMonth(date: vwCalendar.currentPage), animated: true)
    }
    @IBAction func btnActionDone(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.data(date: date, isFrom: isFrom)
    }
}

// MARK: - Custom Methods
extension SelectDateVC {
    func initiallizers() {
        let currentPageDate = vwCalendar.currentPage
        let year = Calendar.current.component(.year, from: currentPageDate)
        let month = Calendar.current.component(.month, from: currentPageDate)
        let day = Calendar.current.component(.day, from: currentPageDate)
        print(year)
        print(month)
        print(day)
        vwCalendar.dataSource = self
        vwCalendar.delegate = self
    }
    
}

extension SelectDateVC:FSCalendarDelegate,FSCalendarDataSource {
    
    func getPreviousMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: -1, to:date)!
    }
    func getNextMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: 1, to:date)!
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print(date)
        let curDate = Date().addingTimeInterval(-24*60*60)
        print(curDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let myDate = dateFormatter.string(from: date)
        
        let fromMyDate = dateFormatter.date(from: fromDate) ?? Date()
        print(fromMyDate)
        
        if isFrom == "to" {
            if date < fromMyDate {
                self.show_Alert(message: "Date before the \(myDate) cannot be selected!!!", title: "Alert")
            }
            else
            {
                self.date = myDate
                self.time = ""
                print(myDate)
                print("time",self.time)
            }
        }
        else
        {
            if date < curDate {
                //            self.show_Alert(message: "Alert", title: "Past date cannot be selected")
                self.date = myDate
                self.time = ""
                print(myDate)
            } else {
                self.date = myDate
                self.time = ""
                print(myDate)
                print("time",self.time)
            }
        }
    }
}
