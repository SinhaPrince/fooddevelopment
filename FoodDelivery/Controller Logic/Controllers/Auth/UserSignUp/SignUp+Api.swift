//
//  SignUp+Api.swift
//  FoodDelivery
//
//  Created by Cst on 4/7/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

extension SignUpVC{
    
    func fetchSignService(_ password:String){
        
        Indicator.shared.showLottieAnimation(view: self.view)
        params["password"] = password
        viewModel.getSignService(APIEndpoint.userSignup, params)
        clouserSetup()
    }
    
    func clouserSetup(){
        viewModel.success = {[weak self] (message) in
            
            if message == "You have successfully signed up"{
                DispatchQueue.main.async {
                    kAppDelegate.appNavigator = ApplicationNavigator(window: kAppDelegate.window, sendViewController: "HomeVC")
                }
            }else{
                self?.handleError(message)
            }
        }
        
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                self?.handleError(message)
                // self?.hideActivityIndicator()
            }
        }
        
    }
    /// To Check Both Email and Mobile already present in database or not
    func checkMobileAndEmail(_ phoneNumber:String) {
        let param = ["email":self.txtEmail.text ?? "",
                     "mobileNumber":params["mobileNumber"] ?? ""] as [String:Any]
        Indicator.shared.showLottieAnimation(view: view)
        ApiManager.instance.fetchApiService(method: .post, url: "checkUserMobileAndEmail", passDict: param) { (result) in
            DispatchQueue.main.async {
                Indicator.shared.hideLoaderPresent()
            }
            switch result{
            case .success(let json):
                if json["status"].intValue == 200{
                    let mobileNumber = self.txtPhoneField.phoneNumber?.suffix((self.txtPhoneField.phoneNumber?.count ?? 0) - (self.txtPhoneField.code?.count ?? 0))
                    let countryCode = "+".appending(self.txtPhoneField.code ?? "")
                    DispatchQueue.main.async {
                        self.setupPhoneAuthentication(countryCode.appending(mobileNumber ?? "")) { (vCode) in
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterCodeVC") as! EnterCodeVC
                            vc.vCode = vCode
                            vc.delegate = self
                            vc.phoneNumber = countryCode.appending(mobileNumber ?? "")
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    
                }else{
                    self.handleError(json["message"].stringValue)
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    self.handleError(error.localizedDescription)
                }
                break
            }
        }
        
    }
}

//MARK:- FireBase
//MARK:-
extension SignUpVC{
    func firebaseConfig(){
        
    }
}

