//
//  SignUpVC.swift
//  FoodDelivery
//
//  Created by call soft on 23/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import ADCountryPicker
import MaterialComponents
import NKVPhonePicker


class SignUpVC: ParentViewController {

    //MARK:- Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var txtFirstName: MDCFilledTextField!
    @IBOutlet weak var txtLastName: MDCFilledTextField!
    @IBOutlet weak var txtEmail: MDCFilledTextField!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var txtPhone: MDCFilledTextField!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var btnCountryCode: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var txtPhoneField:NKVPhonePickerTextField!
    @IBOutlet weak var lblerrorPhone: UILabel!
    //MARK:- Properties
    var picker = ADCountryPicker()
    var params = [String:Any]()
    
    let validation = Validation()
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
    }
  
    func setupPhone(_ code:String){
        txtPhoneField.phonePickerDelegate = self
        txtPhoneField.countryPickerDelegate = self
        txtPhoneField.favoriteCountriesLocaleIdentifiers = [code, "IN"]
        txtPhoneField.rightToLeftOrientation = false
        txtPhoneField.shouldScrollToSelectedCountry = false
        txtPhoneField.flagSize = CGSize(width: 30, height: 50)
        txtPhoneField.enablePlusPrefix = false
        txtPhoneField.font = AppFont.Medium.size(.Poppins, size: 17)
        // Setting initial custom country
        let country = Country.countriesBy(countryCodes: [code])
        txtPhoneField.country = country.first

        // Setting custom format pattern for some countries
        txtPhoneField.customPhoneFormats = ["code" : "# ### ### ## ##",
                                           "IN": "## #### #########"]

    }
    
    
    //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        }
    @IBAction func btnActionSignUp(_ sender: UIButton) {
        print("SignUp")
        
        checkValidation()
        
    }
    @IBAction func btnActionLogin(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActionCountryPicker(_ sender: UIButton) {
        picker.delegate = self
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)
    }


}

//MARK:- Custom Methods
extension SignUpVC : UITextFieldDelegate,ADCountryPickerDelegate,CustomDelegate{
    
    func initiallizers() {
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        txtPhoneField.delegate = self
        txtFirstName.tag = 1
        txtLastName.tag = 2
        txtEmail.tag = 3
        txtPhoneField.tag = 4
        self.lblCountryCode.text = ""
        setupNavigation()
        self.getCurrentCountryCode { (country) in
            self.setupPhone(country)
            self.getDialCode(country) { (code) in
                self.lblCountryCode.text = code
            }
        }
        
        self.txtFirstName.setUp("First Name", "First Name", #imageLiteral(resourceName: "user"))
        self.txtLastName.setUp("Last Name", "Last Name", #imageLiteral(resourceName: "user"))
        self.txtEmail.setUp("Email", "Email", #imageLiteral(resourceName: "email"))
        self.txtPhone.label.setup("Phone Number", AppColor.appLightGray, AppFont.Regular.size(.Poppins, size: 12), .left)
        self.txtPhone.setFloatingLabelColor(AppColor.appLightGray, for: .normal)
        self.txtPhone.setFloatingLabelColor(AppColor.appLightGray, for: .editing)
        self.txtPhone.setNormalLabelColor(AppColor.appLightGray, for: .normal)
        self.txtPhone.placeholder = "XXX - XXX XXXX"
        self.txtPhone.setUnderlineColor(.clear, for: .normal)
        self.txtPhone.setUnderlineColor(.clear, for: .editing)
        self.txtPhone.setFilledBackgroundColor(.clear, for: .normal)
        self.txtPhone.setFilledBackgroundColor(.clear, for: .editing)
        lblerrorPhone.setup("Please enter valid Phone Number!", .red, AppFont.Light.size(.Poppins, size: 9), .left)

    }
    
    
    func checkValidation(){
        
        if txtFirstName.text?.isEmpty ?? false{
            viewFirstName.borderColor = .red
            self.txtFirstName.errorHandler("Please enter First Name!", .red)
            return
        }
        if txtLastName.text?.isEmpty ?? false{
            viewLastName.borderColor = .red
            self.txtLastName.errorHandler("Please enter Last Name!", .red)
            return
        }
        if !validation.validateEmail(txtEmail.text){
            viewEmail.borderColor = .red
            self.txtEmail.errorHandler("Please enter valid Email Id!", .red)
            return
        }
        if txtPhoneField.phoneNumber?.count ?? 0 < 10{
           // self.txtPhone.errorHandler("Please enter valid Phone Number!", .red)
            lblerrorPhone.isHidden = false
            viewPhone.borderColor = .red
            return
        }

        let mobileNumber = txtPhoneField.phoneNumber?.suffix((txtPhoneField.phoneNumber?.count ?? 0) - (txtPhoneField.code?.count ?? 0))
        let countryCode = "+".appending(txtPhoneField.code ?? "")
        params = [
            "firstName":self.txtFirstName.text ?? "",
            "lastName":self.txtLastName.text ?? "",
            "email":self.txtEmail.text ?? "",
            "countryCode":countryCode ,
            "mobileNumber":mobileNumber ?? "",
            "deviceType":"ios",
            "deviceToken":UserDefaults.standard.value(forKey: "DeviceToken") ?? ""] as [String:Any]
        checkMobileAndEmail(params["mobileNumber"] as? String ?? "")
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        viewFirstName.borderColor = AppColor.appLightGray
        viewLastName.borderColor = AppColor.appLightGray
        viewEmail.borderColor = AppColor.appLightGray
        viewPhone.borderColor = AppColor.appLightGray
        self.txtFirstName.errorHandler("", .clear)
        self.txtLastName.errorHandler("", .clear)
        self.txtPhone.errorHandler("", .clear)
        self.txtEmail.errorHandler("", .clear)
        self.lblerrorPhone.isHidden = true

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if !(textField.text?.isEmpty ?? false){
            if nextTag == 2{
                self.txtLastName.becomeFirstResponder()
            }else if nextTag == 3{
                self.txtEmail.becomeFirstResponder()
            }else if nextTag == 4{
                self.txtPhoneField.becomeFirstResponder()
            }else{
                self.btnActionSignUp(btnSignUp)
            }
            
        }else{
            textField.becomeFirstResponder()
        }
     
        return true
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        guard textField.tag == 4 else {
//            return true
//        }
//        guard let text = textField.text else { return false }
//        let newString = (text as NSString).replacingCharacters(in: range, with: string)
//        textField.text = format(with: "XXX - XXX XXXX", phone: newString)
//        return false
//    }
  
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.back], rightBarButtonsType: [])
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 55))
           let titleImageView = UIImageView(image: UIImage(named: "header_logo"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: titleView.frame.width, height: titleView.frame.height)
           titleView.addSubview(titleImageView)
           navigationItem.titleView = titleView
    }
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        _ = picker.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        lblCountryCode.text = dialCode
        
    }
    
    func getvCodeAndotp(vCode: String, otp: String) {
        Indicator.shared.showLottieAnimation(view: view)
        otpVerification(otp, vCode) {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
            vc.isFrom = "signup"
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getPasswordBack(password: String) {
        DispatchQueue.main.async {
            self.fetchSignService(password)
        }
    }
    
}

/// The methods are optional.
extension SignUpVC: CountriesViewControllerDelegate {

    
    func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country) {
        print("✳️ Did select country: \(country.countryCode)")
        print("✳️ Did select country: \(country.formatPattern)")
        print("✳️ Did select country: \(country.name)")
        print("✳️ Did select country: \(country.phoneExtension)")
    }
    
    func countriesViewControllerDidCancel(_ sender: CountriesViewController) {
        print("😕")
    }
}
