//
//  LocationPermissionVC.swift
//  FoodDelivery
//
//  Created by Cst on 4/17/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import GooglePlaces
class LocationPermissionVC: ParentViewController, UISearchBarDelegate,MapInfoDelegate {

    lazy var tableView:UITableView = {
        let table = UITableView()
        table.backgroundColor = .white
        table.separatorStyle = .singleLine
        table.translatesAutoresizingMaskIntoConstraints = false
        table.dataSource = self
        table.delegate = self
        table.isHidden = true
        return table
    }()
    
    lazy var headerView:UIView = {
        let view = UIView()
        view.backgroundColor = AppColor.pink
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var imageView:UIImageView = {
        let image =  UIImageView()
        image.image = UIImage(named: "gps1")
        image.tintColor = .white
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    lazy var headerTitle:UILabel = {
        let lbl = UILabel()
        lbl.text = comClass.createString(Str: "Unable to get location")
        lbl.font = AppFont.SemiBold.size(.Poppins, size: 13)
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    lazy var turnOn:UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.cornerRadius = 6
        button.shadowColor = .black
        button.shadowOffset = CGSize(width: 2, height: 2)
        button.shadowRadius = 4
        button.setTitle(comClass.createString(Str: "TURN ON"), for: .normal)
        button.setTitleColor(AppColor.pink, for: .normal)
        button.titleLabel?.font = AppFont.SemiBold.size(.Poppins, size: 12)
        button.contentHorizontalAlignment = .center
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let locPermissionVM = LocationPermissionVM()
    let searchResultVM = SearchResultViewModel()
    var delegate: MapInfoDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        view.backgroundColor = .white
        // Do any additional setup after loading the view.
    }

    //MARK:- UI/UX Setup
    func setupUI(){
        self.view.addSubview(headerView)
        self.headerView.addSubview(imageView)
        self.headerView.addSubview(headerTitle)
        self.headerView.addSubview(turnOn)
        self.view.addSubview(tableView)
        NSLayoutConstraint.activate([self.headerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),self.headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),headerView.heightAnchor.constraint(equalToConstant: 60),imageView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 15),imageView.widthAnchor.constraint(equalToConstant: 20),imageView.heightAnchor.constraint(equalToConstant: 20),imageView.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),headerTitle.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 20),headerTitle.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),turnOn.leadingAnchor.constraint(equalTo: headerTitle.trailingAnchor, constant: 15),turnOn.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -15),turnOn.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),turnOn.heightAnchor.constraint(equalToConstant: 35),turnOn.widthAnchor.constraint(equalToConstant: 80),tableView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 0),tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
            self.turnOn.addTarget(self, action: #selector(btnTurnOnTap), for: .touchUpInside)
            setupNavigation()
        self.locationPermission { (result) in
            if result == "Access" {
                self.turnOn.isHidden = true
                self.headerTitle.text = "Search your destination"
            }
        }
    }
    
    @objc func btnTurnOnTap(){
        self.locationPermission { (result) in
            if result == "No access" {
             UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            }
        }
    }
    
}

//MARK:- Setup Navigation Bar
extension LocationPermissionVC{
    func setupNavigation(){
        transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let searchBar = UISearchBar()
        searchBar.sizeToFit()
        searchBar.delegate = self
        searchBar.placeholder = comClass.createString(Str: "Search for area, street name..")
        self.navigationItem.titleView = searchBar
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchResultVM.updateSearchResult1(for: searchText)
        self.tableView.isHidden = searchBar.text?.isEmpty ?? false
        self.searchResultVM.didFinishFetch = {
            self.tableView.reloadData()
        }
        
//        self.locPermissionVM.updateSearchResult(for: searchBar)
//        self.tableView.isHidden = searchBar.text?.isEmpty ?? false
//        self.locPermissionVM.didFinishFetch = {
//            self.tableView.reloadData()
//        }
    }
}
//MARK:- UITableView Datasource
extension LocationPermissionVC:UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //self.locPermissionVM.mapItem?.count ?? 0
        self.searchResultVM.placeData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // return self.locPermissionVM.cellInstance(tableView, indexPath: indexPath)
        return self.searchResultVM.cellInstance(tableView, indexPath: indexPath)

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70

    }
}
//MARK:- UItableView Delegate
import RealmSwift
extension LocationPermissionVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let placeClient = GMSPlacesClient()
        placeClient.lookUpPlaceID(self.searchResultVM.placeData[indexPath.row].place_id ?? "") { (place, error) -> Void in
            guard error == nil else{
                self.handleError(error?.localizedDescription ?? "")
                return
            }
            if let place = place {
                let data = SaveDefaultAddress(place.coordinate.latitude.description , place.coordinate.longitude.description , "")
                let realm = try! Realm()
                try! realm.write({
                    let deletedNotifications = realm.objects(SaveDefaultAddress.self)
                    realm.delete(deletedNotifications)
                    realm.add(data)
                })
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                    self.navigationController?.popViewController(animated: false)
                }
            } else {
                //show error
            }
        }
       
    }
}

//MARK:- Location Permission Popup
class LocationPermissionPopUp: ParentViewController,MapInfoDelegate {
    lazy var titleHeader:UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.attributedText = provideAttributedTextToControlLeftAllignCenter("Location Permission is Off\n", "Granting location permission will ensure accurate address and hassle-free delivery", AppFont.Bold.size(.Poppins, size: 17), AppFont.Regular.size(.Poppins, size: 13), AppColor.black, AppColor.lightGrayApp)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    lazy var manuualButton:UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.setTitle(comClass.createString(Str: "Enter Location Manually"), for: .normal)
        button.setTitleColor(AppColor.lightGrayApp, for: .normal)
        button.titleLabel?.font = AppFont.Medium.size(.Poppins, size: 14)
        button.setImage(#imageLiteral(resourceName: "search"), for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        button.contentHorizontalAlignment = .left
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var enableService:UIButton = {
        let button = UIButton()
        button.backgroundColor = AppColor.pink
        button.setTitle(comClass.createString(Str: "Enable Location Service"), for: .normal)
        button.setTitleColor(AppColor.white, for: .normal)
        button.titleLabel?.font = AppFont.SemiBold.size(.Poppins, size: 15)
        button.contentHorizontalAlignment = .center
        button.translatesAutoresizingMaskIntoConstraints = false
        button.cornerRadius = 55/2
        return button
    }()
    
    lazy var veriticalStack: UIStackView = {
        let stack = UIStackView()
        stack.distribution = .fillEqually
        stack.axis = .vertical
        stack.spacing = 10
        stack.addArrangedSubview(enableService)
        stack.addArrangedSubview(manuualButton)
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    lazy var bottomView:UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 20
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner] // Top right corner, Top left corner respectively
        view.translatesAutoresizingMaskIntoConstraints = false
        view.bringSubviewToFront(self.view)
        return view
    }()
    lazy var transView:UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.4
        view.frame = self.view.bounds
        return view
    }()
    
    var controller = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    //MARK:- UI/UX setup
    func setupUI(){
        self.view.addSubview(transView)
        self.view.addSubview(bottomView)
        bottomView.addSubview(titleHeader)
        bottomView.addSubview(veriticalStack)
        NSLayoutConstraint.activate([bottomView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),bottomView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),bottomView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),titleHeader.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 30),titleHeader.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),titleHeader.centerXAnchor.constraint(equalTo: view.centerXAnchor),veriticalStack.topAnchor.constraint(equalTo: titleHeader.bottomAnchor, constant: 30),veriticalStack.widthAnchor.constraint(equalTo: titleHeader.widthAnchor, multiplier: 1),veriticalStack.centerXAnchor.constraint(equalTo: titleHeader.centerXAnchor),veriticalStack.heightAnchor.constraint(equalToConstant: 120),veriticalStack.bottomAnchor.constraint(equalTo: bottomView.bottomAnchor, constant: -15)])
        enableService.addTarget(self, action: #selector(btnEnableTap), for: .touchUpInside)
        manuualButton.addTarget(self, action: #selector(manuualButtonTap), for: .touchUpInside)

    }
    
    @objc func btnEnableTap(){
        self.locationPermission { (result) in
            if result == "No access" {
             UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            }
        }
    }
    @objc func manuualButtonTap(){
        self.dismiss(animated: true) {
            let vc = LocationPermissionVC()
            self.controller.navigationController?.pushViewController(vc, animated: false)
        }
    }
}
