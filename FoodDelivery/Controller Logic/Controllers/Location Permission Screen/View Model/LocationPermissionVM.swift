//
//  LocationPermissionVM.swift
//  FoodDelivery
//
//  Created by Cst on 4/17/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import MapKit
class LocationPermissionVM: BaseViewModel,CellRepresentable{
    
    var rowHeight: CGFloat = 45
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        tableView.register(SearchResultCell.self, forCellReuseIdentifier: SearchResultCell.description())
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultCell.description()) as! SearchResultCell
        cell.setupCell()
        cell.lblAddress.attributedText = GlobalMethods.shared.provideAttributedTextToControlLeftAllignCenter((self.mapItem?[indexPath.row].placemark.name ?? ""), (parseAddress(selectItem: (self.mapItem?[indexPath.row].placemark)!)), AppFont.Medium.size(.Poppins, size: 16), AppFont.Medium.size(.Poppins, size: 12), AppColor.black, AppColor.lightGrayApp,"\n",.left)
        return cell
    }
    
    var mapItem:[MKMapItem]?{
        didSet{
            self.didFinishFetch?()
        }
    }
    
    func updateSearchResult(for searchBar: UISearchBar) {
        let searchBarText = searchBar.text
        if searchBar.text?.isEmpty == true{
            self.mapItem?.removeAll()
        }
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchBarText
        if #available(iOS 13.0, *) {
            request.pointOfInterestFilter = MKPointOfInterestFilter.includingAll
        } else {
            // Fallback on earlier versions
        }
        //request.region = mapView!.region
        if #available(iOS 13.0, *) {
            request.resultTypes = .pointOfInterest
        } else {
            // Fallback on earlier versions
        }
        let search = MKLocalSearch(request: request)
        search.start { (result, error) in
            guard error == nil else {return}
            self.mapItem = result?.mapItems
        }
    }
    func parseAddress(selectItem: MKPlacemark) -> String{
        let firstSpace = ((selectItem.subThoroughfare != nil) && (selectItem.thoroughfare != nil)) ? " ": ""
        let comma = (selectItem.subThoroughfare != nil || selectItem.thoroughfare != nil) && (selectItem.subAdministrativeArea != nil || selectItem.administrativeArea != nil) ? ", ":""
        let secondSpace = (selectItem.subAdministrativeArea != nil && selectItem.administrativeArea != nil) ? " ":""
        let addressLine = String(format: "%@%@%@%@%@%@%@",
                                 // street number
                                 selectItem.subThoroughfare ?? "",
                                 firstSpace,
                                 //street name
                                 selectItem.thoroughfare ?? "",
                                 comma,
                                 // city
                                 selectItem.locality ?? "",
                                 secondSpace,
                                 //state
                                 selectItem.administrativeArea ?? ""
        )
        return addressLine
    }
}

