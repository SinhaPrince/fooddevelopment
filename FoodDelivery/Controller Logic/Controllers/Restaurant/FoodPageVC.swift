//
//  FoodPageVC.swift
//  FoodDelivery
//
//  Created by call soft on 02/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class FoodPageVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var vwScroll: UIView!
    @IBOutlet weak var tblVw: UITableView!
    
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCount: UILabel!
    
    //MARK:- Properties
    var count = 0
    var tblData = ["Italian Bread","Wheat Bread","Parmesan Oregano Bread","Honey Oat Bread","Flat Bread","Make it Habanero Wrap"]
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        vwScroll.cornerRadius = 30
        vwScroll.maskToBounds = false
        vwScroll.clipsToBounds = true
        vwScroll.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
//        ScrollView.cornerRadius = 30
//        ScrollView.maskToBounds = false
//        ScrollView.clipsToBounds = true
//        ScrollView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        initiallizers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
    }
    func setupNavigation(){
        transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tblHeight.constant = tblVw.contentSize.height
    }
    
    //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionAddToCart(_ sender: UIButton) {
    
    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
    self.navigationController?.pushViewController(vc, animated: true)
}
    @IBAction func btnActionAdd(_ sender: UIButton) {
        count = count + 1
        lblCount.text = "\(count)"
    }
    @IBAction func btnActionMinus(_ sender: UIButton) {
        if count <= 0{
            print("No change")
        }
        else {
            count = count - 1
            lblCount.text = "\(count)"
        }
    }
}
//MARK:- Custom Methods
extension FoodPageVC {
        func initiallizers() {
            
            configureTable()
            
            
        }
        
        
    }
//MARK:- UITableViewDelegate and UITableViewDatasource
extension FoodPageVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return tblData.count - 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblData.count
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("FoodPageHeader", owner: self, options: nil)?.first as! FoodPageHeader
        headerView.vwOuter.backgroundColor = AppColor.clearColor
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : FoodPageTableCell = tblVw.dequeueReusableCell(withIdentifier: "FoodPageTableCell") as! FoodPageTableCell
        print("Section = ",indexPath.section)
        cell.lblTitle.text = tblData[indexPath.row]
        cell.vwBottom.isHidden = false
        if indexPath.row == tblData.count - 1 {
            cell.vwBottom.isHidden = true
            
        }
        
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == tblData.count - tblData.count {
            cell.cornerRadius = 30
            cell.maskToBounds = false
            cell.clipsToBounds = true
            cell.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        }
        else if indexPath.row == tblData.count - 1 {
            cell.cornerRadius = 30
            cell.maskToBounds = false
            cell.clipsToBounds = true
            cell.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
            
        }
        else
        {
            cell.cornerRadius = 0
            cell.maskToBounds = false
            cell.clipsToBounds = true
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.backgroundColor = AppColor.clearColor
        tblVw.register(UINib.init(nibName: "FoodPageTableCell", bundle: nil), forCellReuseIdentifier: "FoodPageTableCell")
        tblVw.register(UINib.init(nibName: "FoodPageHeader", bundle: nil), forCellReuseIdentifier: "FoodPageHeader")
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tblVw.cellForRow(at: indexPath) as? FoodPageTableCell {
            cell.imgRadio.image = UIImage.init(named: "radio_s.jpg")
            
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tblVw.cellForRow(at: indexPath) as? FoodPageTableCell {
            cell.imgRadio.image = UIImage.init(named: "radio.jpg")
            
        }
    }
    
}
    
    

