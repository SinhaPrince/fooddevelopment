//
//  Search+Navigation.swift
//  FoodDelivery
//
//  Created by Cst on 4/11/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

extension SearchForRestaurantVC:BaseViewControllerDelegate,UISearchBarDelegate,FiterDelegate{
    
    func getFilterData() {
        self.searchBar.text = ""
        self.searchVM.filterDishResult()
        self.searchVM.filterRestaurantCuisineResult()
        self.searchVM.maindishData = self.searchVM.filterAccToFilterScreenDish
        self.searchVM.mainRestData = self.searchVM.filterAcctoFilterScreen
    }
    
    
    func navigationBarButtonDidTapped(_ buttonType: UINavigationBarButtonType) {
        if buttonType == .filter{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
            vc.restaurantData = self.searchVM.restaurantListData?.data ?? []
            let navigationControlr = UINavigationController(rootViewController: vc)
            vc.comingFrom = "Search"
            vc.filterDelegate = self
            self.present(navigationControlr, animated: true, completion: nil)
        }
    }
    
    func setupNavigation(){
        transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnFilter)
        self.btnFilter.tag = UINavigationBarButtonType.filter.rawValue
        self.btnFilter.addTarget(self, action: #selector(ParentViewController.navigationButtonTapped(_:)), for: .touchUpInside)

        //Setup Navigation
        searchBar.sizeToFit() 
        searchBar.placeholder = "Dishes, Cuisines, & Restaurants"
        searchBar.setImage(#imageLiteral(resourceName: "search"), for: .search, state: .normal)
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar

    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
       // self.collVw.isHidden = false
      
    }
 
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.collVw.isHidden = true
        }else{
            self.collVw.isHidden = false

        }
        self.searchVM.searchBar(searchBar, textDidChange: searchText)
    }
    
}
