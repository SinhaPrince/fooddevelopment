//
//  SearchForRestaurantVC.swift
//  FoodDelivery
//
//  Created by call soft on 01/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class SearchForRestaurantVC: ParentViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var collVw: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    //MARK:- Properties
    //var collData = ["Restaurant","Cuisines","Dish"]
    var collData = [SearchTypeCollectionData(title: "Restaurant", isSelected: true),
                    SearchTypeCollectionData(title: "Cuisines", isSelected: false),
                    SearchTypeCollectionData(title: "Dish", isSelected: false)]
    var selectedValue = Int()
    var restaurantData = [RestaurantData]()
    var dishData = [DishData]()
    let searchVM = SearchViewModel(networking: ApiManager())
    
    var type = String()
    
    
    lazy var btnFilter: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "list_black"), for: .normal)
        button.isHidden = true
        return button
    }()
    let searchBar = UISearchBar()

    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
        setupNavigation()
        tblVw.isHidden = true
        collVw.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.baseDelegate = self
    }
    
}



//MARK:- Custom Methods
extension SearchForRestaurantVC {
    func initiallizers() {
        //First Clear All Old Filter Search Data
        CuisineArraySearch?.removeAll()
        RatingSearch = 0
        DistanceRangeSearch = [0,20]
        TimeRangeSearch = [15,30]
        
        searchVM.controller = self
        initializeTheLocationManager()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            if comClass.isDataExist(){
                self.restaurantListService(comClass.getInfoById().latitude ?? "", comClass.getInfoById().longitude ?? "")
            }else{
                let defaultAddress = self.realm.objects(SaveDefaultAddress.self)
                if defaultAddress.count > 0 {
                    self.restaurantListService(defaultAddress.first?.latitude ?? "",defaultAddress.first?.longitude ?? "")
                }else{
                    self.restaurantListService(self.lat, self.long)
                }
            }
        }
    }
    
    func restaurantListService(_ lat:String,_ long:String){
        Indicator.shared.showLottieAnimation(view: view)
        var param = [String:Any]()
        if !comClass.isDataExist(){
            print("if not  exist.......")
            param = ["longitude" : long,"latitude":lat] as [String:Any]
        }else{
            print("if  exist.......")
            param = ["longitude" : long,"latitude":lat,"userId":comClass.getInfoById().Id ?? ""] as [String:Any]
           
        }
        
        self.searchVM.fetchRestaurantList("getAllRestaurantsData", param) { (status, error) in
            guard error == nil else{
                self.handleError(error?.localizedDescription)
                return
            }
            guard status == 200 else{
                self.handleError(self.searchVM.restaurantListData?.message ?? "")
                self.type = "Restaurant"
                self.searchVM.typeOfSelection = "Restaurant"
                return
            }
           // self.searchVM.mainRestData = self.searchVM.restaurantListData?.data ?? []
          //  self.collVw.isHidden = true
            
        }
        //fetch dish data
        
        self.searchVM.fetchDishData("searchByDish", param)
        self.searchVM.didFinishFetch = {
            self.configureTable()
            self.configureCollection()
        }

        
    }
    
    
}

//MARK:- UICollectionViewDelegate,UICollectionViewDataSource
extension SearchForRestaurantVC : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    
    func configureCollection(){
        
        collVw.delegate = self
        collVw.dataSource = self
        collVw.backgroundColor = AppColor.clearColor
        collVw.register(UINib.init(nibName: "RestaurantInfoCollectionCell", bundle: nil), forCellWithReuseIdentifier: "RestaurantInfoCollectionCell")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: collVw.frame.width/3, height: 50)
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        collVw.collectionViewLayout = layout
        collVw.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : RestaurantInfoCollectionCell = collVw.dequeueReusableCell(withReuseIdentifier: "RestaurantInfoCollectionCell", for: indexPath) as! RestaurantInfoCollectionCell
        cell.itemData = collData[indexPath.row]
        return cell
        
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        for i in 0..<self.collData.count{
            if i == indexPath.row{
                self.collData[indexPath.row].isSelected = true
            }else{
                self.collData[i].isSelected = false
            }
        }
        collVw.reloadData()
        if indexPath.row == 2{
            type = "Dish"
        }else if indexPath.row == 0{
            type = "Restaurant"
        }else{
            type = "Cuisine"
        }
        searchVM.typeOfSelection = type
        self.tblVw.reloadData()
 
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.text = self.collData[indexPath.row].title
        label.sizeToFit()
        return CGSize(width: label.frame.width + 50, height: 50)
    }
    
}
//MARK:- UITableViewDelegate and UITableViewDatasource
extension SearchForRestaurantVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func configureTable() {
        
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.reloadData()
        btnFilter.isHidden = false

        if self.searchVM.mainRestData?.count ?? 0 > 0 || self.searchVM.maindishData?.count ?? 0 > 0 {
          //  self.collVw.isHidden = false
            self.tblVw.isHidden = false
        }else{
          //  self.collVw.isHidden = true
            self.tblVw.isHidden = true
            
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type == "Dish"{
            return  self.searchVM.maindishData?.count ?? 0
        }else{
            return self.searchVM.mainRestData?.count ?? 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.searchVM.cellInstance(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.searchVM.rowHeight
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.searchVM.rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if type == "Dish"{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantDetailVC") as! RestaurantDetailVC
            vc.id = self.searchVM.maindishData?[indexPath.row].resID
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantDetailVC") as! RestaurantDetailVC
            vc.id = self.searchVM.mainRestData?[indexPath.row].Id
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
}

//MARK:- Filter process
extension SearchForRestaurantVC:FilterResultDelegate{
    func filterData(rating: Int, distance: Int, time: Int) {
        self.searchVM.filteration(rating: rating, distance: distance, time: time)
    }
     
}

protocol FilterResultDelegate {
    func filterData(rating:Int,distance:Int,time:Int)
}
