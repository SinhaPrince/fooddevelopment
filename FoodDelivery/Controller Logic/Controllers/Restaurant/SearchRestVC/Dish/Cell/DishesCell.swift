//
//  DishesCell.swift
//  FoodDelivery
//
//  Created by Cst on 5/12/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class DishesCell: UITableViewCell {

    @IBOutlet weak var imageDish: UIImageView!
    @IBOutlet weak var dishName: UILabel!
    @IBOutlet weak var priceStoreName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnRating: UIButton!
    @IBOutlet weak var btnRatingWidth: NSLayoutConstraint!
    var item: CustomDishDataModel?{
        didSet{
            imageDish.sd_setImage(with: URL(string: item?.productImage ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"), options: .lowPriority, context: [:])
            dishName.setup(item?.productName ?? "", AppColor.appLightBlack, AppFont.Medium.size(.Poppins, size: 15), .left)
            priceStoreName.setup("AED \(item?.price?.description ?? "") \(item?.brandData?.storeName ?? "")", AppColor.appLightGray, AppFont.Regular.size(.Poppins, size: 13), .left)
          //  btnRating.isHidden = item?.avgRating ?? 0 > 0 ? false : true
           // btnRatingWidth.constant = item?.avgRating ?? 0 > 0 ? 46 : 0
            btnRating.setTitle(item?.avgRating?.description, for: .normal)
            lblTime.setup("\(item?.areaData?.deliveryTime ?? 0) mins",AppColor.appLightGray, AppFont.Regular.size(.Poppins, size: 13), .left)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
