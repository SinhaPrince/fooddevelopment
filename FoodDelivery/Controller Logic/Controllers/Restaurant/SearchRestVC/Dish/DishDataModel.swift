//
//  DishDataModel.swift
//  FoodDelivery
//
//  Created by Cst on 5/12/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import Foundation
import SwiftyJSON

struct DishDataModel {

    var status: Int?
    var message: String?
    var data: [DishData]?

    init(_ json: JSON) {
        status = json["status"].intValue
        message = json["message"].stringValue
        data = json["data"].arrayValue.map { DishData($0) }
    }

}


struct DishData {

    var Id: String?
    var location: Location?
    var status: String?
    var image: String?
    var loyalty: String?
    var totalOrders: Int?
    var brandId: String?
    var branchNameEn: String?
    var branchNameAr: String?
    var descriptionEn: String?
    var descriptionAr: String?
    var countryCode: String?
    var address: String?
    var latitude: String?
    var longitude: String?
    var openTime: String?
    var closeTime: String?
    var email: String?
    var mobileNumber: String?
    var createdAt: String?
    var busy1Status: Bool?
    var busy2Status: Bool?
    var busy3Status: Bool?
    var busy4Status: Bool?
    var busy5Status: Bool?
    var busyTime: Int?
    var closeStatus: Bool?
    var oepnStatus: Bool?
    var deliveryFee: Int?
    var deliveryTime: Int?
    var avgRating: Int?
    var totalRating: Int?
    var minimumOrderValue: Int?
    var menuId: String?
    var index: Int?
    var serviceType: String?
    var ratingByUsers: Int?
    var dist: Dist?
    var brandData: BrandDataa?
    var areaData: AreaData?
    var categoryData: [CategoryData]?
    var isFav: Bool?
    var itemList: [ItemList]?

    init(_ json: JSON) {
        Id = json["_id"].stringValue
        location = Location(json["location"])
        status = json["status"].stringValue
        image = json["image"].stringValue
        loyalty = json["loyalty"].stringValue
        totalOrders = json["totalOrders"].intValue
        brandId = json["brandId"].stringValue
        branchNameEn = json["branchNameEn"].stringValue
        branchNameAr = json["branchNameAr"].stringValue
        descriptionEn = json["descriptionEn"].stringValue
        descriptionAr = json["descriptionAr"].stringValue
        countryCode = json["countryCode"].stringValue
        address = json["address"].stringValue
        latitude = json["latitude"].stringValue
        longitude = json["longitude"].stringValue
        openTime = json["openTime"].stringValue
        closeTime = json["closeTime"].stringValue
        email = json["email"].stringValue
        mobileNumber = json["mobileNumber"].stringValue
        createdAt = json["createdAt"].stringValue
        busy1Status = json["busy1Status"].boolValue
        busy2Status = json["busy2Status"].boolValue
        busy3Status = json["busy3Status"].boolValue
        busy4Status = json["busy4Status"].boolValue
        busy5Status = json["busy5Status"].boolValue
        busyTime = json["busyTime"].intValue
        closeStatus = json["closeStatus"].boolValue
        oepnStatus = json["oepnStatus"].boolValue
        deliveryFee = json["deliveryFee"].intValue
        deliveryTime = json["deliveryTime"].intValue
        avgRating = json["avgRating"].intValue
        totalRating = json["totalRating"].intValue
        minimumOrderValue = json["minimumOrderValue"].intValue
        menuId = json["menuId"].stringValue
        index = json["index"].intValue
        serviceType = json["serviceType"].stringValue
        ratingByUsers = json["ratingByUsers"].intValue
        dist = Dist(json["dist"])
        brandData = BrandDataa(json["brandData"])
        areaData = AreaData(json["areaData"])
        categoryData = json["categoryData"].arrayValue.map { CategoryData($0) }
        isFav = json["isFav"].boolValue
        itemList = json["itemList"].arrayValue.map { ItemList($0) }
    }

}

struct CustomDishDataModel{
    var brandData: BrandDataa?
    var areaData: AreaData?
    var categoryData: [CategoryData]?
    var dist: Dist?
    var avgRating: Int?
    var resID: String?
    var Id: String?
    var adminVerifyStatus: String?
    var type: String?
    var status: String?
    var deleteStatus: Bool?
    var priceSelection: Bool?
    var category: [Category]?
    var menuAssignStatus: Bool?
    var availabilityStatus: Bool?
    var changeRequest: Bool?
    var changeRequestApporve: String?
    var index: Int?
    var brandId: String?
    var branchId: String?
    var menuId: String?
    var menuCategoryName: String?
    var productName: String?
    var productNameAr: String?
    var description: String?
    var descriptionAr: String?
    var price: Int?
    var productImage: String?
    var createdAt: String?
    var updatedAt: String?

    init(_ json: JSON,_ resID:String,_ avgRating:Int,_ dist:Dist,_ brandData:BrandDataa,_ areaData: AreaData,_ categoryData: [CategoryData]) {
        self.resID = resID
        self.brandData = brandData
        self.dist = dist
        self.areaData = areaData
        self.avgRating = avgRating
        Id = json["_id"].stringValue
        adminVerifyStatus = json["adminVerifyStatus"].stringValue
        type = json["type"].stringValue
        status = json["status"].stringValue
        deleteStatus = json["deleteStatus"].boolValue
        priceSelection = json["priceSelection"].boolValue
        category = json["category"].arrayValue.map { Category($0) }
        menuAssignStatus = json["menuAssignStatus"].boolValue
        availabilityStatus = json["availabilityStatus"].boolValue
        changeRequest = json["changeRequest"].boolValue
        changeRequestApporve = json["changeRequestApporve"].stringValue
        index = json["index"].intValue
        brandId = json["brandId"].stringValue
        branchId = json["branchId"].stringValue
        menuId = json["menuId"].stringValue
        menuCategoryName = json["menuCategoryName"].stringValue
        productName = json["productName"].stringValue
        productNameAr = json["productNameAr"].stringValue
        description = json["description"].stringValue
        descriptionAr = json["descriptionAr"].stringValue
        price = json["price"].intValue
        productImage = json["productImage"].stringValue
        createdAt = json["createdAt"].stringValue
        updatedAt = json["updatedAt"].stringValue
    }
}
