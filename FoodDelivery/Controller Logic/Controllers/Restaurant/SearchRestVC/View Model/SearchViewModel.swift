//
//  View Model.swift
//  FoodDelivery
//
//  Created by Cst on 4/25/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class SearchViewModel: BaseViewModel,CellRepresentable {
    
    var rowHeight: CGFloat = UITableView.automaticDimension
    var restaurantListData: RestaurantListModel?{
        didSet{
            self.filterRestaurantCuisineResult()
        }
    }
    var filterAcctoFilterScreen:[RestaurantData]?
    var dishDataModel : DishDataModel?{
        didSet{
        }
    }
    var filterAccToFilterScreenDish:[CustomDishDataModel]?
    var fillterdishData:[CustomDishDataModel]?
    var maindishData:[CustomDishDataModel]?{
        didSet{
            self.didFinishFetch?()
        }
    }
    var selection = "Restaurant"
    var searchedText = ""
    
    //Custom DishModel
    var customDishData = [CustomDishDataModel]()
    //
    
    //MARK:searchTextForFilter
    var searchTextForFilter:String?{
        
        didSet{
            guard let searchtext = searchTextForFilter else {  return}
            searchedText = searchtext
            if selection == "Dish"{
                dishfilterData = (filterAccToFilterScreenDish?.filter {data in
                    return filterDish(data, searchtext)
                } ?? [CustomDishDataModel]())
                
                
            }else if selection == "Restaurant"{
                filterData = (filterAcctoFilterScreen?.filter {data in
                    
                    return (data.branchNameEn?.lowercased().contains(searchedText.lowercased()) ?? false)
                    
                } ?? [RestaurantData]())
            }else{
                filterData = (filterAcctoFilterScreen?.filter {data in
                    
                    return (filterCuisines(data, searchedText))
                    
                } ?? [RestaurantData]())
            }
            
        }
    }
    
    
    
    //MARK:Type of selection
    var typeOfSelection:String?{
        didSet{
            print("type of selection......\(typeOfSelection!)")
            print("searchText......\(searchedText)")
            selection = typeOfSelection!
            if !searchedText.isEmpty{
                if selection == "Dish"{
                    dishfilterData = (filterAccToFilterScreenDish?.filter {data in
                        
                        return filterDish(data, searchedText)
                        
                        
                    } ?? [CustomDishDataModel]())
                }else if selection == "Restaurant"{
                    filterData = (filterAcctoFilterScreen?.filter {data in
                        
                        return (data.branchNameEn?.lowercased().contains(searchedText.lowercased()) ?? false)
                        
                    } ?? [RestaurantData]())
                }else{
                    filterData = (filterAcctoFilterScreen?.filter {data in
                        
                        return (filterCuisines(data, searchedText))
                        
                    } ?? [RestaurantData]())
                }
            }
        }
    }
    var mainRestData : [RestaurantData]?{
        didSet{
            self.didFinishFetch?()
        }
    }
    var filterData : [RestaurantData]?{
        didSet{
            mainRestData = filterData ?? []
        }
    }
    
    var dishfilterData : [CustomDishDataModel]?{
        didSet{
            maindishData = dishfilterData ?? []
        }
    }
    
    private var dataService: ApiManager?
    var controller = UIViewController()
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    
    
    //Table CellForRow Setup
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        if typeOfSelection == "Dish"{
            tableView.register(UINib(nibName: "DishesCell", bundle: nil), forCellReuseIdentifier: "DishesCell")
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DishesCell") as? DishesCell else {
                return UITableViewCell()
            }
            
            cell.item = self.maindishData?[indexPath.row]
            return cell
        }else{
            tableView.register(UINib(nibName: "FavouritesTableCell", bundle: nil), forCellReuseIdentifier: "FavouritesTableCell")
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavouritesTableCell") as? FavouritesTableCell else {
                return UITableViewCell()
            }
            cell.item = self.mainRestData?[indexPath.row]
            cell.btnFavt.tag = indexPath.row
            cell.btnFavt.addTarget(self, action: #selector(favtTap(_:)), for: .touchUpInside)
            cell.btnTracking.addTarget(self, action: #selector(liveTracking(_:)), for: .touchUpInside)
            return cell
        }
        
    }
    @objc func liveTracking(_ sender:UIButton){
        let vc = controller.storyboard?.instantiateViewController(withIdentifier: "OnItsWayVC") as! OnItsWayVC
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func favtTap(_ sender:UIButton){
        
        if !comClass.isDataExist(){
            controller.show_Alert(message: "Login Required!")
            return
        }
        
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        let params = ["restaurantId":self.mainRestData?[sender.tag].Id ?? ""] as [String:Any]
        self.dataService?.fetchApiService(method: .post, url: "addToFavourite", passDict: params, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                if data["status"].intValue == 200{
                    if data["message"].stringValue == "Remove from favourite successfully"{
                        sender.setImage(#imageLiteral(resourceName: "heart_g"), for: .normal)
                    }else{
                        sender.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
                    }
                }else{
                    self.controller.show_Alert(message: data["message"].stringValue)
                }
                break
            case .failure(let error):
                self.controller.show_Alert(message: error.localizedDescription)
                break
            }
        })
        
    }
    
    /// Get RestaurantList Service
    /// - Parameters:
    ///   - url: Api EndPoint
    ///   - params: Lat and Long as Parameter and UserId If login
    func fetchRestaurantList(_ url:String,_ params:[String:Any],complition:@escaping (Int?,Error?) ->()) {
        self.dataService?.fetchApiService(method: .post, url: url,passDict: params, callback: { (result) in
            switch result{
            case .success(let data):
                self.restaurantListData = RestaurantListModel(data)
                complition(self.restaurantListData?.status,nil)
                break
            case .failure(let error):
                complition(nil,error)
                break
            }
        })
    }
    
    func fetchDishData(_ url:String,_ param:[String:Any]){
        self.dataService?.fetchApiService(method: .post, url: url, passDict: param, callback: { (result) in
            switch result{
            case .success(let data):
                self.dishDataModel = DishDataModel(data)
                self.customDishData.removeAll()
                if let dishData = data["data"].array{
                    dishData.forEach({item in
                        let rating = item["avgRating"].intValue
                        let restId = item["_id"].stringValue
                        let dist = Dist(item["dist"])
                        let brandData = BrandDataa(item["brandData"])
                        let areaData = AreaData(item["areaData"])
                        let categoryData = item["categoryData"].arrayValue.map { CategoryData($0) }
                        if let itemListJson = item["itemList"].array{
                            itemListJson.forEach({itemData in
                                self.customDishData.append(CustomDishDataModel(itemData, restId, rating, dist, brandData, areaData, categoryData))
                            })
                        }
                    })
                }
                
                print("CustomDish Data \(self.customDishData.count)")
                self.filterDishResult()
                break
            case .failure(let error):
                print(error)
                break
            }
        })
    }
    
    func searchBar(_ searchBar: UISearchBar? = nil, textDidChange searchText: String) {
        
        filterContentForSearchText(searchText: searchText)
    }
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        if searchText != "" {
            searchTextForFilter = searchText
        }else{
            self.filterData?.removeAll()
            self.dishfilterData?.removeAll()
        }
    }
    
    func filterCuisines(_ data: RestaurantData,_ searchText:String) -> Bool{
        guard let data = data.brandData?.cuisineArrayEn else {
            return false
        }
        return data.contains(where: {item in
            (item.itemText?.lowercased().contains(searchText.lowercased()) ?? false)
        })
    }
    func filterCuisinesForDish(_ data: CustomDishDataModel,_ searchText:String) -> Bool{
        guard let data = data.brandData?.cuisineArrayEn else {
            return false
        }
        return data.contains(where: {item in
            (item.itemText?.lowercased().contains(searchText.lowercased()) ?? false)
        })
    }

    
    func filterDish(_ data: CustomDishDataModel,_ searchText:String) -> Bool{
        
            (data.productName?.lowercased().contains(searchText.lowercased()) ?? false)
    }
    
    func filteration(rating: Int, distance: Int, time: Int){
        
        filterData = (mainRestData?.filter {data in
            
            return data.avgRating ?? 0 == rating
            
        } ?? [RestaurantData]())
    }
    
    
    //Filter Acc to Filter screen
    
    func filterRestaurantCuisineResult(){
    
        var filterArray: [RestaurantData]?
            filterArray = self.restaurantListData?.data?.filter({ item in
                (item.avgRating ?? 0 >= RatingSearch ?? 0)
            })
            filterArray = filterArray?.filter({item in
                (item.areaData?.deliveryTime ?? 0 >= TimeRangeSearch?.first ?? 15) &&
                (item.areaData?.deliveryTime ?? 0 <= TimeRangeSearch?.last ?? 30)
            })
            filterArray = filterArray?.filter({item in
                item.dist?.calculated ?? 0 >= ((DistanceRangeSearch?.first ?? 0) * 1000) &&
                item.dist?.calculated ?? 0 <= ((DistanceRangeSearch?.last ?? 20) * 1000)
            })
        var count = 0
        CuisineArraySearch?.forEach({item in
            let dd = filterArray?.filter({dd in
                return filterCuisines(dd, item.itemText ?? "")
            })
            if let datt = dd{
                if count <= 0{
                    filterArray = datt
                }else{
                    filterArray?.append(contentsOf: datt)
                }
            }
            count += 1
        })
      
        self.filterAcctoFilterScreen = filterArray
    }
    
    
    func filterDishResult(_ afterApplyingFilter:Bool? = false){
        
        var filterArray: [CustomDishDataModel]?
        if afterApplyingFilter ?? false{
            filterArray = self.maindishData?.filter({ item in
                (item.avgRating ?? 0 >= RatingSearch ?? 0)
            })
        }else{
            filterArray = self.customDishData.filter({ item in
                (item.avgRating ?? 0 >= RatingSearch ?? 0)
            })
        }
        
        filterArray = filterArray?.filter({item in
            (item.areaData?.deliveryTime ?? 0 >= TimeRangeSearch?.first ?? 15) &&
                (item.areaData?.deliveryTime ?? 0 <= TimeRangeSearch?.last ?? 30)
        })
        filterArray = filterArray?.filter({item in
            item.dist?.calculated ?? 0 >= ((DistanceRangeSearch?.first ?? 0) * 1000) &&
                item.dist?.calculated ?? 0 <= ((DistanceRangeSearch?.last ?? 20) * 1000)
        })
        var count = 0
        CuisineArraySearch?.forEach({item in
            let dd = filterArray?.filter({dd in
                return filterCuisinesForDish(dd, item.itemText ?? "")
            })
            if let datt = dd{
                if count <= 0{
                    filterArray = datt
                }else{
                    filterArray?.append(contentsOf: datt)
                }
            }
            count += 1
        })
        
        self.filterAccToFilterScreenDish = filterArray
    }

}

//= ["Restaurant","Cuisines","Dish"]
struct SearchTypeCollectionData {
    var title: String?
    var isSelected: Bool? = false
}
