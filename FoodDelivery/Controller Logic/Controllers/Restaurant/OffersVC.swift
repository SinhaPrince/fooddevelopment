//
//  OffersVC.swift
//  FoodDelivery
//
//  Created by call soft on 02/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class OffersVC: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var tblVw: UITableView!
    //MARK:- Properties
     var tblData = ["Offer 1","Offer 2","Offer 3","Offer 4","Offer 5"]
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
            initiallizers()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
   //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        }
    
    @IBAction func btnActionApply(_ sender: UIButton) {
        print("Offers Apply")
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}

//MARK:- Custom Methods
extension OffersVC {
    func initiallizers() {
        configureTable()
        
        
    }
    
    
}
//MARK:- UITableViewDelegate and UITableViewDatasource
extension OffersVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    let cell : OffersTableCell = tblVw.dequeueReusableCell(withIdentifier: "OffersTableCell") as! OffersTableCell
        cell.lblTitle.text = tblData[indexPath.row]
        cell.vwOuter.borderColor = AppColor.offerBorder
        cell.imgCheck.isHidden = true
    return cell
           
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.backgroundColor = AppColor.clearColor
        tblVw.register(UINib.init(nibName: "OffersTableCell", bundle: nil), forCellReuseIdentifier: "OffersTableCell")
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tblVw.cellForRow(at: indexPath) as? OffersTableCell {
            cell.vwOuter.borderColor = AppColor.pink
            cell.imgCheck.isHidden = false
            
        }
    }
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        if let cell = tblVw.cellForRow(at: indexPath) as? OffersTableCell {
//
//            cell.vwOuter.borderColor = AppColor.offerBorder
//            cell.imgCheck.isHidden = true
//        }
//    }
    
}
