//
//  RestaurantInfoVC.swift
//  FoodDelivery
//
//  Created by call soft on 26/02/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Foundation

class RestaurantInfoVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var bottomVw: UIView!
    @IBOutlet weak var collVw: UICollectionView!
    
    //MARK:- Properties
    var collData = ["Burger","Pizza","Fast Food"]
    let locationManager = CLLocationManager()
    var fromLat = Double()
    var fromLong = Double()
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomVw.cornerRadius = 25
        bottomVw.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        initiallizers()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //MARK:- Button Action
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionMoreDetails(_ sender: UIButton) {
        print("More Details")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantDetailVC") as! RestaurantDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}



//MARK:- Custom Methods
extension RestaurantInfoVC :GMSMapViewDelegate,CLLocationManagerDelegate{
    func initiallizers() {
        configureCollection()
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        
    }
    
    func configureCollection(){
        
        collVw.delegate = self
        collVw.dataSource = self
        collVw.backgroundColor = AppColor.clearColor
        collVw.register(UINib.init(nibName: "RestaurantInfoCollectionCell", bundle: nil), forCellWithReuseIdentifier: "RestaurantInfoCollectionCell")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: collVw.frame.width/3, height: 40)
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        collVw.collectionViewLayout = layout
        collVw.reloadData()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last
        let center = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        print("Coordinates",center)
        fromLat = userLocation!.coordinate.latitude
        fromLong = userLocation!.coordinate.longitude
        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude,
                                              longitude: userLocation!.coordinate.longitude, zoom: 10)
        
        mapView.camera = camera
        
        
        mapView.isMyLocationEnabled = false
        reverseGeocodeCoordinate(inLat: fromLat, inLong: fromLong)
        
        let marker2 = GMSMarker()
        marker2.position = CLLocationCoordinate2D(latitude: fromLat, longitude: fromLong)
        marker2.icon = UIImage(named: "dot_gps.jpg")
        marker2.map = mapView
        locationManager.stopUpdatingLocation()
    }
    
    
    func reverseGeocodeCoordinate(inLat:Double, inLong:Double) {
        
        let cordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: inLat, longitude: inLong)
        
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(cordinate) { response, error in
            
            if let address = response?.results() {
                
                let lines = address.first
                
                print(lines!)
                if let addressNew = lines?.lines {
                    // print("Locality : ",lines?.locality, "Sublocality : ",lines?.subLocality)
                    print(addressNew)
                    //self.address = addressNew[0]
                    //print("Address : ",self.address)
                    //self.lblAddress.textColor = AppColor.black
                    //self.lblAddress.text = self.address
                    
                    
                }
            }
        }
    }
    
}


//MARK:- UICollectionViewDelegate,UICollectionViewDataSource
extension RestaurantInfoVC : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : RestaurantInfoCollectionCell = collVw.dequeueReusableCell(withReuseIdentifier: "RestaurantInfoCollectionCell", for: indexPath) as! RestaurantInfoCollectionCell
         cell.vwOuter.cornerRadius = cell.vwOuter.height/2
        cell.vwOuter.clipsToBounds = false
        cell.lblItem.text = collData[indexPath.item]
        return cell
        
        
    }
    
    
    
}
