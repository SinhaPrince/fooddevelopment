//
//  LoyaltyCell.swift
//  FoodDelivery
//
//  Created by Cst on 5/17/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class LoyaltyCell: UITableViewCell {

    @IBOutlet weak var btnRedeem: UIButton!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var viewTop: UIView!
    
    
    var item: LoyaltyModel?{
        didSet{
            if item?.title == "Gold Bites"{
                viewTop.isHidden = false
            }else{
                viewTop.isHidden = true
            }
            title.setup(item?.title ?? "", AppColor.pink, AppFont.Medium.size(.Poppins, size: 15), .center)
            desc.setup(item?.description ?? "",AppColor.appLightBlack, AppFont.Regular.size(.Poppins, size: 15), .left)
            btnRedeem.titleLabel?.font = AppFont.Medium.size(.Poppins, size: 15)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

struct LoyaltyModel {
    var title: String?
    var description: String?
}
let loyaltyData = [LoyaltyModel(title: "Gold Bites", description: "Get X off when you visit Y times in Z days"),LoyaltyModel(title: "Sliver Bites", description: "Get X off when you visit Y times in Z days"),LoyaltyModel(title: "Bronze Bites", description: "Get X off when you visit Y times in Z days"),LoyaltyModel(title: "Welcome Bites", description: "Get X off when you visit Y times in Z days")]
