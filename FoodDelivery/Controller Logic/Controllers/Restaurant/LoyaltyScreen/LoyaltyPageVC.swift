//
//  LoyaltyPageVC.swift
//  FoodDelivery
//
//  Created by call soft on 09/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class LoyaltyPageVC: ParentViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTopTable: UILabel!
    @IBOutlet weak var lblBottomTable: UILabel!
    //MARK:- Properties
    var collData = ["Dish","Restaurant","Cuisines"]
    var tblData = ["","Second Tier 10%","Third Tier 15%"]
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initiallizers()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tblHeight.constant = tblVw.contentSize.height
        setupNavigation()
        lblBottomTable.isUserInteractionEnabled = true
        lblBottomTable.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoLayaltyInfo)))
    }
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
       
    }
    
    @objc func gotoLayaltyInfo(){
        //LoyaltyMoreInfoVC
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoyaltyMoreInfoVC") as! LoyaltyMoreInfoVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

//MARK:- Custom Methods
extension LoyaltyPageVC {
    func initiallizers() {
        configureTable()
        lblTopTable.provideAttributedTextToControlLeftAllignCenter("Get rewarded for your visits at Brand.\n", "1. Simply tap on the Redeem button next to your loyalty level.\n2. Show the code to your walter on the following screen.", AppFont.Medium.size(.Poppins, size: 16), AppFont.Regular.size(.Poppins, size: 13), AppColor.appLightBlack, AppColor.appLightBlack, .left)
        lblBottomTable.provideAttributedTextToControlLeftAllignCenter("Tap here ", "for more info and the terms and conditions of our loyalty system.", AppFont.Regular.size(.Poppins, size: 13), AppFont.Regular.size(.Poppins, size: 13), .blue, AppColor.appLightBlack, .left)
    }
   
}



//MARK:- UITableViewDelegate and UITableViewDatasource
extension LoyaltyPageVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loyaltyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
          let cell : LoyaltyCell = tblVw.dequeueReusableCell(withIdentifier: "LoyaltyCell") as! LoyaltyCell
            cell.item = loyaltyData[indexPath.row]
            cell.btnRedeem.addTarget(self, action: #selector(UseRewardButton), for: .touchUpInside)
             return cell
        
    }
    @objc func UseRewardButton(_ sender:UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantProfileOfferRequest") as! RestaurantProfileOfferRequest
        vc.navObj = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.backgroundColor = AppColor.clearColor
        tblVw.register(UINib.init(nibName: "LoyaltyCell", bundle: nil), forCellReuseIdentifier: "LoyaltyCell")
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title = UILabel()
        title.setup("Available Rewards", AppColor.appLightBlack, AppFont.Medium.size(.Poppins, size: 15), .left)
        return title
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        25
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantProfileOffer") as! RestaurantProfileOffer
//        vc.navObj = self
//        self.navigationController?.present(vc, animated: true, completion: nil)
//    }
    
}

