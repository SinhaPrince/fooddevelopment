//
//  LoyaltyMoreInfoVC.swift
//  FoodDelivery
//
//  Created by Cst on 5/17/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class LoyaltyMoreInfoVC: ParentViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        // Do any additional setup after loading the view.
    }
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("How Loyalty Works", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
}
