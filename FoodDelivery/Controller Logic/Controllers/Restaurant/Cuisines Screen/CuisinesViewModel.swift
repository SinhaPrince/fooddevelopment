//
//  CuisinesViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 5/9/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class CuisinesViewModel:BaseViewModel,CellRepresentable{
    
    var rowHeight: CGFloat = UITableView.automaticDimension
    var cuisineData = [CuisineArrayEn]()
    
    func setup(_ data:[CuisineArrayEn]){
        cuisineData.append(contentsOf: data)
        cuisineData = removeDuplicateElements(contacts: cuisineData)
    }
    func removeDuplicateElements(contacts: [CuisineArrayEn]) -> [CuisineArrayEn] {
        var uniqueContact = [CuisineArrayEn]()
        for contact in contacts {
            if !uniqueContact.contains(where: {$0.itemText == contact.itemText }) {
                uniqueContact.append(contact)
            }
        }
        return uniqueContact
    }
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "CuisineTableCell", bundle: nil), forCellReuseIdentifier: "CuisineTableCell")
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CuisineTableCell") as? CuisineTableCell else {
            return UITableViewCell()
        }
        cell.item = cuisineData[indexPath.row]
        return cell
    }
}

struct CategoryModelData {
    var name:String
    var id:String
    var isSeleted = false
}
