//
//  CuisinesVC.swift
//  FoodDelivery
//
//  Created by call soft on 02/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class CuisinesVC: ParentViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var tblVw: UITableView!
    //MARK:- Properties
   
    let cuisinesViewModel = CuisinesViewModel()
    var restaurantData = [RestaurantData]()
    var comingFrom : String?
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
        setupNavigation()
        
    }
   
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "Cuisines")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .right
        title.numberOfLines = 0
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
       
    }
   //MARK:- Button Action
  
    @IBAction func btnActionApply(_ sender: UIButton) {
        print("Cuisines Apply")
        if comingFrom == "Search"{
            CuisineArraySearch = self.cuisinesViewModel.cuisineData.filter({item in
                item.isSelected == true
            })
        }else{
            CuisineArray = self.cuisinesViewModel.cuisineData.filter({item in
                item.isSelected == true
            })
        }
        self.navigationController?.popViewController(animated: true)
    }
  
    
}

//MARK:- Custom Methods
extension CuisinesVC {
    func initiallizers() {
        self.restaurantData.forEach({[weak self] item in
           // self?.cuisinesViewModel.setup(item.brandData?.cuisineArrayEn ?? [])
            self?.cuisinesViewModel.setup(item.brandData?.cuisineArrayEn ?? [])
        })
        
        for i in 0..<self.cuisinesViewModel.cuisineData.count{
            self.cuisinesViewModel.cuisineData[i].isSelected = false
        }
        if comingFrom == "Search"{
            CuisineArraySearch?.forEach({item in
              let index = self.cuisinesViewModel.cuisineData.firstIndex(where: {data in
                    data.itemText == item.itemText
                })
                self.cuisinesViewModel.cuisineData[index ?? 0].isSelected = true
            })
        }else{
            CuisineArray?.forEach({item in
              let index = self.cuisinesViewModel.cuisineData.firstIndex(where: {data in
                    data.itemText == item.itemText
                })
                self.cuisinesViewModel.cuisineData[index ?? 0].isSelected = true
            })
        }
        configureTable()
    }
    
    
}
//MARK:- UITableViewDelegate and UITableViewDatasource
extension CuisinesVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.cuisinesViewModel.cuisineData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.cuisinesViewModel.cellInstance(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func configureTable() {
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.backgroundColor = AppColor.clearColor
        tblVw.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.cuisinesViewModel.cuisineData[indexPath.row].isSelected ?? false{
            self.cuisinesViewModel.cuisineData[indexPath.row].isSelected = false
        }else{
            self.cuisinesViewModel.cuisineData[indexPath.row].isSelected = true
        }
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
    
}
