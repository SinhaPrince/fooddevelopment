//
//  RestaurantDetailModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 04, 2021
//
import Foundation
import SwiftyJSON

struct RestaurantDetailModel {

	var status: Int?
	var message: String?
	var data: DataRest?

	init(_ json: JSON) {
		status = json["status"].intValue
		message = json["message"].stringValue
		data = DataRest(json["data"])
	}

}
