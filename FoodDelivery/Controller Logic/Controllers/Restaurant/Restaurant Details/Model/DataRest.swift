//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 04, 2021
//
import Foundation
import SwiftyJSON

struct DataRest {

	var Id: String?
	var location: Location?
	var adminVerificationStatus: String?
	var status: String?
	var menuAllowcateStatus: Bool?
	var image: String?
	var deliveryStatus: String?
	var loyalty: String?
	var totalOrders: Int?
	var brandId: BrandId?
	var branchNameEn: String?
	var branchNameAr: String?
	var descriptionEn: String?
	var descriptionAr: String?
	var countryCode: String?
	var address: String?
	var latitude: String?
	var longitude: String?
	var openTime: String?
	var closeTime: String?
	var email: String?
	var mobileNumber: String?
	var createdAt: String?
	var updatedAt: String?
	var _v: Int?
	var busy1Status: Bool?
	var busy2Status: Bool?
	var busy3Status: Bool?
	var busy4Status: Bool?
	var busy5Status: Bool?
	var busyTime: Int?
	var closeStatus: Bool?
	var oepnStatus: Bool?
	var deliveryFee: Int?
	var deliveryTime: Int?
	var minimumOrderValue: Int?
	var polygonAddress: String?
	var polygonData: [PolygonData]?
	var areaStatus: String?
	var polygonLat: String?
	var polygonLong: String?
	var menuId: String?
	var index: Int?
	var serviceType: String?
	var commission: Int?
	var vat: Int?
	var isFav: Bool?
    var brandData: BrandData?
    var areaData: AreaData?

	init(_ json: JSON) {
        areaData = AreaData(json["areaData"])

		Id = json["_id"].stringValue
		location = Location(json["location"])
		adminVerificationStatus = json["adminVerificationStatus"].stringValue
		status = json["status"].stringValue
		menuAllowcateStatus = json["menuAllowcateStatus"].boolValue
		image = json["image"].stringValue
		deliveryStatus = json["deliveryStatus"].stringValue
		loyalty = json["loyalty"].stringValue
		totalOrders = json["totalOrders"].intValue
		brandId = BrandId(json["brandId"])
		branchNameEn = json["branchNameEn"].stringValue
		branchNameAr = json["branchNameAr"].stringValue
		descriptionEn = json["descriptionEn"].stringValue
		descriptionAr = json["descriptionAr"].stringValue
		countryCode = json["countryCode"].stringValue
		address = json["address"].stringValue
		latitude = json["latitude"].stringValue
		longitude = json["longitude"].stringValue
		openTime = json["openTime"].stringValue
		closeTime = json["closeTime"].stringValue
		email = json["email"].stringValue
		mobileNumber = json["mobileNumber"].stringValue
		createdAt = json["createdAt"].stringValue
		updatedAt = json["updatedAt"].stringValue
		_v = json["__v"].intValue
		busy1Status = json["busy1Status"].boolValue
		busy2Status = json["busy2Status"].boolValue
		busy3Status = json["busy3Status"].boolValue
		busy4Status = json["busy4Status"].boolValue
		busy5Status = json["busy5Status"].boolValue
		busyTime = json["busyTime"].intValue
		closeStatus = json["closeStatus"].boolValue
		oepnStatus = json["oepnStatus"].boolValue
		deliveryFee = json["deliveryFee"].intValue
		deliveryTime = json["deliveryTime"].intValue
		minimumOrderValue = json["minimumOrderValue"].intValue
		polygonAddress = json["polygonAddress"].stringValue
		polygonData = json["polygonData"].arrayValue.map { PolygonData($0) }
		areaStatus = json["areaStatus"].stringValue
		polygonLat = json["polygonLat"].stringValue
		polygonLong = json["polygonLong"].stringValue
		menuId = json["menuId"].stringValue
		index = json["index"].intValue
		serviceType = json["serviceType"].stringValue
		commission = json["commission"].intValue
		vat = json["vat"].intValue
		isFav = json["isFav"].boolValue
        brandData = BrandData(json["brandData"])

	}

}
