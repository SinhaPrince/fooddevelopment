//
//  PolygonData.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 04, 2021
//
import Foundation
import SwiftyJSON

struct PolygonData {

	var lat: Double?
	var lng: Double?

	init(_ json: JSON) {
		lat = json["lat"].doubleValue
		lng = json["lng"].doubleValue
	}

}