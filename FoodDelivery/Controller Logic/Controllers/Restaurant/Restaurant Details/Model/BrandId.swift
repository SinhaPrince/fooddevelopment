//
//  BrandId.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 04, 2021
//
import Foundation
import SwiftyJSON

struct BrandId {

	var Id: String?
	var cuisineArrayEn: [CuisineArrayEn]?
	var firstName: String?
	var lastName: String?
    var logo: String?
	init(_ json: JSON) {
		Id = json["_id"].stringValue
		cuisineArrayEn = json["cuisineArrayEn"].arrayValue.map { CuisineArrayEn($0) }
		firstName = json["firstName"].stringValue
		lastName = json["lastName"].stringValue
        logo = json["logo"].stringValue
	}

}
