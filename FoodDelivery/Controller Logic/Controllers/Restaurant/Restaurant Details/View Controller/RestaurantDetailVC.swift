//
//  RestaurantDetailVC.swift
//  FoodDelivery
//
//  Created by call soft on 01/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import SkeletonView

class RestaurantDetailVC: ParentViewController, UIGestureRecognizerDelegate, showNavigationColorDelegate, BaseViewControllerDelegate {
    func showNaviColor(bool: Bool) {
        if bool{
            nontransparentNavigation()
            self.navigationItem.title = self.restViewModel.restaurantInfo?.data?.branchNameEn ?? ""
            self.navigationController?.navigationBar.tintColor = UIColor.white

        }else{
            transparentNavigation()
            self.navigationItem.title = ""
        }
    }
    
    //MARK:- Outlets
    @IBOutlet weak var listcollVw: UICollectionView!
    @IBOutlet weak var viewAddCart: UIView!
    @IBOutlet weak var btnViewCart: UIButton!
    @IBOutlet weak var imageRest: UIImageView!
    @IBOutlet weak var lblRestName: UILabel!
    @IBOutlet weak var lblRestAddress: UILabel!
    @IBOutlet weak var restStatus: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var brandLogo: UIImageView!
    @IBOutlet weak var btnRating: UIButton!
    @IBOutlet weak var btnTime: UIButton!
    @IBOutlet weak var btnFreeDel: UIButton!
    @IBOutlet weak var btnLiveTranking: UIButton!
    @IBOutlet weak var restInfo: UIView!
    @IBOutlet weak var btnFavt: UIButton!
    @IBOutlet weak var btnNavigation: UIButton!
    //MARK:- Properties
    var collData = ["Dish","Restaurant","Cuisines"]
    var itemAdded = "1"
    var id:String?
    var comingFrom:String?
    //Injection
    let restViewModel = RestaurantViewModel(networking: ApiManager())
    
    lazy var slideView: MenuListView = {
        let view = MenuListView(dragViewAnimatedTopSpace:0, viewDefaultHeightConstant:UIScreen.main.bounds.size.height/(sizeConfiguration() == "S" ? 3 : 2))
        view.backgroundColor = .white
        view.controller = self
       // view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
   
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeTheLocationManager()
        self.restInfo.isHidden = true
        self.restInfo.fadeOut()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.restViewModel.comingFrom =  self.comingFrom ?? ""
            self.getRestaurantDetail()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideNavigationBar(false)
        self.transparentNavigation()
        self.slideView.downToBottom()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
        self.btnNavigation.addTarget(self, action: #selector(btnNavigationTap), for: .touchUpInside)
        self.baseDelegate = self
        let nav = self.navigationController?.navigationBar
          //  nav?.barStyle = UIBarStyle.black
        nav?.tintColor = AppColor.pink
        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,
                                    NSAttributedString.Key.font: AppFont.Medium.size(.Poppins, size: 14)]
        
    }
    
    @objc func btnViewCartTap(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
    @objc func btnNavigationTap(){
        let data = self.restViewModel.restaurantInfo?.data
        comClass.openMapButtonAction(self, data?.latitude ?? "", data?.longitude ?? "")
    }
    
    func setupNavigation(){
        transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let buttonSearch = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        buttonSearch.backgroundColor = .white
        buttonSearch.cornerRadius = 8
        buttonSearch.setImage(#imageLiteral(resourceName: "search"), for: .normal)
        buttonSearch.contentHorizontalAlignment = .center
        buttonSearch.tag = UINavigationBarButtonType.search.rawValue
        buttonSearch.addTarget(self, action: #selector(self.navigationButtonTapped(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: buttonSearch)
    }
    
    func navigationBarButtonDidTapped(_ buttonType: UINavigationBarButtonType) {
         if buttonType == .search{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SearchForRestaurantVC") as! SearchForRestaurantVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- Custom Methods
extension RestaurantDetailVC:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if comingFrom == "Brand"{
            return self.restViewModel.restaurantInfo?.data?.brandData?.brandData?.cuisineArrayEn?.count ?? 0
        }else{
          return  self.restViewModel.restaurantInfo?.data?.brandId?.cuisineArrayEn?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      return self.restViewModel.collectionCellInstance1(collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidth: CGFloat = flowLayout.itemSize.width
        let cellSpacing: CGFloat = flowLayout.minimumInteritemSpacing
        var cellCount = CGFloat(collectionView.numberOfItems(inSection: section))
        var collectionWidth = collectionView.frame.size.width
        var totalWidth: CGFloat
        if #available(iOS 11.0, *) {
            collectionWidth -= collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right
        }
        repeat {
            totalWidth = cellWidth * cellCount + cellSpacing * (cellCount - 1)
            cellCount -= 1
        } while totalWidth >= collectionWidth
        
        if (totalWidth > 0) {
            let edgeInset = (collectionWidth - totalWidth) / 2
            return UIEdgeInsets.init(top: flowLayout.sectionInset.top, left: edgeInset, bottom: flowLayout.sectionInset.bottom, right: edgeInset)
        } else {
            return flowLayout.sectionInset
        }
    }
    func initiallizers() {
        listcollVw.backgroundColor = .white
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.itemSize = CGSize(width: 105, height: 40)
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        listcollVw.setCollectionViewLayout(layout, animated: true)
        listcollVw.delegate = self
        listcollVw.dataSource = self
        listcollVw.showsHorizontalScrollIndicator = false
        slideView.delegate = self
        self.view.insertSubview(slideView, at: 1)
        self.viewAddCart.bringSubviewToFront(slideView)
        self.btnViewCart.addTarget(self, action: #selector(btnViewCartTap), for: .touchUpInside)
        self.listcollVw.reloadData()
        self.slideView.getMenuData(self.restViewModel.restaurantInfo?.data?.menuId ?? "")
        self.setRestaurantData()
    }
    
    func setRestaurantData(){
        btnFavt.addTarget(self, action: #selector(getFavt(_:)), for: .touchUpInside)

        let data = self.restViewModel.restaurantInfo?.data
        self.lblRestName.text = data?.branchNameEn ?? ""
        self.lblRestAddress.text = data?.address ?? ""
      //  self.brandLogo.sd_setImage(with: URL(string: data?.brandData?.image ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"), options: .lowPriority, context: [:])
        if self.comingFrom == "Brand"{
            self.brandLogo.setImage(withImageId: data?.brandData?.logo ?? "" , placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"))
            self.imageRest.sd_setImage(with: URL(string: data?.image ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"), options: .lowPriority, context: [:])
        }else{
            self.brandLogo.setImage(withImageId: data?.image ?? "", placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"))
            self.imageRest.sd_setImage(with: URL(string: data?.brandId?.logo ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"), options: .lowPriority, context: [:])
          
        }
        self.btnTime.setTitle("\(data?.areaData?.deliveryTime ?? 0) Mins", for: .normal)
        self.btnFreeDel.setTitle("AED \(data?.areaData?.deliveryFee ?? 0)", for: .normal)
//        if  data?.brandData?.avgRating ?? 0 > 0{
//            self.btnRating.isHidden = false
//            btnTime.contentHorizontalAlignment = .right
//        }else{
//            self.btnRating.isHidden = true
//            btnTime.contentHorizontalAlignment = .left
//        }
        self.btnRating.setTitle(data?.brandData?.avgRating?.description, for: .normal)
        self.btnRating.setImage(#imageLiteral(resourceName: "star"), for: .normal)
        self.btnTime.setImage(#imageLiteral(resourceName: "clock_c"), for: .normal)
        self.btnFreeDel.setImage(#imageLiteral(resourceName: "scooter_y"), for: .normal)
        self.btnLiveTranking.setTitle("Live Tracking", for: .normal)
        
        if data?.serviceType == "Market Place"{
            self.btnLiveTranking.isHidden = true
            restStatus.contentHorizontalAlignment = .center
        }else{
            self.btnLiveTranking.isHidden = false
        }
        self.btnLiveTranking.setImage(#imageLiteral(resourceName: "tracking"), for: .normal)
        self.btnFavt.setImage((data?.isFav ?? false) ? #imageLiteral(resourceName: "heart") : #imageLiteral(resourceName: "heart_g") , for: .normal)
        if data?.oepnStatus ?? false{
            restStatus.setImage(UIImage(named: "open"), for: .normal)
            restStatus.tintColor = .green
            restStatus.setTitle("Open", for: .normal)
        }else if (data?.closeStatus ?? false){
            restStatus.setImage(UIImage(named: "open"), for: .normal)
            restStatus.tintColor = .red
            restStatus.setTitle("Closed", for: .normal)
        }else{
            restStatus.setImage(UIImage(named: "open"), for: .normal)
            restStatus.tintColor = .orange
            restStatus.setTitle("Busy", for: .normal)
        }
        
        self.restInfo.isHidden = false
        self.restInfo.fadeIn()
    }
    
    @objc func getFavt(_ sender:UIButton){
        
        if !comClass.isDataExist(){
            self.show_Alert(message: "Login Required!")
            return
        }
        
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        let params = ["restaurantId":self.restViewModel.restaurantInfo?.data?.Id ?? ""] as [String:Any]
        ApiManager.instance.fetchApiService(method: .post, url: "addToFavourite", passDict: params, header: header, callback: { (result) in
            switch result{
            case .success(let data):
                if data["status"].intValue == 200{
                    if data["message"].stringValue == "Remove from favourite successfully"{
                        sender.setImage(#imageLiteral(resourceName: "heart_g"), for: .normal)
                    }else{
                        sender.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
                    }
                }else{
                    self.show_Alert(message: data["message"].stringValue)
                }
                break
            case .failure(let error):
                self.show_Alert(message: error.localizedDescription)
                break
            }
        })
        
    }
}

//MARK:- Network Calling
extension RestaurantDetailVC{
    func getRestaurantDetail(){
        Indicator.shared.showLottieAnimation(view: view)
        //comingFrom
        var param = [String:Any]()
        var strUrl = ""
        if comingFrom == "Brand"{
            param = ["brandId" : self.id ?? "","longitude":longg,"latitude":lattt]
            strUrl = "getRestaurantByBrand"
        }else{
            param = ["restaurantId" : self.id ?? "","longitude":longg,"latitude":lattt]
            strUrl = "getRestaurantDetails"
        }
        self.restViewModel.getRestaurantInfo(strUrl,param) { (status, error) in
            guard error == nil else{
                self.handleError(error?.localizedDescription ?? "")
                return
            }
            guard status == 200 else{
                self.handleError(self.restViewModel.restaurantInfo?.message ?? "")
                return
            }
            self.initiallizers()
        }
    }
}

//MARK: - Screen sizes
extension UIDevice {
    var iPhoneX: Bool {
        //  UIScreen.main.nativeBounds.height == 2436
        return true
    }
    var iPhone: Bool {
        // UIDevice.current.userInterfaceIdiom == .phone
        return true
    }
    
    enum ScreenType: String {
        case iPhones_4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhones_X_XS = "iPhone X or iPhone XS"
        case iPhone_XR_11 = "iPhone XR or iPhone 11"
        case iPhone_XSMax_11ProMax = "iPhone XS Max or iPhone 11 Pro Max"
        case iPhone_11Pro_X_XS = "iPhone 11 Pro or iPhone X or iPhone XS"
        case iPhone_12_Pro = "iPhone 12 or iPhone 12 Pro"
        case iPhone_12ProMax = "iPhone 12 Pro Max"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 1792:
            return .iPhone_XR_11
        case 2436:
            return .iPhone_11Pro_X_XS
        case 2688:
            return .iPhone_XSMax_11ProMax
        case 2532:
            return .iPhone_12_Pro
        case 2778:
            return .iPhone_12ProMax
        default:
            return .unknown
        }
    }
    
}


