//
//  MenuListView.swift
//  FoodDelivery
//
//  Created by Cst on 4/30/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import SkeletonView

class MenuListView: UIView {
    
    //  MARK: - Public properties
    public var viewCornerRadius:CGFloat = 8
    public var viewBackgroundColor:UIColor = UIColor.clear
    public var controller = UIViewController()
    public var delegate: showNavigationColorDelegate?
    // MARK: - Private properties
    private var dragViewAnimatedTopMargin:CGFloat = 25.0 // View fully visible (upper spacing)
    private var viewDefaultHeight:CGFloat = 80.0// View height when appear
    private var gestureRecognizer = UIPanGestureRecognizer()
    private var dragViewDefaultTopMargin:CGFloat!
    private var viewLastYPosition = 0.0
    
    
    lazy var buttonMenu:UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        button.contentHorizontalAlignment = .center
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        return button
    }()
    
    lazy var menuCategoryCollection:UICollectionView = {
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.backgroundColor = .white
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.itemSize = CGSize(width: 60, height: 60)
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        collection.setCollectionViewLayout(layout, animated: true)
        collection.delegate = self
        collection.showsHorizontalScrollIndicator = false
        //        alarmCollectionView.backgroundColor = UIColor.clear
        return collection
    }()
    
    lazy var menuListTable:UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorStyle = .singleLine
        table.backgroundColor = .white
        table.showsVerticalScrollIndicator = false
        table.isUserInteractionEnabled = true
        table.alwaysBounceVertical = false
        table.scrollsToTop = true
        table.bounces = false
        return table
    }()
    
    let menuVM = MenuListViewModel(networking: ApiManager())
    
    required init(dragViewAnimatedTopSpace:CGFloat, viewDefaultHeightConstant:CGFloat)
    {
        dragViewAnimatedTopMargin = dragViewAnimatedTopSpace
        viewDefaultHeight = viewDefaultHeightConstant
        
        let screenSize: CGRect = UIScreen.main.bounds
        dragViewDefaultTopMargin = screenSize.height - viewDefaultHeight
        
        super.init(frame: CGRect(x: 0, y:dragViewDefaultTopMargin , width: screenSize.width, height: screenSize.height))
        
        self.backgroundColor = viewBackgroundColor//.withAlphaComponent(0.20) //
        self.layer.cornerRadius = self.viewCornerRadius
        self.clipsToBounds = true
        
        self.addSubview(buttonMenu)
        self.addSubview(menuCategoryCollection)
        self.addSubview(menuListTable)
        
        NSLayoutConstraint.activate([buttonMenu.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
                                     buttonMenu.heightAnchor.constraint(equalToConstant: 40),
                                     buttonMenu.widthAnchor.constraint(equalToConstant: 40),
                                     buttonMenu.centerYAnchor.constraint(equalTo: menuCategoryCollection.centerYAnchor, constant: 0),
                                     menuCategoryCollection.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
                                     menuCategoryCollection.leadingAnchor.constraint(equalTo: buttonMenu.trailingAnchor, constant: 0),
                                     menuCategoryCollection.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
                                     menuCategoryCollection.heightAnchor.constraint(equalToConstant: 60),
                                     menuListTable.topAnchor.constraint(equalTo: menuCategoryCollection.bottomAnchor, constant: 5),
                                     menuListTable.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
                                     menuListTable.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
                                     menuListTable.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -90)])
        
        self.layoutIfNeeded()
        gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        self.addGestureRecognizer(gestureRecognizer)
        self.menuCategoryCollection.addGestureRecognizer(gestureRecognizer)
        self.menuListTable.addGestureRecognizer(gestureRecognizer)
        self.menuListTable.register(UINib(nibName: "MenuListCell", bundle: nil), forCellReuseIdentifier: "MenuListCell")
        self.menuListTable.dataSource = self
        self.menuListTable.isSkeletonable = true
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        self.menuListTable.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7215660045)), animation: animation, transition: .crossDissolve(0.25))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            
            var newTranslation = CGPoint()
            var oldTranslation = CGPoint()
            newTranslation = gestureRecognizer.translation(in: self.superview)
            
            print(newTranslation.y)
            if(!(newTranslation.y < 0 && self.frame.origin.y + newTranslation.y <= dragViewAnimatedTopMargin))
            {
                self.translatesAutoresizingMaskIntoConstraints = true
                self.center = CGPoint(x: self.center.x, y: self.center.y + newTranslation.y)
                
                if (newTranslation.y < 0)
                {
                    if("\(self.frame.size.width)" != "\(String(describing: self.superview?.frame.size.width))")
                    {
                        
                        UIView.animate(withDuration: 0, delay: 0, options: [.curveEaseOut],
                                       animations: {
                                        self.frame = CGRect(x: self.frame.origin.x , y:self.frame.origin.y , width: self.frame.size.width , height: self.frame.size.height)
                                       }, completion: nil)
                    }
                }
                else
                {
                    if("\(self.frame.size.width)" != "\((self.superview?.frame.size.width)! - 20)")
                    {
                        UIView.animate(withDuration: 0, delay: 0, options: [.curveEaseOut],
                                       animations: {
                                        self.frame = CGRect(x: self.frame.origin.x , y:self.frame.origin.y , width: self.frame.size.width, height: self.frame.size.height)
                                       }, completion: nil)
                        
                        
                    }
                }
                
                // self.layoutIfNeeded()
                gestureRecognizer.setTranslation(CGPoint.zero, in: self.superview)
                
                oldTranslation.y = newTranslation.y
            }
            else
            {
                self.frame.origin.y = dragViewAnimatedTopMargin
                self.isUserInteractionEnabled = false
            }
            
        }
        
        else if (gestureRecognizer.state == .ended)
        {
            
            self.isUserInteractionEnabled = true
            let vel = gestureRecognizer.velocity(in: self.superview)
            let finalY: CGFloat = 50.0
            let curY: CGFloat = self.frame.origin.y
            let distance: CGFloat = curY - finalY
            let springVelocity: CGFloat = 1.0 * vel.y / distance
            
            if(springVelocity > 0 && self.frame.origin.y <= dragViewAnimatedTopMargin)
            {
                self.frame = CGRect(x: 0, y:self.frame.origin.y , width: (self.superview?.frame.size.width)!, height: self.frame.size.height)
                self.delegate?.showNaviColor(bool: true)
                
            }
            else if (springVelocity > 0)
            {
                
                if (self.frame.origin.y < (self.superview?.frame.size.height)!/3 && springVelocity < 7)
                {
                    UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 6, initialSpringVelocity: 1, options: UIView.AnimationOptions.curveEaseOut, animations: ({
                        if("\(self.frame.size.width)" != "\(String(describing: self.superview?.frame.size.width))")
                        {
                            self.frame = CGRect(x: 0, y:self.frame.origin.y , width: (self.superview?.frame.size.width)!, height: self.frame.size.height)
                        }
                        
                        self.frame.origin.y = self.dragViewAnimatedTopMargin
                    }), completion: nil)
                }
                else
                {
                    UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: UIView.AnimationOptions.curveEaseOut, animations: ({
                        
                        if(self.frame.size.width != (self.superview?.frame.size.width)! - 20)
                        {
                            self.frame = CGRect(x: 0, y:self.frame.origin.y , width: (self.superview?.frame.size.width)!, height: self.frame.size.height)
                        }
                        self.delegate?.showNaviColor(bool: false)
                        self.frame.origin.y = self.dragViewDefaultTopMargin
                    }), completion:  { (finished: Bool) in
                        
                    })
                }
            }
            else if (springVelocity == 0)// If Velocity zero remain at same position
            {
                
                UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: UIView.AnimationOptions.curveEaseOut, animations: ({
                    
                    self.frame.origin.y = CGFloat(self.viewLastYPosition)
                    
                    if(self.frame.origin.y == self.dragViewDefaultTopMargin)
                    {
                        if("\(self.frame.size.width)" == "\(String(describing: self.superview?.frame.size.width))")
                        {
                            self.frame = CGRect(x: 0, y:self.frame.origin.y , width: self.frame.size.width , height: self.frame.size.height)
                        }
                    }
                    else{
                        if("\(self.frame.size.width)" != "\(String(describing: self.superview?.frame.size.width))")
                        {
                            self.frame = CGRect(x: 0, y:self.frame.origin.y , width: (self.superview?.frame.size.width)!, height: self.frame.size.height)
                        }
                    }
                    
                }), completion: nil)
            }
            else
            {
                if("\(self.frame.size.width)" != "\(String(describing: self.superview?.frame.size.width))")
                {
                    self.frame = CGRect(x: 0, y:self.frame.origin.y , width: (self.superview?.frame.size.width)!, height: self.frame.size.height)
                }
                
                
                UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 6, initialSpringVelocity: 1, options: UIView.AnimationOptions.curveEaseOut, animations: ({
                    self.delegate?.showNaviColor(bool: true)
                    self.frame.origin.y = self.dragViewAnimatedTopMargin
                }), completion: nil)
            }
            viewLastYPosition = Double(self.frame.origin.y)
            self.menuCategoryCollection.addGestureRecognizer(gestureRecognizer)
            self.menuListTable.addGestureRecognizer(gestureRecognizer)
            self.addGestureRecognizer(gestureRecognizer)
        }
        
    }
    
    public func downToBottom(){
        
        if self.menuVM.menuList?.data?.itemData?.count ?? 0 <= 0{
            return
        }
        
        self.isUserInteractionEnabled = true
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: UIView.AnimationOptions.curveEaseOut, animations: ({
            
            if(self.frame.size.width != (self.superview?.frame.size.width)! - 20)
            {
                self.frame = CGRect(x: 0, y:self.frame.origin.y , width: (self.superview?.frame.size.width)!, height: self.frame.size.height)
            }
            self.delegate?.showNaviColor(bool: false)
            self.frame.origin.y = self.dragViewDefaultTopMargin
        }), completion:  { (finished: Bool) in
            
        })
    }
    @objc func buttonAction(sender: UIButton!) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MenuCategoryVC") as! MenuCategoryVC
        vc.menuCategoryData = self.menuVM.refreshMenuData
        vc.delegate = self
        controller.present(vc, animated: true, completion: nil)
    }
    
}

//MARK:- UITableView Delegate and CollectionView Delegate

extension MenuListView:SkeletonTableViewDataSource,UICollectionViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,ReturnIndexDelegate{
    func indexNo(index: Int) {
        let IIndexPath = IndexPath(row: 0, section: index)
        if self.menuVM.refreshMenuData[IIndexPath.section].menu?.count ?? 0 <= 0{
            self.controller.show_Alert(message: "No data found! in this category")
            return
        }
        self.menuListTable.scrollToRow(at: IIndexPath, at: .bottom, animated: true)
        for (index,_) in self.menuVM.refreshMenuData.enumerated(){
            self.menuVM.refreshMenuData[index].isSelected = false
        }
        self.menuVM.refreshMenuData[IIndexPath.row].isSelected = true
        menuCategoryCollection.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //  self.menuVM.menuList?.data?.itemData?.count ?? 0
        self.menuVM.refreshMenuData.count 
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int{
        5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // self.menuVM.menuList?.data?.itemData?[section].itemList?.count ?? 0
        self.menuVM.refreshMenuData[section].menu?.count ?? 0
        
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "MenuListCell"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.menuVM.cellInstance(self.menuListTable, indexPath: indexPath)
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let titleHeader = UILabel(frame: CGRect(x: 10, y: 10, width: 200, height: 50))
        //titleHeader.setup(self.menuVM.menuList?.data?.itemData?[section].menuName ?? "", AppColor.appLightBlack, AppFont.Medium.size(.Poppins, size: 16), .left)
        titleHeader.setup(self.menuVM.refreshMenuData[section].category ?? "", AppColor.appLightBlack, AppFont.Medium.size(.Poppins, size: 16), .left)
        titleHeader.backgroundColor = .white
        return titleHeader
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 100))
        view.backgroundColor = .white
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == (self.menuVM.refreshMenuData.count ) - 1{
            return 100
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        50
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // self.menuVM.menuList?.data?.itemData?.count ?? 0
        self.menuVM.refreshMenuData.count 
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let IIndexPath = IndexPath(row: 0, section: indexPath.row)
        if self.menuVM.refreshMenuData[indexPath.row].menu?.count ?? 0 <= 0{
            self.controller.show_Alert(message: "No data found! in this category")
            return
        }
        self.menuListTable.scrollToRow(at: IIndexPath, at: .bottom, animated: true)
        for (index,_) in self.menuVM.refreshMenuData.enumerated(){
            self.menuVM.refreshMenuData[index].isSelected = false
        }
        self.menuVM.refreshMenuData[indexPath.row].isSelected = true
        menuCategoryCollection.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        print("willDisplayHeaderView",section)
        let indexPath = IndexPath.init(row: section, section: 0)
        self.menuCategoryCollection.scrollToItem(at: indexPath, at: .right, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let firstVisibleIndexPath = tableView.indexPathsForVisibleRows?[0]
        for (index,_) in self.menuVM.refreshMenuData.enumerated(){
            self.menuVM.refreshMenuData[index].isSelected = false
        }
        self.menuVM.refreshMenuData[firstVisibleIndexPath!.section].isSelected = true
        menuCategoryCollection.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        self.menuVM.collectionCellInstance1(self.menuCategoryCollection, indexPath: indexPath)
    }
//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if let cell = self.menuCategoryCollection.cellForItem(at: indexPath) as? MenuCategoryCell{
//            cell.btnCat.backgroundColor = .white
//            cell.btnCat.setTitleColor(AppColor.appLightBlack, for: .normal)
//        }
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = controller.storyboard?.instantiateViewController(withIdentifier: "FoodPageVC") as! FoodPageVC
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = self.menuVM.refreshMenuData[indexPath.row].category ?? ""
        let width = self.estimatedFrame(text: text, font: AppFont.Medium.size(.Poppins, size: 13)).width
        return CGSize(width: width, height: 60)
        
    }
    
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let size = CGSize(width: 200, height: 1000) // temporary size
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesFontLeading)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Reload Collection View
        //   self.menuCategoryCollection.reloadData()
        // Find centre point of collection view
        let visiblePoint = CGPoint(x: self.menuListTable.center.x + self.menuListTable.contentOffset.x, y: self.menuListTable.center.y + self.menuListTable.contentOffset.y)
        // Find index path using centre point
        guard let newIndexPath = self.menuListTable.indexPathForRow(at: visiblePoint) else { return }
        let index = self.menuListTable.indexPathsForVisibleRows
        // Select the new centre item
        print("========IndexPath=======\(index?.first?.first)")
        
        //        for i in 0..<self.menuVM.refreshMenuData.count{
        //            if i == index?.first?.first{
        //                self.menuVM.refreshMenuData[i].isSelected = true
        //            }else{
        //                self.menuVM.refreshMenuData[i].isSelected = false
        //            }
        //        }
        //        self.menuCategoryCollection.reloadData()
        
    }
}

//MARK:- Network Calling

extension MenuListView{
    public func getMenuData(_ id:String){
        //Loader
        var param = ["menuId":id] as [String:Any]
        if comClass.isDataExist(){
            param["userId"] = comClass.getInfoById().Id ?? ""
        }
        self.menuVM.getMenuData("getMenuList", param) { (status, error) in
            guard error == nil else{
                self.controller.show_Alert(message: error?.localizedDescription ?? "")
                return
            }
            guard status == 200 else{
                if self.menuVM.menuList?.message?.contains("Something went wrong") ?? false{
                    
                    self.controller.navigationController?.popViewController(animated: false)
                    self.controller.show_Alert(message: "No Data Found!")
                    return
                }
                self.controller.show_Alert(message: self.menuVM.menuList?.message ?? "")
                return
            }
            
            self.menuListTable.delegate = self
            self.menuListTable.dataSource = self
            self.menuCategoryCollection.dataSource = self
            self.menuCategoryCollection.delegate = self
            self.menuListTable.reloadData()
            self.menuListTable.stopSkeletonAnimation()
            self.hideSkeleton()
            self.menuCategoryCollection.reloadData()
        }
        
        self.menuVM.didFinishFetch = {
            let IIndexPath = IndexPath(row: 0, section: self.menuVM.index ?? 0)
            if self.menuVM.refreshMenuData[self.menuVM.index ?? 0].menu?.count ?? 0 <= 0{
                self.controller.show_Alert(message: "No data found! in this category")
                return
            }
            self.menuListTable.scrollToRow(at: IIndexPath, at: .bottom, animated: true)
            for (index,_) in self.menuVM.refreshMenuData.enumerated(){
                self.menuVM.refreshMenuData[index].isSelected = false
            }
            self.menuVM.refreshMenuData[IIndexPath.section].isSelected = true
            self.menuCategoryCollection.reloadData()
        }
    }
}

protocol showNavigationColorDelegate {
    func showNaviColor(bool:Bool)
}
