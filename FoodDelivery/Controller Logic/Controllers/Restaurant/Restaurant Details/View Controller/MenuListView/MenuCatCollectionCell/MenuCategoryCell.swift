//
//  MenuCategoryCell.swift
//  FoodDelivery
//
//  Created by Cst on 4/30/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class MenuCategoryCell: UICollectionViewCell {

    @IBOutlet weak var btnCat: UIButton!
    
    var item: UpdateBaseMenuData?{
        didSet{
            if item?.isSelected ?? false{
                //btnCat.backgroundColor = AppColor.pink
                btnCat.setTitleColor(AppColor.pink, for: .normal)
            }else{
               // btnCat.backgroundColor = AppColor.white
                btnCat.setTitleColor(AppColor.appLightBlack, for: .normal)
            }
            btnCat.setTitle(item?.category ?? "", for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
