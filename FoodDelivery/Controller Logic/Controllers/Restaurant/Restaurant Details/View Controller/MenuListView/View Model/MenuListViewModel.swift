//
//  MenuListViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 4/30/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class MenuListViewModel:BaseViewModel,CellRepresentable{
    
    var rowHeight: CGFloat = 78
    
    var menuList:MenuDataModel?
    var refreshMenuData = [UpdateBaseMenuData]()
    var index:Int?{
        didSet{
            self.didFinishFetch?()
        }
    }
    
    private var dataService: ApiManager?
    var controller = UIViewController()
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "MenuListCell", bundle: nil), forCellReuseIdentifier: "MenuListCell")
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell") as? MenuListCell else {
            return UITableViewCell()
        }
    //    cell.itemList = menuList?.data?.itemData?[indexPath.section].itemList?[indexPath.row]
        cell.itemList = self.refreshMenuData[indexPath.section].menu?[indexPath.row]
        return cell
    }
    
    
    
    func collectionCellInstance1(_ tableView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        tableView.register(UINib(nibName: "MenuCategoryCell", bundle: nil), forCellWithReuseIdentifier: "MenuCategoryCell")
        guard let cell = tableView.dequeueReusableCell(withReuseIdentifier: "MenuCategoryCell", for: indexPath) as? MenuCategoryCell else {
            return UICollectionViewCell()
        }
      //  cell.item = menuList?.data?.itemData?[indexPath.row]
        cell.item = self.refreshMenuData[indexPath.row]
        cell.btnCat.tag = indexPath.row
        cell.btnCat.addTarget(self, action: #selector(btnCatTap), for: .touchUpInside)
//        cell.btnCat.backgroundColor = AppColor.white
//        cell.btnCat.setTitleColor(AppColor.appLightBlack, for: .normal)
        return cell
    }
    
    @objc func btnCatTap(_ sender:UIButton){
        index = sender.tag
    }
    
    //MARK:- Network Call
    
    func getMenuData(_ url:String,_ params:[String:Any],complition:@escaping (Int?,Error?) ->()){
        self.dataService?.fetchApiService(method: .post, url: url,passDict: params, callback: { (result) in
            switch result{
            case .success(let data):
                self.menuList = MenuDataModel(data)
                
                if self.refreshMenuData.count > 0{
                    self.refreshMenuData.removeAll()
                }
                var menuData = [UpdatedMenuData]()
                self.menuList?.data?.recommended?.forEach{item in
                    menuData.append(UpdatedMenuData(Id: item.Id, adminVerifyStatus: item.adminVerifyStatus, type: item.type, status: item.status, deleteStatus: item.deleteStatus, priceSelection: item.priceSelection, category: item.category, menuAssignStatus: item.menuAssignStatus, availabilityStatus: item.availabilityStatus, changeRequest: item.changeRequest, changeRequestApporve: item.changeRequestApporve, index: item.index, brandId: item.brandId, branchId: item.branchId, menuId: item.menuId, menuCategoryName: item.menuCategoryName, productName: item.productName, productNameAr: item.productNameAr, description: item.description, descriptionAr: item.descriptionAr, price: item.price, productImage: item.productImage, createdAt: item.createdAt, updatedAt: item.updatedAt))
                }
                if menuData.count > 0 {
                    self.refreshMenuData.append(UpdateBaseMenuData(category: "Recommended", menu: menuData,isSelected: true))
                }
                self.menuList?.data?.itemData?.forEach{data in
                    menuData.removeAll()
                    data.itemList?.forEach{item in
                        
                     //   for _ in 0..<20{
                            menuData.append(UpdatedMenuData(Id: item.Id, adminVerifyStatus: item.adminVerifyStatus, type: item.type, status: item.status, deleteStatus: item.deleteStatus, priceSelection: item.priceSelection, category: item.category, menuAssignStatus: item.menuAssignStatus, availabilityStatus: item.availabilityStatus, changeRequest: item.changeRequest, changeRequestApporve: item.changeRequestApporve, index: item.index, brandId: item.brandId, branchId: item.branchId, menuId: item.menuId, menuCategoryName: item.menuCategoryName, productName: item.productName, productNameAr: item.productNameAr, description: item.description, descriptionAr: item.descriptionAr, price: item.price, productImage: item.productImage, createdAt: item.createdAt, updatedAt: item.updatedAt))
                       // }
                        
                    }
                    if menuData.count > 0 {
                        self.refreshMenuData.append(UpdateBaseMenuData(category: data.menuName, menu: menuData,isSelected: false))
                    }
                }
                
                complition(self.menuList?.status,nil)
                break
            case .failure(let error):
                complition(nil,error)
                break
            }
        })
    }
    
}
