//
//  MenuListCell.swift
//  FoodDelivery
//
//  Created by Cst on 4/30/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import SkeletonView
class MenuListCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var imaView:UIImageView!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var btnAdd:UIButton!
    @IBOutlet weak var lblItemCount:UILabel!
    var count = 0
    
    var itemList: UpdatedMenuData?{
        didSet{
            imaView.contentMode = .scaleAspectFill
            if itemList?.changeRequestApporve == "Approve"{
                imaView.sd_setImage(with: URL(string: itemList?.productImage ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder_cuisine"), options: .highPriority, context: [ : ])
            }else{
                imaView.image = #imageLiteral(resourceName: "placeholder_cuisine")
            }
            lblPrice.text = "AED \(itemList?.price ?? 0)"
            lblTitle.provideAttributedTextToControlLeftAllignCenter("\(itemList?.productName ?? "")\n", itemList?.description ?? "", AppFont.Medium.size(.Poppins, size: 13), AppFont.Regular.size(.Poppins, size: 12), AppColor.appLightBlack, AppColor.appLightGray, .left)
            lblItemCount.setup("x4", AppColor.pink, AppFont.Medium.size(.Poppins, size: 11), .left)
            btnAdd.addTarget(self, action: #selector(btnAddTap), for: .touchUpInside)
            hideSkeletonfromView([lblTitle,lblPrice,lblItemCount,imaView,btnAdd,contentView])
        }
    }
    
    @objc func btnAddTap(){
        self.count += 1
        self.lblItemCount.isHidden = false
        self.lblItemCount.text = "x\(self.count)"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func hideSkeletonfromView(_ element:[UIView]){
        element.forEach({item in
            item.stopSkeletonAnimation()
            item.hideSkeleton()
        })
        
    }
    
    
}

struct MenuListBaseModel {
    var category: String!
    var data: [MenuListModel]!
}
struct MenuListModel {
    var image: UIImage!
    var title: String!
    var description: String!
    var price: String!
    var count: Int!
}

