//
//  MenuDataModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 01, 2021
//
import Foundation
import SwiftyJSON

struct MenuDataModel {

	let status: Int?
	let message: String?
	let data: MenuData?

	init(_ json: JSON) {
		status = json["status"].intValue
		message = json["message"].stringValue
		data = MenuData(json["data"])
	}

}
