//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 01, 2021
//
import Foundation
import SwiftyJSON

struct MenuData {

	let recommended: [Recommended]?
	var menuCategory: [MenuCategory]?
    var itemData: [ItemData]?

	init(_ json: JSON) {
		recommended = json["recommended"].arrayValue.map { Recommended($0) }
		menuCategory = json["menuCategory"].arrayValue.map { MenuCategory($0) }
		itemData = json["itemData"].arrayValue.map { ItemData($0) }
	}

}
