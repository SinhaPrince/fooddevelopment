//
//  MenuCategory.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 01, 2021
//
import Foundation
import SwiftyJSON

struct MenuCategory {

	let status: String?
	let adminVerificationStatus: String?
	let index: Int?
	let deleteStatus: Bool?
	let Id: String?
	let menuId: String?
	let brandId: String?
	let name: String?
	let nameAr: String?
	let createdAt: String?
	let updatedAt: String?
	let _v: Int?
    var isSelected: Bool?
	init(_ json: JSON) {
		status = json["status"].stringValue
		adminVerificationStatus = json["adminVerificationStatus"].stringValue
		index = json["index"].intValue
		deleteStatus = json["deleteStatus"].boolValue
		Id = json["_id"].stringValue
		menuId = json["menuId"].stringValue
		brandId = json["brandId"].stringValue
		name = json["name"].stringValue
		nameAr = json["nameAr"].stringValue
		createdAt = json["createdAt"].stringValue
		updatedAt = json["updatedAt"].stringValue
		_v = json["__v"].intValue
        isSelected = false
	}

}
