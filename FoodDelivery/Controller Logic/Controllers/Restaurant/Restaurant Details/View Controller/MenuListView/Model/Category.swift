//
//  Category.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 01, 2021
//
import Foundation
import SwiftyJSON

struct Category {

	let categoryNameEn: String?
	let categoryNameAr: String?
	let choice: [Choice]?

	init(_ json: JSON) {
		categoryNameEn = json["categoryNameEn"].stringValue
		categoryNameAr = json["categoryNameAr"].stringValue
		choice = json["choice"].arrayValue.map { Choice($0) }
	}

}