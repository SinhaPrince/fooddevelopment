//
//  Choice.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 01, 2021
//
import Foundation
import SwiftyJSON

struct Choice {

	let name: String?
	let price: Int?

	init(_ json: JSON) {
		name = json["name"].stringValue
		price = json["price"].intValue
	}

}