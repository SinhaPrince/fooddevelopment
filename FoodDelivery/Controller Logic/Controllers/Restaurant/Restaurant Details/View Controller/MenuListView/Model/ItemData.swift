//
//  ItemData.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 01, 2021
//
import Foundation
import SwiftyJSON

struct ItemData {

	var itemList: [ItemList]?
	let menuName: String?

	init(_ json: JSON) {
        itemList = json["itemList"].arrayValue.map { ItemList($0) }
		menuName = json["menuName"].stringValue
	}

    
}
//
//  ItemList.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 04, 2021
//
import Foundation
import SwiftyJSON

struct ItemList {

    var Id: String?
    var adminVerifyStatus: String?
    var type: String?
    var status: String?
    var deleteStatus: Bool?
    var priceSelection: Bool?
    var category: [Category]?
    var menuAssignStatus: Bool?
    var availabilityStatus: Bool?
    var changeRequest: Bool?
    var changeRequestApporve: String?
    var index: Int?
    var brandId: String?
    var branchId: String?
    var menuId: String?
    var menuCategoryName: String?
    var productName: String?
    var productNameAr: String?
    var description: String?
    var descriptionAr: String?
    var price: Int?
    var productImage: String?
    var createdAt: String?
    var updatedAt: String?

    init(_ json: JSON) {
        Id = json["_id"].stringValue
        adminVerifyStatus = json["adminVerifyStatus"].stringValue
        type = json["type"].stringValue
        status = json["status"].stringValue
        deleteStatus = json["deleteStatus"].boolValue
        priceSelection = json["priceSelection"].boolValue
        category = json["category"].arrayValue.map { Category($0) }
        menuAssignStatus = json["menuAssignStatus"].boolValue
        availabilityStatus = json["availabilityStatus"].boolValue
        changeRequest = json["changeRequest"].boolValue
        changeRequestApporve = json["changeRequestApporve"].stringValue
        index = json["index"].intValue
        brandId = json["brandId"].stringValue
        branchId = json["branchId"].stringValue
        menuId = json["menuId"].stringValue
        menuCategoryName = json["menuCategoryName"].stringValue
        productName = json["productName"].stringValue
        productNameAr = json["productNameAr"].stringValue
        description = json["description"].stringValue
        descriptionAr = json["descriptionAr"].stringValue
        price = json["price"].intValue
        productImage = json["productImage"].stringValue
        createdAt = json["createdAt"].stringValue
        updatedAt = json["updatedAt"].stringValue
    }

}

struct UpdatedMenuData {
    var Id: String?
    var adminVerifyStatus: String?
    var type: String?
    var status: String?
    var deleteStatus: Bool?
    var priceSelection: Bool?
    var category: [Category]?
    var menuAssignStatus: Bool?
    var availabilityStatus: Bool?
    var changeRequest: Bool?
    var changeRequestApporve: String?
    var index: Int?
    var brandId: String?
    var branchId: String?
    var menuId: String?
    var menuCategoryName: String?
    var productName: String?
    var productNameAr: String?
    var description: String?
    var descriptionAr: String?
    var price: Int?
    var productImage: String?
    var createdAt: String?
    var updatedAt: String?
    
}

struct UpdateBaseMenuData {
    var category:String?
    var menu: [UpdatedMenuData]?
    var isSelected: Bool?
}
