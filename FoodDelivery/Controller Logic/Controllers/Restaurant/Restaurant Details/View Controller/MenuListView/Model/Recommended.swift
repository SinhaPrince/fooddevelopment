//
//  Recommended.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 01, 2021
//
import Foundation
import SwiftyJSON

struct Recommended {

	let Id: String?
	let adminVerifyStatus: String?
	let type: String?
	let status: String?
	let deleteStatus: Bool?
	let priceSelection: Bool?
	let category: [Category]?
	let menuAssignStatus: Bool?
	let availabilityStatus: Bool?
	let changeRequest: Bool?
	let changeRequestApporve: String?
	let index: Int?
	let brandId: String?
	let branchId: String?
	let menuId: String?
	let menuCategoryName: String?
	let productName: String?
	let productNameAr: String?
	let description: String?
	let descriptionAr: String?
	let price: Int?
	let productImage: String?
	let createdAt: String?
	let updatedAt: String?

	init(_ json: JSON) {
		Id = json["_id"].stringValue
		adminVerifyStatus = json["adminVerifyStatus"].stringValue
		type = json["type"].stringValue
		status = json["status"].stringValue
		deleteStatus = json["deleteStatus"].boolValue
		priceSelection = json["priceSelection"].boolValue
		category = json["category"].arrayValue.map { Category($0) }
		menuAssignStatus = json["menuAssignStatus"].boolValue
		availabilityStatus = json["availabilityStatus"].boolValue
		changeRequest = json["changeRequest"].boolValue
		changeRequestApporve = json["changeRequestApporve"].stringValue
		index = json["index"].intValue
		brandId = json["brandId"].stringValue
		branchId = json["branchId"].stringValue
		menuId = json["menuId"].stringValue
		menuCategoryName = json["menuCategoryName"].stringValue
		productName = json["productName"].stringValue
		productNameAr = json["productNameAr"].stringValue
		description = json["description"].stringValue
		descriptionAr = json["descriptionAr"].stringValue
		price = json["price"].intValue
		productImage = json["productImage"].stringValue
		createdAt = json["createdAt"].stringValue
		updatedAt = json["updatedAt"].stringValue
	}

}