//
//  RestaurantViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 5/4/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class RestaurantViewModel:BaseViewModel,CellRepresentable{
    
    var rowHeight: CGFloat = 100
    var comingFrom = String()
    var restaurantInfo:RestaurantDetailModel?
    var controller = UIViewController()
    private var dataService: ApiManager?
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    
    func collectionCellInstance1(_ tableView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        tableView.register(UINib(nibName: "RestaurantInfoCollectionCell", bundle: nil), forCellWithReuseIdentifier: "RestaurantInfoCollectionCell")
        guard let cell = tableView.dequeueReusableCell(withReuseIdentifier: "RestaurantInfoCollectionCell", for: indexPath) as? RestaurantInfoCollectionCell else {
            return UICollectionViewCell()
        }
        if comingFrom == "Brand"{
            cell.item = restaurantInfo?.data?.brandData?.brandData?.cuisineArrayEn?[indexPath.row]
        }else{
            cell.item = restaurantInfo?.data?.brandId?.cuisineArrayEn?[indexPath.row]
        }
        return cell
    }
    
    //MARK:- Network Call
    
    func getRestaurantInfo(_ url:String,_ params:[String:Any],complition:@escaping (Int?,Error?) ->()){
        self.dataService?.fetchApiService(method: .post, url: url,passDict: params, callback: { (result) in
            switch result{
            case .success(let data):
                self.restaurantInfo = RestaurantDetailModel(data)
                complition(self.restaurantInfo?.status,nil)
                break
            case .failure(let error):
                complition(nil,error)
                break
            }
        })
    }
}
