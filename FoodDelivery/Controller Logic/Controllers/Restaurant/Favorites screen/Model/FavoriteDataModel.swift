//
//  FavoriteDataModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 05, 2021
//
import Foundation
import SwiftyJSON

struct FavoriteDataModel {

	var status: Int?
	var message: String?
	var data: [FavoriteData]?

	init(_ json: JSON) {
		status = json["status"].intValue
		message = json["message"].stringValue
		data = json["data"].arrayValue.map { FavoriteData($0) }
	}

}
