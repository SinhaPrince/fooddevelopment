//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on May 05, 2021
//
import Foundation
import SwiftyJSON

struct FavoriteData {

	var Id: String?
	var restaurantId: String?
	var userId: String?
	var createdAt: String?
	var restaurantData: RestaurantData?
	var brandData: BrandDataa?
    var areaData: AreaData?

	init(_ json: JSON) {
        areaData = AreaData(json["areaData"])
		Id = json["_id"].stringValue
		restaurantId = json["restaurantId"].stringValue
		userId = json["userId"].stringValue
		createdAt = json["createdAt"].stringValue
		restaurantData = RestaurantData(json["restaurantData"])
		brandData = BrandDataa(json["brandData"])
	}

}
