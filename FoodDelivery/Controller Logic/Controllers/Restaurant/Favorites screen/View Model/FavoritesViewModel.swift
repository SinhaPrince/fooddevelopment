//
//  FavoritesViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 5/5/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
class FavoritesViewModel:BaseViewModel,CellRepresentable{
    
    var rowHeight: CGFloat = UITableView.automaticDimension
    var favtData: FavoriteDataModel?
    var controller = UIViewController()
    private var dataService: ApiManager?
    // MARK: - Constructor
    init(networking:ApiManager) {
        self.dataService = networking
    }
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "FavouritesTableCell", bundle: nil), forCellReuseIdentifier: "FavouritesTableCell")
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavouritesTableCell") as? FavouritesTableCell else {
            return UITableViewCell()
        }
        cell.favtItem = favtData?.data?[indexPath.row]
        cell.btnFavt.tag = indexPath.row
        cell.btnFavt.addTarget(self, action: #selector(favtTap(_:)), for: .touchUpInside)
        cell.btnFavt.setImage(#imageLiteral(resourceName: "heart") , for: .normal)
        return cell
    }
    
    @objc func favtTap(_ sender:UIButton){
       
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        let params = ["restaurantId":self.favtData?.data?[sender.tag].restaurantId ?? ""] as [String:Any]
        Indicator.shared.showProgressView(controller.view)
        self.dataService?.fetchApiService(method: .post, url: "addToFavourite", passDict: params, header: header, callback: { (result) in
            Indicator.shared.hideLoaderView()
            switch result{
            case .success(let data):
                if data["status"].intValue == 200{
                    self.didFinishFetch?()
                }else{
                    self.controller.show_Alert(message: data["message"].stringValue)
                }
                break
            case .failure(let error):
                self.controller.show_Alert(message: error.localizedDescription)
                break
            }
        })

    }
    
    func getFavoriteData(_ param:[String:Any],complition:@escaping (Int?,Error?) ->()){
        let header = ["Authorization":comClass.getInfoById().jwtToken] as? [String:String]
        self.dataService?.fetchApiService(method: .post, url: "getFavoriteRestaurants",passDict: param, header: header, callback: { (result) -> (Void) in
            switch result{
            case .success(let data):
                self.favtData = FavoriteDataModel(data)
                complition(self.favtData?.status,nil)
                break
            case .failure(let error):
                complition(nil,error)
                break
            }
        })

    }
}
