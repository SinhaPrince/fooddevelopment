//
//  MyFavoritesVC.swift
//  FoodDelivery
//
//  Created by call soft on 05/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit
import SkeletonView
class MyFavoritesVC: ParentViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblVw: UITableView!
    
    //MARK:- Properties
   let favtViewModel = FavoritesViewModel(networking: ApiManager())
    lazy var noData: UILabel = {
        let lbl = UILabel()
        lbl.setup("No Favorite Added Yet!", AppColor.appLightBlack, AppFont.Medium.size(.Poppins, size: 16), .center)
        lbl.frame = view.bounds
        lbl.isHidden = true
        view.bringSubviewToFront(lbl)
        view.addSubview(lbl)
        return lbl
    }()
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeTheLocationManager()
        self.tblVw.register(UINib(nibName: "FavouritesTableCell", bundle: nil), forCellReuseIdentifier: "FavouritesTableCell")
        self.tblVw.dataSource = self
        self.tblVw.isSkeletonable = true
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight, duration: 0.4, autoreverses: true)
        self.tblVw.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7215660045)), animation: animation, transition: .crossDissolve(0.5))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.initiallizers()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavigationBar(false)
        setupNavigation()
        self.favtViewModel.controller = self
        self.favtViewModel.didFinishFetch = {
            self.initiallizers()
        }

    }
    
    func setupNavigation(){
        self.transparentNavigation()
        setupNavigationBarTitle("", leftBarButtonsType: [.backHomeNAll], rightBarButtonsType: [])
        let title = UILabel()
        title.text = comClass.createString(Str: "My Favorites")
        title.textColor = AppColor.black
        title.font = AppFont.Medium.size(.Poppins, size: 16)
        title.textAlignment = .left
        self.navigationItem.leftBarButtonItems?.append(UIBarButtonItem(customView: title))
       
    }
   
}


//MARK:- Custom Methods
extension MyFavoritesVC {
    func initiallizers() {
        let param = ["latitude":lattt ,"longitude":longg] as [String:Any]
        //Indicator.shared.showLottieAnimation(view: view)
        self.favtViewModel.getFavoriteData(param) { (status, error) in
            guard error == nil else{
                self.handleError(error?.localizedDescription)
                return
            }
            guard status == 200 else{
                self.handleError(self.favtViewModel.favtData?.message ?? "")
                return
            }
            if self.favtViewModel.favtData?.data?.count ?? 0 > 0 {
                self.noData.isHidden = true
            }else{
                self.noData.isHidden = false
            }
            self.configureTable()
        }
        
    }
    
}
//MARK:- UITableViewDelegate and UITableViewDatasource
extension MyFavoritesVC : UITableViewDelegate, UITableViewDataSource,SkeletonTableViewDataSource{
   
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int{
        5
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "FavouritesTableCell"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favtViewModel.favtData?.data?.count ?? 0
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.favtViewModel.cellInstance(tableView, indexPath: indexPath)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tblVw.stopSkeletonAnimation()
        self.tblVw.hideSkeleton()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func configureTable() {
        self.tblVw.stopSkeletonAnimation()
        self.tblVw.hideSkeleton()
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantDetailVC") as! RestaurantDetailVC
        vc.id = self.favtViewModel.favtData?.data?[indexPath.row].restaurantId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
