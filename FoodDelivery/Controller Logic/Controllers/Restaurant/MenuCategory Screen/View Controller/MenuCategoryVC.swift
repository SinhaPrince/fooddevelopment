//
//  MenuCategoryVC.swift
//  FoodDelivery
//
//  Created by call soft on 03/03/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

protocol ReturnIndexDelegate {
    func indexNo(index:Int)
}

class MenuCategoryVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var bottomVw: UIView!
    
    //MARK:- Properties
    var menuCategoryData = [UpdateBaseMenuData]()
    let menuCatViewModel = MenuCategoryViewModel()
    var delegate: ReturnIndexDelegate?
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initiallizers()
        bottomVw.cornerRadius = 25
        bottomVw.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
    }
    //MARK:- Button Action
    @IBAction func btnActionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

//MARK:- Custom Methods
extension MenuCategoryVC {
    func initiallizers() {
        configureTable()
    }
}

//MARK:- UITableViewDelegate and UITableViewDatasource
extension MenuCategoryVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuCategoryData.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return menuCatViewModel.cellInstance(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            self.delegate?.indexNo(index: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func configureTable() {
        self.menuCatViewModel.menuCategoryData = self.menuCategoryData
        tblVw.delegate = self
        tblVw.dataSource = self
        tblVw.backgroundColor = AppColor.clearColor
        tblVw.reloadData()
    }
  
}
