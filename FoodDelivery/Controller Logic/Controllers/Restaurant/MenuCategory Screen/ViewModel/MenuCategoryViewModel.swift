//
//  MenuCategoryViewModel.swift
//  FoodDelivery
//
//  Created by Cst on 5/5/21.
//  Copyright © 2021 Farheen. All rights reserved.
//

import UIKit

class MenuCategoryViewModel:BaseViewModel,CellRepresentable{
    var rowHeight: CGFloat = 50
    var menuCategoryData: [UpdateBaseMenuData]?
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "FoodPageTableCell", bundle: nil), forCellReuseIdentifier: "FoodPageTableCell")
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FoodPageTableCell", for: indexPath) as? FoodPageTableCell else {
            return UITableViewCell()
        }
        cell.item = self.menuCategoryData?[indexPath.row]
        cell.imgRadio.isHidden = true
        return cell
    }
}
